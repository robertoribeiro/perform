#! /usr/bin/python

import subprocess
import re
import sys
import os
import time

bin_folder="/home/rr/work/perform/PERFORM"

pk_bin="mcml_fluor-pkernel-release"
ft_bin="mcml_fluor-fullt-release"

live=0

def execute2(command):
    process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    runoutput = ""
    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == '' and process.poll() != None:
            break
        if live:
        	sys.stdout.write(nextline)
        runoutput+=nextline
        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return runoutput
    else:
        raise ProcessException(command, exitCode, output)

def execute(command):
    process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = ""
    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        nextlineERR = process.stderr.readline()
        if nextlineERR != '' and process.poll() != None:
        	print "Run error: " + nextlineERR
        	break
        if nextline == '' and process.poll() != None:
            break
        if live:
        	sys.stdout.write(nextline)
        output+=nextline
        sys.stdout.flush()
        sys.stderr.flush()

	#fin = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return output
    else:
        raise ProcessException(command, exitCode, output)

def parse_time(result):
	for line in result.split("\n"):
		if 'Simulation done in' in line:
			time=re.findall(r'\d+\.\d+', line)
			break

	return float(time[0])

def parse_tHops(result):
	for line in result.split("\n"):
		if 'Total hops:' in line:
			time=re.findall(r'\d+', line)
			break

	return float(time[0])

def parse_deviation(result):
	for line in result.split("\n"):
		if 'Relative standard deviation:' in line:
			time=re.findall(r'\d+\.\d+', line)
			break

	return float(time[0])

def parse_time_gpumcml(result):
	for line in result.split("\n"):
		if '>>>>>>Simulation time: ' in line:
			time=re.findall(r'\d+\.\d+', line)
			break

	return round(float(time[0])/1000, 3)


def multiple_runs(command, n_runs, parse_fun):
	print command
	result=[]
	full_output=""
	for x in xrange(n_runs):
		print "Run %d..." % x
		try:
			full_output+=execute(command)
		except Exception:
			print "Process failed"
			pass
		result.append(parse_fun(full_output))
	return min(result), full_output

def make_line(result_line):
	 return "\t".join(map(str,result_line)) + "\n"

def run_mcml_fluor_fullt(n_runs, dev,initial_dice,cpu_flops,n_photons, input_file, log):
	command=[bin_folder + ft_bin + '/PERFORM' , str(dev) , str(initial_dice) , str(cpu_flops) , str(n_photons), input_file]
	log.write(' '.join(command))
	minr, full_output = multiple_runs(command, n_runs, parse_time)
	log.write(full_output)
	return minr

def run_mcml_fluor_pkernel(n_runs, dev,initial_dice,cpu_flops,n_photons, input_file, log):
	command=[bin_folder  + pk_bin + '/PERFORM' , str(dev) , str(initial_dice) , str(cpu_flops) , str(n_photons), input_file]
	log.write(' '.join(command))
	minr, full_output = multiple_runs(command, n_runs, parse_time)
	log.write(full_output)
	return minr

def run_mcml_fluor_pkernelSIMD32(n_runs, dev,initial_dice,cpu_flops,n_photons, input_file, log):
	command=[bin_folder + pk_bin + '/PERFORM' , str(dev) , str(initial_dice) , str(cpu_flops) , str(n_photons), input_file]
	log.write(' '.join(command))
	minr, full_output = multiple_runs(command, n_runs, parse_time)
	log.write(full_output)
	return minr

def run_simple_gpumcml(n_runs, n_photons, input_file, log):
	command=[bin_folder + '/../simple_gpumcml/gpumcml.sm_20' , input_file]
	log.write(' '.join(command))
	minr, full_output = multiple_runs(command, n_runs, parse_time_gpumcml)
	log.write(full_output)
	return minr

def run_fast_gpumcml(n_runs, n_photons, input_file, log):
	command=[bin_folder + '/../fast-gpumcml/gpumcml.sm_20' , input_file]
	log.write(' '.join(command))
	minr, full_output = multiple_runs(command, n_runs, parse_time_gpumcml)
	log.write(full_output)
	return minr

def generate_input(instance,n, base_input_file):
	infile = open(base_input_file)
	outfile = open(instance + '/test.mci', 'w')
	for line in infile:
		outfile.write(re.sub('\d+\t* *# No. of photons', str(n) + '\t # No. of photons', line))
	return instance + '/test.mci'

def build_fullt_pkernel_compare(n_runs, dev, initial_dice, cpu_flops,n_photons_set, base_input_file, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for n_photons in n_photons_set:
		input_file=generate_input(instance,n_photons, base_input_file)
		result_line=[]
		result_line.append(n_photons);
		#run_mcml_fluor_fullt
		fastest_fullt = run_mcml_fluor_fullt(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file, log)
		result_line.append(fastest_fullt);
		print fastest_fullt
		#run_mcml_fluor_pkernel
		fastest_pkernel = run_mcml_fluor_pkernel(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file,log)
		result_line.append(fastest_pkernel);
		print fastest_pkernel
		#
		speedup=fastest_fullt/fastest_pkernel
		#run_mcml_fluor_pkernelSIMD32
		fastest_pkernel = run_mcml_fluor_pkernelSIMD32(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file, log)
		result_line.append(fastest_pkernel);
		print fastest_pkernel
		#
		speedup2=fastest_fullt/fastest_pkernel
		#run_simple_gpumcml
		fastest_pkernel = run_simple_gpumcml(n_runs,n_photons, input_file, log)
		result_line.append(fastest_pkernel);
		print fastest_pkernel
		#run_fast_gpumcml
		fastest_pkernel = run_fast_gpumcml(n_runs,n_photons, input_file, log)
		result_line.append(fastest_pkernel);
		print fastest_pkernel
		#
		#speedup2=fastest_fullt/fastest_pkernel		
		result_line.append(round(speedup, 2));
		result_line.append(round(speedup2, 2));
		print result_line
		result.write(make_line(result_line))

def build_fluor_fullt_pkernel_compare(n_runs, dev, initial_dice, cpu_flops,n_photons_set,base_input_file, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for n_photons in n_photons_set:
		input_file=generate_input(instance,n_photons,base_input_file)
		result_line=[]
		result_line.append(n_photons);
		#run_mcml_fluor_fullt
		fastest_fullt = run_mcml_fluor_fullt(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file, log)
		result_line.append(fastest_fullt);
		print fastest_fullt
		#run_mcml_fluor_pkernel
		fastest_pkernel = run_mcml_fluor_pkernel(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file,log)
		result_line.append(fastest_pkernel);
		print fastest_pkernel
		#
		speedup=fastest_fullt/fastest_pkernel
		result_line.append(round(speedup, 2));
		print result_line
		result.write(make_line(result_line))

def fluor_pk_multi_device(n_runs, devs, initial_dice, cpu_flops,n_photons_set,base_input_file, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for n_photons in n_photons_set:
		input_file=generate_input(instance,n_photons,base_input_file)
		result_line=[]
		result_line.append(n_photons);
		for dev in devs:
			#run_mcml_fluor_fullt
			fastest=0.0
			fastest = run_mcml_fluor_pkernel(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file, log)
			result_line.append(fastest);
			print fastest
		print result_line
		result.write(make_line(result_line))

def fluor_pk_assess_idice(n_runs, dev, initial_dices, cpu_flops,n_photons_set,base_input_file, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for n_photons in n_photons_set:
		input_file=generate_input(instance,n_photons,base_input_file)
		result_line=[]
		result_line.append(n_photons);
		for initial_dice in initial_dices:
			#run_mcml_fluor_fullt
			fastest = run_mcml_fluor_pkernel(n_runs, dev, initial_dice, cpu_flops, n_photons, input_file, log)
			result_line.append(fastest);
			print fastest
		print result_line
		result.write(make_line(result_line))

def fluor_pk_assess_cpuflops(n_runs, devs, initial_dice, cpu_flops,n_photons,base_input_file, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for cpu_flop in cpu_flops:
		input_file=generate_input(instance,n_photons,base_input_file)
		result_line=[]
		result_line.append(cpu_flop);
		for dev in devs:
			#run_mcml_fluor_fullt
			fastest = run_mcml_fluor_pkernel(n_runs, dev, initial_dice, cpu_flop, n_photons, input_file, log)
			result_line.append(fastest);
			print fastest
		print result_line
		result.write(make_line(result_line))

def fluor_pk_assess_cpuflops_dices(n_runs, dev, initial_dices, cpu_flops,n_photons,base_input_file, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	input_file=generate_input(instance,n_photons,base_input_file)
	for initial_dice in initial_dices:
		result_line=[]
		result_line.append(initial_dice);
		for cpu_flop in cpu_flops:
			#run_mcml_fluor_fullt
			fastest = run_mcml_fluor_pkernel(n_runs, dev, initial_dice, cpu_flop, n_photons, input_file, log)
			result_line.append(fastest);
			print fastest
		print result_line
		result.write(make_line(result_line))

n_runs=2

orig_input_file= bin_folder + "/data/mcml/input/test.mci"
orig_input_file_dense= bin_folder + "/data/mcml/input/test-dense.mci"
orig_input_file_moredense= bin_folder + "/data/mcml/input/test-more-dense.mci"

devs=[1,2,3,4, 5, 6]
initial_dices=[2, 4 ,8 , 16]
cpu_flops=[100,  200,  400]

n_photonsM=[0.5,1,2,4, 8, 16, 32, 64, 128 ]
n_photons_set = [1024*1024*x for x in n_photonsM] 

#build_fullt_pkernel_compare(n_runs, devs[1], 0, 100,n_photons_set, orig_input_file_dense, "fullt_pkernel_compare_dense_high")

#build_fluor_fullt_pkernel_compare(n_runs, devs[1], 0, 100,n_photons_set, orig_input_file, "fullt_pkernel")
fluor_pk_multi_device(n_runs, devs, initial_dices[3], cpu_flops[1],n_photons_set,orig_input_file, "multi-device-16-200-gtx")
#fluor_pk_assess_idice(n_runs, devs[6], initial_dices, cpu_flops[0],n_photons_set,orig_input_file, "assess_idice")
#fluor_pk_assess_cpuflops(n_runs, devs[4:6], initial_dices[3], cpu_flops,n_photons_set[5],orig_input_file, "assess_cpuflops")
#fluor_pk_assess_cpuflops_dices(n_runs, devs[5], initial_dices, cpu_flops,n_photons_set[6],orig_input_file, "assess_cpuflops_dice")


