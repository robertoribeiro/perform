/*
 * utils.h
 *
 *  Created on: Nov 27, 2013
 *      Author: jbarbosa
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <common/common.h>

int read_input(const char * file_name, Input &input);

#endif /* UTILS_H_ */
