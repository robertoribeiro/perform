#include "common/common.h"

#include <assert.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>

#include <fcntl.h>
#include <unistd.h>
#ifdef __APPLE__
#include <sys/syscall.h>
#else
#include <syscall.h>
#endif

#include "common/util.h"
#include "common/dbg.h"

extern void DCD_SVM_multi_lockfree(Input &input, Parameter &param, Output &output);

int read_input(const char * file_name, Input &input);

double validate(Input &input, Output &output);

#include <sys/resource.h>

int main(int argc, char **argv) {
  char * data = NULL;
  double epsilon = 0.1;
  int max_iteration = 1000;
  int option_index = 0;
  int thread = 4;
  int algorithm = 0;
  int device = 0;

  {
    rlim_t kStackSize = 256 * 1024 * 1024;   // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0) {
      if (rl.rlim_cur < kStackSize) {
        rl.rlim_cur = kStackSize;
        result = setrlimit(RLIMIT_STACK, &rl);
        if (result != 0) {
          fprintf(stderr, "setrlimit returned result = %d\n", result);
        }
      }
    }
  }

  //bind_core(1);


  Input input;
  Parameter param;
  Input _predict;
  Input * predict;
  {
    char * predict_file = NULL;
    /*
     * parse argument
     */
    static struct option long_options[] = {  //
        { "data", required_argument, 0, 'd' },  //
            { "epsilon", required_argument, 0, 'e' },  //
            { "iteration", required_argument, 0, 'i' },  //
            { "thread", required_argument, 0, 't' },  //
            { "predict", required_argument, 0, 'p' },  //
            { "algorithm", required_argument, 0, 'a' },  //
            { "device", required_argument, 0, 'x' },  //
            { 0, 0, 0, 0 }  //
        };

    while (true) {
      char c = getopt_long(argc, argv, "d:e:i:t:p:a:x", long_options, &option_index);
      if (c == -1) {
        break;
      }
      switch (c) {
        case 'd':
          data = optarg;
          break;
        case 'e':
          epsilon = atof(optarg);
          break;
        case 'i':
          max_iteration = atoi(optarg);
          break;
        case 't':
          thread = atoi(optarg);
          break;
        case 'a':
          algorithm = atoi(optarg);
          break;
        case 'x':
          device = atoi(optarg);
          break;
        case 'p':
          predict_file = optarg;
          break;
        default:
          break;
      }
    }

    DBG("%-20s : %s\n", "input", data);

    int ret = read_input(data, input);
    if (0 != ret) {
      DBG_BACKTRACE("%s\n", "read_input");
      return ret;
    }

    char buf[4096];
    sprintf(buf, "%s", data);
    buf[strlen(data) - 4] = 0;

//    ret = read_input(buf, input_galois);
//    if (0 != ret) {
//      DBG_BACKTRACE("%s\n", "read_input");
//      return ret;
//    }

    param.algorithm = algorithm;
    param.device = device;
    param.C = 1;
    param.epsilon = epsilon;
    param.max_iteration = max_iteration;
    param.thread_num = thread;

    DBG("%-20s : %g\n", "epsilon", epsilon);
    DBG("%-20s : %d\n", "iteration", max_iteration);
    DBG("%-20s : %d\n", "thread", thread);

    if (predict_file) {
      read_input(predict_file, _predict);
      predict = &_predict;
    } else {
      predict = &input;
    }
  }

  Output output;
  output.w = new double[input.n];

  uint64_t a = get_usec();
  uint64_t b = get_usec();

  //reset_bind();


    a = get_usec();
    DCD_SVM_multi_lockfree(input, param, output);
    b = get_usec();
    DBG("DCD_SVM_multi_lockfree. # of thread CUDA: Validate %.2f %\n", validate(*predict, output) * 100.0);


}

int read_input(const char * file_name, Input &input) {
  int ret = 0;

  int fd = open(file_name, O_RDONLY);
  if (-1 == fd) {
    DBG_BACKTRACE("%s : %s\n", "open", strerror(errno));
    ret = errno;
    return ret;
  }

  ssize_t bytes = 0;

  int l = 0;
  bytes = read(fd, &l, sizeof(l));
  assert(sizeof(l) == bytes);

  input.l = l;

  input.y = new double[l];
  input.x = new Instance[l];

  int n = 0;
  bytes = read(fd, &n, sizeof(n));
  assert(sizeof(n) == bytes);

  input.n = n;

  double total_size = 0;
  for (int i = 0; i < l; i++) {
    int y = 0;
    bytes = read(fd, &y, sizeof(y));
    assert(sizeof(y) == bytes);
    input.y[i] = double(y);

    Instance * instance = &(input.x[i]);

    int size = 0;
    bytes = read(fd, &size, sizeof(size));
    assert(sizeof(size) == bytes);

    instance->size = size;
    instance->features = new Feature[size];

    for (int j = 0; j < size; j++) {
      Feature feature;
      bytes = read(fd, &feature.id, sizeof(feature.id));
      assert(sizeof(feature.id) == bytes);

      assert(0 < feature.id);
      feature.id--;

      bytes = read(fd, &feature.value, sizeof(feature.value));
      assert(sizeof(feature.value) == bytes);

      instance->features[j] = feature;
    }

    total_size += size;
  }

  DBG("%-20s : %10d\n", "l (# of instances)", input.l);
  DBG("%-20s : %10d\n", "n (# of features)", input.n);
  DBG("%-20s : %10.2f\n", "avg feat / inst", total_size / input.l);
  DBG("%-20s : %10.2f\n", "avg feat freq", total_size / input.n);

  close(fd);

  return ret;
}

double validate(Input &input, Output &output) {
  int hit = 0;
  for (int i = 0; i < input.l; i++) {
    double y = 0.0;
    Instance * instance = &(input.x[i]);

    for (int j = 0; j < instance->size; j++) {
      Feature feature = instance->features[j];
      y += output.w[feature.id] * feature.value;
    }

    if (y * input.y[i] > 0.0) {
      hit++;
    }
  }

  return double(hit) / input.l;
}
