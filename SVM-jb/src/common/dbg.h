#ifndef DBG_H_
#define DBG_H_

#ifndef	__PRETTY_FUNCTION__
#define	__PRETTY_FUNCTION__ __func__
#endif

#define DBG(fmt, ...) \
	__dbg(fmt, __FILE__, __PRETTY_FUNCTION__, __LINE__, __VA_ARGS__);

#define DBG_BACKTRACE(fmt, ...) \
	__dbg_backtrace(fmt, __FILE__, __PRETTY_FUNCTION__, __LINE__, __VA_ARGS__);

#include "dbg_inl.h"

#endif /* DBG_H_ */
