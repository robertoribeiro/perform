/*
 * common.h
 *
 *  Created on: Nov 7, 2013
 *      Author: seonggu
 */

#ifndef COMMON_H_
#define COMMON_H_

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif

#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif

#include <assert.h>
#include <errno.h>

#include <stdint.h>
#include <inttypes.h>
#include <limits.h>
#include <float.h>

#define INF DBL_MAX

enum Mode {
  MODE_UNKNOWN,
  MODE_TRAIN,
  MODE_PREDICT,
};

struct Feature {
  double value;
  int id;
};

struct Instance {
  Feature * features;
  int size;
};

struct Input {
  int n;  // dimension of instance
  int l;  // dimension of feature space
  double * y;
  Instance * x;
};

struct Output {
  double * w;
};

struct Parameter {
  double epsilon;
  int max_iteration;

  double C;

  int thread_num;

  int algorithm;
  int device;
};

//#include "Galois/Galois.h"
//#include "Galois/Graphs/LCGraph.h"
//
//typedef Galois::Graph::LC_CSR_Graph<double, double> LC_Graph;

#include <vector>

struct Input_Galois {
  int n;  // dimension of instance
  int l;  // dimension of feature space

  std::vector<int> x;
  std::vector<double> y;

//  LC_Graph graph;
  std::vector<std::vector<int> > w_invert;
};

#endif /* COMMON_H_ */
