/*
 * util.h
 *
 *  Created on: Nov 8, 2013
 *      Author: seonggu
 */

#ifndef UTIL_H_
#define UTIL_H_

#include "common.h"

#include <assert.h>
#include <string.h>

#include <sched.h>
#include <unistd.h>

#include <sys/time.h>

#include "dbg.h"


inline uint64_t rdtsc(void) {
  uint64_t lo, hi;
  asm( "rdtsc" : "=a" (lo), "=d" (hi) );
  return (lo | (hi << 32));
}

inline uint64_t get_usec(void) {
  struct timeval tv;
  assert(0 == gettimeofday(&tv, NULL));

  return tv.tv_sec * 1000000L + tv.tv_usec;
}

#endif /* UTIL_H_ */
