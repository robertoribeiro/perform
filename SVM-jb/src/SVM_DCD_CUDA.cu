#include <common/common.h>
#include <utils/utils.h>
#include <defs.h>
#include <limits>

#include <common/dbg.h>
#include <common/util.h>
#include <iostream>
#include <assert.h>
#include <errno.h>
#include <cstdlib>

#include <stdint.h>
#include <inttypes.h>
#include <limits.h>
#include <float.h>

#include <cuda.h>
#include <helper_cuda.h>




#define LOSS_NORM 1

const double inf = std::numeric_limits<double>::max();

#define THREADS 256

__global__ void svm_dcd_cpu_single(double* Q, double* U, double* D, double* PGs,
		double* alpha, double* y, double* w, double* xi_value, int* xi_idx,
		int* xi_bounds, int size, double epsilon) {

	int i = threadIdx.x + (blockIdx.x * blockDim.x);
	if (i >= size) return;
	for (; i < size; i += (blockDim.x * gridDim.x)) {
		double yi = y[i];
		double C = U[i];
		double ai = alpha[i];
		double Dii = D[i];
		double Qii = Q[i];

		/*
		 * part a
		 */
		double G = 0.0;
		double wx = 0.0;

		for (int lb = xi_bounds[i], ub = xi_bounds[i + 1]; lb < ub; lb++) {
			wx += w[xi_idx[lb]] * xi_value[lb];
		}

		G = yi * wx - 1 + Dii * ai;

		/*
		 * part c
		 */
		double PG = 0.0;
		if (ai == 0.0) {
			PG = fmin(G, 0.0);
		} else if (ai == C) {
			PG = fmax(G, 0.0);
		} else if ((0 < ai) && (C > ai)) {
			PG = G;
		}

		/*
		 * part c
		 */
		//if (fabs(PG) > 1.0e-12) {  // why 1.0e-12 instead of 0.0?
		if (0.0 != PG) {
			double ai_prev = ai;
			alpha[i] = fmin(fmax(ai - G / Qii, 0.0), C);
			double delta = (alpha[i] - ai_prev) * yi;

			for (int lb = xi_bounds[i], ub = xi_bounds[i + 1]; lb < ub; lb++) {
				w[xi_idx[lb]] += delta * xi_value[lb];
			}

		}

		PGs[i] = PG;
		__syncthreads();
	}
}

__global__ void svm_dcd_cpu_vector(double* Q, double* U, double* D, double* PGs,
		double* alpha, double* y, double* w, double* xi_value, int* xi_idx,
		int* xi_bounds, int size, double epsilon, int* pointer) {

	__shared__ int i;
	__shared__ double yi;
	__shared__ double C;
	__shared__ double ai;
	__shared__ double Dii;
	__shared__ double Qii;
	__shared__ double acc[THREADS];

	if (threadIdx.x == 0) {
		i = atomicAdd(pointer, 1);
	}
	__syncthreads();

	if (i >= size) return;

	while ( i < size) {

		if (threadIdx.x == 0) {
			yi = y[i];
			C = U[i];
			ai = alpha[i];
			Dii = D[i];
			Qii = Q[i];
		}

		acc[threadIdx.x] = 0.0f;
		/*
		 *  part a
		 */
		double G = 0.0;
		double wx = 0.0;
		__syncthreads();

		for (int lb = xi_bounds[i] + threadIdx.x, ub = xi_bounds[i + 1]; lb < ub; lb += blockDim.x) {
			acc[threadIdx.x] += w[xi_idx[lb]] * xi_value[lb];
		}

		__syncthreads();

		for (int offset = blockDim.x / 2; offset > 0; offset >>= 1) {
			if (threadIdx.x < offset) {
				acc[threadIdx.x] += acc[threadIdx.x + offset];
			}
			__syncthreads();
		}

		G = yi * acc[0] - 1 + Dii * ai;

		/*
		 * part c
		 */
		double PG = 0.0;
		if (ai == 0.0) {
			PG = fmin(G, 0.0);
		} else if (ai == C) {
			PG = fmax(G, 0.0);
		} else if ((0 < ai) && (C > ai)) {
			PG = G;
		}

		/*
		 * part c
		 */
		//if (fabs(PG) > 1.0e-12) {  // why 1.0e-12 instead of 0.0?
		if (0.0 != PG) {
			double ai_prev = ai;
			alpha[i] = fmin(fmax(ai - G / Qii, 0.0), C);
			double delta = (alpha[i] - ai_prev) * yi;

			for (int lb = xi_bounds[i] + threadIdx.x, ub = xi_bounds[i + 1];
					lb < ub; lb += blockDim.x) {
				w[xi_idx[lb]] += delta * xi_value[lb];
			}

		}
		PGs[i] = PG;
		if (threadIdx.x == 0) {
			i = atomicAdd(pointer, 1);
		}
		__syncthreads();
	}
}

template<typename T>
void cudaAllocInit(T** ptr, size_t size) {
	checkCudaErrors(cudaMalloc((void**) ptr, sizeof(T) * size));
	checkCudaErrors(cudaMemset(*ptr, 0, sizeof(T) * size));

//	size_t free, total;
//			cuMemGetInfo(&free, &total);
//			fprintf(stderr, " mem %ld total %ld\n", free / 1024 / 1024, total / 1024 / 1024);
}

template<typename T>
void cudaZero(T* ptr, size_t size) {
	checkCudaErrors(cudaMemset(ptr, 0, sizeof(T) * size));
}

template<typename T>
void copyToDEVICE(T* dst, T* src, size_t size) {
	checkCudaErrors(
			cudaMemcpy(dst, src, sizeof(T) * size, cudaMemcpyHostToDevice));
}
template<typename T>
void copyFromDEVICE(T* dst, T* src, size_t size) {
	checkCudaErrors(
			cudaMemcpy(dst, src, sizeof(T) * size, cudaMemcpyDeviceToHost));
}

void svm_dcd_iter_gpu(double* Q, double* U, double* D, double* PGs,
		double* alpha, double* y, double* w, Instance* xi, int l, int n,
		double epsilon, int maximum_iter, int algorithm, int threads,
		int device) {

	double* Q_d;
	double* U_d;
	double* D_d;
	double* PGs_d;
	double* alpha_d;
	double* y_d;
	double* w_d;
	double* xi_d;
	int* xi_id_d;
	int* xi_bounds_d;

	checkCudaErrors(cudaSetDevice(0));

	cudaAllocInit<double>(&Q_d, l);
	copyToDEVICE(Q_d, Q, l);
	cudaAllocInit<double>(&U_d, l);
	copyToDEVICE(U_d, U, l);
	cudaAllocInit<double>(&D_d, l);
	copyToDEVICE(D_d, D, l);
	cudaAllocInit<double>(&PGs_d, l);
	copyToDEVICE(PGs_d, PGs, l);
	cudaAllocInit<double>(&alpha_d, l);
	copyToDEVICE(alpha_d, alpha, l);
	cudaAllocInit<double>(&y_d, l);
	copyToDEVICE(y_d, y, l);
	cudaAllocInit<double>(&w_d, n);
	copyToDEVICE(w_d, w, l);



	int total_size = 0;
	int *bound = new int[l];
	bound[0] = 0;
	for (int i = 0; i < l; i++) {
		Instance xii = xi[i];
		total_size += xii.size;
		bound[i + 1] = total_size;
	}

	double* xi_values = new double[total_size];
	int* xi_ids = new int[total_size];

	int count = 0;
	for (int i = 0; i < l; i++) {
		Instance xii = xi[i];
		for (int j = 0; j < xii.size; j++) {
			Feature feature = xii.features[j];
			xi_values[count] = feature.value;
			xi_ids[count] = feature.id;
			count++;
		}
	}

	if (count != total_size) {
		DBG("Sizes don't match %d %d\n", total_size, count);
		exit(0);
	}

	cudaAllocInit(&xi_bounds_d, l);
	copyToDEVICE(xi_bounds_d, bound, l);



	cudaAllocInit(&xi_d, total_size);
	copyToDEVICE(xi_d, xi_values, total_size);

	cudaAllocInit(&xi_id_d, total_size);
	copyToDEVICE(xi_id_d, xi_ids, total_size);

	int iter = 0;
	uint64_t a = get_usec();

	int* pointer;
	cudaAllocInit(&pointer, 1);

	if(threads > l) threads=l;

	int blocks = ceil(threads / THREADS);

	if (algorithm == 1) {
		DBG("Lounching SVM Simple kernel geometry %d blocks %d threads/block\n",
				blocks, THREADS);
	}
	if (algorithm == 2) {
		DBG("Lounching SVM Vector kernel geometry %d blocks %d threads/block\n",
				blocks, THREADS);
	}



	for (iter = 0; iter < maximum_iter; iter++) {
//		DBG("Iteration %d\n", iter);
		if (algorithm == 1) {
			svm_dcd_cpu_single<<<blocks, THREADS>>>(Q_d, U_d, D_d, PGs_d,
					alpha_d, y_d, w_d, xi_d, xi_id_d, xi_bounds_d, l, epsilon);
		}
		if (algorithm == 2) {
			svm_dcd_cpu_vector<<<blocks, THREADS>>>(Q_d, U_d, D_d, PGs_d,
					alpha_d, y_d, w_d, xi_d, xi_id_d, xi_bounds_d, l, epsilon,
					pointer);
		}
		copyFromDEVICE(PGs, PGs_d, l);
		cudaZero(pointer, 1);

		double PG_max = PGs[0];
		double PG_min = PGs[0];

		for (int i = 1; i < l; i++) {
			double PG = PGs[i];
			PG_max = std::max(PG_max, PG);
			PG_min = std::min(PG_min, PG);
		}

		if ((PG_max - PG_min) < epsilon) {
			break;
		}
	}

	uint64_t b = get_usec();

	DBG("# of thread %d: iteration %d of %d\n", (threads), iter, maximum_iter);
	DBG("# of thread %d: Main loop takes %.3f sec \n", (threads),
			(b - a) / 1000000.0);
	copyFromDEVICE(w, w_d, n);

}

void svm_dcd_iter_cpu(double* Q, double* U, double* D, double* PGs,
		double* alpha, double* y, double* w, Instance* xi, int size,
		double epsilon) {

	int i = 0;
	for (int i = 0; i < size; i++) {
		const Instance xii = xi[i];
		const double yi = y[i];
		const double C = U[i];
		const double ai = alpha[i];
		const double Dii = D[i];
		const double Qii = Q[i];

		/*
		 * part a
		 */
		double G = 0.0;
		double wx = 0.0;
		for (Feature * f = xii.features, *f_max = f + xii.size; f < f_max;
				f++) {
			wx += w[f->id] * f->value;
		}

		G = yi * wx - 1 + Dii * ai;

		/*
		 * part c
		 */
		double PG = 0.0;
		if (ai == 0.0) {
			PG = std::min(G, 0.0);
		} else if (ai == C) {
			PG = std::max(G, 0.0);
		} else if ((0 < ai) && (C > ai)) {
			PG = G;
		}

		/*
		 * part c
		 */
		//if (fabs(PG) > 1.0e-12) {  // why 1.0e-12 instead of 0.0?
		if (0.0 != PG) {
			double ai_prev = ai;
			alpha[i] = std::min(std::max(ai - G / Qii, 0.0), C);
			double delta = (alpha[i] - ai_prev) * yi;
			for (Feature * f = xii.features, *f_max = f + xii.size; f < f_max;
					f++) {
				w[f->id] += delta * f->value;
			}

		}

		PGs[i] = PG;
	}
}

void DCD_SVM_multi_lockfree(Input &input, Parameter &param, Output &output) {

	const int l = input.l;
	const int n = input.n;

	DCD_SVM_Parameter svm_param;

	svm_param.y = new double[l];
	for (int i = 0; i < l; i++) {
		if (input.y[i] > 0.0) {
			svm_param.y[i] = +1.0;
		} else {
			svm_param.y[i] = -1.0;
		}
	}

	svm_param.x = input.x;
	svm_param.w = output.w;

	svm_param.alpha = new double[l];
	for (int i = 0; i < l; i++) {
		svm_param.alpha[i] = 0.0;
	}

	for (int i = 0; i < n; i++) {
		svm_param.w[i] = 0.0;
	}

	svm_param.idx = new int[l];
	for (int i = 0; i < l; i++) {
		svm_param.idx[i] = i;
	}

	svm_param.D = new double[l];
	svm_param.U = new double[l];

	for (int i = 0; i < l; i++) {
		if (1 == LOSS_NORM) {
			svm_param.D[i] = 0.0;
			svm_param.U[i] = param.C;
		} else if (2 == LOSS_NORM) {
			svm_param.D[i] = 1 / (2 * param.C);
			svm_param.U[i] = INF;
		} else {
			assert(0);
		}
	}

	svm_param.Q = new double[l];
	for (int i = 0; i < l; i++) {
		svm_param.Q[i] = 0.0;

		Instance xi = svm_param.x[i];
		for (int j = 0; j < xi.size; j++) {
			Feature feature = xi.features[j];
			svm_param.Q[i] += feature.value * feature.value;
			// y * y is 1, so omitted
		}
	}

// Q bar is Q + D
	for (int i = 0; i < l; i++) {
		svm_param.Q[i] += svm_param.D[i];
	}

	for (int i = 0; i < l; i++) {
		svm_param.w[i] = 0.0;

		Instance xi = svm_param.x[i];
		for (int j = 0; j < xi.size; j++) {
			Feature feature = xi.features[j];
			svm_param.w[i] += svm_param.y[i] * svm_param.alpha[i]
					* feature.value;
		}
	}

	svm_param.PGs = new double[l];

	if (param.algorithm == 0) {
		uint64_t a = get_usec();
		int iter = 0;
		int iter_max = param.max_iteration;
		for (iter = 1; iter < iter_max; iter++) {

			svm_dcd_iter_cpu(svm_param.Q, svm_param.U, svm_param.D,
					svm_param.PGs, svm_param.alpha, svm_param.y, svm_param.w,
					svm_param.x, l, param.epsilon);

			double PG_max = svm_param.PGs[0];
			double PG_min = svm_param.PGs[0];

			for (int i = 1; i < l; i++) {
				double PG = svm_param.PGs[i];
				PG_max = std::max(PG_max, PG);
				PG_min = std::min(PG_min, PG);
			}

			if ((PG_max - PG_min) < param.epsilon) {
				break;
			}
		}
		uint64_t b = get_usec();
		delete[] svm_param.PGs;

		DBG("# of thread %d: iteration %d of %d\n", param.thread_num, iter,
				iter_max);
		DBG("# of thread %d: Main loop takes %.3f sec \n", param.thread_num,
				(b - a) / 1000000.0);
	} else {
		svm_dcd_iter_gpu(svm_param.Q, svm_param.U, svm_param.D, svm_param.PGs,
				svm_param.alpha, svm_param.y, svm_param.w, svm_param.x, l, n,
				param.epsilon, param.max_iteration, param.algorithm,
				param.thread_num, param.device);
	}

	delete[] svm_param.y;
	delete[] svm_param.alpha;
	delete[] svm_param.idx;
	delete[] svm_param.D;
	delete[] svm_param.U;
	delete[] svm_param.Q;

}
