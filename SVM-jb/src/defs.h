/*
 * defs.h
 *
 *  Created on: Nov 27, 2013
 *      Author: jbarbosa
 */

#ifndef DEFS_H_
#define DEFS_H_


struct DCD_SVM_Parameter {
  Instance * x;
  double * y;

  double * alpha;
  int * idx;
  double * D;
  double * U;
  double * Q;

  double * w;

  double epsilon;
  int max_iteration;
  double * PGs;
};



#endif /* DEFS_H_ */
