/*
 * device_configs.h
 *
 *  Created on: Apr 17, 2012
 *      Author: rr
 */

#ifndef DEVICE_CONFIGS_H_
#define DEVICE_CONFIGS_H_


#endif /* DEVICE_CONFIGS_H_ */

enum RESOURCE_ID_e {
	GPU0 = 0, GPU1 = 1, GPU2 = 2
};

typedef enum RESOURCE_ID_e RESOURCE_ID;

extern RESOURCE_ID GPU_0;
extern RESOURCE_ID GPU_1;
extern RESOURCE_ID GPU_2;





