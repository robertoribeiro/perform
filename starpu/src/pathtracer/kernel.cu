#include "path.h"
#include "camera.h"
#include "ray.h"
#include "geometry/bvhaccel.h"
#include "geometry/triangle.h"
#include "geometry/light.h"
#include "geometry.h"
#include "math.h"
#include "utils.h"
#include "randomgen.h"
#include "config.h"

//#include "user_defined_task.h"

#include "my_cutil_math.h"
#include "math_functions.h"

#include "pathtracer.cuh"

#include <starpu.h>
#include <starpu_cuda.h>

__device__
inline
bool Shade(PT_CONFIG* CONFIG, Path* path, Point *vertices, Normal *vertNormals,
		RGB *vertColors, Triangle *triangles, TriangleLight *lights,
		BVHAccelArrayNode *bvh, RayHit& rayHit, Geometry* geometry,
		RAYCOUNTER_TYPE* ray_count) {

	uint tracedShadowRayCount;

	if (rayHit.index == 0xffffffffu) {
		return false;
	}

	// Something was hit
	unsigned int currentTriangleIndex = rayHit.index;
	RGB triInterpCol = triangles[currentTriangleIndex].InterpolateColor(
			vertColors, rayHit.b1, rayHit.b2);
	Normal shadeN = triangles[currentTriangleIndex].InterpolateNormal(
			vertNormals, rayHit.b1, rayHit.b2);

	// Calculate next step
	path->depth++;

	// Check if I have to stop
	if (path->depth >= CONFIG->MAX_PATH_DEPTH) {
		// Too depth, terminate the path
		return false;
	} else if (path->depth > 0.6 * CONFIG->MAX_PATH_DEPTH) {

		// Russian Rulette
		const float p = Min(1.f,
				triInterpCol.filter() * AbsDot(shadeN, path->pathRay.d));

		if (p > getFloatRNG(path->seed)) {
			path->throughput /= p;
		} else {
			// Terminate the path
			return false;
		}
	}

	//--------------------------------------------------------------------------
	// Build the shadow ray
	//--------------------------------------------------------------------------

	// Check if it is a light source
	float RdotShadeN = Dot(path->pathRay.d, shadeN);
	if (geometry->IsLight(currentTriangleIndex)) {
		// Check if we are on the right side of the light source
		if ((path->depth == 1) && (RdotShadeN < 0.f)) {
			path->radiance += triInterpCol * path->throughput;
		}
		// Terminate the path
		return false;
	}

	if (RdotShadeN > 0.f) {
		// Flip shade  normal
		shadeN = -shadeN;
	} else
		RdotShadeN = -RdotShadeN;

	path->throughput *= RdotShadeN * triInterpCol;

	// Trace shadow rays
	//const Point hitPoint = path->pathRay(rayHit.t);

	const Point hitPoint = path->pathRay.o + path->pathRay.d * rayHit.t; // = path->pathRay(rayHit.t);

	tracedShadowRayCount = 0;
	const float lightStrategyPdf = static_cast<float>(SHADOWRAY)
			/ static_cast<float>(geometry->nLights);

	float lightPdf[SHADOWRAY];
	RGB lightColor[SHADOWRAY];
	Ray shadowRay[SHADOWRAY];

	for (unsigned int i = 0; i < SHADOWRAY; ++i) {
		// Select the light to sample
		const unsigned int currentLightIndex = geometry->SampleLights(
				getFloatRNG(path->seed));
		//	const TriangleLight &light = scene->lights[currentLightIndex];

		// Select a point on the surface

		lightColor[tracedShadowRayCount] = Sample_L(currentLightIndex, geometry,
				hitPoint, shadeN, getFloatRNG(path->seed),
				getFloatRNG(path->seed), &lightPdf[tracedShadowRayCount],
				&shadowRay[tracedShadowRayCount], vertices, vertNormals,
				vertColors, triangles, lights);

		// Scale light pdf for ONE_UNIFORM strategy
		lightPdf[tracedShadowRayCount] *= lightStrategyPdf;

//		int x = (int) path->screenX;
//				int y = (int) path->screenY;
//				if (x == 240 && y == 240)
//					printf("%u\n",currentLightIndex);

		// Using 0.1 instead of 0.0 to cut down fireflies
		if (lightPdf[tracedShadowRayCount] > 0.1f)
			tracedShadowRayCount++;
	}

	RayHit rh[SHADOWRAY];

	for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
		atomicAdd((RAYCOUNTER_TYPE*) ray_count, 1);
		Intersect(shadowRay[i], rh[i], bvh, triangles, vertices);
	}

	if ((tracedShadowRayCount > 0)) {
		for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
			const RayHit *shadowRayHit = &rh[i];
			if (shadowRayHit->index == 0xffffffffu) {
				// Nothing was hit, light is visible
				path->radiance += path->throughput * lightColor[i]
						/ lightPdf[i];
			}
		}
	}

	//--------------------------------------------------------------------------
	// Build the next vertex path ray
	//--------------------------------------------------------------------------

	// Calculate exit direction

	float r1 = 2.f * M_PI * getFloatRNG(path->seed);
	float r2 = getFloatRNG(path->seed);
	float r2s = sqrt(r2);
	const Vector w(shadeN);

	Vector u;
	if (fabsf(shadeN.x) > .1f) {
		const Vector a(0.f, 1.f, 0.f);
		u = Cross(a, w);
	} else {
		const Vector a(1.f, 0.f, 0.f);
		u = Cross(a, w);
	}
	u = Normalize(u);

	Vector v = Cross(w, u);

	Vector newDir = u * (cosf(r1) * r2s) + v * (sinf(r1) * r2s)
			+ w * sqrtf(1.f - r2);
	newDir = Normalize(newDir);

	path->pathRay.o = hitPoint;
	path->pathRay.d = newDir;

	return true;
}

__global__ void kernel(PT_CONFIG* CONFIG, PerspectiveCamera* camera,
		RGB* pixelsRadiance, BVHAccelArrayNode* bvh, Point *vertices,
		Normal *vertNormals, RGB *vertColors, Triangle *triangles,
		TriangleLight *lights, Geometry* geometry, prePath* paths,
		RAYCOUNTER_TYPE* ray_count, uint n, unsigned n_pixels,
		uint npixel_start) {

	//int tID = (gridDim.x * blockDim.x * blockIdx.y * blockDim.y) +
	//		(gridDim.x * blockDim.x * threadIdx.y) + (blockDim.x * blockIdx.x + threadIdx.x) + paths.X.start;

	int len_X = gridDim.x * blockDim.x;
	int pos_x = blockIdx.x * blockDim.x + threadIdx.x;
	int pos_y = blockIdx.y * blockDim.y + threadIdx.y;

	int tID = pos_y * len_X + pos_x;

	if (tID < n) {

		bool not_done = false;

		prePath pp = paths[tID];
		Path p = Path(&pp);
		RayHit hit;
		Ray a;

		camera->GenerateRay(&p);

		do {
			atomicAdd((RAYCOUNTER_TYPE*) ray_count, 1);
			a = Ray(p.pathRay.o, p.pathRay.d);
			Intersect(a, hit, bvh, triangles, vertices);
			not_done = Shade(CONFIG, &p, vertices, vertNormals, vertColors,
					triangles, lights, bvh, hit, geometry, ray_count);

		} while (not_done);

		int x = (int) p.screenX;
		int y = (int) p.screenY;

		uint pixel_1d_global = x + y * CONFIG->WIDTH;

		if ((pixel_1d_global < npixel_start)
				|| (pixel_1d_global > (npixel_start + n_pixels))) {
			printf("ERROR!!!\n");

		}

		const int pixel_1d_local = pixel_1d_global - npixel_start;

		atomicAdd(&(pixelsRadiance[pixel_1d_local].r), p.radiance.r);
		atomicAdd(&(pixelsRadiance[pixel_1d_local].g), p.radiance.g);
		atomicAdd(&(pixelsRadiance[pixel_1d_local].b), p.radiance.b);

		//if (tID==0)printf("%u\n",ray_count);
	}

}

void k_CUDA_pathtracer(void *buffers[], void *cl_arg) {

	struct starpu_vector_interface *vector0 =
			(struct starpu_vector_interface *) buffers[0];
	unsigned n_pixels = STARPU_VECTOR_GET_NX(vector0);
	RGB *pixelsRadiance = (RGB*) STARPU_VECTOR_GET_PTR(vector0);

	struct starpu_vector_interface *vector1 =
			(struct starpu_vector_interface *) buffers[1];
	char *geometry_chunk = (char*) STARPU_VECTOR_GET_PTR(vector1);

	struct starpu_vector_interface *vector2 =
			(struct starpu_vector_interface *) buffers[2];
	BVHAccelArrayNode *bvh = (BVHAccelArrayNode*) STARPU_VECTOR_GET_PTR(
			vector2);

	struct starpu_vector_interface *vector3 =
			(struct starpu_vector_interface *) buffers[3];
	unsigned n = STARPU_VECTOR_GET_NX(vector3);
	prePath *paths = (prePath*) STARPU_VECTOR_GET_PTR(vector3);

	struct starpu_vector_interface *vector4 =
			(struct starpu_vector_interface *) buffers[4];
	byte_address arg_buffer = (byte_address) STARPU_VECTOR_GET_PTR(vector4);

	struct starpu_vector_interface *vector5 =
			(struct starpu_vector_interface *) buffers[5];
	RAYCOUNTER_TYPE *raycount = (RAYCOUNTER_TYPE*) STARPU_VECTOR_GET_PTR(
			vector5);

	PerspectiveCamera *camera = (PerspectiveCamera *) arg_buffer;
	Geometry *geometry = (Geometry*) (arg_buffer + sizeof(PerspectiveCamera));
	PT_CONFIG* CONFIG = (PT_CONFIG*) (arg_buffer + sizeof(PerspectiveCamera)
			+ sizeof(Geometry));

	/**
	 * hack
	 */

	cudaStreamSynchronize(starpu_cuda_get_local_stream());

	Geometry geometry_host;
	cudaMemcpy(&geometry_host, geometry, sizeof(Geometry),
			cudaMemcpyDeviceToHost);

	char *vertices_c = (char*) geometry_chunk;
	char *vertNormals_c = vertices_c
			+ geometry_host.vertexCount * sizeof(Point);
	char *vertColors_c = vertNormals_c
			+ geometry_host.vertexCount * sizeof(Normal);
	char *triangles_c = vertColors_c + geometry_host.vertexCount * sizeof(RGB);
	char *lights_c = triangles_c
			+ geometry_host.triangleCount * sizeof(Triangle);

	Point *vertices = (Point*) (vertices_c);
	Normal *vertNormals = (Normal*) (vertNormals_c);
	RGB *vertColors = (RGB*) (vertColors_c);
	Triangle *triangles = (Triangle*) (triangles_c);
	TriangleLight *lights = (TriangleLight*) (lights_c);

	int sqrtn = sqrt(n);

	dim3 blockDIM = dim3(16, 16);
	dim3 gridDIM = dim3((sqrtn / blockDIM.x) + 1, (sqrtn / blockDIM.y) + 1);
	//dim3 gridDIM = dim3( 1,1);

	//RAYCOUNTER_TYPE ray_count = 0;
	//RAYCOUNTER_TYPE* ray_count_d;
	//cudaMalloc((void**) &ray_count_d, sizeof(RAYCOUNTER_TYPE));
	//cudaMemcpy(ray_count_d, &t->ray_count, sizeof(RAYCOUNTER_TYPE), cudaMemcpyHostToDevice);

//	Task_pathtracer* gtask_d;
//	cudaMalloc((void**) &gtask_d, sizeof(Task_pathtracer));
//	cudaMemcpy(gtask_d, t_, sizeof(Task_pathtracer), cudaMemcpyHostToDevice);

//	PerspectiveCamera* camera_d;
//		cudaMalloc((void**) &camera_d, sizeof(PerspectiveCamera));
//		cudaMemcpy(camera_d, t->camera, sizeof(PerspectiveCamera), cudaMemcpyHostToDevice);
//

	//fprintf(stderr, "Launching CUDA kernel for %d paths...\n",n);

	int npath_start = STARPU_VECTOR_GET_OFFSET(vector3)
			/ STARPU_VECTOR_GET_ELEMSIZE(vector3);

	int npixel_start = STARPU_VECTOR_GET_OFFSET(vector0)
			/ STARPU_VECTOR_GET_ELEMSIZE(vector0);

	/**
	 * HUGE HACK: STARPU_VECTOR_GET_OFFSET is bugged in CUDA. Using task counter. This
	 * required that task are ALL same sized!
	 */

	uint off =*(uint*)cl_arg * n_pixels;
		npixel_start = off;

	fprintf(stderr,
				"Launching CUDA kernel for %d paths...\n",n);



	kernel<<<gridDIM,blockDIM,0,starpu_cuda_get_local_stream()>>>(CONFIG,camera,
			pixelsRadiance, (BVHAccelArrayNode*)(bvh), vertices, vertNormals,
			vertColors, triangles, lights,geometry,paths, raycount,n,n_pixels,npixel_start);

	cudaStreamSynchronize(starpu_cuda_get_local_stream());

	// ATTENTION
	//cudaStreamSynchronize(t->stream);
	//cudaMemcpy(&ray_count, ray_count_d, sizeof(RAYCOUNTER_TYPE), cinterudaMemcpyDeviceToHost);
	//t->update_ray_count(ray_count);

//	cudaFree(ray_count_d);
//	cudaFree(camera_d);
//	cudaFree(geometry_d);
//	cudaFree(gtask_d);

	//

	//	cudaFree(task_exec_d);

}
