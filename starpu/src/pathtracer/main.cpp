
#include <stdio.h>
#include <stdlib.h>
#include "perform.h"
#include "core.h"
#include "config.h"

int jobPATHTRACER(int argc, char *argv[]);

int main(int argc, char *argv[]) {

	argv[1] = "2";//sch
	argv[2] = "4";//dev
	argv[3] = "240";//dice
	argv[4] = "480";//heigth
	argv[5] = "480";//width
	argv[6] = "16";//spp
	argv[7] = "10";//max depth
	argv[8] = "2";//scene

//#define SCENE1 "scenes/luxball.scn"
//#define SCENE2 "scenes/simple.scn"
//#define SCENE3 "scenes/interp.scn"
//#define SCENE4 "scenes/loft.scn"
//#define SCENE5 "scenes/kitchen.scn"

	//fprintf(stderr, "sch: %s dev: %s dice: %s timesteps: %s nbodies: %s\n", argv[1], argv[2], argv[3], argv[4],	argv[5]);

	//initPERFORM(atoi(argv[1]), atoi(argv[2]),atoi(argv[3]));

	double time = jobPATHTRACER(argc, argv);

	//shutdownPERFORM();

	LOG(0.1,cerr << "\n\nRun-time terminated. Job time: " << time << "s." << endl;)

	exit(0);

	return 0;
}
