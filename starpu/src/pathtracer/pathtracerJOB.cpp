#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "display/film.h"
#include "display/displayfunc.h"
#include "path.h"
#include "randomgen.h"
#include "geometry.h"
#include "geometry/bvhaccel.h"
#include "camera.h"
#include "config.h"
#include "perform.h"
#include <time.h>

#include "utils.h"
//#include "user_defined_task.h"

#include <omp.h>

#include <cuda.h>
#include <cuda_runtime.h>

//#include <boost/thread/mutex.hpp>

#include "pathtracer.hpp"

#include <starpu.h>
#include <starpu_profiling.h>
#include <starpu_data_filters.h>


#include "../device_configs.h"

PT_CONFIG CONFIG;

static struct starpu_codelet cl;

static struct starpu_perfmodel mult_perf_model;

void k_CUDA_pathtracer(void *buffers[], void *cl_arg);

int jobPATHTRACER(int argc, char *argv[]) {


	initCONFIG(argv);

	uint dice_v = atoi(argv[3]);


	assert((CONFIG.WIDTH*CONFIG.HEIGHT) % dice_v == 0);

	uint block = (CONFIG.WIDTH*CONFIG.HEIGHT*CONFIG.SPP)/dice_v;

	//2d path distr
	assert (block >= CONFIG.WIDTH * CONFIG.SPP);
	assert (block % CONFIG.WIDTH * CONFIG.SPP == 0);


	cerr << "PARAMETERS:" << CONFIG.WIDTH << "x" << CONFIG.HEIGHT << "x" << CONFIG.SPP << "x"
			<< SHADOWRAY << "x" << CONFIG.MAX_PATH_DEPTH << endl;

	// Entities
	RGB *pixelsRadiance;
	float *pixels;
	PerspectiveCamera *camera = new PerspectiveCamera();
	BVHAccel *bvh;
	Geometry *geometry = new Geometry(); // include object and light areas
	RAYCOUNTER_TYPE raycount = 0;

	ParseSceneFile(camera, geometry);
	//
	bvh = new BVHAccel(geometry->triangleCount, geometry->triangles, geometry->vertices);


	starpu_malloc((void**)&pixelsRadiance,PIXEL_COUNT * sizeof(RGB));

	pixels = new float[PIXEL_COUNT * 3];

	memset(pixelsRadiance, 0, PIXEL_COUNT * sizeof(RGB));

	memset(pixels, 0, 3 * PIXEL_COUNT * sizeof(float));

	prePath* paths = preGeneratePaths( false);


	PERFORM_acc_timer* timer = new PERFORM_acc_timer();

	uint chunk_length = geometry->vertexCount * sizeof(Point) + geometry->vertexCount
			* sizeof(Normal) + geometry->vertexCount * sizeof(RGB) + geometry->triangleCount
			* sizeof(Triangle) + geometry->nLights * sizeof(TriangleLight);

	geometry->Inline();

	byte_address arg_buffer;
	starpu_malloc((void**)&arg_buffer,
			sizeof(PerspectiveCamera) + sizeof(Geometry) + sizeof(CONFIG));

	memcpy(arg_buffer, camera, sizeof(PerspectiveCamera));
	memcpy(arg_buffer + sizeof(PerspectiveCamera), (void*) geometry, sizeof(Geometry));
	memcpy(arg_buffer + sizeof(PerspectiveCamera) + sizeof(Geometry), &CONFIG, sizeof(PT_CONFIG));


	RAYCOUNTER_TYPE* counter;
	starpu_malloc((void**)&counter,	 dice_v*sizeof(RAYCOUNTER_TYPE));
	memset(counter,0,dice_v*sizeof(RAYCOUNTER_TYPE) );


	struct starpu_conf conf;
	starpu_conf_init(&conf);

	//conf.calibrate = 0;

	int dev_config = atoi(argv[2]);

	switch (dev_config) {
	case 0://CPU
		conf.ncuda = 0;
		break;
	case 1://GPU
		conf.ncpus = 0;
		conf.ncuda = 1;
		break;
	case 2://CPU + GPU
		conf.ncuda = 1;
		break;
	case 3://2xGPU
		conf.ncpus = 0;
		conf.ncuda = 2;
		break;
	case 4://CPU + 2xGPU
		conf.ncuda = 2;
		break;
	case 5://3xGPU
		conf.ncpus = 0;
		conf.ncuda = 3;
		break;
	case 6://CPU + 3xGPU
		conf.ncuda = 3;
		break;

	default:
		break;
	}

	conf.nopencl = 0;

	switch (atoi(argv[1])) {
	case 0:
		conf.sched_policy_name = "eager";
		break;
	case 1:
		conf.sched_policy_name = "ws";
		break;
	case 2:
		conf.sched_policy_name = "dmda";
		//if (dev_config == 0 || dev_config == 2 || dev_config == 4 || dev_config == 6)
	//		conf.ncpus = 12;
		break;
	default:
		break;
	}

	//conf.single_combined_worker = 1;

	//The eager scheduler uses a central task queue, from which workers draw tasks to work on. This however does not permit to prefetch data since the scheduling decision is taken late. If a task has a non-0 priority, it is put at the front of the queue.
	//The prio scheduler also uses a central task queue, but sorts tasks by priority (between -5 and 5).
	//The random scheduler distributes tasks randomly according to assumed worker overall performance.
	//The ws (work stealing) scheduler schedules tasks on the local worker by default. When a worker becomes idle, it steals a task from the most loaded worker.
	//The dm (deque model) scheduler uses task execution performance models into account to perform an HEFT-similar scheduling strategy: it schedules tasks where their termination time will be minimal.
	//The dmda (deque model data aware) scheduler is similar to dm, it also takes into account data transfer time.
	//The dmdar (deque model data aware ready) scheduler is similar to dmda, it also sorts tasks on per-worker queues by number of already-available data buffers.
	//The dmdas (deque model data aware sorted) scheduler is similar to dmda, it also supports arbitrary priority values.
	//The heft (heterogeneous earliest finish time) scheduler is similar to dmda, it also supports task bundles.
	//The pheft (parallel HEFT) scheduler is similar to heft, it also supports parallel tasks (still experimental).
	//The pgreedy (parallel greedy) scheduler is similar to greedy, it also supports parallel tasks (still experimental).

	conf.use_explicit_workers_cuda_gpuid = 1;
	//conf.workers_cuda_gpuid = {1,0};
	conf.workers_cuda_gpuid[2] = GPU1;
	conf.workers_cuda_gpuid[1] = GPU2;
	conf.workers_cuda_gpuid[0] = GPU0;

	starpu_init(&conf);

	starpu_profiling_status_set(STARPU_PROFILING_ENABLE);

	fprintf(stderr, "CPUs: %d GPUs:%d\n", conf.ncpus, conf.ncuda);

	starpu_data_handle_t pixelsRadiance_D;
	starpu_vector_data_register(&pixelsRadiance_D, 0, (uintptr_t) pixelsRadiance, PIXEL_COUNT,
			sizeof(RGB));

	starpu_data_handle_t geometry_chunk_D;
	starpu_vector_data_register(&geometry_chunk_D, 0, (uintptr_t) geometry->chunk, chunk_length,
			sizeof(char));

	starpu_data_handle_t bvh_D;
	starpu_vector_data_register(&bvh_D, 0, (uintptr_t) bvh->bvhTree, bvh->nNodes,
			sizeof(BVHAccelArrayNode));

	starpu_data_handle_t paths_D;
	starpu_vector_data_register(&paths_D, 0, (uintptr_t) paths, PATH_COUNT, sizeof(prePath));

	starpu_data_handle_t args_D;
	starpu_vector_data_register(&args_D, 0, (uintptr_t) arg_buffer,
			sizeof(PerspectiveCamera) + sizeof(Geometry) + sizeof(PT_CONFIG), sizeof(byte_address));

	starpu_data_handle_t counter_D;
	starpu_vector_data_register(&counter_D, 0, (uintptr_t) (counter), dice_v,
			sizeof(RAYCOUNTER_TYPE));

	mult_perf_model.type = STARPU_HISTORY_BASED;

	mult_perf_model.symbol = "pathtracer";

	cl.where = STARPU_CPU | STARPU_CUDA;

	/* CPU implementation of the codelet */
	cl.cpu_funcs[0] = k_CPU_pathtracer;
	cl.cpu_funcs[1] = NULL;

	cl.cuda_funcs[0] = k_CUDA_pathtracer;
	cl.cuda_funcs[1] = NULL;

	cl.nbuffers = 6;

	cl.modes[0] = STARPU_RW;
	cl.modes[1] = STARPU_R;
	cl.modes[2] = STARPU_R;
	cl.modes[3] = STARPU_R;
	cl.modes[4] = STARPU_R;
	cl.modes[5] = STARPU_RW;

	cl.model = &mult_perf_model;

	struct starpu_data_filter f_pixelsRadiance_D;
	f_pixelsRadiance_D.filter_func = starpu_vector_filter_block;
	f_pixelsRadiance_D.nchildren = dice_v;
	f_pixelsRadiance_D.get_child_ops = 0;
	f_pixelsRadiance_D.get_nchildren = 0;
	starpu_data_partition(pixelsRadiance_D, &f_pixelsRadiance_D);

	struct starpu_data_filter f_paths_D;
	f_paths_D.filter_func = starpu_vector_filter_block;
	f_paths_D.nchildren = dice_v;
	f_paths_D.get_child_ops = 0;
	f_paths_D.get_nchildren = 0;
	starpu_data_partition(paths_D, &f_paths_D);

	struct starpu_data_filter f_count_D;
	f_count_D.filter_func = starpu_vector_filter_block;
	f_count_D.nchildren = dice_v;
	f_count_D.get_child_ops = 0;
	f_count_D.get_nchildren = 0;
	starpu_data_partition(counter_D, &f_count_D);

	uint n_subtasks = starpu_data_get_nb_children(paths_D);

	timer->start();

	/* Submit a task on each sub-vector */
	uint* args;
	starpu_malloc((void**)&args,	 n_subtasks*sizeof(uint));

	struct starpu_task** tasks_v = new struct starpu_task*[n_subtasks];

	for (uint i = 0; i < n_subtasks; i++) {
		/* Get subdata number i (there is only 1 dimension) */
		starpu_data_handle_t paths_sub_handle = starpu_data_get_sub_data(paths_D, 1, i);
		starpu_data_handle_t pixel_sub_handle = starpu_data_get_sub_data(pixelsRadiance_D, 1, i);
		starpu_data_handle_t counter_sub_handle = starpu_data_get_sub_data(counter_D, 1, i);

		tasks_v[i] = starpu_task_create();

		struct starpu_task* task = tasks_v[i];

		task->handles[0] = pixel_sub_handle;
		task->handles[1] = geometry_chunk_D;
		task->handles[2] = bvh_D;
		task->handles[3] = paths_sub_handle;
		task->handles[4] = args_D;
		task->handles[5] = counter_sub_handle;
		task->cl = &cl;
		task->synchronous = 0;
		args[i] = i;
		task->cl_arg = args + i;
		task->cl_arg_size = sizeof(uint);
		//starpu_task_submit(task);
	}



	for (uint i = 0; i < n_subtasks; i++) {
		starpu_task_submit(tasks_v[i]);
	}


	fprintf(stderr, "All tasks submitted...\n");

	starpu_task_wait_for_all();

	fprintf(stderr, "Retrieving data...\n");

	starpu_data_unpartition(pixelsRadiance_D, 0);
	starpu_data_unpartition(counter_D, 0);

	fprintf(stderr, "Data retrieved.\n");

	timer->stop();

	for (uint i = 0; i < n_subtasks; i++) {
		raycount += counter[i];
	}

	fprintf(stderr, "%lld\t", raycount);
	fprintf(stdout, "%.3f\t", timer->duration);
	//fprintf(stderr, "%.2f\t", ((raycount / timer->duration)) / 1000000);
	//fprintf(stderr, "Million rays per second: %.2f MRays/s\n",
//			((raycount / timer->duration)) / 1000000);

	film = new Film(CONFIG.WIDTH, CONFIG.HEIGHT);

	UpdateScreenBuffer(film, CONFIG.WIDTH, CONFIG.HEIGHT, pixelsRadiance, pixels);

	film->pixels = pixels;

	InitGlut(argc, argv, CONFIG.WIDTH, CONFIG.HEIGHT);

	clock_t a = clock();
	string s = "_" + to_string<clock_t> (a, std::dec);
	string MD = "MD" + string(argv[7]);
	string S = "S" + string(argv[8]);
	string SPP = "SPP" + string(argv[6]);
	string SH = "SH" + to_string<int> (SHADOWRAY, std::dec);
	string SHF = "SHF" + to_string<int> (0, std::dec);
	film->SavePPM("image" + MD + S + SPP + SH + SHF + s + ".ppm");

	/* Display the occupancy of all workers during the test */
	int worker;

	for (worker = 0; worker < starpu_worker_get_count(); worker++)
		{
			struct starpu_profiling_worker_info worker_info;
			int ret = starpu_profiling_worker_get_info(worker, &worker_info);
			STARPU_ASSERT(!ret);

			double total_time = starpu_timing_timespec_to_us(&worker_info.total_time);
			double executing_time = starpu_timing_timespec_to_us(&worker_info.executing_time);
			double sleeping_time = starpu_timing_timespec_to_us(&worker_info.sleeping_time);
			double overhead_time = total_time - executing_time - sleeping_time;

			float executing_ratio = 100.0*executing_time/total_time;
			float sleeping_ratio = 100.0*sleeping_time/total_time;
			float overhead_ratio = 100.0 - executing_ratio - sleeping_ratio;

			char workername[128];
			starpu_worker_get_name(worker, workername, 128);
			fprintf(stderr, "Worker %s:\n", workername);
			fprintf(stderr, "\ttotal time : %.2lf ms\n", total_time*1e-3);
			fprintf(stderr, "\texec time  : %.2lf ms (%.2f %%)\n", executing_time*1e-3, executing_ratio);
			fprintf(stderr, "\tblocked time  : %.2lf ms (%.2f %%)\n", sleeping_time*1e-3, sleeping_ratio);
			fprintf(stderr, "\toverhead time: %.2lf ms (%.2f %%)\n", overhead_time*1e-3, overhead_ratio);
		}

	starpu_shutdown();

	RunGlut();


	return EXIT_SUCCESS;
}

