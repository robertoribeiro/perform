/***************************************************************************
 *   Copyright (C) 1998-2009 by David Bucciarelli (davibu@interfree.it)    *
 *                                                                         *
 *   This file is part of SmallLuxGPU.                                     *
 *                                                                         *
 *   SmallLuxGPU is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *  SmallLuxGPU is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 *   This project is based on PBRT ; see http://www.pbrt.org               *
 *   and Lux Renderer website : http://www.luxrender.net                   *
 ***************************************************************************/

#ifndef _PATH_H
#define	_PATH_H

#include "ray.h"
#include "geometry/spectrum.h"
#include "config.h"
#include "random.h"
#include "perform_utils.h"
#include <omp.h>
#include "float.h"
//#include "pathtracer.h"
#include <starpu.h>


#define RAYCOUNTER_TYPE unsigned long long int


class prePath {
public:

	float screenX, screenY;//, strat;
	Seed seed;

	//Seed seed;

	__HD__
	prePath() {
		screenX = -1.f;
		screenY = -1.f;
		//seed.z1 = 0ul;
		//seed.z2 = 0ul;
		//seed.z3 = 0ul;
		//seed.z4 = 0ul;


	}
	__HD__
	~prePath() {
	}

};

class Path {
public:

	float screenX, screenY;
	RGB throughput;
	RGB radiance;
	int depth;
	baseRay pathRay;
	Seed seed;

	//float strat;

	__HD__
	Path(prePath* p) {

		screenX = p->screenX;
		screenY = p->screenY;

		throughput = RGB(1.f, 1.f, 1.f);
		radiance = RGB(0.f, 0.f, 0.f);
		depth = 0;

		//strat = p->strat;
		seed = p->seed;

	}

	__HD__
	Path() {

		throughput = RGB(1.f, 1.f, 1.f);
		radiance = RGB(0.f, 0.f, 0.f);
		depth = 0;

	}

	__HD__
	~Path() {

	}

};

// incorrect
template<typename T>
void static shuffleROOKS2(T* deck) {

	fprintf(stderr, "Rooks Shuffeling...\n");

	int stepY = 4 * 4;
	int stepX = 8 * 4;
	int kX, kY;
	float strat = CONFIG.STRATA;

	for (int rr = 0; rr < 30; rr++)
		lrand48();

	int lenX = CONFIG.WIDTH * strat;
	int lenY = CONFIG.HEIGHT * strat;

	for (int y = 0; y < lenY / stepY; y++) {
		for (int x = 0; x < lenX / stepX; x++) {

			for (int yy = y * stepY; yy < (y * stepY + stepY); yy++)
				for (int xx = x * stepX; xx < (x * stepX + stepX); xx++) {

					kX = lrand48() % (stepX);
					//kY = lrand48() % stepY;

					T temp = deck[yy * lenX + xx];
					deck[yy * lenX + xx] = deck[(yy) * lenX + (x + kX)];
					deck[(yy) * lenX + (x + kX)] = temp;

				}

			for (int xx = x * stepX; xx < (x * stepX + stepX); xx++)
				for (int yy = y * stepY; yy < (y * stepY + stepY); yy++) {

					//kX = lrand48() % stepX;
					kY = lrand48() % (stepY);

					T temp = deck[yy * lenX + xx];
					deck[yy * lenX + xx] = deck[(y + kY) * lenX + (xx)];
					deck[(y + kY) * lenX + (xx)] = temp;

				}

		}

	}
}

void static setup_shuffled_indicesX(vector<int>& shuffled_indices, int x, int y) {

	shuffled_indices.reserve(x * y);
	vector<int> indices;

	for (int j = 0; j < x; j++)
		indices.push_back(j);

	for (int p = 0; p < y; p++) {
		random_shuffle(indices.begin(), indices.end());
		for (int j = 0; j < x; j++) {
			shuffled_indices.push_back(indices[j]);
		}
	}
}

void static setup_shuffled_indicesY(vector<int>& shuffled_indices, int x, int y) {

	shuffled_indices.reserve(x * y);
	vector<int> indices;

	for (int j = 0; j < y; j++)
		indices.push_back(j);

	for (int p = 0; p < x; p++) {
		random_shuffle(indices.begin(), indices.end());
		for (int j = 0; j < y; j++) {
			shuffled_indices.push_back(indices[j]);
		}
	}
}

// rooks total
template<typename T>
void static shuffleROOKS_b(T* d_deck, T* deck) {

	fprintf(stderr, "Rooks Shuffeling...\n");

	int stepY = 4 * 16;
	int stepX = 8 * 16;

	float strat = CONFIG.STRATA;

	int lenX = CONFIG.WIDTH * strat;
	int lenY = CONFIG.HEIGHT * strat;

	for (int y = 0; y < lenY; y += stepY) {
		for (int x = 0; x < lenX; x += stepX) {

			vector<int> shuffled_indices;
			setup_shuffled_indicesX(shuffled_indices, stepX, stepY);

			for (int yy = y, sy = 0; yy < (y + stepY); yy++, sy++)
				for (int xx = x, sx = 0; xx < (x + stepX); xx++, sx++) {

					int target = shuffled_indices[sy * stepX + sx];
					d_deck[yy * lenX + xx] = deck[yy * lenX + x + target];
					//d_deck[yy * lenX + xx]  = deck[yy * lenX + xx];

				}

			T* tmp = deck;
			deck = d_deck;
			d_deck = tmp;

			vector<int> shuffled_indices2;
			setup_shuffled_indicesY(shuffled_indices2, stepX, stepY);

			for (int xx = x, sx = 0; xx < (x + stepX); xx++, sx++)
				for (int yy = y, sy = 0; yy < (y + stepY); yy++, sy++) {

					int target = shuffled_indices2[sx * stepY + sy];
					d_deck[yy * lenX + xx] = deck[(y + target) * lenX + xx];
					//d_deck[yy * lenX + xx]  = deck[yy * lenX + xx];

				}

			tmp = deck;
			deck = d_deck;
			d_deck = tmp;

		}

	}
}
//http://www.cs.sfu.ca/~torsten/Teaching/Cmpt461/LectureNotes/PDF/06_sampling.pdf
//http://graphics.stanford.edu/~mmp/chapters/pbrt_chapter7.pdf
//http://web.cs.wpi.edu/~emmanuel/courses/cs563/S10/talks/wk3_p1_wadii_sampling_techniques.pdf
//http://stackoverflow.com/questions/6446350/shuffling-and-nrooks-constraint-preservation
//http://delivery.acm.org/10.1145/590000/581914/p125-bekaert.pdf?ip=193.136.19.36&acc=ACTIVE%20SERVICE&CFID=90852317&CFTOKEN=90640133&__acm__=1332244507_2da3bc165f9f27b91699f482823feb43
//http://graphics.ucsd.edu/~henrik/papers/coherent_path_tracing.pdf
// rooks partial
template<typename T>
void static shuffleROOKS(T* deck) {

	fprintf(stderr, "Rooks Shuffeling...\n");

	int stepY = 4 * 8;
	int stepX = 8 * 8;
	int kX, kY;
	float strat = CONFIG.STRATA;

	for (int rr = 0; rr < 30; rr++)
		lrand48();

	int lenX = CONFIG.WIDTH * strat;
	int lenY = CONFIG.HEIGHT * strat;

	for (int y = 0; y < lenY; y += stepY) {
		for (int x = 0; x < lenX; x += stepX) {

			for (int yy = y; yy < (y + stepY); yy++)
				for (int xx = x; xx < (x + stepX); xx++) {

					kX = lrand48() % (stepX);
					//kY = lrand48() % stepY;

					T temp = deck[yy * lenX + xx];
					deck[yy * lenX + xx] = deck[(yy) * lenX + (x + kX)];
					deck[(yy) * lenX + (x + kX)] = temp;

				}

			for (int xx = x; xx < (x + stepX); xx++)
				for (int yy = y; yy < (y + stepY); yy++) {

					//kX = lrand48() % stepX;
					kY = lrand48() % (stepY);

					T temp = deck[yy * lenX + xx];
					deck[yy * lenX + xx] = deck[(y + kY) * lenX + (xx)];
					deck[(y + kY) * lenX + (xx)] = temp;

				}

		}

	}
}

// http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
template<typename T>
void static shuffle(T* deck, int shuffle_size) {

	fprintf(stderr, "Shuffeling %d...\n",shuffle_size);


	int stepY = 4 * shuffle_size;
	int stepX = 8 * shuffle_size;
	int kX, kY;
	float strat = CONFIG.STRATA;

	for (int rr = 0; rr < 30; rr++)
		lrand48();

	int lenX = CONFIG.WIDTH * strat;
	int lenY = CONFIG.HEIGHT * strat;

	for (int y = 0; y < lenY; y += stepY) {
		for (int x = 0; x < lenX; x += stepX) {

			for (int yy = y; yy < (y + stepY); yy++)
				for (int xx = x; xx < (x + stepX); xx++) {

					kX = lrand48() % stepX;
					kY = lrand48() % stepY;

					T temp = deck[yy * lenX + xx];
					deck[yy * lenX + xx] = deck[(y + kY) * lenX + (x + kX)];
					deck[(y + kY) * lenX + (x + kX)] = temp;

				}
		}

	}
}
static prePath* preGeneratePaths(bool do_shuffle,int shuffle_size = 0) {

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();

	prePath* prePaths;
	starpu_malloc((void**)&prePaths,PATH_COUNT*sizeof(prePath));

	//prePath* prePaths = new prePath[PATH_COUNT];

	Seed a = mwc();

	int strat = int(CONFIG.STRATA);

	if (do_shuffle && ((int) strat * CONFIG.HEIGHT % 4 != 0 ||
		(int) strat * CONFIG.WIDTH < shuffle_size * 8 ||
		(strat * CONFIG.WIDTH) % (8*shuffle_size) != 0 ||
		(strat * CONFIG.HEIGHT) % (4*shuffle_size) != 0)) {
		printf("ERROR: check sizes\n");
		exit(0);
	}

	for (int currentSampleScreenY = 0; currentSampleScreenY < CONFIG.HEIGHT ; currentSampleScreenY++) {
		for (int currentSampleScreenX = 0; currentSampleScreenX < CONFIG.WIDTH; currentSampleScreenX++) {

			for (uint sample = 0; sample < CONFIG.SPP; sample++) {

				// In order to improve ray coherence
				uint stepX = sample % (int) strat;
				uint stepY = sample / (int) strat;

				//position in 1d array of the 2d sample position
				int _offset = (currentSampleScreenY * CONFIG.WIDTH * CONFIG.SPP)
										+ (currentSampleScreenX * CONFIG.SPP + sample);


//int _offset = (currentSampleScreenY * strat + stepY) * CONFIG.WIDTH * strat
									//	+ (currentSampleScreenX * strat + stepX);

				float u = fabs(getFloatRNG(a) - 0.001);

				prePaths[_offset].screenX = currentSampleScreenX + (stepX + u) / float(strat);

				u = fabs(getFloatRNG(a) - 0.001);

				prePaths[_offset].screenY = currentSampleScreenY + (stepY + u) / float(strat);

				//if (prePaths[_offset].screenY >= float(CONFIG.HEIGHT))
				//	printf("here");
				prePaths[_offset].seed = mwc();
				//prePaths[_offset].seed =_offset;
			}
		}
	}

	//int lenX = CONFIG.WIDTH * strat;
	//int lenY = CONFIG.HEIGHT * strat;

	//	for (int y = 0; y < lenY; y++) {
	//			for (int x = 0; x < lenX; x++) {
	//				printf("%3.u ",prePaths[y*lenX + x].seed);
	//			}
	//			printf("\n");
	//	}

	//prePath* d_paths = new prePath[PATH_COUNT];
	timer->start();

	if (do_shuffle) {
		//shuffleROOKS_b<prePath> (d_paths, prePaths);
		//shuffleROOKS2<prePath> (prePaths);
		//d_paths = prePaths;
		shuffle<prePath> (prePaths,shuffle_size);

	}

	//printf("\n");

	//	for (int y = 0; y < lenY; y++) {
	//			for (int x = 0; x < lenX; x++) {
	//				printf("%3.u ",prePaths[y*lenX + x].seed);
	//			}
	//			printf("\n");
	//	}

	timer->stop();

	fprintf(stderr, "Path pre-pregenration time: %s\n", timer->print());

	return prePaths;

}


static prePath* preGeneratePaths_(bool sh) {

	prePath* prePaths = new prePath[PATH_COUNT];


	uint currentSampleScreenX = 0;
	uint currentSampleScreenY = 0;
	uint currentSubSampleIndex = 0;
	float strat = ceil(sqrt(CONFIG.SPP));

	for (int _i = 0; _i < PATH_COUNT; _i++) {

		uint scrX, scrY;

		// In order to improve ray coherency
		uint stepX = currentSubSampleIndex % (int) strat;
		uint stepY = currentSubSampleIndex / strat;

		scrX = currentSampleScreenX;
		scrY = currentSampleScreenY;

		currentSubSampleIndex++;
		if (currentSubSampleIndex == CONFIG.SPP) {
			currentSubSampleIndex = 0;
			currentSampleScreenX++;
			if (currentSampleScreenX >= CONFIG.WIDTH) {

				currentSampleScreenX = 0;
				currentSampleScreenY++;

				if (currentSampleScreenY >= CONFIG.HEIGHT) {
					currentSampleScreenY = 0;
				}
			}
		}

		//	const float r1 = (stepX + getFloatRNG(&p->seed)) / 4.f - .5f;
		//	const float r2 = (stepY + getFloatRNG(&p->seed)) / 4.f - .5f;
		//
		//

		//		const float r1 = (stepX - 2.f) / 4.f;
		//		const float r2 = (stepY - 2.f) / 4.f;

		const float r1 = (stepX) / strat;
		const float r2 = (stepY) / strat;

		prePaths[_i].screenX = scrX + r1;
		prePaths[_i].screenY = scrY + r2;
		//prePaths[_i].strat = strat;
		prePaths[_i].seed = mwc();

	}

	return prePaths;

}

#endif	/* _PATH_H */

