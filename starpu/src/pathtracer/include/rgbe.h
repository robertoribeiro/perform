#ifndef _H_RGBE
#define _H_RGBE
/* THIS CODE CARRIES NO GUARANTEE OF USABILITY OR FITNESS FOR ANY PURPOSE.
 * WHILE THE AUTHORS HAVE TRIED TO ENSURE THE PROGRAM WORKS CORRECTLY,
 * IT IS STRICTLY USE AT YOUR OWN RISK.  */

/* utility for reading and writing Ward's rgbe image format.
 See rgbe.txt file for more details.
 */

#include <stdio.h>

#include <math.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "core.h"


class RGBe {
public:
	unsigned char rgbe[4];
	__HD__
	RGBe() {
	}

	__HD__
	RGBe(RGB r)	 {
		float v;
			  int e;

			  v = r.r;
			  if (r.g > v) v = r.g;
			  if (r.b > v) v = r.b;
			  if (v < 1e-32) {
			    rgbe[0] = rgbe[1] = rgbe[2] = rgbe[3] = 0;
			  }
			  else {
			    v = frexp(v,&e) * 256.0/v;
			    rgbe[0] = (unsigned char) (r.r * v);
			    rgbe[1] = (unsigned char) (r.g * v);
			    rgbe[2] = (unsigned char) (r.b * v);
			    rgbe[3] = (unsigned char) (e + 128);
			  }
	}

	__forceinline void
	float2rgbe(float red, float green, float blue)
	{
	  float v;
	  int e;

	  v = red;
	  if (green > v) v = green;
	  if (blue > v) v = blue;
	  if (v < 1e-32) {
	    rgbe[0] = rgbe[1] = rgbe[2] = rgbe[3] = 0;
	  }
	  else {
	    v = frexp(v,&e) * 256.0/v;
	    rgbe[0] = (unsigned char) (red * v);
	    rgbe[1] = (unsigned char) (green * v);
	    rgbe[2] = (unsigned char) (blue * v);
	    rgbe[3] = (unsigned char) (e + 128);
	  }
	}

	/* standard conversion from rgbe to float pixels */
	/* note: Ward uses ldexp(col+0.5,exp-(128+8)).  However we wanted pixels */
	/*       in the range [0,1] to map back into the range [0,1].            */
	__HD__
	__forceinline void
	getRGB(RGB& r)
	{

	//	float *red, float *green, float *blue
	  float f;

	  if (rgbe[3]) {   /*nonzero pixel*/
	    f = ldexp(1.0,rgbe[3]-(int)(128+8));
	    r.r = rgbe[0] * f;
	    r.g = rgbe[1] * f;
	    r.b = rgbe[2] * f;
	  }
	  else
		  r.r = r.g = r.b = 0.0;
	}
};

#endif /* _H_RGBE */

