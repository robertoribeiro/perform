#ifndef PTRACER_H_
#define PTRACER_H_

#include "path.h"
#include "camera.h"
#include "ray.h"
#include "geometry/bvhaccel.h"
#include "geometry/triangle.h"
#include "geometry/light.h"
#include "geometry.h"
#include "utils.h"
#include "randomgen.h"
#include "config.h"

#include "my_cutil_math.h"

//#include "math_functions.h"

__device__  __forceinline
float4 make_float4(const Point& p) {
	float4 t;
	t.x = p.x;
	t.y = p.y;
	t.z = p.z;
	t.w = 0.f;
	return t;
}

__device__  __forceinline
float4 make_float4(const Vector& p) {
	float4 t;
	t.x = p.x;
	t.y = p.y;
	t.z = p.z;
	t.w = 0.f;
	return t;
}

__device__  __forceinline
float4 make_float4(const Normal& p) {
	float4 t;
	t.x = p.x;
	t.y = p.y;
	t.z = p.z;
	t.w = 0.f;
	return t;
}

__device__  __forceinline
float4 cross(float4 a, float4 b) {
	return make_float4(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x, 0.f);
}

__device__ __forceinline
int BBoxIntersectP(const float4 rayOrig, const float4 invRayDir,
		const float mint, const float maxt, const float4 pMin, const float4 pMax) {

	const float4 l1 = (pMin - rayOrig) * invRayDir;
	const float4 l2 = (pMax - rayOrig) * invRayDir;

	const float4 tNear = fminf(l1, l2);
	const float4 tFar = fmaxf(l1, l2);

	float t0 = max(max(max(tNear.x, tNear.y), max(tNear.x, tNear.z)), mint);
	float t1 = min(min(min(tFar.x, tFar.y), min(tFar.x, tFar.z)), maxt);

	return (t1 > t0);
}

__device__ __forceinline
bool TriangleIntersect(const float4& rayOrig, const float4& rayDir,
		const float minT, float *maxT, unsigned int *hitIndex, float *hitB1, float *hitB2,
		const unsigned int currentIndex, Point *verts, Triangle *tris) {

	// Load triangle vertices
	Point *p0 = &verts[tris[currentIndex].v[0]];
	Point *p1 = &verts[tris[currentIndex].v[1]];
	Point *p2 = &verts[tris[currentIndex].v[2]];

	float4 v0 = make_float4(p0->x, p0->y, p0->z, 0.f);
	float4 v1 = make_float4(p1->x, p1->y, p1->z, 0.f);
	float4 v2 = make_float4(p2->x, p2->y, p2->z, 0.f);

	// Calculate intersection
	float4 e1 = v1 - v0;
	float4 e2 = v2 - v0;
	float4 s1 = cross(rayDir, e2);

	 float divisor = dot(s1, e1);
	if (divisor == 0.f)
		return false;

	 float invDivisor = 1.f / divisor;

	// Compute first barycentric coordinate
	 float4 d = rayOrig - v0;
	 float b1 = dot(d, s1) * invDivisor;
	if (b1 < 0.f)
		return false;

	// Compute second barycentric coordinate
	 float4 s2 = cross(d, e1);
	 float b2 = dot(rayDir, s2) * invDivisor;
	if (b2 < 0.f)
		return false;
//
	 float b0 = 1.f - b1 - b2;
	if (b0 < 0.f)
		return false;
//
	// Compute _t_ to intersection point
	 float t = dot(e2, s2) * invDivisor;
	if (t<minT || t> * maxT)
		return false;

	*maxT = t;
	*hitB1 = b1;
	*hitB2 = b2;
	*hitIndex = currentIndex;

	return true;
}


__device__ __forceinline
void Intersect(const Ray &ray, RayHit& rayHit, BVHAccelArrayNode* bvhTree, Triangle *triangles,
		Point *vertices) {

	float4 rayOrig = make_float4 (ray.o.x, ray.o.y, ray.o.z, 0.f);
	float4 rayDir =make_float4 (ray.d.x, ray.d.y, ray.d.z, 0.f);

	float minT = ray.mint;
	float maxT = ray.maxt;

	float4 invRayDir = make_float4(1.f) / make_float4(ray.d);

	unsigned int hitIndex = 0xffffffffu;
	unsigned int currentNode = 0; // Root Node
	float b1, b2;
	unsigned int stopNode = bvhTree[0].skipIndex; // Non-existent

	float4 pMin;
	float4 pMax;

	while (currentNode < stopNode) {

		pMin = make_float4(bvhTree[currentNode].bbox.pMin.x,
				bvhTree[currentNode].bbox.pMin.y, bvhTree[currentNode].bbox.pMin.z, 0.f);
		 pMax = make_float4(bvhTree[currentNode].bbox.pMax.x,
				bvhTree[currentNode].bbox.pMax.y, bvhTree[currentNode].bbox.pMax.z, 0.f);

		if (BBoxIntersectP(rayOrig, invRayDir, minT, maxT, pMin, pMax)) {
			const unsigned int triIndex = bvhTree[currentNode].primitive;


			if (triIndex != 0xffffffffu)
				TriangleIntersect(rayOrig, rayDir, minT, &maxT, &hitIndex, &b1, &b2, triIndex,
						vertices, triangles);

			currentNode++;
		} else {
			//bvhTree[currentNode].skipIndex;
			currentNode = bvhTree[currentNode].skipIndex;
		}
	}

	// Write result
	rayHit.t = maxT;
	rayHit.b1 = b1;
	rayHit.b2 = b2;
	rayHit.index = hitIndex;
}

__device__ __forceinline
RGB Sample_L(uint triIndex, Geometry *objs, const Point &p, const Normal &N, const float u0,
		const float u1, float *pdf, Ray *shadowRay, Point *vertices, Normal *vertNormals,
		RGB *vertColors, Triangle *triangles, TriangleLight *lights) {

	const Triangle &tri = triangles[objs->meshLightOffset + triIndex];

	const float area = lights[triIndex].area;

	Point samplePoint;
	float b0, b1, b2;

	tri.Sample(vertices, u0, u1, &samplePoint, &b0, &b1, &b2);
	Normal sampleN = vertNormals[tri.v[0]]; // Light sources are supposed to be flat

	Vector wi = samplePoint - p;
	const float distanceSquared = wi.LengthSquared();
	const float distance = sqrtf(distanceSquared);
	wi /= distance;

	const float SampleNdotMinusWi = Dot(sampleN, -wi);
	const float NdotMinusWi = Dot(N, wi);
	if ((SampleNdotMinusWi <= 0.f) || (NdotMinusWi <= 0.f)) {
		*pdf = 0.f;
		return RGB(0.f, 0.f, 0.f);
	}

	*shadowRay = Ray(p, wi, RAY_EPSILON, distance - RAY_EPSILON);
	*pdf = distanceSquared / (SampleNdotMinusWi * NdotMinusWi * area);

	// Return interpolated color
	return tri.InterpolateColor(vertColors, b0, b1, b2);
}

__device__  __forceinline
float ComputeProbability(RGB& brdf, Vector& i_direction, Normal& shadeN) {

	// Russian Rulette
	return Min(1.f, brdf.filter() * AbsDot(shadeN, i_direction));

}

//__device__  inline
//void DistrurbeSample(prePath *p, Seed& s) {
//
//	p->screenX += getFloatRNG(s) / p->strat;
//	p->screenY += getFloatRNG(s) / p->strat;
//
//}
//__device__  inline
//void DistrurbeSample(Path *p) {
//
//	p->screenX += getFloatRNG(p->seed) / p->strat;
//	p->screenY += getFloatRNG(p->seed) / p->strat;
//
//}

#endif
