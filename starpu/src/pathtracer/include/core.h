/*
 * common.h
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <cuda.h>
#include <cuda_runtime.h>
#include <iostream>
#include <map>
#include <vector>
#include <sys/time.h>
#include <cstdlib>

#define MAX_DEVICES 5
#define MAX_ARCHITECTURES 2

#define LOG_LEVEL 0.1/** 0.1, 0.5, 1 ,2 , 3 */

#define LOG(X,Y) \
		if (X <= LOG_LEVEL) {Y} \

//enum RESOURCE_ID_e {
//	CPU0 = 0, GPU0 = 1, GPU1 = 2, GPU2 = 3, GPU3 = 4, NONE = 5
//};

//typedef enum RESOURCE_ID_e RESOURCE_ID;

using namespace std;
#ifdef TEST_IDLES
#define EXECUTIONS 1
#else
#define EXECUTIONS 1
#endif

#if defined(__CUDACC__)
#define __HD__ 			__device__
#define __H_D__			__host__ __device__
#define __noinline 		__noinline__
#define __forceinline 	__forceinline__

#else
#define __HD__
#define __H_D__
#define __noinline
#define __forceinline 	__inline__ __attribute__((__always_inline__))

#endif

enum RESOURCE_TYPE {
	CPU = 100, GPU = 101
}; /* will allow a more specific type definition e.g GPU_fermi */
enum OPERATION_T {
	KILL = 102,
	FLUSH = 103,
	RETRIEVE_TO_HOST = 104,
	COPY_TO_DEVICE = 105,
	CHECK = 106,
	CHECK_AND_WAIT = 107,
	RETRIEVE_TO_HOST_BLOCKING = 108,
	FREE_CHUNK = 109,
	RETRIEVE_TO_HOST_AND_FREE = 12,
	DISPATCH_TASK = 121,
	JOB_FINISHED = 124,
	TASKS_FINISHED = 126,
	JOB_STARTED = 125
}; /* will allow a more specific type definition e.g GPU_fermi */
typedef enum MSI {
	M = 110, S = 111, I = 113, NA = 114, SaP = 115, Q = 116
} MSI_T;

//typedef unsigned int uint;

typedef enum {
	R = 116, RW = 117, W = 129
} PERMISSION_T;

enum TASK_TYPE {
	REGULAR = 118, IRREGULAR = 119
};

enum SCH_T {
	RR_DUM = 122, WRR_DUM = 123, DD_DUM = 127, DD_DYN = 128, WRAND_DUM = 130

};

typedef struct s_point3D {

	float x;
	float y;
	float z;

} Point3D;

typedef float* FLOAT_ARRAY;
typedef int* INT_ARRAY;
typedef char* byte_address;
typedef void* address;
typedef char byte;

extern unsigned int taskID_incrementor;
extern int tasks_to_execute;
//extern RESOURCE_ID hostID;
extern pthread_mutex_t tasks_to_execute_mutex;

int add_tasks_to_execute(int v);
const char* get_enum_string(int _enum);
//void config_resource(RESOURCE_ID res,bool idle = false);
void initDeviceConfig(int dev_config);
void* PERFORM_alloc(size_t size);
void PERFORM_free(void* p);

#endif /* COMMON_H_ */
