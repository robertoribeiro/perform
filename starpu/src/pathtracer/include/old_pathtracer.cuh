/*
 * old_pathtracer.cuh
 *
 *  Created on: Mar 2, 2012
 *      Author: rr
 */

#ifndef OLD_PATHTRACER_CUH_
#define OLD_PATHTRACER_CUH_



__device__
__forceinline
bool TriangleIntersect(Triangle* tri, const Ray &ray, const Point *verts, RayHit *rayHit) {

	const Point &p0 = verts[tri->v[0]];
	const Point &p1 = verts[tri->v[1]];
	const Point &p2 = verts[tri->v[2]];

	const Vector e1 = p1 - p0;
	const Vector e2 = p2 - p0;
	const Vector s1 = Cross(ray.d, e2);

	const float divisor = Dot(s1, e1);
	if (divisor == 0.f)
		return false;

	const float invDivisor = 1.f / divisor;

	// Compute first barycentric coordinate
	const Vector d = ray.o - p0;
	const float b1 = Dot(d, s1) * invDivisor;
	if (b1 < 0.f)
		return false;

	// Compute second barycentric coordinate
	const Vector s2 = Cross(d, e1);
	const float b2 = Dot(ray.d, s2) * invDivisor;
	if (b2 < 0.f)
		return false;

	const float b0 = 1.f - b1 - b2;
	if (b0 < 0.f)
		return false;

	// Compute _t_ to intersection point
	const float t = Dot(e2, s2) * invDivisor;
	if (t < ray.mint || t > ray.maxt)
		return false;

	rayHit->t = t;
	rayHit->b1 = b1;
	rayHit->b2 = b2;

	return true;
}

__device__
__forceinline
bool BBox_IntersectP(BBox bbox, const Ray &ray, float *hitt0, float *hitt1) {

	float t0 = ray.mint, t1 = ray.maxt;
	for (int i = 0; i < 3; ++i) {
		// Update interval for _i_th bounding box slab
		const float invRayDir = 1.f / ray.d[i];
		float tNear = (bbox.pMin[i] - ray.o[i]) * invRayDir;
		float tFar = (bbox.pMax[i] - ray.o[i]) * invRayDir;
		// Update parametric interval from slab intersection $t$s
		if (tNear > tFar)
			swap(&tNear, &tFar);
		t0 = tNear > t0 ? tNear : t0;
		t1 = tFar < t1 ? tFar : t1;
		if (t0 > t1)
			return false;
	}
	if (hitt0)
		*hitt0 = t0;
	if (hitt1)
		*hitt1 = t1;
	return true;
}

__device__
__forceinline
bool Intersect(const Ray &ray, RayHit& rayHit, BVHAccelArrayNode* bvhTree, Triangle *triangles,
		Point *vertices) {

	rayHit.t = INFINITY;
	rayHit.index = 0xffffffffu;
	unsigned int currentNode = 0; // Root Node
	unsigned int stopNode = bvhTree[0].skipIndex; // Non-existent
	bool hit = false;
	RayHit triangleHit;

	while (currentNode < stopNode) {
		if (BBox_IntersectP(bvhTree[currentNode].bbox, ray, NULL, NULL)) {
			if (bvhTree[currentNode].primitive != 0xffffffffu) {
				//float tt, b1, b2;
				Triangle t = triangles[bvhTree[currentNode].primitive];
				if (t.v[0] == 0 && t.v[1] == 0)
					printf("-------- error\n");
				else {
					if (TriangleIntersect(&t, ray, vertices, &triangleHit)) {
						hit = true; // Continue testing for closer intersections
						if (triangleHit.t < rayHit.t) {
							rayHit.t = triangleHit.t;
							rayHit.b1 = triangleHit.b1;
							rayHit.b2 = triangleHit.b2;
							rayHit.index = bvhTree[currentNode].primitive;
						}
					}
				}
			}

			currentNode++;
		} else
			currentNode = bvhTree[currentNode].skipIndex;
	}

	return hit;
}


#endif /* OLD_PATHTRACER_CUH_ */
