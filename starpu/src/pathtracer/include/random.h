#ifndef _RANDOM_H_
#define _RANDOM_H_

//#include <optix_math.h>

// Generate random float in [0, 1)
__host__ __device__ __inline__ unsigned int lcg(unsigned int &prev) {
	const uint LCG_A = 1664525u;
	const uint LCG_C = 1013904223u;
	prev = (LCG_A * prev + LCG_C);
	return prev & 0x00FFFFFF;
}

__host__ __device__ __inline__ unsigned int lcg2(unsigned int &prev) {
	prev = (prev * 8121 + 28411) % 134456;
	return prev;
}

__host__ __device__ __inline__ float getFloatRNG(Seed& prev) {
	return ((float) lcg(prev) / (float) 0x01000000);
}

// Multiply with carry
__host__ __inline__ uint mwc() {
	static unsigned long long r[4];
	static unsigned long long carry;
	static bool init = false;

	if (!init) {
		init = true;
		unsigned int seed = 7654321u, seed0, seed1, seed2, seed3;
		r[0] = seed0 = lcg2(seed);
		r[1] = seed1 = lcg2(seed0);
		r[2] = seed2 = lcg2(seed1);
		r[3] = seed3 = lcg2(seed2);
		carry = lcg2(seed3);
	}

	unsigned long long sum = 2111111111ull * r[3] + 1492ull * r[2] + 1776ull * r[1] + 5115ull
			* r[0] + 1ull * carry;

	r[3] = r[2];
	r[2] = r[1];
	r[1] = r[0];
	r[0] = static_cast<unsigned int> (sum); // lower half
	carry = static_cast<unsigned int> (sum >> 32); // upper half
	return static_cast<unsigned int> (r[0]);
}


#endif
