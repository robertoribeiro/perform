#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <cmath>
#include "perform_utils.h"
#include "core.h"


using namespace std;

double clocks_to_sec(double clocks) {
	return clocks * (0.00000000253);
}

double StandardDeviation(int* list,int number_of_items){



	// find the mean (average) of the data
	float xbar = 0;
	for (int i = 0; i < number_of_items; i++)
		xbar = xbar + list[i];

	xbar = xbar / number_of_items;

	// find the standard deviation
	float numerator = 0;
	float denominator = number_of_items;

	for (int i = 1; i < number_of_items; i++)
		numerator = numerator + pow((list[i] - xbar), 2);

	float standard_deviation = sqrt (numerator/denominator);

	return standard_deviation;
}

double get_random_float_zero_to(int max) {
	return rand() / (double) RAND_MAX;
}

double getGEMMFlops(double N, double time) {
	double n = (double) N;
	double mflops = (2.0 * (n * 0.01) * (n * 0.01) * (n * 0.01)) / time;
	return mflops;
}



void printMatrixFloat(float *A, int rows, int cols) {
	printf("\n\n");
	int ii, jj;
	for (ii = 0; ii < rows; ii++) {
		for (jj = 0; jj < cols; jj++)
			cout << A[ii * cols + jj] << " ";
		printf("\n");
	}
}




void initMatrixRandom(float *A, const int rows, const int cols) {

	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			A[ii * cols + jj] = (rand() % 200) - 100;
}



void setMatrixToZero(char *A, const int rows, const int cols) {
	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			A[ii * cols + jj] = '-';
}

void printMatrixChar(char *A, int rows, int cols) {
	printf("\n\n");
	int ii, jj;
	for (ii = 0; ii < rows; ii++) {
		for (jj = 0; jj < cols; jj++)
			printf("%c ", A[ii * cols + jj]);
		printf("\n");
	}
}

