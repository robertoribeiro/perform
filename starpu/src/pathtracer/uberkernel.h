/*
 * uberkernel.h
 *
 *  Created on: Jun 14, 2011
 *      Author: rr
 */
#ifndef UBERKERNEL_H
#define UBERKERNEL_H

#define PATHTRACER

void k_CUDA_wrapper_uberkernel(Task* t,ubkTask* task_prms, int child_task_count);


#endif /* UBERKERNEL_H_ */
