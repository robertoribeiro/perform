// OpenGL Graphics includes
#include <GL/glew.h>
#include <GL/freeglut.h>

// CUDA utilities and system includes
#include <cutil_inline.h>    // includes cuda.h and cuda_runtime_api.h
#include <cutil_gl_inline.h> // includes cuda_gl_interop.h// includes cuda_gl_interop.h
#include <rendercheck_gl.h>
#include <vector_types.h>

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <boost/math/distributions/negative_binomial.hpp>


#define MAX_EPSILON_ERROR 10.0f
#define THRESHOLD		  0.30f
#define REFRESH_DELAY	  10 

////////////////////////////////////////////////////////////////////////////////
// constants
const unsigned int window_width = 1024;
const unsigned int window_height = 768;

unsigned int nbds;

// vbo variables
GLuint vbo;
struct cudaGraphicsResource *cuda_vbo_resource;
void *d_vbo_buffer = NULL;

float g_fAnim = 0.0;

// mouse controls
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = 0.0, rotate_y = 0.0;
float translate_z = -3.0;

unsigned int timer = 0;

// texture params
int useTextures = 1;
GLuint tex[1];
char * texFile = "particle.bmp";
int textEnabled = 0;
bool cam_local=true; // switch between camera local to agent and high camera
// Auto-Verification Code
const int frameCheckNumber = 4;
int fpsCount = 0; // FPS count for averaging
int fpsLimit = 1; // FPS limit for sampling
unsigned int frameCount = 0;

// CheckFBO/BackBuffer class objects
//CheckRender       *g_CheckRender = NULL;

#define MAX(a,b) ((a > b) ? a : b)

////////////////////////////////////////////////////////////////////////////////
// CUDA methods

extern "C" void setupNBODIES(int argc, char *argv[]);

extern "C" void cleanNBODIES();

//extern "C" void launch_kernel(float4* pos);

extern "C" float getFlops();

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
int start(int argc, char** argv);
void cleanup();

////////////////////////////////////////////////////////////////////////////////
// GL functionality
int initGL(int *argc, char** argv);
void createVBO(GLuint* vbo, const GLvoid * data);
void deleteVBO(GLuint* vbo, struct cudaGraphicsResource *vbo_res);
int LoadBitmap(char *filename);

// rendering callbacks
void display();
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void timerEvent(int value);

///////////////////////////////////////////////////////////////////////////////

// local camera position
GLdouble cam_pos[]={0.0, 1.2,0};
// local camera view direction
GLdouble cam_vd[]={0.0, 1.2, -1.0};
// camera orientation angle
float cam_alpha=0.0;

#define BH_FLOAT

#ifdef BH_FLOAT
typedef float BH_TYPE;
#else
typedef double BH_TYPE;
#endif

typedef struct s_Point3D {

	BH_TYPE x;
	BH_TYPE y;
	BH_TYPE z;

} Point3D;




void drawAxes(){
	//Render World axis
	glColor3f(0.0,0.0,0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(15.0,0.0,0.0);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,0.0,15.0);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,15.0,0.0);
	glEnd();
	glPushMatrix();

	//Z Axis
	glColor3f(1.0,.0,.0);
	glTranslatef(0,0,15);
	glutWireCone(.75,1.0,20.0,1.0);

	//X Axis
	glColor3f(.0,1.0,.0);
	glTranslatef(15,0,-15);
	glRotatef(90,0,1,0);
	glutWireCone(.75,1.0,20.0,1.0);

	glPopMatrix();
	glPushMatrix();

	//Y Axis
	glColor3f(.0,.0,1.0);
	glTranslatef(0,15,0);
	glRotatef(-90,1,0,0);
	glutWireCone(.75,1.0,20.0,1.0);
	glPopMatrix();

}

void moveCamara(float dir){
	// vector para onde a camera esta apontar
	float vecX = cam_vd[0] - cam_pos[0];
	float vecY = cam_vd[1] - cam_pos[1];
	float vecZ = cam_vd[2] - cam_pos[2];

	// Calcula o modulo do vector
	float normal = 1 /(float)sqrt(vecX * vecX + vecY * vecY + vecZ * vecZ);

	// Normaliza as direcçoes, para ter o tamanho do vector em cada eixo
	vecX *= normal;
	vecY *= normal;
	vecZ *= normal;

	// Multiplico o valor passado como direcçao pelo tamanho do vector no eixo X e no Z

	cam_pos[0] += vecX * dir;
	cam_pos[2] += vecZ * dir;

	//Aqui faco o mesmo mas com a direcçao para onde a camera esta a apontar
	cam_vd[0] += vecX * dir;
	cam_vd[2] += vecZ * dir;

}

void rotateCamara(float angle){
	// Calculo antecipadamente o Cosseno e o Seno do angulo
	float CossAng = (float)cos(angle);
	float SinAng = (float)sin(angle);

	// Velocidade em  torno de o qual angulo deve rotar
	float xSpeed = 0;
	float ySpeed = 0.5;
	float zSpeed = 0;

	// Pego o vector para onde a camera esta apontar
	float vecX = cam_vd[0] - cam_pos[0];
	float vecY = cam_vd[1] - cam_pos[1];
	float vecZ = cam_vd[2] - cam_pos[2];

	// Calcula o modulo do vector
	float normal = 1 /(float)sqrt(vecX * vecX + vecY * vecY + vecZ * vecZ);

	// Normaliza as direcçoes, para ter o tamanho do vector em cada eixo
	vecX *= normal;
	vecY *= normal;
	vecZ *= normal;

	// CALCULA O NOVO X
	float NewVecX = (CossAng + (1 - CossAng) * xSpeed) * vecX;
	NewVecX += ((1 - CossAng) * xSpeed * ySpeed - zSpeed * SinAng)* vecY;
	NewVecX += ((1 - CossAng) * xSpeed * zSpeed + ySpeed * SinAng) * vecZ;

	// CALCULA O NOVO Y
	float NewVecY = ((1 - CossAng) * xSpeed * ySpeed + zSpeed * SinAng) * vecX;
	NewVecY += (CossAng + (1 - CossAng) * ySpeed) * vecY;
	NewVecY += ((1 - CossAng) * ySpeed * zSpeed - xSpeed * SinAng) * vecZ;

	// CALCULA O NOVO Z
	float NewVecZ = ((1 - CossAng) * xSpeed * zSpeed - ySpeed * SinAng) * vecX;
	NewVecZ += ((1 - CossAng) * ySpeed * zSpeed + xSpeed * SinAng) * vecY;
	NewVecZ += (CossAng + (1 - CossAng) * zSpeed) * vecZ;

	//Adiciono a nova vista a antiga, corrigindo assim a visao da camera.
	cam_vd[0] = cam_pos[0] + NewVecX;
	cam_vd[1] = cam_pos[1] + NewVecY;
	cam_vd[2] = cam_pos[2] + NewVecZ;
}


void load_data_set(Point3D* particles, char *filename, int nbodies, int& timesteps,
		BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol) {
	BH_TYPE vx, vy, vz;
	register FILE *f;

	int f_nbodies;
	int f_timesteps;

	f = fopen(filename, "r+t");
	if (f == NULL) {
		fprintf(stderr, "file not found: %s\n", filename);
		exit(-1);
	}

	fscanf(f, "%d", &f_nbodies);
	fscanf(f, "%d", &f_timesteps);
	fscanf(f, "%f", &dtime);
	fscanf(f, "%f", &eps);
	fscanf(f, "%f", &tol);

	//particles = new particle[nbodies];

	if (f_nbodies < nbodies)
		nbodies = f_nbodies;

	BH_TYPE garbage;
	for (int i = 0; i < (nbodies); i++) {
		fscanf(f, "%fE", &garbage);
		fscanf(f, "%fE", &(particles[i].x));
		fscanf(f, "%fE", &(particles[i].y));
		fscanf(f, "%fE", &(particles[i].z));
		fscanf(f, "%fE", &garbage);
		fscanf(f, "%fE", &garbage);
		fscanf(f, "%fE", &garbage);

	}

	fclose(f);

}

void load_data_set2(Point3D* particles, char *filename, int nbodies, int& timesteps,
		BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol) {
	BH_TYPE vx, vy, vz;
	register FILE *f;

	int f_nbodies;
	int f_timesteps;

	f = fopen(filename, "r+t");
	if (f == NULL) {
		fprintf(stderr, "file not found: %s\n", filename);
		exit(-1);
	}

	fscanf(f, "%d", &f_nbodies);
	fscanf(f, "%d", &f_timesteps);
	fscanf(f, "%f", &dtime);
	fscanf(f, "%f", &eps);
	fscanf(f, "%f", &tol);
	//particles = new particle[nbodies];

	if (f_nbodies < nbodies)
		printf("Not enough particles\n");

	BH_TYPE garbage;


	for (int i = 0; i < (nbodies); i++) {

		fscanf(f, "%fE", &(particles[i].x));
		fscanf(f, "%fE", &(particles[i].y));
		fscanf(f, "%fE", &(particles[i].z));
		fscanf(f, "%fE", &garbage);

	}


	fclose(f);

}



float GaussNum(){


	double GaussNum = 0.0;
	int NumInSum = 100;
	for(int i = 0; i < NumInSum; i++)
	{
		GaussNum += ((double)rand()/(double)RAND_MAX - 0.5);
	}

	GaussNum = GaussNum*sqrt((double)12/(double)NumInSum);

	return GaussNum;
}



void Gauss(Point3D* particles,int nbds){

	float maxr=0.0;

	for (int kk =0; kk < nbds; kk++){

		float x = GaussNum();
		float y = GaussNum();
		float z = GaussNum();

		float raio = sqrt(x*x+y*y+z*z);


		float c = log(sqrt(nbds));

//		particles[kk].x= x * pow(raio,c);
//		particles[kk].y= y * pow(raio,c);
//		particles[kk].z= z * pow(raio,c);

		particles[kk].x= (x * pow(c,raio));
		particles[kk].y= (y * pow(c,raio));
		particles[kk].z= (z * pow(c,raio));

		raio = sqrt(particles[kk].x*particles[kk].x+particles[kk].y*particles[kk].y+particles[kk].z*particles[kk].z);

		if (maxr < raio) maxr = raio;

	}

	printf("Raio maximo %.10f\n",maxr);

}



void ReadGaussPointSet(Point3D* particles,int nbds){

	fprintf(stderr,"Using Gauss distribution...\n");
	fprintf(stderr,"Generation %d points...",nbds);
	float maxr=0.0;

	FILE* f = fopen("/home/rr/work/perform/PERFORM/data/1MGaussPopints.txt", "r");
	if (f == NULL) {
			fprintf(stderr, "file not found\n");
			exit(-1);
		}

	float x,y,z;


	for (int kk =0; kk < nbds; kk++){

		fscanf(f, "%f", &(x));
		fscanf(f, "%f", &(y));
		fscanf(f, "%f", &(z));

		float raio = sqrt(x*x+y*y+z*z);

		float c = log(sqrt(nbds));

		particles[kk].x= x * nbds;
		particles[kk].y= y * nbds;
		particles[kk].z= z * nbds;

		//particles[kk].x= x * pow(raio,c);
		//particles[kk].y= y * pow(raio,c);
		//particles[kk].z= z * pow(raio,c);

//		particles[kk].x= x * pow(c,raio);
//		particles[kk].y= y * pow(c,raio);
//		particles[kk].z= z * pow(c,raio);

		//raio = sqrt(particles[kk].x*particles[kk].x+particles[kk].y*particles[kk].y+particles[kk].z*particles[kk].z);

		//if (maxr < raio) maxr = raio;

	}

	fprintf(stderr,"done!\n");


	//printf("Raio maximo %.10f\n",maxr);

}

void balanced(Point3D* points,int nbds) {


	int n = nbds, i = 0;

	float r = 162600; // gauss 512

	for (; i < n; i++) {
		points[i].x = rand() * (r * 2) / RAND_MAX - r;// + 1.0/double(rand()%1000+2); //the 1.0/ unnecessary?
		points[i].y = rand() * (r * 2) / RAND_MAX - r;// + 1.0/double(rand()%1000+2);
		points[i].z = rand() * (r * 2) / RAND_MAX - r;// + 1.0/double(rand()%1000+2);
	}
}

int main(int argc, char** argv) {

	srand(time(0));

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	nbds = 1024*512;

	int timesteps;

	//char* filename = "/home/rr/work/perform/PERFORM/data/BarnesHut.inC";
	//char* filename = "/home/rr/work/perform/PERFORM/data/samecorner.txt";


	Point3D* particles = new Point3D[nbds];

	//load_data_set2(particles, filename, nbds, timesteps, dtime, eps, tol);

	ReadGaussPointSet(particles,nbds);
	//balanced(particles,nbds);


	if (0 == initGL(&argc, argv)) {
		return 0;
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	// create VBO
	createVBO(&vbo, (const GLvoid*) particles);

    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);


	glutMainLoop();

}

int initGL(int *argc, char **argv) {

	glutInit(argc, argv);
	//	glutInitDisplayMode (GLUT_SINGLE);

	//glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH /*| GLUT_ALPHA*/);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("Cuda GL Interop (VBO)");
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMotionFunc(motion);
	glutTimerFunc(REFRESH_DELAY, timerEvent, 0);

	// initialize necessary OpenGL extensions
	glewInit();

	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glDisable(GL_DEPTH_TEST);

	// viewport
	glViewport(0, 0, window_width, window_height);

	// projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat) window_width / (GLfloat) window_height, 0.1, 10000000.0);

	return 1;
}



////////////////////////////////////////////////////////////////////////////////
//! Create VBO
////////////////////////////////////////////////////////////////////////////////
void createVBO(GLuint* vbo, const GLvoid* data) {

	glGenBuffers(1, vbo);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo);

	// initialize buffer object
	unsigned int size = nbds * 3 * sizeof(float);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


}

////////////////////////////////////////////////////////////////////////////////
//! Delete VBO
////////////////////////////////////////////////////////////////////////////////
void deleteVBO(GLuint* vbo, struct cudaGraphicsResource *vbo_res) {
	if (vbo) {
		// unregister this buffer object with CUDA
		//cudaGraphicsUnregisterResource(vbo_res);

		glBindBuffer(1, *vbo);
		glDeleteBuffers(1, vbo);

		*vbo = 0;
	} else {
		//	cudaFree(d_vbo_buffer);
		d_vbo_buffer = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////
//! Display callback
////////////////////////////////////////////////////////////////////////////////
void display() {

	//cutStartTimer(timer);

	GLfloat sizes[2];
	float quadratic[] = { 0.003f, 0.0f, 0.003f };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	if(!cam_local){
		gluLookAt(75,50,75,0,0,0,0,1.0,0);
	}
	else gluLookAt(cam_pos[0],cam_pos[1],cam_pos[2],cam_vd[0],cam_vd[1],cam_vd[2],0,1.0,0);


	glTranslatef(0.0, 0.0, translate_z);
	glRotatef(rotate_x, 1.0, 0.0, 0.0);
	glRotatef(rotate_y, 0.0, 1.0, 0.0);


	drawAxes();

	// render from the vbo
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexPointer(3, GL_FLOAT, 0, 0);

	glEnableClientState(GL_VERTEX_ARRAY);

	if (textEnabled) {
		//********************************
		glEnable(GL_TEXTURE_2D);
		glGetFloatv(GL_ALIASED_POINT_SIZE_RANGE, sizes);
		glEnable(GL_POINT_SPRITE_ARB);
		glPointParameterfARB(GL_POINT_SIZE_MAX_ARB, sizes[1]);
		glPointParameterfARB(GL_POINT_SIZE_MIN_ARB, sizes[0]);
		glPointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB, quadratic);
		glTexEnvi(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_COLOR);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glDepthMask(GL_FALSE);

		//******************************************
	} else {
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_POINT_SPRITE_ARB);
		glPointParameterfARB(GL_POINT_SIZE_MAX_ARB, 1);
		glPointParameterfARB(GL_POINT_SIZE_MIN_ARB, 1);
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	glColor3f(0.0f, 0.0f, 0.0f);
	glDrawArrays(GL_POINTS, 0, nbds);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_POINT_SPRITE_ARB);

	glutSwapBuffers();

	g_fAnim += 0.01f;

	//cutStopTimer(timer);
	//computeFPS();
}

void timerEvent(int value) {
	glutPostRedisplay();
	glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
}

void cleanup() {
	deleteVBO(&vbo, cuda_vbo_resource);

}


void keyboard (unsigned char key, int x, int y) {
	switch (key) {
	case 27:   // ESCape
		exit (0);
		break;
	case 'c':  if(cam_local)cam_local=false;else cam_local=true;
	break;
	case 'w':	moveCamara(1);
	break;
	case 'r':	moveCamara(200);
	break;
	case 'f':	moveCamara(-2000);
	break;
	case 's':	moveCamara(-200);
	break;
	case 'a':	rotateCamara(0.1f);
	break;
	case 'd':	rotateCamara(-0.1f);
	break;
	case 'z':	nbds/=2;
	break;
	default:
		break;
	}
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////
//! Mouse event handlers
////////////////////////////////////////////////////////////////////////////////
void mouse(int button, int state, int x, int y) {
	if (state == GLUT_DOWN) {
		mouse_buttons |= 1 << button;
	} else if (state == GLUT_UP) {
		mouse_buttons = 0;
	}

	mouse_old_x = x;
	mouse_old_y = y;
}

void motion(int x, int y) {
	float dx, dy;
	dx = (float) (x - mouse_old_x);
	dy = (float) (y - mouse_old_y);

	if (mouse_buttons & 1) {
		rotate_x += dy * 0.2f;
		rotate_y += dx * 0.2f;
	} else if (mouse_buttons & 4) {
		translate_z += dy * 0.01f;
	}

	mouse_old_x = x;
	mouse_old_y = y;
}

