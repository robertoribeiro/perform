#ifndef BARNESHUTCOMM_H_
#define BARNESHUTCOMM_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>


using namespace std;

#define BH_FLOAT

#ifdef BH_FLOAT
typedef float BH_TYPE;
#else
typedef double BH_TYPE;
#endif



typedef struct s_point3D {

	float x;
	float y;
	float z;

} Point3D;

typedef float* FLOAT_ARRAY;


typedef struct BH_TYPE4_struct {
	BH_TYPE x;
	BH_TYPE y;
	BH_TYPE z;
	BH_TYPE mass;

	int id;

} mass_position;

typedef struct s_particle {

	mass_position mass_pos;

	BH_TYPE velx;
	BH_TYPE vely;
	BH_TYPE velz;
	BH_TYPE accx;
	BH_TYPE accy;
	BH_TYPE accz;
	BH_TYPE t_accx;
		BH_TYPE t_accy;
		BH_TYPE t_accz;
	int lock;

} particle;

typedef enum {
	CELL, PARTICLE, NILL
} TYPE_T;

typedef struct cell_s {
	mass_position mass_pos;
	TYPE_T child_types[8];
	int child_indexes[8];
	int parent_cell_index;
} cell;

void ReadGaussPointSet(Point3D* particles,int nbds);

void PointsToParticleSystem(Point3D* points, int nbodies, FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, FLOAT_ARRAY mass);

void PointsToParticleSystem(Point3D* points, int nbodies, particle* particles);


void PrintBH_TYPE(BH_TYPE d);

#endif
