#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>
#include <string.h>

#include "BarnesHut_common.h"

using namespace std;


void ReadGaussPointSet(Point3D* particles,int nbds){

	fprintf(stderr,"Using Gauss distribution...\n");
	fprintf(stderr,"Generation %d points...",nbds);
	float maxr=0.0;

	FILE* f = fopen("/home/rr/work/perform/PERFORM/data/1MGaussPopints.txt", "r");
	if (f == NULL) {
			fprintf(stderr, "file not found\n");
			exit(-1);
		}

	float x,y,z;


	for (int kk =0; kk < nbds; kk++){

		fscanf(f, "%f", &(x));
		fscanf(f, "%f", &(y));
		fscanf(f, "%f", &(z));

		float raio = sqrt(x*x+y*y+z*z);

		float c = log(sqrt(nbds));
		//float c = log(nbds/1024);

		//varia muito no centro
		particles[kk].x= x * pow(raio,c);
		particles[kk].y= y * pow(raio,c);
		particles[kk].z= z * pow(raio,c);

//		particles[kk].x= x * pow(c,raio);
//		particles[kk].y= y * pow(c,raio);
//		particles[kk].z= z * pow(c,raio);

		//raio = sqrt(particles[kk].x*particles[kk].x+particles[kk].y*particles[kk].y+particles[kk].z*particles[kk].z);

		//if (maxr < raio) maxr = raio;

	}

	fprintf(stderr,"done!\n");


	//printf("Raio maximo %.10f\n",maxr);

}

void PointsToParticleSystem(Point3D* points, int nbodies, FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, FLOAT_ARRAY mass) {

	for (int i = 0; i < (nbodies); i++) {

		posx[i]=points[i].x;
		posy[i]=points[i].y;
		posz[i]=points[i].z;

		mass[i] = 1;
	}

}

void PointsToParticleSystem(Point3D* points, int nbodies, particle* particles) {

	for (int i = 0; i < (nbodies); i++) {

		particles[i].mass_pos.x = points[i].x;
		particles[i].mass_pos.y = points[i].y;
		particles[i].mass_pos.z = points[i].z;

		particles[i].mass_pos.mass=1;
		particles[i].velx = 0.0f;
		particles[i].vely = 0.0f;
		particles[i].velz = 0.0f;
		particles[i].mass_pos.id = i;
		particles[i].lock = 0;

		particles[i].accx = 0.0;
		particles[i].accy = 0.0;
		particles[i].accz = 0.0;
	}

}
