#!/bin/bash
set -x

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib
export LD_LIBRARY_PATH;

nvcc -arch=sm_20 -O3 -m32  -Xptxas -dlcm=cg BurtscherGCG.2.2.cu -o Burtscher2.0
chmod 777 Burtscher2.0
