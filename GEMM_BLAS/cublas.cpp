/*
 * atlas.cpp
 *
 *  Created on: Aug 23, 2011
 *      Author: rr
 */

#include <iostream>

#include "dgemm.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>

double job_GEMM(int argc, char *argv[]) {

	cublasStatus_t status;
	double* h_A;
	double* h_B;
	double* h_C;
	double* d_A = 0;
	double* d_B = 0;
	double* d_C = 0;
	double alpha = 1.0f;
	double beta = 0.0f;

	int N = atoi(argv[1]);

	int n2 = N * N;


	cudaHostAlloc(&h_A, sizeof(double) * N * N, cudaHostAllocDefault);
	cudaHostAlloc(&h_B, sizeof(double) * N * N, cudaHostAllocDefault);
	cudaHostAlloc(&h_C, sizeof(double) * N * N, cudaHostAllocDefault);

	//h_A = (double*) (malloc(sizeof(double) * N * N));
	//h_B = (double*) (malloc(sizeof(double) * N * N));
	//h_C = (double*) (malloc(sizeof(double) * N * N));

	initMatrix(h_A, N, N, 5);
	initMatrixIdentity(h_B, N, N);
	initMatrix(h_C, N, N, 0);

	cublasHandle_t handle;

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();


	cudaSetDevice(0);


	status = cublasCreate(&handle);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! CUBLAS initialization error\n");
		return 0;
	}
	timer->start();

	/* Allocate device memory for the matrices */
	if (cudaMalloc((void**) &d_A, n2 * sizeof(d_A[0])) != cudaSuccess) {
		fprintf(stderr, "!!!! device memory allocation error (allocate A)\n");
		return 0;
	}
	if (cudaMalloc((void**) &d_B, n2 * sizeof(d_B[0])) != cudaSuccess) {
		fprintf(stderr, "!!!! device memory allocation error (allocate B)\n");
		return 0;
	}
	if (cudaMalloc((void**) &d_C, n2 * sizeof(d_C[0])) != cudaSuccess) {
		fprintf(stderr, "!!!! device memory allocation error (allocate C)\n");
		return 0;
	}


	/* Initialize the device matrices with the host matrices */
	status = cublasSetVector(n2, sizeof(h_A[0]), h_A, 1, d_A, 1);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! device access error (write A)\n");
		return 0;
	}
	status = cublasSetVector(n2, sizeof(h_B[0]), h_B, 1, d_B, 1);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! device access error (write B)\n");
		return 0;
	}
	status = cublasSetVector(n2, sizeof(h_C[0]), h_C, 1, d_C, 1);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! device access error (write C)\n");
		return 0;
	}


	/* Performs operation using cublas */
	status = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N, &alpha, d_A, N, d_B, N, &beta, d_C, N);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! kernel execution error.\n");
		return 0;
	}

	/* Allocate host memory for reading back the result from device memory */
	h_C = (double*) malloc(n2 * sizeof(h_C[0]));
	if (h_C == 0) {
		fprintf(stderr, "!!!! host memory allocation error (C)\n");
		return 0;
	}

	/* Read the result back */
	status = cublasGetVector(n2, sizeof(h_C[0]), d_C, 1, h_C, 1);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! device access error (read C)\n");
		return 0;
	}

	timer->stop();

	fprintf(stdout, "%s\t", timer->print());

	//checkMatrices(h_C, h_A, N, N);

	/* Memory clean up */
	cudaFree(h_A);
	cudaFree(h_B);
	cudaFree(h_C);

	if (cudaFree(d_A) != cudaSuccess) {
		fprintf(stderr, "!!!! memory free error (A)\n");
		return 0;
	}
	if (cudaFree(d_B) != cudaSuccess) {
		fprintf(stderr, "!!!! memory free error (B)\n");
		return 0;
	}
	if (cudaFree(d_C) != cudaSuccess) {
		fprintf(stderr, "!!!! memory free error (C)\n");
		return 0;
	}

	/* Shutdown */
	status = cublasDestroy(handle);
	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! shutdown error (A)\n");
		return EXIT_FAILURE;
	}

	return timer->duration;
}
