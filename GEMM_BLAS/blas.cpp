/*
 * atlas.cpp
 *
 *  Created on: Aug 23, 2011
 *      Author: rr
 */

#include <iostream>

#include "dgemm.h"
#include "mkl.h"

double job_GEMM(int argc, char *argv[]) {

	int N = atoi(argv[1]);
	PERFORM_acc_timer* timer = new PERFORM_acc_timer();


	double *matrixA = (double*) (malloc(sizeof(double) * N * N));
	double *matrixB = (double*) (malloc(sizeof(double) * N * N));
	double *matrixC = (double*) (malloc(sizeof(double) * N * N));

	initMatrix(matrixA, N, N, 5);
	initMatrixIdentity(matrixB, N, N);
	initMatrix(matrixC, N, N, 0);

	timer->start();


	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, matrixA, N, matrixB, N,
			1.0, matrixC, N);

	timer->stop();

	fprintf(stdout, "%s\t", timer->print());

	//checkMatrices(matrixC, matrixA, N, N);


	return timer->duration;

}

