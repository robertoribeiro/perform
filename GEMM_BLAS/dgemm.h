/*
 * dgemm.h
 *
 *  Created on: Apr 6, 2011
 *      Author: rr
 */

#ifndef DGEMM_H_
#define DGEMM_H_

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <cmath>
#include <sstream>
using namespace std;

class PERFORM_acc_timer {
public:

	timeval t;
	timeval global_start;

	double duration;

	PERFORM_acc_timer() {
		duration = 0;
	}

	~PERFORM_acc_timer() {
	}

	void start() {
		gettimeofday(&t, NULL);
	}

	void stop() {
		timeval end;
		gettimeofday(&end, NULL);
		duration += get_interval(t, end) / 1000.0; //seconds
	}

	/**
	 * returns ms
	 */
	static double get_interval(timeval start, timeval end) {
		double v = (double) (end.tv_sec * 1000.0 + end.tv_usec / 1000.0 - start.tv_sec * 1000.0
				- start.tv_usec / 1000.0 + 0.5);
		return v;

	}

	char* print() {
		char* buffer = new char[32];
		snprintf(buffer, 32, "%g", duration);
		return buffer;
	}

};

static void initMatrix(double *A, const int rows, const int cols, double value = 0) {
	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			A[ii * cols + jj] = value;
}
static void checkMatrices(double *A, double *B, const int rows, const int cols) {

	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			if (A[ii * cols + jj] != B[ii * cols + jj]) {
				cout << "\nMatrices are different!" << endl;
				return;
			}
	cout << "\nMatrices are equal!" << endl;
}

static void initMatrixRandom(float *A, const int rows, const int cols) {
	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			A[ii * cols + jj] = (rand() % 200) - 100;
}
static void initMatrixIdentity(double *A, const int rows, const int cols) {
	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			if (ii == jj)
				A[ii * cols + jj] = 1;
			else
				A[ii * cols + jj] = 0;
}

#endif /* DGEMM_H_ */
