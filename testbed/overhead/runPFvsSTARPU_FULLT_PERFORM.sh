#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work/perform"
results_folder=$work_folder"/testbed/overhead/PERFORM"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt


sleeptime=1

BIN="$work_folder/PERFORM/overhead_RELEASE/PERFORM"

# save and change IFS
OLDIFS=$IFS
IFS=: 


ntasks="1:4:64:128:256:512:1024"
#dev_configs="0:1:2:3:4:5:6"
dev_configs="0:1:2:3:4"
mapper="3"



it=0
for ntask in $ntasks; do
	echo -e -n $ntask"\t" >> $out
		for dev_config in $dev_configs; do
			
			ndevs=0
			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec1=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec2=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec3=`echo $x | bc`
			sleep $sleeptime
			
			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec4=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec5=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec6=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec7=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec8=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec9=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec10=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec11=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec12=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec13=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec14=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config 0 $ndevs $ntask")
			exec15=`echo $x | bc`
			sleep $sleeptime

			it=$((it+1))

echo -e "$exec1, $exec2, $exec3, $exec4, $exec5, $exec6, $exec7, $exec8, $exec9, $exec10, $exec11, $exec12, $exec13, $exec14, $exec15"
echo -e "$exec1\n$exec2\n$exec3\n$exec4\n$exec5\n$exec6\n$exec7\n$exec8\n$exec9\n$exec10\n$exec11\n$exec12\n$exec13\n$exec14\n$exec15" | sort -n | head -5 | awk 'NR == 1 { max=$1; min=$1; total=0 }   { if ($1>max) max=$1; if ($1<min) min=$1; total+=$1; count+=1}   END {printf "%.3f ", total/count}' >> $out

		done
	echo -e >> $out 
	it=0
done


##for bin in $bins; do
# it=$((it+1))
#	mkdir $out_folder"/BIN"$it
#	cp $bin $out_folder"/BIN"$it
#done


#echo -n -e "SIZES\t" >> $log

##for size in $sizes; do
#	echo -n -e $size"\t" >> $log
#done

#echo -e "\n" >> $log

#itt=0
#for bin in ${binsp[@]}; do
#	itt=$((itt+1))
#	echo "BIN"$itt": "$bin >> $log
#	echo -e "\n" >> $log
#done

else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./runPFvsSTARPU_FULLT.sh $out_folder
