#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work/perform"
results_folder=$work_folder"/testbed/idle_overhead/PERFORM"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt


sleeptime=2

BIN="$work_folder/PERFORM/overhead_RELEASE/PERFORM"

# save and change IFS
OLDIFS=$IFS
IFS=: 


ntasks="64:128:256:512:1024"
#dev_configs="0:1:2:3:4:5:6"
dev_configs=$2
mapper="3"



it=0
for ntask in $ntasks; do
	echo -e -n $ntask"\t" >> $out
				
			ndevs=0
			x=$(sh -c "$BIN $mapper $dev_configs 0 $ndevs $ntask")
			exec1=`echo $x`
			sleep $sleeptime	

			echo -e "$exec1" >> $out
		

	it=0
done


##for bin in $bins; do
# it=$((it+1))
#	mkdir $out_folder"/BIN"$it
#	cp $bin $out_folder"/BIN"$it
#done


#echo -n -e "SIZES\t" >> $log

##for size in $sizes; do
#	echo -n -e $size"\t" >> $log
#done

#echo -e "\n" >> $log

#itt=0
#for bin in ${binsp[@]}; do
#	itt=$((itt+1))
#	echo "BIN"$itt": "$bin >> $log
#	echo -e "\n" >> $log
#done

else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./runPFvsSTARPU_FULLT.sh $out_folder
