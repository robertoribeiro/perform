#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work/perform"
results_folder=$work_folder"/testbed/GEMM/GEMM_idles"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

dice_value=$3
mapper=$2

#BIN1="$work_folder/PERFORM/Release_GEMM_PERFORM/PERFORM"
#BIN1P=" $scheduler 0 $dice_value"
BIN1="$work_folder/PERFORM/Release_GEMM_PERFORM/PERFORM"
BIN1P=" $mapper 1 $dice_value"
BIN2="$work_folder/PERFORM/Release_GEMM_PERFORM/PERFORM"
BIN2P=" $mapper 2 $dice_value"
BIN3="$work_folder/PERFORM/Release_GEMM_PERFORM/PERFORM"
BIN3P=" $mapper 3 $dice_value"

BIN4="$work_folder/PERFORM/Release_GEMM_PERFORM_OPT/PERFORM"
BIN4P=" $mapper 0 $dice_value"
BIN5="$work_folder/PERFORM/Release_GEMM_PERFORM_OPT/PERFORM"
BIN5P=" $mapper 1 $dice_value"
BIN6="$work_folder/PERFORM/Release_GEMM_PERFORM_OPT/PERFORM"
BIN6P=" $mapper 2 $dice_value"
BIN7="$work_folder/PERFORM/Release_GEMM_PERFORM_OPT/PERFORM"
BIN7P=" $mapper 3 $dice_value"

BIN8="$work_folder/GEMM_BLAS/BLAS/GEMM_BLAS"
BIN8P=""
BIN9="$work_folder/GEMM_BLAS/CUBLAS/GEMM_BLAS"
BIN9P=""

# save and change IFS
OLDIFS=$IFS
IFS=: 

declare -a bins=$BIN1:$BIN2:$BIN3:$BIN4:$BIN5:$BIN6:$BIN7:$BIN8::$BIN9
declare -a binsp=$BIN1$BIN1P:$BIN2$BIN2P:$BIN3$BIN3P:$BIN4$BIN4P:$BIN5$BIN5P:$BIN6$BIN6P:$BIN7$BIN7P:$BIN8$BIN8P:$BIN9$BIN9P


sizes="2048:4096:6144:8192:10240:12288"
#:14336"
#:16384:18432:20480:22528:24576:26624:28672:30720"

it=0

for size in $sizes; do
	echo -e -n $size"\t" >> $out
		#for bin in ${bins[@]}; do
		for bin in $binsp; do
			it=$((it+1))
			sh -c "$bin $size" >> $out 
		done
	echo -e >> $out 
	it=0
done


for bin in $bins; do
	it=$((it+1))
	mkdir $out_folder"/BIN"$it
	cp $bin $out_folder"/BIN"$it
done




echo -n -e "SIZES\t" >> $log

for size in $sizes; do
	echo -n -e $size"\t" >> $log
done

echo -e "\n" >> $log

itt=0
for bin in ${binsp[@]}; do
	itt=$((itt+1))
	echo "BIN"$itt": "$bin >> $log
	echo -e "\n" >> $log
done


else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./run.sh $out_folder
