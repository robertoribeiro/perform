#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work"
results_folder=$work_folder"/testbed/GEMM/GEMM_idles_paper_0.0"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

dice_value=16
dev_config=$2
sizes=$3


BIN5="$work_folder/perform/PERFORM/GEMM_OPT_RELEASE/PERFORM"
BIN6="$work_folder/perform/PERFORM/GEMM_OPT_RELEASE/PERFORM"
BIN7="$work_folder/perform/PERFORM/GEMM_OPT_RELEASE/PERFORM"
BIN8="$work_folder/perform/PERFORM/GEMM_OPT_RELEASE/PERFORM"

BIN5P=" 0 $dev_config $dice_value"
BIN6P=" 1 $dev_config $dice_value"
BIN7P=" 2 $dev_config $dice_value"
BIN8P=" 3 $dev_config $dice_value"

# save and change IFS
OLDIFS=$IFS
IFS=: 

declare -a bins=$BIN5:$BIN6:$BIN7:$BIN8
declare -a binsp=$BIN5$BIN5P:$BIN6$BIN6P:$BIN7$BIN7P:$BIN8$BIN8P


#sizes="4096"
#sizes="8192"



it=0

for size in $sizes; do
	echo -e -n $size"\t" >> $out
		#for bin in ${bins[@]}; do
		for bin in $binsp; do
			it=$((it+1))
			sh -c "$bin $size" >> $out 
			sleep 3	
		done
	echo -e >> $out 
	it=0
done


for bin in $bins; do
	it=$((it+1))
	mkdir $out_folder"/BIN"$it
	cp $bin $out_folder"/BIN"$it
done




echo -n -e "SIZES\t" >> $log

for size in $sizes; do
	echo -n -e $size"\t" >> $log
done

echo -e "\n" >> $log

itt=0
for bin in ${binsp[@]}; do
	itt=$((itt+1))
	echo "BIN"$itt": "$bin >> $log
	echo -e "\n" >> $log
done


echo -e "\n" >> $out
echo -e "\n" >> $out

else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./run_idles.sh $out_folder
