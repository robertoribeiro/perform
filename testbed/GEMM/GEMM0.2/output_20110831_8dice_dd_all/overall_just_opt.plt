#!/bin/bash


folder=`pwd`

font=Helvetica,17
font2=Helvetica,15

gnuplot << EOF
set title "DD map - SGEMM_OPT - dice8"  font "$font"

#set size 2,2
#set terminal svg size 520,440
#set term postscript eps enhanced
set terminal postscript eps enhanced solid  "$font"

#set terminal latex 
set output "overall_just_opt.eps"

set key outside
set key rmargin center

set xlabel "Matrix row size" font "$font"
set ylabel "MFLOPS" font "$font"
set xtics font "$font2"
set ytics font "$font2"
set log xy
set xtics (2048,4096,6144,8192,10240,12288,14336)
set format y "10^{%L}"

#set lmargin at screen 0.10
#set rmargin at screen 0.85
#set bmargin at screen 0.15
#set tmargin at screen 0.95

set pointsize 1.7

plot "$folder/results.txt" using 1:6 title 'PF\_OPT-1CPU' w linespoints, \
"$folder/results.txt" using 1:7 title 'PF\_OPT-1GPU' w linespoints, \
"$folder/results.txt" using 1:8 title 'PF\_OPT-2GPU' w linespoints, \
"$folder/results.txt" using 1:9 title 'PF\_OPT-1CPU+2GPU' w linespoints, \
"$folder/results.txt" using 1:10 title 'MKL-1CPU' w linespoints, \
"$folder/results.txt" using 1:11 title 'CUBLAS-1GPU' w linespoints



EOF
