#!/bin/bash


folder1="/home/rr/work/perform/testbed/GEMM/GEMM0.2/output_20110902_16dice_160CPU_1350GPU_all"
folder2="/home/rr/work/perform/testbed/GEMM/GEMM0.2/output_20110902_8dice_160CPU_1350GPU_all"
folder3="/home/rr/work/perform/testbed/GEMM/GEMM0.2/output_20110831_16dice_dd_all"
folder4="/home/rr/work/perform/testbed/GEMM/GEMM0.2/output_20110831_8dice_dd_all"

font=Helvetica,17
font2=Helvetica,15

gnuplot << EOF
set title 'SGEMM - PERFORM\_all 160CPU'  font "$font"

#set size 2,2
#set terminal svg size 520,440
#set term postscript eps enhanced
set terminal postscript eps enhanced solid  "$font"

#set terminal latex 
set output "overall 160CPU.eps"

set key outside
set key rmargin center

set xlabel "Matrix row size" font "$font"
set ylabel "MFLOPS" font "$font"
set xtics font "$font2"
set ytics font "$font2"
set log xy
set xtics (2048,4096,6144,8192,10240,12288,14336)
set format y "10^{%L}"

#set lmargin at screen 0.10
#set rmargin at screen 0.85
#set bmargin at screen 0.15
#set tmargin at screen 0.95

set pointsize 1.7

plot "$folder2/results.txt" using 1:4 title 'd8\_ST\_naive' w linespoints, \
"$folder2/results.txt" using 1:8 title 'd8\_ST\_OPT' w linespoints, \
"$folder1/results.txt" using 1:4 title 'd16\_ST\_naive' w linespoints, \
"$folder1/results.txt" using 1:8 title 'd16\_ST\_OPT' w linespoints, \
"$folder4/results.txt" using 1:5 title 'd8\_DD\_naive' w linespoints, \
"$folder4/results.txt" using 1:9 title 'd8\_DD\_OPT' w linespoints, \
"$folder3/results.txt" using 1:5 title 'd16\_DD\_naive' w linespoints, \
"$folder3/results.txt" using 1:9 title 'd16\_DD\_OPT' w linespoints



EOF
