#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work/perform"
results_folder=$work_folder"/testbed/PATHTRACER/DC-SHADOW"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

mapper=3
height=480
width=480
scene=1
max_depth=10
sleeptime=5

BIN="$work_folder/PERFORM/pathtracer_FullT_RELEASE/PERFORM"
#BIN="$work_folder/PERFORM/pathtracer_UBK_FineT_SIMD32_RELEASE/PERFORM"


# save and change IFS
OLDIFS=$IFS
IFS=: 

#spps="16:25:36:64:100:144:225:400"
spp="225"
#dev_configs="1:2:3:4:5:6"
dev_config="1"
dice_value="8"
#dice_values="2:4:6"
#ranks="250:700:1580"
rank="675"

sizes="shadow1.h:shadow2.h:shadow4.h:shadow8.h:shadow16.h:shadow32.h"


it=0
for size in $sizes; do
		echo -e -n $size"\t" >> $out
			it=$((it+1))
			cd "/home/rr/work/perform/PERFORM/src/applications/pathtracer/include/"
			cp $size "shadowX.h"


			cd "$work_folder/PERFORM/pathtracer_FullT_RELEASE"
#			cd "$work_folder/PERFORM/pathtracer_UBK_FineT_SIMD32_RELEASE"
			make clean
			make

			cd "$work_folder/testbed/PATHTRACER"

			x=$(sh -c "$BIN $mapper $dev_config $dice_value $height $width $spp $max_depth $scene $rank")
			exec1=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config $dice_value $height $width $spp $max_depth $scene $rank")
			exec2=`echo $x | bc`
			sleep $sleeptime

			x=$(sh -c "$BIN $mapper $dev_config $dice_value $height $width $spp $max_depth $scene $rank")
			exec3=`echo $x | bc`
			sleep $sleeptime
			
echo -e "$exec1, $exec2, $exec3, $exec4, $exec5, $exec6, $exec7, $exec8, $exec9, $exec10"
echo -e "$exec1\n$exec2\n$exec3\n$exec4\n$exec5\n$exec6\n$exec7\n$exec8\n$exec9\n$exec10" | awk 'NR == 1 { max=$1; min=$1; sum=0 }
   { if ($1>max) max=$1; if ($1<min) min=$1; }
   END {printf "%.2f ", max}' >> $out

	echo -e >> $out 
	it=0
done


else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./runShadow.sh $out_folder
