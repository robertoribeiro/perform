#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work"
results_folder=$work_folder"/testbed/PATHTRACER/PT0.2"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

mapper=0
dev=1
dice_value=0
height=512
width=512
spp=1
max_depth=10
scene=1

pre_p="$mapper $dev $dice_value $height $width"
pos_p="$max_depth $scene"

BIN1="$work_folder/perform/PERFORM/pathtracer_FullT_RELEASE/PERFORM"
BIN1P=" $pre_p"

BIN2="$work_folder/perform/PERFORM/pathtracer_UBK_FineT_SIMD32_RELEASE/PERFORM"
BIN2P=" $pre_p"

# save and change IFS
OLDIFS=$IFS
IFS=: 

declare -a bins=$BIN1:$BIN2
declare -a binsp=$BIN1$BIN1P:$BIN2$BIN2P

sizes="1:4:16:25:36:64:81:100"

it=0
for size in $sizes; do
	echo -e -n $size"\t" >> $out
		for bin in $binsp; do
			it=$((it+1))

			x=$(sh -c "$bin $size $pos_p")
			echo -ne $x >> $out
			sleep 3

		done
	echo -e >> $out 
	it=0
done


for bin in $bins; do
	it=$((it+1))
	mkdir $out_folder"/BIN"$it
	cp $bin $out_folder"/BIN"$it
done




echo -n -e "SIZES\t" >> $log

for size in $sizes; do
	echo -n -e $size"\t" >> $log
done

echo -e "\n" >> $log

itt=0
for bin in ${binsp[@]}; do
	itt=$((itt+1))
	echo "BIN"$itt": "$bin >> $log
	echo -e "\n" >> $log
done


else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./runSPP.sh $out_folder
