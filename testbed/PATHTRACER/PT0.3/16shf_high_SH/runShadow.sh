#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work"
results_folder=$work_folder"/testbed/PATHTRACER/PT0.3"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

mapper=0
dev=1
dice_value=0
height=512
width=512
scene=1

spp=81
max_depth=12

#spp=36
#max_depth=10

pre_p="$mapper $dev $dice_value $height $width $spp"
pos_p="$max_depth $scene"

BIN1="$work_folder/perform/PERFORM/pathtracer_FullT_RELEASE/PERFORM"
BIN1P=" $pre_p"

BIN2="$work_folder/perform/PERFORM/pathtracer_UBK_FineT_SIMD32_RELEASE/PERFORM"
BIN2P=" $pre_p"

# save and change IFS
OLDIFS=$IFS
IFS=: 

declare -a bins=$BIN1:$BIN2
declare -a binsp=$BIN1$BIN1P:$BIN2$BIN2P

sizes="shadow1.h:shadow4.h:shadow8.h:shadow16.h:shadow32.h:shadow64.h"

it=0
for size in $sizes; do
	echo -e -n $size"\t" >> $out
			it=$((it+1))
			cd "../../perform/PERFORM/src/applications/pathtracer/include/"
			cp $size "shadowX.h"

			cd "$work_folder/perform/PERFORM/pathtracer_FullT_RELEASE/"
			make clean
			make
			cd "$work_folder/testbed/PATHTRACER"
			x=$(sh -c "$BIN1$BIN1P $pos_p")
			echo -ne $x >> $out
			sleep 3


			cd "$work_folder/perform/PERFORM/pathtracer_UBK_FineT_SIMD32_RELEASE/"
			make clean
			make
			cd "$work_folder/testbed/PATHTRACER"
			x=$(sh -c "$BIN2$BIN2P $pos_p")
			echo -ne $x >> $out
			sleep 3
	echo -e >> $out 
	it=0
done


for bin in $bins; do
	it=$((it+1))
	mkdir $out_folder"/BIN"$it
	cp $bin $out_folder"/BIN"$it
done




echo -n -e "SIZES\t" >> $log

for size in $sizes; do
	echo -n -e $size"\t" >> $log
done

echo -e "\n" >> $log

itt=0
for bin in ${binsp[@]}; do
	itt=$((itt+1))
	echo "BIN"$itt": "$bin >> $log
	echo -e "\n" >> $log
done


else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./runShadow.sh $out_folder
