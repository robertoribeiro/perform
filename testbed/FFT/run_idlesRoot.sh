#!/bin/bash
set -x

./run_idles.sh 256_dev3 /home/rr/work/perform/PERFORM/data/256.jpg 3 > log1_idles.txt 2>&1
./run_idles.sh 512_dev3 /home/rr/work/perform/PERFORM/data/512.jpg 3 > log2_idles.txt 2>&1
./run_idles.sh 1024_dev3 /home/rr/work/perform/PERFORM/data/1024.jpg 3 > log3_idles.txt 2>&1
./run_idles.sh 2048_dev3 /home/rr/work/perform/PERFORM/data/2048.jpg 3 > log4_idles.txt 2>&1


#./run_idles.sh 512_dev2 /home/rr/work/perform/PERFORM/data/512.jpg 2 > log1_idles.txt 2>&1
##./run_idles.sh 1024_dev2 /home/rr/work/perform/PERFORM/data/1024.jpg 2 > log2_idles.txt 2>&1


