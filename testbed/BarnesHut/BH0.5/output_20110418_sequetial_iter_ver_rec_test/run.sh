#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

out_folder="output_"`date +%Y%m%d_`$1

if [[ ! -e $out_folder ]]
then

mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

N_RUNS=1
timesteps=25

BIN1=../PERFORM/Debug_sequential/rec/PERFORM
BIN2=../PERFORM/Debug_sequential/iter/PERFORM
#BIN3=../PERFORM/Debug_nbody_PERFORM/PERFORM
#BIN4=../barneshut_TBB_lonestar_Burtscher/Debug/barneshut_TBB_lonestar_Burtscher
#BIN5=../barneshut_CUDA_Burtsher/Default/barneshut_CUDA_Burtsher


sizes="1000 2000 4000 8000 16000 32000 64000 100000"
for size in $sizes; do
	echo -e -n $size"\t" >> $out
	./$BIN1 1 $size $timesteps >> $out 
	./$BIN2 1 $size $timesteps >> $out
	##./$BIN3 1 $size $timesteps >> $out
	#./$BIN4 1 $size $timesteps >> $out
	#./$BIN5 1 $size $timesteps >> $out
	echo >> $out
done



echo -n -e "SIZES\t" >> $log
echo -n -e "BIN1\t" >> $log
echo "BIN2\t" >> $log
#echo -n -e "BIN3\t" >> $log
#echo -n -e "BIN4\t" >> $log
#echo "BIN5" >> $log
echo -e "\n" >> $log

echo -e "timesteps:" $timesteps >> $log
echo "BIN1:" >> $log
echo "Nbody naive recursive sequential CPU" >> $log
echo -e "\n" >> $log
echo "BIN2:" >> $log
echo "body naive iterative sequential CPU" >> $log
echo -e "\n" >> $log
#echo "BIN3:" >> $log
#echo "Nbody iterative PERFORM version" >> $log
#echo -e "\n" >> $log
#echo "BIN4:" >> $log
#echo "Nbody opt recursive pure-TBB single device version" >> $log
#echo -e "\n" >> $log
#echo "BIN5:" >> $log
#echo "Nbody opt iterative pure-CUDA single device version" >> $log
echo -e "\n" >> $log

else
echo "Nothing done!! Folder exists, add a differnt comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi

cp ./run.sh $out_folder
