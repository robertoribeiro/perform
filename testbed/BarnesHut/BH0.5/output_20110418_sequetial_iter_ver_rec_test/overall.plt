#!/bin/bash


folder=`pwd`

font=Arial,11

gnuplot << EOF
set title "Barnes-Hut different cpu - 25 timesteps"  font "$font"
#set size 0.8,0.8
set terminal png size 520,440
set output "overall.png"
set key top left font "$font"
set key box font "$font"
set xlabel "number of particles x1000 | log" font "$font"
set ylabel "time seconds| log" font "$font"
set xtics font "$font"
set ytics font "$font"
set log xy
set xtics (1,2,4,8,16,32,64,100) 
plot "$folder/results.txt" using 1:2 title "recursive" w linespoints, \
"$folder/results.txt" using 1:3 title "sequential" w linespoints

EOF
