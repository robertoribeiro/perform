#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work/perform"
results_folder=$work_folder"/testbed/BarnesHut/BH"
out_folder=$results_folder"/output_"`date +%Y%m%d_`$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

N_RUNS=1
timesteps=25

BIN1=$work_folder/PERFORM/Debug_nbody_TBB/PERFORM
BIN2=$work_folder/PERFORM/Debug_nbody_CUDA/PERFORM
BIN3=$work_folder/PERFORM/Debug_nbody_multi_CUDA/PERFORM
BIN4=$work_folder/barneshut_TBB_lonestar_Burtscher/Debug/barneshut_TBB_lonestar_Burtscher
BIN5=$work_folder/barneshut_CUDA_Burtsher/Burtsher2.0
BIN6=$work_folder/PERFORM/Debug_nbody_PERFORM/PERFORM

declare -a bins
bins=( $BIN1 $BIN2 $BIN3 $BIN4 $BIN5 $BIN6 )
sizes="1024 2048 4096 8192 16384 32768 65536 131072 200000"

it=0


for size in $sizes; do

	echo -e -n $size"\t" >> $out
	for bin in ${bins[@]}; do

	it=$((it+1))


	$bin 1 $size $timesteps >> $out 
	mkdir $out_folder"/BIN"$it
	cp $bin $out_folder"/BIN"$it

	

	done
echo -e "\n" >> $log
it=0
done


echo -n -e "SIZES\t" >> $log

for size in $sizes; do
	echo -n -e $size"\t" >> $log
done

echo -e "\n" >> $log

echo -e "timesteps:" $timesteps >> $log

itt=0
for bin in ${bins[@]}; do
	itt=$((itt+1))
	echo "BIN"$itt": "$bin >> $log
	echo -e "\n" >> $log
done


else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi

cp ./run.sh $out_folder
