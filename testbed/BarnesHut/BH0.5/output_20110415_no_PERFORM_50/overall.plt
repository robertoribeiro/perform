#!/bin/bash


folder=`pwd`

font=Arial,11

gnuplot << EOF
set title "Barnes-Hut different platforms analysis - 50 timesteps"  font "$font"
#set size 0.8,0.8
set terminal png size 520,440
set output "overall.png"
set key top left font "$font"
set key box font "$font"
set xlabel "number of particles x1000 | log" font "$font"
set ylabel "time seconds| log" font "$font"
set xtics font "$font"
set ytics font "$font"
set log xy
set xtics (1,2,4,8,16,32,64,100) 
plot "$folder/results.txt" using 1:2 title "naive CUDA" w linespoints, \
 "$folder/results.txt" using 1:3 title "naive_TBB" w linespoints, \
"$folder/results.txt" using 1:4 title "opt_TBB" w linespoints, \
"$folder/results.txt" using 1:5 title "opt_CUDA" w linespoints

EOF
