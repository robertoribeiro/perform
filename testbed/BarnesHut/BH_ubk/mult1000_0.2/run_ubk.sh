#!/bin/bash
set -x

if [ $# -gt 0 ] 
then

work_folder="/home/rr/work/perform"
results_folder=$work_folder"/testbed/BarnesHut/BH_ubk"
out_folder=$results_folder"/"$1

if [[ ! -e $out_folder ]]
then
mkdir $results_folder
mkdir $out_folder

out=$out_folder/results.txt
log=$out_folder/log.txt

dice_value=0
mapper=0
timesteps=1
data=/home/rr/work/perform/PERFORM/data/my_center.txt

BIN2="$work_folder/PERFORM/Release_nbodyBH_UBK_Fine_T_SIMD32/PERFORM"
BIN2P=" $mapper 1 $dice_value $data $timesteps"
BIN1="$work_folder/PERFORM/Release_nbodyBH_Full_T/PERFORM"
BIN1P=" $mapper 1 $dice_value $data $timesteps"

# save and change IFS
OLDIFS=$IFS
IFS=: 

declare -a bins=$BIN1:$BIN2
declare -a binsp=$BIN1$BIN1P:$BIN2$BIN2P

sizes="16:32:64:128:256:512:900"

it=0


for size in $sizes; do
	echo -e -n $size"\t" >> $out
		#for bin in ${bins[@]}; do
		for bin in $binsp; do
			it=$((it+1))

			x=$(sh -c "$bin $size")
			exec1=`echo $x | bc`
			sleep 3

echo -n $x >> $out
			

		done
	echo -e >> $out 
	it=0
done


for bin in $bins; do
	it=$((it+1))
	mkdir $out_folder"/BIN"$it
	cp $bin $out_folder"/BIN"$it
done




echo -n -e "SIZES\t" >> $log

for size in $sizes; do
	echo -n -e $size"\t" >> $log
done

echo -e "\n" >> $log

itt=0
for bin in ${binsp[@]}; do
	itt=$((itt+1))
	echo "BIN"$itt": "$bin >> $log
	echo -e "\n" >> $log
done


else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
# restore it
IFS=$OLDIFS
cp ./run_ubk.sh $out_folder
