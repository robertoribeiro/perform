data<- read.csv("~/work/perform/testbed/GEMM_tts1.csv")
data2<- read.csv("~/work/perform/testbed/CONV_tts1.csv")
data3<- read.csv("~/work/perform/testbed/BH_tts1.csv")

par(cex.lab=0.7,cex.axis=0.7,tck=0.03,mgp=c(1.5, 0.4, 0),lwd=0.3,mfrow=c(1,3),mar=c(3, 2, 3, 1),xpd=F,font=2)

g_range <- range(0, max(max(data$RR_DUM),  max(data$WRR_DUM), max(data$DD_DUM), max(data$DD_DYN)))
max1= max(g_range)

plot(data$RR_DUM,type="b", pch=1, lty=1,xaxs="i",yaxs="i",ylim=g_range,main="GEMM",xlab="Matrix size",ylab="TTS(s)",
     axes=F,lwd=1)

lines(data$WRR_DUM,type="b", pch=2, lty=1,lwd=1)
lines(data$DD_DUM,type="b", pch=3, lty=1,lwd=1)
lines(data$DD_DYN,type="b", pch=4, lty=1,lwd=1)

axis(1,at=1:6, lab=data$X,lwd=0.3)
axis(2,las=2,lwd=0.3)

linesname<-c("RR_DUM","WRR_DUM       ","DD_DUM","DD_DYN")
pos1=structure(list(x = 1.37810372700792, y = 33.8400348903307), .Names = c("x", 
"y"))



abline(h=seq(0,max1,5),col="black",v=seq(1, 6,1),lty=3)
legend(pos1, linesname,pch=seq(1,4),lty=1,y.intersp=1.6,bty="o",cex=0.7);
box()


g_range <- range(0, data2$RR_DUM, data2$WRR_DUM,data2$DD_DUM,data2$DD_DYN)
max1= max(g_range)

plot(data2$RR_DUM,type="b", pch=1, lty=1,xaxs="i",yaxs="i", ylim=g_range,main="CONV",xlab="Image size",ylab="TTS(s)",axes=F,lwd=1)

lines(data2$WRR_DUM,type="b", pch=2, lty=1,lwd=1)
lines(data2$DD_DUM,type="b", pch=3, lty=1,lwd=1)
lines(data2$DD_DYN,type="b", pch=4, lty=1,lwd=1)

axis(1,at=1:6, lab=data2$X,lwd=0.3)
axis(2,las=2,lwd=0.3)

linesname<-c("RR_DUM","WRR_DUM       ","DD_DUM","DD_DYN")
pos2=structure(list(x = 1.26183135663962, y = 1.8965409822784213), .Names = c("x", 
"y"))


abline(h=seq(0,1.5,0.5),col="black",v=seq(1, 6,1),lty=3)
legend(pos2, linesname,pch=seq(1,4),lty=1,y.intersp=1.6,bty="N",cex=0.7,xpd=T);
box()

g_range <- range(0, data3$RR_DUM, data3$WRR_DUM,data3$DD_DUM,data3$DD_DYN)
max1= max(g_range)

plot(data3$RR_DUM,type="b", pch=1, lty=1,xaxs="i",yaxs="i", ylim=g_range,main="BH",xlab="Particles",ylab="TTS(s)",axes=F,lwd=1)
lines(data3$WRR_DUM,type="b", pch=2, lty=1,lwd=1)
lines(data3$DD_DUM,type="b", pch=3, lty=1,lwd=1)
lines(data3$DD_DYN,type="b", pch=4, lty=1,lwd=1)

axis(1,at=1:8, lab=data3$X,lwd=0.3)
axis(2,las=2,lwd=0.3)

linesname<-c("RR_DUM","WRR_DUM       ","DD_DUM","DD_DYN")
pos=structure(list(x = 1.26183135663962, y = 37.2707540436095), .Names = c("x", "y"))

abline(h=seq(0,max1,10),col="black",v=seq(1, 8,1),lty=3)
legend(pos, linesname,pch=seq(1,4),lty=1,y.intersp=1.6,bty="o",cex=0.7);
box()
