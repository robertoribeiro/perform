data<- read.csv("~/work/perform/testbed/GEMM_E.csv")
data2<- read.csv("~/work/perform/testbed/CONV_E.csv")
data3<- read.csv("~/work/perform/testbed/BH_E.csv")

par(cex.lab=0.7,cex.axis=0.7,tck=0.02,mgp=c(1.5, 0.4, 0),lwd=0.3,mfrow=c(1,3),mar=c(3, 2, 3, 1),xpd=F,font=2)

g_range <- range(0,1)
max1= max(g_range)

plot(data$RR_DUM,type="b", pch=1, lty=1,xaxs="i",yaxs="i", ylim=g_range,main="GEMM",xlab="Matrix size",ylab="Efficiency",
     axes=F,lwd=1)

lines(data$WRR_DUM,type="b", pch=2, lty=1,lwd=1)
lines(data$DD_DUM,type="b", pch=3, lty=1,lwd=1)
lines(data$DD_DYN,type="b", pch=4, lty=1,lwd=1)

axis(1,at=1:6, lab=data$X,lwd=0.3)
axis(2,las=2,lwd=0.3)

linesname<-c("RR_DUM","WRR_DUM     ","DD_DUM","DD_DYN")
pos1=structure(list(x = 1.17810372700792, y = 0.98), .Names = c("x", 
"y"))



abline(h=seq(0,1,0.2),col="black",v=seq(1, 6,1),lty=3)
legend(pos1, linesname,pch=seq(1,4),lty=1,y.intersp=1.6,bty="o",cex=0.7);
box()


g_range <- range(0,1)
max1= max(g_range)

plot(data2$RR_DUM,type="b", pch=1, lty=1,xaxs="i",yaxs="i", ylim=g_range,main="CONV",xlab="Image size",ylab="Efficiency",axes=F,lwd=1)

lines(data2$WRR_DUM,type="b", pch=2, lty=1,lwd=1)
lines(data2$DD_DUM,type="b", pch=3, lty=1,lwd=1)
lines(data2$DD_DYN,type="b", pch=4, lty=1,lwd=1)

axis(1,at=1:6, lab=data2$X,lwd=0.3)
axis(2,las=2,lwd=0.3)

linesname<-c("RR_DUM","WRR_DUM       ","DD_DUM","DD_DYN")
pos2=structure(list(x = 1.26183135663962, y = 0.98), .Names = c("x", 
"y"))


abline(h=seq(0,1,0.2),col="black",v=seq(1, 6,1),lty=3)
legend(pos2, linesname,pch=seq(1,4),lty=1,y.intersp=1.6,bty="N",cex=0.7,xpd=T);
box()

g_range <- range(0,1)
max1= max(g_range)

plot(data3$RR_DUM,type="b", pch=1, lty=1,xaxs="i",yaxs="i", ylim=g_range,main="BH",xlab="Particles",ylab="Efficiency",axes=F,lwd=1)
lines(data3$WRR_DUM,type="b", pch=2, lty=1,lwd=1)
lines(data3$DD_DUM,type="b", pch=3, lty=1,lwd=1)
lines(data3$DD_DYN,type="b", pch=4, lty=1,lwd=1)

axis(1,at=1:8, lab=data3$X,lwd=0.3)
axis(2,las=2,lwd=0.3)

linesname<-c("RR_DUM","WRR_DUM       ","DD_DUM","DD_DYN")
pos=structure(list(x = 1.26183135663962, y = 0.98), .Names = c("x", "y"))

abline(h=seq(0,1,0.2),col="black",v=seq(1, 8,1),lty=3)
legend(pos, linesname,pch=seq(1,4),lty=1,y.intersp=1.6,bty="o",cex=0.7);
box()
