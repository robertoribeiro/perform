#!/bin/bash


folder=`pwd`

gnuplot << EOF
# set terminal postscript landscape noenhanced monochrome #              dashed defaultplex "Helvetica" 11
set terminal png
set output "overall.png"
set key top left
set key box
set xlabel "number of particles" 
set ylabel "time milliseconds" 
set title "Barnes-Hut different platforms analysis" 
#set log xy
set xrange [ 1000 : 100000 ] noreverse nowriteback
#set ytics border mirror norotate 0.5
plot "$folder/results.txt" using 1:2 title "naive CUDA" w l
# "NP1_BC3_BOUND100Error_SAMPLE2.plotdata" using 1:2 title "NP1_BC3_B100Error_SAMPLE2.plotdata" w l, \
#"NP1_BC3_BOUND100Error_SAMPLE3.plotdata" using 1:2 title "NP1_BC3_B100Error_SAMPLE3.plotdata" w l,\
# "NP1_BC3_BOUND100Error_SAMPLE4.plotdata" using 1:2 title "NP1_BC3_B100Error_SAMPLE4.plotdata" w l

EOF
