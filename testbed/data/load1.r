data<- read.csv("~/work/perform/testbed/GEMM_load1.csv")
data2<- read.csv("~/work/perform/testbed/CONV_load1.csv")
data3<- read.csv("~/work/perform/testbed/BH_load1.csv")

par(cex=0.1,tck=0.02,mgp=c(2, 0.4, 0),xpd=NA,lwd=0.3,mfrow=c(1,4),mar=c(5, 3, 3, 1),font=2)

#,mar=c(2.5, 5.5, 4, 13.0)


g_range=range(0, max(sum(data$RR_DUM),sum(data$WRR_DUM),sum(data$DD_DUM),sum(data$DD_DYN)))
max1= max(sum(data$RR_DUM),sum(data$WRR_DUM),sum(data$DD_DUM),sum(data$DD_DYN))

bardensity <- c(0,20,-1)
barangle <- t(t(c(45,135,-45))) 

barplot(cbind(data$RR_DUM,data$WRR_DUM,data$DD_DUM,data$DD_DYN),yaxp=c(0, max1,2), ylim=c(0,max1), las=2,
        yaxs="i",axes=F,names.arg=c("RR_DUM","WRR_DUM","DD_DUM","DD_DYN"),main="GEMM - 10240",density=bardensity,angle=barangle,col="black",
       )

title(ylab="% Tasks",mgp=c(2,1,0))
axis(2,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)
#axis(4,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)

barplot(cbind(data2$RR_DUM,data2$WRR_DUM,data2$DD_DUM,data2$DD_DYN),yaxp=c(0, max1,2), main="CONV - 2048",las=2,
        ylim=c(0,max1),yaxs="i",axes=F,names.arg=c("RR_DUM","WRR_DUM","DD_DUM","DD_DYN"),density=bardensity,angle=barangle,col="black",
      )

title(ylab="% Tasks",mgp=c(2,1,0))
axis(2,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)
#axis(4,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)

barplot(cbind(data3$RR_DUM,data3$WRR_DUM,data3$DD_DUM,data3$DD_DYN),yaxp=c(0, max1,2), main="BH - 512k",las=2, ylim=c(0,max1),
        yaxs="i",axes=F,names.arg=c("RR_DUM","WRR_DUM","DD_DUM","DD_DYN"),density=bardensity,angle=barangle,col="black",
      )
        
title(ylab="% Tasks",mgp=c(2,1,0))
axis(2,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)
#axis(4,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)


pos<-structure(list(x = 5.67726926451425, y = 0.703008423671491), .Names = c("x", 
"y"))

legend(legend=data$X,cex=1.0, bty="n", pos,y.intersp=1.5,density=bardensity,angle=barangle,col="black")

 