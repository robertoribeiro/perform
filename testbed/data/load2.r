data<- read.csv("~/work/perform/testbed/GEMM_load2.csv")
data2<- read.csv("~/work/perform/testbed/CONV_load2.csv")
data3<- read.csv("~/work/perform/testbed/BH_load2.csv")

par(cex=0.5,tck=0.02,mgp=c(2, 0.4, 0),xpd=NA,lwd=0.3,mfcol=c(1,4),font=2)
par(mar=c(4, 5, 3, 0))
#,mar=c(2.5, 5.5, 4, 13.0)


max1= 1

bardensity <- c(0,20,-1)
barangle <- t(t(c(45,135,-45))) 




barplot(cbind(data$X4096,data$X8192,data$X10240,data$X12288),yaxp=c(0, max1,2), ylim=c(0,max1),cex.names=0.8,
        yaxs="i",axes=F,names.arg=c("4096","8192","10240","12288"),main="GEMM - DD_DYN",density=bardensity,angle=barangle,col="black",ylab="% Tasks",xlab="Matrix size")

axis(2,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)
#axis(4,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)

barplot(cbind(data2$X256,data2$X1024,data2$X1024,data2$X2048),yaxp=c(0, max1,2), main="CONV - DD_DUM",cex.names=0.8,
        ylim=c(0,max1),yaxs="i",axes=F,names.arg=c("256","1024","1024","2048"),density=bardensity,angle=barangle,col="black",ylab="% Tasks",xlab="Image size")

axis(2,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)
#axis(4,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)



barplot(cbind(data3$X64k,data3$X128k,data3$X256k,data3$X512k),yaxp=c(0, max1,2), main="BH - DD_DYN",cex.names=0.8,
        ylim=c(0,max1),yaxs="i",axes=F,names.arg=c("64k","128k","256k","512k"),density=bardensity,angle=barangle,col="black",ylab="% Tasks",xlab="Particles")

axis(2,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)
#axis(4,at=seq(0,1,0.20), lab=seq(0,100,20),las=2)


pos<-structure(list(x = 5.04366870102349, y = 0.74015582456776), .Names = c("x", 
"y"))
legend(legend=data$X,cex=1.0, bty="n", pos,y.intersp=1.5,density=bardensity,angle=barangle,col="black")

