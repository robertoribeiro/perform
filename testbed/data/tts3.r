data<- read.csv("~/work/perform/testbed/GEMM_tts3.csv")
data2<- read.csv("~/work/perform/testbed/CONV_tts3.csv")
data3<- read.csv("~/work/perform/testbed/BH_tts3.csv")

par(cex.lab=0.7,tck=0.02,cex.axis=0.7,mgp=c(2, 0.6, 0),mfrow=c(1,3),mar=c(6,3,3, 1),xpd=F,font=2)



#-------------------------------------------
max1 = max(data$GPU,data$CPU_MKL,data$CPU.2xGPU)*1.20

Y_it = at=seq(1,max1,10)
Y_lab = at=seq(0,max1,10)


Xlab =  c("CPU_MKL","GPU_cuBLAS","PF_GPU","PF_2xGPU")

barplot(cbind(data$CPU_MKL,data$GPU_cuBLAS,data$GPU,data$X2xGPU), ylim=c(1,max1), las=2,log="y",ylab="log Tts(s)",
        yaxs="i",axes=F,names.arg=Xlab,main="GEMM - 10240 - DD_DYN",col="black")

axis(2,at=Y_it, lab=Y_lab,las=2)
#-------------------------------------------
max1 = max(data2$CPU,data2$CPU.GPU,data2$CPU.2xGPU)*1.05

Y_lab = at=seq(0,2,1)

Y_it = at=seq(0,2,1)

Xlab =  c("CPU_MKL","GPU_CUFFT","PF_CPU+GPU","PF_CPU+2xGPU")

barplot(cbind(data2$CPU_MKL,data2$GPU_CUFFT,data2$CPU.GPU,data2$CPU.2xGPU), ylim=c(0.001,2), las=2,log="y",ylab="log Tts(s)",
        yaxs="i",axes=F,names.arg=Xlab,main="CONV - 1024 - DD_DUM",col="black")

axis(2,at=Y_it, lab=Y_lab,las=2)

#-------------------------------------------
max1 = max(data3$CPU_Burtscher,data3$GPU_Burtscher,data3$GPU,data3$X2xGPU)*1.20

Y_it = at=seq(1,max1,30)
Y_lab = at=seq(0,max1,30)

Xlab =  c("CPU_Burtscher","GPU_Burtscher","PF_GPU","PF_2xGPU")

barplot(cbind(data3$CPU_Burtscher,data3$GPU_Burtscher,data3$GPU,data3$X2xGPU), ylim=c(1,max1), las=2,log="y",ylab="log Tts(s)",
        yaxs="i",axes=F,names.arg=Xlab,main="BH - 512k - WRR_DUM",col="black")

axis(2,at=Y_it, lab=Y_lab,las=2)

