#!/bin/bash
set -x

cd FFT
./runRoot.sh
cd ../GEMM
./runRoot.sh
cd ../BarnesHut
./runRoot.sh
