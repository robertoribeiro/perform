data<- read.csv("~/work/perform/testbed/GEMM_tts2.csv")
data2<- read.csv("~/work/perform/testbed/CONV_tts2.csv")
data3<- read.csv("~/work/perform/testbed/BH_tts2.csv")

par(cex.lab=0.7,tck=0.02,cex.axis=0.7,mgp=c(2.5, 0.6, 0),mfrow=c(1,3),mar=c(5,4,3, 1),xpd=F,font=2)

bardensity <- c(0,20,-1)
barangle <- t(t(c(45,135,-45))) 


#-------------------------------------------
max1 = max(data$CPU,data$GPU,data$X2xGPU,data$CPU.2xGPU)*1.10
Y_it = at=seq(0,max1,10)
Xlab = c("CPU","GPU","2xGPU","CPU+2xGPU")

barplot(cbind(data$CPU,data$GPU,data$X2xGPU,data$CPU.2xGPU), ylim=c(0.5,max1), las=2,log="y",ylab="log Tts(s)",
        yaxs="i",axes=T,names.arg=Xlab,main="GEMM - 10240",col="black")

#axis(2,Y_it, lab=Y_it,las=2)

#-------------------------------------------
max1 = max(data2$CPU,data2$GPU,data2$X2xGPU,data2$CPU.2xGPU)*1.10


Xlab = c("CPU","GPU","2xGPU","CPU+2xGPU")

barplot(cbind(data2$CPU,data2$GPU,data2$X2xGPU,data2$CPU.2xGPU), ylim=c(0.5,max1), las=2,log="y",ylab="log Tts(s)",
        yaxs="i",axes=T,names.arg=Xlab,main="CONV - 1024",col="black")


#-------------------------------------------
max1 = max(data3$CPU,data3$GPU,data3$X2xGPU,data3$CPU.2xGPU)*1.20
Y_it = at=seq(0,max1,30)
Xlab = c("CPU","GPU","2xGPU","CPU+2xGPU")

barplot(cbind(data3$CPU,data3$GPU,data3$X2xGPU,data3$CPU.2xGPU), ylim=c(0.5,max1), las=2,log="y",ylab="log Tts(s)",
        yaxs="i",axes=T,names.arg=Xlab,main="BH - 512k",col="black")

#axis(2,at=Y_it, lab=Y_it,las=2)
pos1=structure(list(x = 2.47810372700792, y = 120.8400348903307), .Names = c("x", "y"))

