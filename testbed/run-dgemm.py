#! /usr/bin/python

import subprocess
import re
import sys
import os
import time



bin_folder="/home/rr/work/dgemm/"
bin="perform/PERFORM/bin"


live=0

def execute(command):
	process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	out = process.communicate()[0]
	print out
	return out

def execute2(command):
    process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    runoutput = ""
    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == '' and process.poll() != None:
            break
        if live:
        	sys.stdout.write(nextline)
        runoutput+=nextline
        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return runoutput
    else:
        raise ProcessException(command, exitCode, output)

def execute3(command):
    process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = ""
    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        nextlineERR = process.stderr.readline()
        if nextlineERR != '' and process.poll() != None:
        	print "Run error: " + nextlineERR
        	break
        if nextline == '' and process.poll() != None:
            break
        if live:
        	sys.stdout.write(nextline)
        output+=nextline
        sys.stdout.flush()
        sys.stderr.flush()

	#fin = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return output
    else:
        raise ProcessException(command, exitCode, output)

def parse_time(result):
	time=[]
	for line in result.split("\n"):
		if 'Simulation done in' in line:
			time=re.findall(r'\d+\.\d+', line)
			break

	if len(time) == 0:
		time.append(sys.float_info.max)
	return float(time[0])


def multiple_runs(command, n_runs, parse_fun):
	print command
	result=[]
	full_output=""
	for x in xrange(n_runs):
		print "Run %d..." % x
		try:
			full_output+=execute(command)
		except Exception:
			print "Process failed"
			pass
		result.append(parse_fun(full_output))
	return min(result), full_output

def make_line(result_line):
	 return "\t".join(map(str,result_line)) + "\n"

def run_dgemm(n_runs, dev,initial_dice,cpu_flops,size, log):
	command=[bin_folder + bin + '/PERFORM' , str(dev) , str(initial_dice) , str(cpu_flops) , str(size)]
	log.write(' '.join(command))
	minr, full_output = multiple_runs(command, n_runs, parse_time)
	log.write(full_output)
	return minr


def multi_device(n_runs, devs, initial_dice, cpu_flops,sizes, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for size in sizes:
		result_line=[]
		result_line.append(size);
		for dev in devs:
			#run_mcml_fluor_fullt
			fastest=0.0
			fastest = run_dgemm(n_runs, dev, initial_dice, cpu_flops, size,  log)
			result_line.append(fastest);
			print fastest
		print result_line
		result.write(make_line(result_line))
		result.flush()

def assess_cpuflops_dices(n_runs, devs, initial_dices, cpu_flops,size, comment):
	instance = str(int(round(time.time() * 1000))) + "." + comment
	os.mkdir(instance)
	log = open(instance + '/log', 'a')
	result = open(instance + '/result', 'a')
	for dev in devs:
		for initial_dice in initial_dices:
			result_line=[]
			result_line.append(initial_dice);
			for cpu_flop in cpu_flops:
				#run_mcml_fluor_fullt
				fastest = run_dgemm(n_runs, dev, initial_dice, cpu_flop, size, log)
				result_line.append(fastest);
				print fastest
			print result_line
			result.write(make_line(result_line))
		result.write("\n\n")

n_runs=2

devs=[0,1,2,3,4, 5, 6]
initial_dices=[2, 4 ,8 , 16]
cpu_flops=[100,  200,  400]

sizes=[2,3,4,5,6,7,8]
sizes_set = [1024*x for x in sizes] 


#build_fullt_pkernel_compare(n_runs, devs[1], 0, 100,n_photons_set, orig_input_file_dense, "fullt_pkernel_compare_dense_high")

#build_fluor_fullt_pkernel_compare(n_runs, devs[0], 2, 100,n_photons_set, orig_input_file, "fullt_pkernel_final")
#multi_device(n_runs, devs, initial_dices[3], cpu_flops[2],sizes_set,"multi-device-gtx")
assess_cpuflops_dices(n_runs, devs[3:5], initial_dices, cpu_flops,sizes_set[6],"assess")
#fluor_pk_assess_idice(n_runs, devs[6], initial_dices, cpu_flops[0],n_photons_set,orig_input_file, "assess_idice")
#fluor_pk_assess_cpuflops(n_runs, devs[4:6], initial_dices[3], cpu_flops,n_photons_set[5],orig_input_file, "assess_cpuflops")
#fluor_pk_assess_cpuflops_dices(n_runs, devs[5], initial_dices, cpu_flops,n_photons_set[6],orig_input_file, "assess_cpuflops_dice")


