/*
 * device_configs.cpp
 *
 *  Created on: Apr 17, 2012
 *      Author: rr
 */

#include "perform.h"
#include "device_apis/CUDA/DeviceAPI_CUDA.h"
#include "device_apis/CPU/DeviceAPI_CPU.h"
#include "uberkernel.h"

RESOURCE_ID CPU_0 = CPU0;
RESOURCE_ID GPU_0 = GPU0;
RESOURCE_ID GPU_1 = GPU1;
RESOURCE_ID GPU_2 = GPU2;


uint CUDA_DEVICE_0 = 1;
uint CUDA_DEVICE_1 = 2;
uint CUDA_DEVICE_2 = 0;

void config_resource(RESOURCE_ID res, bool idle, int cpu_flops) {

	if (res == CPU_0) {

		DeviceAPI* apiCPU_0 = new DeviceAPI_CPU(12, CPU_0);
		ResourceInfo* infoCPU_0 = new ResourceInfo(CPU, CPU_0, 24, 8000);
		infoCPU_0->idle = idle;

		infoCPU_0->TPFs = FLOPS(cpu_flops);

		resource_manager->addResource(infoCPU_0, CPU_0, apiCPU_0);
		if (!idle)
			resource_manager->resources_info.systemTPFS.value += infoCPU_0->TPFs.value;
		resource_manager->resources_info.max_TPFS.value = infoCPU_0->TPFs.value;

	} else if (res == GPU_0) {

		DeviceAPI* apiGPU_0 = new DeviceAPI_CUDA_streamed(GPU_0,CUDA_DEVICE_0, 15);
		ResourceInfo* infoGPU_0 = new ResourceInfo(GPU, GPU_0, 1350);
		infoGPU_0->idle = idle;
		infoGPU_0-> TPFs = FLOPS(1350.0f);//not real
		resource_manager->addResource(infoGPU_0, GPU_0, apiGPU_0);
		resource_manager->resources_info.systemTPFS.value += infoGPU_0->TPFs.value;
		resource_manager->resources_info.max_TPFS.value = infoGPU_0->TPFs.value;

	} else if (res == GPU_1) {

		DeviceAPI* apiGPU_1 = new DeviceAPI_CUDA_streamed(GPU_1, CUDA_DEVICE_1, 15);
		ResourceInfo* infoGPU_1 = new ResourceInfo(GPU, GPU_1, 1350);
		infoGPU_1->idle = idle;
		infoGPU_1-> TPFs = FLOPS(1350.0f);//not real

		resource_manager->addResource(infoGPU_1, GPU_1, apiGPU_1);
		resource_manager->resources_info.systemTPFS.value += infoGPU_1->TPFs.value;
		resource_manager->resources_info.max_TPFS.value = infoGPU_1->TPFs.value;

	} else if (res == GPU_2) {

		DeviceAPI* apiGPU_2 = new DeviceAPI_CUDA_streamed(GPU_2, CUDA_DEVICE_2, 15);
		ResourceInfo* infoGPU_2 = new ResourceInfo(GPU, GPU_2, 1350);
		infoGPU_2->idle = idle;
		infoGPU_2-> TPFs = FLOPS(1350.0f);//not real

		resource_manager->addResource(infoGPU_2, GPU_2, apiGPU_2);
		resource_manager->resources_info.systemTPFS.value += infoGPU_2->TPFs.value;
		resource_manager->resources_info.max_TPFS.value = infoGPU_2->TPFs.value;

	}

}

void initDeviceConfig(int dev_config, int cpu_flops) {
	switch (dev_config) {
	case 0:
		config_resource(CPU_0, false, cpu_flops);
		break;
	case 1:
		config_resource(CPU_0, true, cpu_flops);
		config_resource(GPU_0);
		break;
	case 2:
		config_resource(CPU_0, false, cpu_flops);
		config_resource(GPU_0);
		break;
	case 3:
		config_resource(CPU_0, true, cpu_flops);
		config_resource(GPU_0);
		config_resource(GPU_1);
		break;
	case 4:
		config_resource(CPU_0, false, cpu_flops);
		config_resource(GPU_0);
		config_resource(GPU_1);
		break;
	case 5:
		config_resource(CPU_0, true, cpu_flops);
		config_resource(GPU_2);
		config_resource(GPU_1);
		config_resource(GPU_0);
		//config_resource(GPU_3);
		break;
	case 6:
		config_resource(CPU_0, false, cpu_flops);
		config_resource(GPU_0);
		config_resource(GPU_1);
		config_resource(GPU_2);
		//config_resource(GPU_3);
		break;
	default:
		break;
	}

}
