/*
 * device_configs.h
 *
 *  Created on: Apr 17, 2012
 *      Author: rr
 */

#ifndef DEVICE_CONFIGS_H_
#define DEVICE_CONFIGS_H_


#endif /* DEVICE_CONFIGS_H_ */


extern RESOURCE_ID CPU_0;
extern RESOURCE_ID GPU_0;
extern RESOURCE_ID GPU_1;
extern RESOURCE_ID GPU_2;

extern uint CUDA_DEVICE_0;
extern uint CUDA_DEVICE_1;
extern uint CUDA_DEVICE_2;


#define N_SM 14


