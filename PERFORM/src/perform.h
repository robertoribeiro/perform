/*
 * perform.h
 *
 *  Created on: Apr 6, 2011
 *      Author: rr
 */

#ifndef PERFORM_H_
#define PERFORM_H_

#include "task/Task.h"
#include "taskcontainer/TaskContainerQueue.h"
#include "taskmapper/TaskMapper.h"
#include "resourcemanager/ResourceManager.h"
#include "datalibrary/DataLibrary.h"
#include "perform_utils.h"
#include "kernels.h"
#include "datalibrary/Domain.h"
#include "performancemodel/PerformanceModel.h"
#include "core.h"
#include "device_configs.h"

extern RESOURCE_ID hostID; /* strictly necessary for DL initialization */

//#define TEST_IDLES
//#define TEST_TIME

extern ResourceManager* resource_manager;
extern TaskMapper* performMAPPER;
extern DataLibrary* performDL;
extern TaskContainer* performQ;
extern PerformanceModel* performPM;

void wait_for_all_tasks();
void wait_stopPERFORM();
void initPERFORM(int sch,int dev_config, int initialdice,int cpu_flops);
void shutdownPERFORM();
void* PERFORM_alloc(size_t size);
void PERFORM_free(void* p);


void PERFORM_signal_JobFinished(double time);
void PERFORM_signal_JobStarted();
void PERFORM_signal_taskSubmissionEnded();

#endif /* PERFORM_H_ */
