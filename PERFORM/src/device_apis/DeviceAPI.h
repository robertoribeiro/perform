/*
 * DeviceAPI.h
 *
 *  Created on: Dec 1, 2010
 *      Author: rr
 */

#ifndef DEVICEAPI_H_
#define DEVICEAPI_H_

#include "task/Task.h"


class DeviceAPI {
public:

	RESOURCE_ID resource_id;
	long long bytes_transfered;


	DeviceAPI(RESOURCE_ID ids);


	virtual void processTask(Task* t);

	virtual bool copy_chunk_to_device(void*&,data_chunk* dc,Task* task) ;

	virtual void retrieve_data_chunk(data_chunk* dc,  SPLICE_OP op,bool blocking=false);

	virtual void init();

	virtual uint pending_tasks();

	virtual void shutdown();

	virtual void executeTask(Task* t) ;

	/**
	 * Moves data
	 */
	bool setTaskDataReady(Task* t);
	void retrieveTaskDataToHost(Task* t);

	/**
	 * Following lazy memory copy model and using fermi CCE.
	 * Although controlled by the same thread, copies to host are in a diferent stream.
	 * Copies in these devices are async so i'm usign cuda events to manage data copies
	 * Invoked each chunk copy to host, task chunks retrieval and set task data ready
	 */
	virtual void checkAsyncOps(bool blocking=false);

	virtual int opsLeft();

	virtual void free_chunk(data_chunk* dc);

	virtual bool check_if_memory_available(data_chunk* dc) ;

	virtual bool allocate_chunk(void*&,data_chunk* );


};

#endif /* DEVICEAPI_H_ */
