/*
 * DeviceAPI_CUDA.h
 *
 *  Created on: Dec 11, 2010
 *      Author: rr
 */

#ifndef DEVICEAPI_CUDA_STREAMED_H_
#define DEVICEAPI_CUDA_STREAMED_H_

#include "../DeviceAPI.h"
#include "cudaEventsRegistry.h"
#include "CUDA_slave.h"
#include <map>
#include <iostream>
#include <vector>
#include <cuda.h>
#include <cuda_runtime.h>

using namespace std;

class DeviceAPI_CUDA_streamed: public DeviceAPI {
public:

	int m_device_hw_id;
	uint m_maxStreams;
	std::map<unsigned int, cudaStream_t> tasksCudaStreams;

	//cudaEventsRegistry<Task*>* task_termination_events;
	cudaEventsRegistry<data_chunk*>* copy_to_host_events;

	CUDA_executor* executor;

	//byte_trf

	/*
	 * persistent streams
	 */
	cudaStream_t retrieveStream;
	vector<cudaStream_t> live_streams;

	int position;

	DeviceAPI_CUDA_streamed(RESOURCE_ID ids, int hw_id, int maxStreams);

	virtual ~DeviceAPI_CUDA_streamed();

	void retrieve_data_chunk(data_chunk* dc, SPLICE_OP op, bool blocking);

	void init();

	void shutdown();

	void executeTaskKernel(Task* t); /* Analyze depetandent of task implementation/specification */

	void executeTask(Task* t);

	cudaStream_t getStream(Task* task);

	bool copy_chunk_to_device(void*&,data_chunk* dc, Task* task);

	cudaEvent_t createEvent();

	void checkAsyncOps(bool blocking = false);

	int opsLeft();

	uint pending_tasks();

	int pending_events() ;

	void free_chunk(data_chunk* dc);

	bool check_if_memory_available(data_chunk* dc) ;


	bool allocate_chunk(void*&,data_chunk* dc);


};

#endif /* DEVICEAPI_CUDA_STREAMED_ */

