/*
 * CudaConcurrentQueue.h
 *
 *  Created on: Mar 10, 2011
 *      Author: jbarbosa
 */

#ifndef WORKQUEUE_H_
#define WORKQUEUE_H_

#include "core.h"
#include "task/Task.h"
#include "mutex.h"
#include "cuda_utils.h"

template<class T>
class __align__(16) CudaConcurrentQueue {
public:

	T* _list;

	volatile uint _dequeue;
	volatile uint _enqueue;
	volatile uint _length;

	SpinLock _lock;

	__HD__ __forceinline uint getSize() {

		return (_enqueue - _dequeue);
	}

	__HD__ __forceinline bool isEmpty() {

		//if(getSize() < 0) printf(" global queue empty \n");
		return getSize() <= 0;

	}

	__HD__ __forceinline bool isFull() {
		//if(getSize() > _length) printf(" global queue full \n");

		return getSize() >= _length;
	}

	__HD__ __forceinline void clear() {
		_enqueue = 0;
		_dequeue = 0;
		_lock.mutex = -1;
	}

	__HD__ __forceinline CudaConcurrentQueue(T* task_prms, int elements, int LENGTH) :	_dequeue(0), _length(LENGTH) {
		clear();
		_list = task_prms;
		_enqueue = elements;
		__threadfence();
	}

	__HD__ CudaConcurrentQueue()
	{
		clear();
	}


	__HD__ __forceinline void setBuffer(T* task_prms, int elements, int LENGTH) {
		clear();

		_list = task_prms;
		_enqueue = elements;
		_dequeue = 0;
		_length = LENGTH;


	}

	__HD__
	~CudaConcurrentQueue() {
		delete _list;
	}

	__HD__
	bool dequeue(T& v) {
		if (isEmpty()) {
			//printf("Error empty global\n");
			return false;
		}

		if(getSize() > TIDX) {
			register uint a = atomicAdd((uint*)&_dequeue,1);
			v = _list[ a  % _length];
		}	else  {
			//printf("Error, size : %d\n",getSize());
			return false;
		}

		return true;
	}

	__HD__
	bool enqueue(T elem) {
		if (isFull()) {
			printf("Error full global\n");
			return false;
		}
		register uint a = atomicAdd((uint*)&_enqueue, 1);
		_list[a  % _length] = elem;
		__threadfence_system();
		return true;
	}

	__HD__
	bool enqueue_bag(T* elem, int elements_bag) {

		if (isFull()) {
			printf("Error full bag global\n");
			return false;
		}

		for(int item=0; item < elements_bag; item++) {
			register uint a = atomicAdd((uint*)&_enqueue,1);
			_list[a  % _length] = elem[item];
		}

		//		for (int li = 0; li < WARPSIZE; li++) {
		//			if (li == threadIdx.x) {
		//				if (elements_bag != 0) {
		//					a = _enqueue;
		//					_enqueue = (_enqueue + elements_bag);
		//
		//					for (int j = 0; j < elements_bag; j++) {
		//						_list[(a + j) % _length] = elem[j];
		//					}
		//				}
		//			}
		//		}
		return true;
	}

	__HD__
	void Acquire() {
		_lock.Acquire();
	}

	__HD__
	void Release() {
		_lock.Release();
	}

	__HD__
	bool tryLock() {
		return _lock.Try();
	}

};



template<class T>
class __align__(16) CudaLocalQueue {
public:
	T* _list;

	volatile uint _enqueue;
	volatile uint _length;
	volatile uint _dequeue;

	SpinLock _lock;

	__HD__
		void Acquire() {
			_lock.Acquire();
		}

		__HD__
		void Release() {
			_lock.Release();
		}

		__HD__
		bool tryLock() {
			return _lock.Try();
		}


	__HD__ __forceinline uint getSize() {
		return (_enqueue - _dequeue);
	}

	__HD__ __forceinline bool isEmpty() {

//		if(getSize() < 0) {
//			printf(" local queue empty \n");
//			//__trap();
//		}
		return (getSize() <= 0);
	}

	__HD__ __forceinline bool isFull() {

		return ((getSize() + QUEUE_FULL_HANDICAP) >= _length );
	}

	__HD__ __forceinline void clear() {
		_enqueue = 0;
		_dequeue = 0;
		_lock.mutex = -1;

	}

	__HD__ __forceinline CudaLocalQueue(T* task_prms, int elements, int LENGTH) :
																				_dequeue(0), _length(LENGTH) {

		clear();

		_list = task_prms;
		_enqueue = elements;
	}

	__HD__ CudaLocalQueue()
	{
		clear();
	}

	__HD__ __forceinline void setBuffer(T* task_prms, int elements, int LENGTH) {
		clear();

		_list = task_prms;
		_enqueue = elements;
		_dequeue = 0;
		_length = LENGTH;

	}

	__HD__
	~CudaLocalQueue() {
		//delete _list;
	}

	__HD__
	bool dequeue(volatile T& v) {
		if (isEmpty()) {
			return false;
		}


//		unsigned int pos = _dequeue;
//
//		while(pos != atomicCAS((uint*)&_dequeue,pos,pos+1)) {
//			pos = _dequeue;
//			if(getSize() == 0) return false;
//		}
//
//		v = _list[ pos  % _length];

		if(getSize() > TIDX) {
			register uint a = atomicAdd((uint*)&_dequeue,1);
			v = _list[ a  % _length];

		}	else  {
			return false;
		}

		__threadfence_system();
		return true;
	}

	__HD__
	bool enqueue(T elem) {
		if (isFull()) {
			printf("Local queue full\n");
			return false;
		}

		register uint a = atomicAdd((uint*)&_enqueue, 1);

		if ((a - _dequeue) > _length) {
				return false;
		}

		_list[a  % _length] = elem;

		return true;
	}


	/**
	 * ensure from the outside that the queue has enough room. Can't just use isFull()
	 */
	__HD__
	bool enqueue_bag(T* elem, int elements_bag) {
		if (isFull()) {
			return false;
		}

		register uint a;
		for(int item=0; item < elements_bag; item++) {
			 a = atomicAdd((uint*)&_enqueue,1);
			_list[a  % _length] = elem[item];

		}

		return true;
	}

	__HD__
	bool dequeue_bag(T* elem, int warp_count) {
		if (isEmpty()) {
			return false;
		}

		register uint a;
		int item=0;
		for (item = 0; item < warp_count; item++) {
			if (getSize() > TIDX) {
				a = atomicAdd((uint*) &_dequeue, 1);
				elem[(item*WARPSIZE)+TIDX] = _list[a % _length];
			} else {
				break;
			}

		}

		return true;
	}

	__HD__
		bool dequeue_bag_SIMD32(T* elem, int warp_count) {
			if (isEmpty()) {
				return false;
			}

			register uint a;
			int item=0;
			for (item = 0; item < warp_count; item++) {
				if (getSize() > TIDX) {
					a = atomicAdd((uint*) &_dequeue, 1);
					elem[item] = _list[a % _length];
				} else {
					break;
				}

			}

			return true;
		}

};




#endif /* WORKQUEUE_H_ */
