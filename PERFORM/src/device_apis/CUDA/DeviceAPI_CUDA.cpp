/*
 * DeviceAPI_CUDA.cpp
 *
 *  Created on: Dec 11, 2010
 *      Author: rr
 */

#include "DeviceAPI_CUDA.h"
//#include "cuda.h"

#include "datalibrary/DataLibrary.h"
#include "kernels.h"
#include "uberkernel.h"
#include "core.h"
#include "perform_utils.h"

void k_CUDA_wrapper_uberkernel(Task* t, ubkTask* task_prms,
		int child_task_count);

extern DataLibrary* performDL;

DeviceAPI_CUDA_streamed::DeviceAPI_CUDA_streamed(RESOURCE_ID ids, int hw_id,
		int maxStreams) :
		DeviceAPI(ids) {
	m_device_hw_id = hw_id;
	m_maxStreams = maxStreams;
	copy_to_host_events = new cudaEventsRegistry<data_chunk*>(resource_id);
	executor = new CUDA_executor(this, ids);

}

DeviceAPI_CUDA_streamed::~DeviceAPI_CUDA_streamed() {
	// TODO Auto-generated destructor stub
}

void DeviceAPI_CUDA_streamed::init() {
	//cudaSetDeviceFlags(cudaDeviceMapHost);

	__E(cudaSetDevice(m_device_hw_id));

	size_t memAvail, memTotal;

	__E(cudaMemGetInfo(&memAvail, &memTotal));

	//cudaDeviceSetLimit(cudaLimitMallocHeapSize, (size_t)(memAvail*0.9f));
	//cudaThreadSetLimit(cudaLimitMallocHeapSize, (size_t)(memAvail*0.7f));
	LOG(0.5, checkCUDAmemory("init")
	;
	)

	__E(cudaStreamCreate(&retrieveStream));
	//retrieveStream =0;
	executor->start();
}

cudaStream_t DeviceAPI_CUDA_streamed::getStream(Task* task) {

	cudaStream_t stream = task->stream;

	if (stream == NULL) {

		std::map<unsigned int, cudaStream_t>::iterator it;

		//it = tasksCudaStreams.find(requestingTask);
		//if (it == tasksCudaStreams.end()) {
		if (live_streams.size() == 0) {
			__E(cudaStreamCreate(&stream));
			live_streams.push_back(stream);
			position = 0;

		} else if (live_streams.size() < m_maxStreams) {
			__E(cudaStreamCreate(&stream));
			live_streams.push_back(stream);

		} else {

			//if (pos == live_streams.end())	pos = live_streams.begin();
			if (position == m_maxStreams)
				position = 0;
			//cout << "reusing streams, position: " << position << endl;
			//stream = (pos);
			stream = live_streams[position];
			position++;

		}

		//tasksCudaStreams.insert(pair<unsigned int, cudaStream_t> (requestingTask, stream));

		//	} else
		//		stream = it->second;
	}

	return stream;
}

cudaEvent_t DeviceAPI_CUDA_streamed::createEvent() {

	cudaEvent_t event;		// = CUDAcallCreateEvent();
	__E(cudaEventCreate(&event));
	return event;

}
/*
 * The chunk copy is associated with the task by the GPU stream used
 */
bool DeviceAPI_CUDA_streamed::copy_chunk_to_device(void*& ptr, data_chunk* dc,
		Task* task) {

	address new_pointer = NULL;

	//float b = dc->n_bytes / 1024 / 1024;

	//LOG(2,printf("Allocating %f mb\n",b);)

	if (!check_if_memory_available(dc)) {
		//checkCUDAmemory();
		fprintf(stderr, "WARNING: memory full\n");
		return false;
	}

	cudaError_t err = cudaMalloc((void**) &new_pointer, dc->n_bytes);
	if (cudaErrorMemoryAllocation == err) {
		checkCUDAmemory();
		fprintf(stderr, "Critical ERROR: memory full\n");
		return false;
	}

	// TODO always requesting stream, not required if stored in task object!

	switch (dc->domain->n_dim_space) {
	case 2:
		uint src_pitch;
		if (dc->parent_chunk == NULL)
			src_pitch = dc->pitch * dc->element_size;
		else
			src_pitch = dc->parent_chunk->pitch
					* dc->parent_chunk->element_size;

		__E(
				cudaMemcpy2DAsync(new_pointer, dc->pitch * dc->element_size,
						dc->hook_pointer, src_pitch,
						dc->domain->Y.length() * dc->element_size,
						dc->domain->X.length(), cudaMemcpyHostToDevice,
						getStream(task)));

		//		cudaMemcpy2D(new_pointer, dc->pitch * dc->element_size, dc->hook_pointer,
		//						src_pitch,
		//						dc->domain->Y.length() * dc->element_size, dc->domain->X.length(),
		//						cudaMemcpyHostToDevice);

		break;
	case 1:
		__E(
				cudaMemcpyAsync(new_pointer, dc->hook_pointer, dc->n_bytes,
						cudaMemcpyHostToDevice, getStream(task)));

		break;
	default:
		break;
	}

	//bytes_transfered += dc->n_bytes;
	__sync_fetch_and_add(&bytes_transfered, dc->n_bytes);

	//printf("%x\n",getStream(task));

	ptr = new_pointer;

	return true;
}

bool DeviceAPI_CUDA_streamed::allocate_chunk(void*& ptr, data_chunk* dc) {

	void* new_pointer = NULL;

	float b = dc->n_bytes / 1024 / 1024;

	LOG(2, printf("Alocating %f mb\n", b)
	;
	)

	cudaError_t err = cudaMalloc((void**) &new_pointer, dc->n_bytes);
	if (cudaErrorMemoryAllocation == err) {
		checkCUDAmemory();
		fprintf(stderr, "WARNING: memory full\n");
		return false;
	}

	__E(cudaMemset(new_pointer, 0, dc->n_bytes));

	ptr = new_pointer;

	return true;
}

/*
 * wait for all operations to be performed
 */
void DeviceAPI_CUDA_streamed::checkAsyncOps(bool blocking) {

	// "almost" the only time it is called
	//checkCUDAError();

	if (blocking)
		//cudaThreadSynchronize();
		__E(cudaStreamSynchronize(retrieveStream));

	copy_to_host_events->check();

}

/*
 * If the Data Library requested a chunk its because the last task that required that chunk has competed
 * Uses a different stream
 *
 */
void DeviceAPI_CUDA_streamed::retrieve_data_chunk(data_chunk* dc, SPLICE_OP op,
		bool blocking) {

	checkAsyncOps();

	cudaEvent_t event;
	__E(cudaEventCreate(&event));

	unsigned long long* tmp;
	switch (dc->domain->n_dim_space) {
	case 2:
		switch (op) {
		case OW:
			uint dst_pitch;
			if (dc->parent_chunk == NULL)
				dst_pitch = dc->pitch * dc->element_size;
			else
				dst_pitch = dc->parent_chunk->pitch
						* dc->parent_chunk->element_size;

			__E(
					cudaMemcpy2DAsync(dc->hook_pointer, dst_pitch,
							dc->getDataPointer(resource_id),
							dc->pitch * dc->element_size,
							dc->domain->Y.length() * dc->element_size,
							dc->domain->X.length(), cudaMemcpyDeviceToHost,
							(cudaStream_t) retrieveStream));

			//		cudaMemcpy2D(dc->hook_pointer,
			//						dst_pitch,
			//						dc->getDataPointer(resource_id), dc->pitch * dc->element_size,
			//						dc->domain->Y.length() * dc->element_size, dc->domain->X.length(),
			//						cudaMemcpyDeviceToHost);
			break;
		default:
			printf("PERFORM: Error, op not defined CUDA");
			break;
		}
		break;

	case 1:
		switch (op) {
		case OW:
			__E(
					cudaMemcpyAsync(dc->hook_pointer,
							dc->getDataPointer(resource_id), dc->n_bytes,
							cudaMemcpyDeviceToHost,
							(cudaStream_t) retrieveStream));
			break;
		case ADD_UINT64:

			tmp = (unsigned long long*) malloc(dc->n_bytes);
			__E(
					cudaMemcpyAsync(tmp, dc->getDataPointer(resource_id),
							dc->n_bytes, cudaMemcpyDeviceToHost,
							(cudaStream_t) retrieveStream));
			break;

		default:
			printf("PERFORM: Error, op not defined CUDA");
			break;
		}

		break;
	default:
		break;
	}

	__E(cudaEventRecord(event, retrieveStream));


	__sync_fetch_and_add(&bytes_transfered, dc->n_bytes);

	copy_to_host_events->add(dc, event);

	if (blocking) {

		__E(cudaEventSynchronize(event));
		checkAsyncOps();

		if (op == ADD_UINT64) {
			unsigned long long* tmp2 = (unsigned long long*) dc->hook_pointer;
			for (int i = 0; i < dc->domain->X.length(); i++) {
				tmp2[i] += tmp[i];
			}
		}

	}

}

void DeviceAPI_CUDA_streamed::shutdown() {

	//checkAsyncOps(true);
	executor->kill();
}

void DeviceAPI_CUDA_streamed::executeTask(Task*t) {

	executor->execute(t);
}

uint DeviceAPI_CUDA_streamed::pending_tasks() {
	//return executor->pending_tasks();
	return executor->getExecuting();
}

void DeviceAPI_CUDA_streamed::executeTaskKernel(Task* t) {

	t->get_task_data_pointers(resource_id);
	cudaStream_t stream = getStream(t); // A stream per task
	t->stream = stream;

	if (t->type == REGULAR) {

		kernel_function_ptr k = t->get_kernel(GPU);

		//	printf("Excuting stream %d to task %d\n",stream,t->getID());

		if (k != NULL) {
			k(t);
		}
	} else {

#ifdef UBK_IMPLEMENTED

		ubkTask* v;
		int size = t->diceTo(v, SIMD);
		k_CUDA_wrapper_uberkernel(t, v, size); //,outbox_buffer,size_out);

#endif

	}

	__E(cudaStreamSynchronize(stream));
	t->setTaskDone(resource_id);

}

int DeviceAPI_CUDA_streamed::opsLeft() {
	return (copy_to_host_events->size());
}

void DeviceAPI_CUDA_streamed::free_chunk(data_chunk* dc) {

	void* pt = dc->getDataPointer(resource_id);
	if (pt != NULL) {
		__E(cudaFree(pt));
		//cudaDeviceSynchronize();

		//printf("chunk %d physically free\n",dc->id);
		//checkCUDAmemory("after free");
		//performDL->print_registry();
	}
}

int DeviceAPI_CUDA_streamed::pending_events() {
	return copy_to_host_events->size();
}

bool DeviceAPI_CUDA_streamed::check_if_memory_available(data_chunk* dc) {

	size_t memAvail;
	__E(cudaMemGetInfo(&memAvail, NULL));

	//checkCUDAmemory("check");

#if defined PATHTRACER
	size_t memAvailPT = memAvail - 619430400;
#elif defined BH
	size_t memAvailPT = memAvail - 645152256;
#else
	size_t memAvailPT = memAvail;
#endif

	if ((size_t) (dc->n_bytes * 1.15) > memAvailPT)
		return false;
	else
		return true;
}

