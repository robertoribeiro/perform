/*
 * cuda_utils.h
 *
 *  Created on: Apr 13, 2012
 *      Author: rr
 */

#ifndef CUDA_UTILS_H_
#define CUDA_UTILS_H_

#include "cuda.h"
#include "cuda_runtime.h"
#include "assert.h"

void inline checkCUDAmemory(char* t = NULL) {

	//cudaDeviceSynchronize();
	size_t free, total;
	cuMemGetInfo(&free, &total);
	fprintf(stdout, "%s mem %ld total %ld\n", t, free / 1024 / 1024,
			total / 1024 / 1024);

}

void inline checkCUDAError(char* t = NULL) {
#ifdef __DEBUG

	cudaError_t err = cudaGetLastError();
	if (cudaSuccess != err) {
		checkCUDAmemory("error");
		fprintf(stderr, "Cuda error %s: %s.\n", t, cudaGetErrorString(err));
		exit(-1);
	}
#endif
}

void inline __E(cudaError_t err) {
	if (cudaSuccess != err) {
		checkCUDAmemory("error");
		fprintf(stderr, "CUDA Runtime API error: %s.\n",
				cudaGetErrorString(err));
		exit(-1);
	}
}

//Round a / b to nearest higher integer value
static int IntDivUp(int a, int b) {
	return (a % b != 0) ? (a / b + 1) : (a / b);
}

// compute grid and thread block size for a given number of elements
static void ComputeGridSize(int n, int blockSize, int &numBlocks,
		int &numThreads) {
	numThreads = min(blockSize, n);
	numBlocks = IntDivUp(n, numThreads);
	assert(numBlocks < 65535);
}

#endif /* CUDA_UTILS_H_ */
