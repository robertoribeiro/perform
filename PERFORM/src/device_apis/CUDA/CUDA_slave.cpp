/*
 * ResourceControl.cpp
 *
 *  Created on: Nov 30, 2010
 *      Author: rr
 */

#include "CUDA_slave.h"
#include "device_apis/CUDA/DeviceAPI_CUDA.h"
#include "tbb/tick_count.h"
#include "datalibrary/DataLibrary.h"

extern DataLibrary* performDL;

void CUDA_executor::run() {

	DeviceAPI_CUDA_streamed* ap = (DeviceAPI_CUDA_streamed*) api;

	cudaSetDevice(ap->m_device_hw_id);

	while (!kill_requested) {

		tick_count t0 = tick_count::now();
		SystemMessage* op = system_operations->pop();
		if (op) {
			execute_operation(op);
			if (system_operations->size() == 0)
				tasks->skip = false; // after executing an op if there is more ops, do not pop tasks
		}


		Task* t = tasks->pop();
		tick_count t1 = tick_count::now();
		tick_count::interval_t elapsed = (t1 - t0);
		TotalIdleTime += elapsed;
		//IdleTime= elapsed;

		if (t != NULL) {

			LOG(2,performDL->print_registry();)

			tick_count t0 = tick_count::now();
			__sync_fetch_and_add(&executing, 1);
			ap->executeTaskKernel(t);
			__sync_fetch_and_sub(&executing, 1);

			time_last_finished = tick_count::now();

			tick_count t1 = tick_count::now();
			tick_count::interval_t elapsed = (t1 - t0);
			TotalExec += elapsed;

			//checkCUDAmemory("execution");
		}

	}
}

void CUDA_executor::execute_operation(SystemMessage* op) {

	switch (op->type) {

	case TASKS_FINISHED:
		resource_manager->stats_setIdleTime(resource_id,tick_count::now()- time_last_finished);
			break;
		case JOB_FINISHED:
			resource_manager->stats_setTotalIdleTime(resource_id, TotalIdleTime,TotalExec);
			op->setDone();
			break;
	case JOB_STARTED:
		TotalIdleTime = tick_count::interval_t();
		break;
	default:
		break;
	}
}

