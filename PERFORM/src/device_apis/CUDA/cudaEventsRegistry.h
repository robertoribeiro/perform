/*
 * cudaEventsRegistry.h
 *
 *  Created on: Jan 25, 2011
 *      Author: rr
 */

#ifndef CUDAEVENTSREGISTRY_H_
#define CUDAEVENTSREGISTRY_H_

#include <iostream>
#include <vector>
#include <typeinfo>
#include <cuda.h>
#include <cuda_runtime.h>
#include "core.h"
#include "datalibrary/DataLibrary.h"

extern DataLibrary* performDL;

using namespace std;

typedef void (*callback)(void);

template<class T>
class cudaEventsRegistry {

private:
	vector<pair<T, cudaEvent_t> > events;
	RESOURCE_ID resource_id;

public:

	/*cudaEventsRegistry(callback e) {
	 eventDoneCallback = e;
	 }*/
	cudaEventsRegistry(RESOURCE_ID dev_) {
		resource_id = dev_;
		// TODO Auto-generated constructor stub

	}
	virtual ~cudaEventsRegistry() {

	}

	void add(T t, cudaEvent_t v) {
		events.push_back(pair<T, cudaEvent_t> (t, v));
	}

	void check() {

		LOG(3,cout << "Checking for events..." << endl;)

		if (events.size() != 0) {
			typename std::vector<pair<T, cudaEvent_t> >::iterator i;
			typename std::vector<pair<T, cudaEvent_t> >::iterator end = events.end();

			for (i = events.begin(); i != end; i++) {

				if (cudaEventQuery(i->second) == cudaSuccess) {
					LOG(2,cout << "Event done!" << endl;)
					data_chunk* dc = ((data_chunk*) (i->first));
					performDL->register_location_update(dc, hostID, R, dc->hook_pointer);

					events.erase(i);
				}
			}
		}
	}

	int size() {
		return events.size();
	}

};

#endif /* CUDAEVENTSREGISTRY_H_ */
