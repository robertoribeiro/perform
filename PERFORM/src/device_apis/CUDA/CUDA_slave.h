/*
 * ResourceControl.h
 *
 *  Created on: Nov 30, 2010
 *      Author: rr
 */

#ifndef RESOURCECONTROLCUDA_SQ_H_
#define RESOURCECONTROLCUDA_SQ_H_

#include <iostream>
#include <stdlib.h>
#include <pthread.h>
#include "ConcurrentQueue.h"
#include "task/Task.h"
#include "core.h"
#include "../../device_apis/DeviceAPI.h"
//#include "../DeviceAPIs/DeviceAPI_CPU.h"
//#include "../kernels.h"
#include "tbb/tick_count.h"
#include "resourcemanager/ResourceControl.h"

using namespace std;
using namespace tbb;


/**
 * With this thread there is no need to use events for task completion awareness
 */
class CUDA_executor {
public:

	RESOURCE_ID resource_id;
	pthread_t thread;
	ConcurrentQueue<Task*>* tasks;
	DeviceAPI* api;


	ConcurrentQueue<SystemMessage*>* system_operations;

	bool kill_requested;


	tick_count time_last_finished;
	tick_count::interval_t TotalIdleTime;
	tick_count::interval_t TotalExec;


private:
	volatile uint executing;

public:

	CUDA_executor(DeviceAPI* api_, RESOURCE_ID dev) {

		resource_id = dev;
		api = api_;
		tasks = new ConcurrentQueue<Task*> (true);
		executing = 0;
		system_operations = new ConcurrentQueue<SystemMessage*> (false);

	//TotalIdleTimes = 0;a

		//pthread_mutex_init(&critical_section, NULL);

		TotalExec = tick_count::interval_t();

	}

	virtual ~CUDA_executor() {

	}

	uint getExecuting(){
		return tasks->size();
	}

	void kill() {
		kill_requested = true;
		tasks->kill_requested = true;
		pthread_cond_broadcast(&(tasks->cond_empty));
		pthread_join(thread, 0);
	}

	void start() {
		kill_requested = false;

		pthread_create(&thread, NULL, &CUDA_executor::start_thread, this);
	}

	static void* start_thread(void* p) {
		reinterpret_cast<CUDA_executor *> (p)->run();
		return NULL;
	}

	void run();

	void execute(Task* w) {
		tasks->push(w);
		LOG(2,cout << "CUDA kernel added" << endl;)
	}

	int pending_tasks(){
		return tasks->size();
	}

	void wake() {
		pthread_cond_signal(&(tasks->cond_empty));
	}

	void printQueue() {
		cout << "ResourceID " << resource_id << " queue has " << tasks->size() << " tasks" << endl;
	}

	void execute_operation(SystemMessage* op);

	void add_operation(SystemMessage* w) {

		system_operations->push(w);
		tasks->skip = true;
		LOG(2,cout << "operation added" << endl;)
		pthread_cond_broadcast(&(tasks->cond_empty));
	}
};

#endif /* RESOURCECONTROLCUDA_SQ_H_ */
