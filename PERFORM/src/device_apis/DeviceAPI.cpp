/*
 * DeviceAPI.cpp
 *
 *  Created on: Dec 23, 2010
 *      Author: rr
 */

#include "DeviceAPI.h"
#include "datalibrary/DataLibrary.h"
#include "taskcontainer/TaskContainer.h"

extern DataLibrary* performDL;
extern TaskContainer* performQ;

DeviceAPI::DeviceAPI(RESOURCE_ID ids) {

	resource_id = ids;
	bytes_transfered = 0;

}

void DeviceAPI::init() {
	cout << "WARNING: using empty method init" << endl;

}

void DeviceAPI::shutdown() {
	cout << "WARNING: using empty method shutdown" << endl;
}

uint DeviceAPI::pending_tasks() {
	return 0;
}
bool DeviceAPI::copy_chunk_to_device(void*& v, data_chunk* dc, Task* t) {
	cout << "WARNING: using empty method copy_chunk_to_device" << endl;
	return NULL;
}

bool DeviceAPI::allocate_chunk(void*& v, data_chunk* dc) {
	cout << "WARNING: using empty method allocate_chunk" << endl;
	return NULL;
}

void DeviceAPI::executeTask(Task* t) {

	cout << "WARNING: using empty executioner executeTask" << endl;

}

void DeviceAPI::processTask(Task* t) {

	LOG(0.5,
			cout << "Processing task " << t->getID() << " in resource "
					<< resource_id << endl
			;
	)

	setTaskDataReady(t);

}

bool DeviceAPI::setTaskDataReady(Task* t) {

	bool copySuccess;

	checkAsyncOps();

	for (ListNode<taskAssociated_chunk>* __it = t->data_chunks->getIterator();
			t->data_chunks->canStep(__it); t->data_chunks->step(__it)) {

		data_chunk* i = t->data_chunks->getCursorValue(__it).first;
		PERMISSION_T i_p = t->data_chunks->getCursorValue(__it).second;

		if (i_p == WD) {
			if (!performDL->device_has_chunk(resource_id, i)) {
				void * data;
				copySuccess = allocate_chunk(data, i);

				if (copySuccess)
					performDL->register_location_update(i, resource_id, i_p,
							data);
				else {
					performDL->revertTaskDataReady(t, resource_id);
					return copySuccess;
				}
			}

		} else if (i_p == W) {
			if (performDL->device_has_chunk(resource_id, i))
				performDL->register_write_request(i, resource_id);
			else {
				void * data;
				copySuccess = allocate_chunk(data, i);

				if (copySuccess)
					performDL->register_location_update(i, resource_id, i_p,
							data);
				else {
					performDL->revertTaskDataReady(t, resource_id);
					return copySuccess;
				}
			}

		} else if (!(performDL->device_has_a_valid_copy(resource_id, i))) {

			void * data;
			copySuccess = copy_chunk_to_device(data, i, t);

			if (copySuccess)
				performDL->register_location_update(i, resource_id, i_p, data);
			else {
				performDL->revertTaskDataReady(t, resource_id);
				return copySuccess;
			}

		} else {
			/**
			 * the chunk is already there, invalidate copies in RW case, and remove SaP tag in S case
			 * Attention id state is M?
			 * */
			if (i_p == RW)
				performDL->register_write_request(i, resource_id);
			else {
				performDL->register_share_request(i, resource_id);
			}
		}

		if (i_p == R)
			performDL->mark_queried(i, resource_id);
	}
	//t->data_chunks->unlock();

	//performDL->print_registry();

	return true;

}

//void DeviceAPI::setTaskDataReady(Task* t) {
//
//	checkAsyncOps();
//
//	t->data_chunks->lock();
//	for (t->data_chunks->gotoBeginning(); t->data_chunks->canStep(); t->data_chunks->step()) {
//
//		data_chunk* i = t->data_chunks->getCursor().first;
//		PERMISSION_T i_p = t->data_chunks->getCursor().second;
//
//		if (!(performDL->device_has_a_valid_copy(resource_id, i))) {
//
//			if (!performDL->device_has_a_valid_copy(hostID, i)) {
//				performDL->print_registry();
//				fprintf(stderr, "WARNING: chunk %d may be in an invalid state\n", i->id);
//			}
//
//			void * data = copy_chunk_to_device(i, t->getID());
//
//			performDL->register_location_update(i, resource_id, i_p, data);
//
//		} else {
//			/**
//			 * the chunk is already there, invalidate copies in RW case, and remove SaP tag in S case
//			 * Attention id state is M?
//			 * */
//			if (i_p == RW)
//				performDL->register_write_request(i, resource_id);
//			else {
//				performDL->register_share_request(i, resource_id);
//			}
//		}
//
//		if (i_p == R)
//			performDL->mark_queried(i, resource_id);
//	}
//	t->data_chunks->unlock();
//
//}

void DeviceAPI::retrieveTaskDataToHost(Task* t) {

	//t->data_chunks->lock();
	for (ListNode<taskAssociated_chunk>* __it = t->data_chunks->getIterator();
			t->data_chunks->canStep(__it); t->data_chunks->step(__it)) {
		//for (t->data_chunks->gotoBeginning(); t->data_chunks->canStep(); t->data_chunks->step()) {
		data_chunk* i = t->data_chunks->getCursorValue(__it).first;
		if (!(performDL->device_has_a_valid_copy(hostID, i))) {
			retrieve_data_chunk(i, OW); /* be aware of asyncrony Analyze */
		}
	}
	//t->data_chunks->unlock();

	checkAsyncOps();
}

void DeviceAPI::retrieve_data_chunk(data_chunk* dc, SPLICE_OP op,
		bool blocking) {
	cout << "WARNING: using empty retrieve_data_chunk retrieve_data_chunk"
			<< endl;

}

void DeviceAPI::checkAsyncOps(bool blocking) {
	//cout << "ATTENTION: using empty flusher for device " << resource_id << endl;
}

int DeviceAPI::opsLeft() {
	return 0;
}

void DeviceAPI::free_chunk(data_chunk* dc) {
	cout << "WARNING: using empty freerer free_chunk" << endl;
}

bool DeviceAPI::check_if_memory_available(data_chunk* dc) {
	return true;
}

