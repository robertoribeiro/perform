/*
 * DeviceAPI_CPU.h
 *
 *  Created on: Dec 1, 2010
 *      Author: rr
 */

#ifndef DEVICEAPI_TBB_H_
#define DEVICEAPI_TBB_H_

#include <iostream>
#include <string>
#include <algorithm>
#include "../DeviceAPI.h"

#include "tbb/blocked_range.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"


using namespace tbb;
using namespace std;

class DeviceAPI_CPU: public DeviceAPI {
public:
 	DeviceAPI_CPU(int n_tbb_threads, RESOURCE_ID ids);

	DeviceAPI_CPU(RESOURCE_ID ids);

	virtual ~DeviceAPI_CPU();

	void executeTask(Task* t, bool blocking) {}

	void executeTask(Task* t);

	void retrieve_data_chunk(data_chunk* dc, SPLICE_OP op,bool blocking);

	bool copy_chunk_to_device(void*& ptr, data_chunk* dc,Task* t);

	bool allocate_chunk(void*& ptr, data_chunk* dc);

	void free_chunk(data_chunk* dc);

	void shutdown(){

	}

	void init();

};

#endif /* DEVICEAPI_TBB_H_ */
