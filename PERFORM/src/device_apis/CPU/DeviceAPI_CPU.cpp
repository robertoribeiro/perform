/*
 * DeviceAPI_CPU.cpp
 *
 *  Created on: Dec 1, 2010
 *      Author: rr
 */

#include "DeviceAPI_CPU.h"

#include "datalibrary/DataLibrary.h"

extern DataLibrary* performDL;

using namespace tbb;

DeviceAPI_CPU::DeviceAPI_CPU(int n_tbb_threads, RESOURCE_ID ids) :
	DeviceAPI(ids) {
}

DeviceAPI_CPU::DeviceAPI_CPU(RESOURCE_ID ids) :
	DeviceAPI(ids) {

}

void DeviceAPI_CPU::init() {


}

void DeviceAPI_CPU::executeTask(Task* t) { /* Analyze dependent of task implementation/specification */

	kernel_function_ptr k = t->get_kernel(CPU);
	void** data_pointers;
	void** kernel_args;
	if (k != NULL) {
		t->get_task_data_pointers(resource_id);
		//kernel_args = params->get_kernel_args();
		//k(data_pointers,kernel_args,0);
		k(t);
		t->setTaskDone(resource_id);
	}
}


DeviceAPI_CPU::~DeviceAPI_CPU() {

}

void DeviceAPI_CPU::free_chunk(data_chunk* dc) {
	void* pt = dc->getDataPointer(resource_id);
	//if (pt != NULL)
	//	free(dc->getDataPointer(resource_id));

}

bool DeviceAPI_CPU::copy_chunk_to_device(void*& ptr, data_chunk* dc,Task* t) {

	address new_pointer = NULL;

	float b = dc->n_bytes / 1024 / 1024;

	LOG(2,printf("Allocating %f mb\n",b);)

	new_pointer = malloc(dc->n_bytes);

	// TODO always requesting stream, not required if stored in task object!

	__sync_fetch_and_add(&bytes_transfered, dc->n_bytes);
	byte_address data;
	int num_lines;
	void* src;
	void* dst;
	int length;

	switch (dc->domain->n_dim_space) {
	case 2:

		num_lines = dc->domain->X.length();
		data = (byte_address) (dc->hook_pointer);
		for (int i = 0; i < num_lines; i++) {
			src = data + i * dc->parent_chunk->pitch * dc->parent_chunk->element_size;
			dst = ((byte_address) new_pointer) + i * dc->pitch * dc->element_size;
			length = dc->domain->Y.length();
			memcpy(dst, src, length * dc->parent_chunk->element_size);
		}
		break;
	case 1:
		data = (byte_address) (dc->hook_pointer);
		src = data;
		length = dc->domain->X.length() * dc->parent_chunk->element_size;
		memcpy(new_pointer, src, length);
		break;
	default:
		break;
	}

	ptr = new_pointer;

	return true;
}

bool DeviceAPI_CPU::allocate_chunk(void*& ptr, data_chunk* dc) {

	void* new_pointer = NULL;

	float b = dc->n_bytes / 1024 / 1024;

	LOG(2,printf("Alocating %f mb\n",b);)

	new_pointer = malloc(dc->n_bytes);
	memset(new_pointer, 0, dc->n_bytes);

	ptr = new_pointer;

	return true;
}


//void DeviceAPI_CPU::retrieve_data_chunk(data_chunk* dc, bool blocking) {
//	performDL->register_location_update(dc, hostID, R, dc->hook_pointer);
//}


void DeviceAPI_CPU::retrieve_data_chunk(data_chunk* dc,  SPLICE_OP op , bool blocking) {

	byte_address dst_hook;
	byte_address src_hook;

	int num_lines;
	void* src;
	void* dst;
	int length;

	if (dc->getDataPointer(resource_id) == dc->hook_pointer) return;

	switch (dc->domain->n_dim_space) {
	case 2:

		num_lines = dc->domain->X.length();
		dst_hook = (byte_address) (dc->hook_pointer);
		src_hook = (byte_address) dc->getDataPointer(resource_id);

		for (int i = 0; i < num_lines; i++) {
			dst = dst_hook + i * dc->parent_chunk->pitch * dc->parent_chunk->element_size;
			src = src_hook + i * dc->pitch * dc->element_size;
			length = dc->domain->Y.length();
			memcpy(dst, src, length * dc->parent_chunk->element_size);
		}
		break;
	case 1:

		dst_hook = (byte_address) (dc->hook_pointer);
		src_hook = (byte_address) dc->getDataPointer(resource_id);
		dst = dst_hook;

		length = dc->domain->X.length() * dc->parent_chunk->element_size;
		memcpy(dst, src_hook, length);
		break;
	default:
		break;
	}


	__sync_fetch_and_add(&bytes_transfered, dc->n_bytes);

}

