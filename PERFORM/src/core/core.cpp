//#include "perform.h"

#include "taskmapper/TaskMapper_DD_DUM.h"
#include "taskmapper/TaskMapper_DD_DYN.h"
#include "taskmapper/TaskMapper_RR_DUM.h"
#include "taskmapper/TaskMapper_WRR_DUM.h"
#include "taskcontainer/TaskContainerQueue.h"
#include "device_apis/CUDA/DeviceAPI_CUDA.h"
#include "device_apis/CPU/DeviceAPI_CPU.h"
#include "perform_utils.h"
#include "perform.h"

int tasks_to_execute;
pthread_mutex_t tasks_to_execute_mutex;
unsigned int taskID_incrementor;
RESOURCE_ID hostID = CPU_0; /* strictly necessary for DL initialization */

ResourceManager* resource_manager;
DataLibrary* performDL;
TaskContainer* performQ;
TaskMapper* performMAPPER;
PerformanceModel* performPM;
PERFORM_timer *performPROF = new PERFORM_timer();

const char* get_enum_string(int _enum) {

	const char* r;
	switch (_enum) {
	case CPU:
		r = "CPU";
		break;
	case GPU:
		r = "GPU/CUDA";
		break;
	case M:
		r = "M";
		break;
	case MS:
			r = "MS";
			break;
	case I:
		r = "I";
		break;
	case S:
		r = "S";
		break;
	case SaP:
		r = "SaP";
		break;
	case Q:
		r = "Q";
		break;
	case NA:
		r = "NA";
		break;
	case CPU0:
		r = "CPU0";
		break;
	case GPU2:
		r = "GPU2";
		break;
	case GPU0:
		r = "GPU0";
		break;
	case GPU1:
		r = "GPU1";
		break;
	case GPU3:
		r = "GPU3";
		break;
	case RR_DUM:
		r = "RR_DUM";
		break;
	case WRR_DUM:
		r = "WRR_DUM";
		break;
	case DD_DUM:
		r = "DD_DUM";
		break;
	case DD_DYN:
		r = "DD_DYN";
		break;
	default:
		r = "UNKNOWN";
		break;
	}
	return r;//c_str();
}

int add_tasks_to_execute(int v) {

	int r;
	pthread_mutex_lock(&tasks_to_execute_mutex);
	tasks_to_execute = tasks_to_execute + v;
	LOG(1,cout << "Tasks to execute:" << tasks_to_execute << endl;)
	r = tasks_to_execute;
	pthread_mutex_unlock(&tasks_to_execute_mutex);

	return r;
}

void* PERFORM_alloc(size_t size) {
	void *p;
	__E(cudaHostAlloc(&p, size, cudaHostAllocDefault));



	//cudaMallocHost(&p, size );

	//p = malloc(size);
	//cudaHostRegister(p,size,cudaHostRegisterPortable);

	//printf("Allocating %d mb\n",size/1024/1024);
	//p =(void*) new char[size];

	return p;
}

void PERFORM_free(void* p) {

	__E(cudaFreeHost(p));
	//delete[] (char*)p;
	//checkCUDAError();

}

void initPERFORM(int sch, int dev_config, int initial_dice, int cpu_flops) {

	srand(time(NULL));

	hostID = CPU_0;
	tasks_to_execute = 0;
	taskID_incrementor = 0;
	pthread_mutex_init(&tasks_to_execute_mutex, NULL);

	performQ = new TaskContainerQueue();

	resource_manager = new ResourceManager();

	performDL = new DataLibrary(resource_manager->device_count);

	initDeviceConfig(dev_config,cpu_flops);

	switch (sch) {
	case 0:
		performMAPPER = new TaskMapper_RR_DUM(performQ);
		break;
	case 1:
		performMAPPER = new TaskMapper_WRR_DUM(performQ);
		break;
	case 2:
		performMAPPER = new TaskMapper_DD_DUM(performQ,initial_dice);
		break;
	case 3:
		performMAPPER = new TaskMapper_DD_DYN(performQ, initial_dice);
		break;
	default:
		break;
	}

	performPM = new FLOPSPerformanceModel();

	performMAPPER->start();

	resource_manager->startControllers();

	LOG(0.1,cout << "PERFORM initialized..." << endl;)

	//#############################################
}

void wait_for_all_tasks() {
	// wait for all done
	int atts = 0;
	while (/*performMAPPER->hasWork() || */tasks_to_execute != 0) {
		LOG(3,cout << "Tasks left!" << endl;)
		//sleep(1);
		pthread_yield();
		//atts++;
		//if (atts > 15) {
			//LOG(0.5,resource_manager->printAllQueues();)
		//}
		//((DataLibraryMSIbased*) (performDL))->print_registry();
		//resource_manager->checkAsyncOps();
	}
}

void wait_for_all_retrievals() {

	//performDL->retrieve_off_host_chunks();

	while (!(resource_manager->allDone())) {
		LOG(1,cout << "Operations left!" << endl;)
		//resource_manager->wakeWorkers();
		sleep(1);
		//((DataLibraryMSIbased*) (performDL))->print_registry();
		LOG(0.5,resource_manager->printAllOpsQueues();)
	}
}

void wait_stopPERFORM() {

	wait_for_all_tasks();
	wait_for_all_retrievals();

}

void shutdownPERFORM() {
	/*
	 * kill everything
	 * also deletes heap memory
	 * and check for copies done
	 */
	wait_for_all_retrievals();

	//LOG(0.5,performDL->print_registry();)

	//resource_manager->stats_setVolumeDataTransf(CPU0, performDL->bytes_transfered);

	delete performDL;
	wait_for_all_retrievals();

	resource_manager->stats_print();

	resource_manager->killControllers();
	performMAPPER->kill();
	delete resource_manager;
	delete performMAPPER;

	//#############################################
	/*ticks t0 = getticks();

	 sleep(1);
	 ticks t1 = getticks();
	 double clocks = elapsed(t1, t0);*/

}

/**
 * Just tells de conttrollers to store their idle time when the job is finished, when the user has the results.
 */
void PERFORM_signal_JobFinished(double time) {

	std::map<RESOURCE_ID, ResourceControl*>::iterator i;
	for (i = resource_manager->controllers.begin(); i != resource_manager->controllers.end(); i++) {
		SystemMessage* op = new SystemMessage(JOB_FINISHED, NULL, i->second);
		(i->second)->add_operation(op);
		op->wait_completion();

	}

	resource_manager->stats_setTotalTime(time);

}

void PERFORM_signal_taskSubmissionEnded() {

	std::map<RESOURCE_ID, ResourceControl*>::iterator i;
	for (i = resource_manager->controllers.begin(); i != resource_manager->controllers.end(); i++) {
		SystemMessage* op = new SystemMessage(TASKS_FINISHED, NULL, i->second);
		(i->second)->add_operation(op);
		//op->wait_completion();

	}

}

void PERFORM_signal_JobStarted() {
	SystemMessage* op = new SystemMessage(JOB_STARTED, NULL);

	std::map<RESOURCE_ID, ResourceControl*>::iterator i;
	for (i = resource_manager->controllers.begin(); i != resource_manager->controllers.end(); i++) {
		(i->second)->add_operation(op);
	}
}

