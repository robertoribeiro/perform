/*
 * TaskContainerQueue.h
 *
 *  Created on: Nov 24, 2010
 *      Author: rr
 */

#ifndef TASKCONTAINERQUEUE_H_
#define TASKCONTAINERQUEUE_H_


#include "ConcurrentQueue.h"
#include "TaskContainer.h"

class TaskContainerQueue : public TaskContainer {

private:
	//list<Task*> tasklist;
	//queue<Task*,list<Task*> > tasks;// TODO

	ConcurrentQueue<Task* >* tasks;


public:
	TaskContainerQueue();
	virtual ~TaskContainerQueue();

	Task* getTask();
	Task* getTaskNonBlocking();
	void addTasks(vector<Task*>*);
	//void addTasks(Task_ubk* t);
	void addTasks(Task* t);
	//void addTasks(Task* set, int max_size,Task_ubk* main_task);
	void addTasks(Task** tasksIN,int count);

	void remapTasks(Task* t);


	bool hasWork(){

		return !tasks->empty();
	}

	void kill(){
		tasks->kill_requested = true;
		pthread_cond_broadcast(&(tasks->cond_empty));

	}

	void printQueue() {
			cout << "TaskContainer has " << tasks->size() << " tasks" << endl;
		}
};

#endif /* TASKCONTAINERQUEUE_H_ */
