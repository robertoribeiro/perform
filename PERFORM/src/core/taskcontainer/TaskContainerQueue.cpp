/*
 * TaskContainerQueue.cpp
 *
 *  Created on: Nov 24, 2010
 *      Author: rr
 */

#include "TaskContainerQueue.h"
#include "task/Task.h"
#include <iostream>
#include <stdlib.h>

TaskContainerQueue::TaskContainerQueue() {
	// TODO Auto-generated constructor stub
	tasks = new ConcurrentQueue<Task*> (true);

}

TaskContainerQueue::~TaskContainerQueue() {
	// TODO Auto-generated destructor stub
}

void TaskContainerQueue::addTasks(vector<Task*>* tasksIN) {

	for (int i = 0; i < tasksIN->size(); i++) {
		tasks->push(tasksIN->at(i));
		//task_to_execute++;
		add_tasks_to_execute(1);
		LOG(1,cout << "Task added\n";)

	}

}

void TaskContainerQueue::addTasks(Task** tasksIN, int count) {

	for (int i = 0; i < count; i++) {
		tasks->push(tasksIN[i]);
		//task_to_execute++;
		add_tasks_to_execute(1);
		LOG(1,cout << "Task added\n";)

	}

}

void TaskContainerQueue::addTasks(Task* t) {

	tasks->push(t);
	add_tasks_to_execute(1);
	LOG(1,cout << "Task added\n";)

}

void TaskContainerQueue::remapTasks(Task* t) {

	tasks->push(t);
	LOG(1,cout << "Task added\n";)

}

Task* TaskContainerQueue::getTask() {

	Task * r = tasks->pop();

	return r;
}

Task* TaskContainerQueue::getTaskNonBlocking() {

	tasks->temp_no_block = true;
	Task * r = tasks->pop();
	tasks->temp_no_block = false;
	return r;
}

