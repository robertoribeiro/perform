/*
 * TaskContainer.h
 *
 *  Created on: Nov 24, 2010
 *      Author: rr
 */

#ifndef TASKCONTAINER_H_
#define TASKCONTAINER_H_

#include "task/Task.h"

class TaskContainer {
public:

	TaskContainer() {

	}
	;
	virtual ~TaskContainer();

	virtual Task* getTask()=0; /* Returns an array with n tasks */
	virtual Task* getTaskNonBlocking() =0;
	virtual void addTasks(vector<Task*>*)=0; /* Appends tasks to the container */
	// virtual void addTasks(Task_ubk* t)=0;
	virtual void addTasks(Task* t)=0; /* Appends tasks to the container */
	virtual void addTasks(Task** tasksIN, int count)=0;

	// virtual void addTasks(Task* set, int max_size,Task_ubk* main_task)=0;
	virtual bool hasWork()=0;
	virtual void printQueue()=0;
	virtual void kill()=0;
	virtual void remapTasks(Task* t)=0;

};

#endif /* TASKCONTAINER_H_ */
