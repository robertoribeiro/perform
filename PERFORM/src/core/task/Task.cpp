/*
 * Task.cpp
 *
 *  Created on: Nov 23, 2010
 *      Author: rr
 */

#include "Task.h"
#include "../datalibrary/DataLibrary.h"
#include "../datalibrary/Domain.h"

extern DataLibrary* performDL;
extern unsigned int taskID_generator;

using namespace std;

void Task::setTaskDone(RESOURCE_ID in) {
	pthread_mutex_lock(mutex);
	performDL->register_task_completion(this, in);
	done = true;
	add_tasks_to_execute(-1);
	pthread_mutex_unlock(mutex);
	pthread_cond_broadcast(completion_wait_line);
}

void Task::wait_completion() {
	pthread_mutex_lock(mutex);
	if (!done) {
		//printf("Not done, waiting...\n");
		pthread_cond_wait(completion_wait_line, mutex);
	}
	pthread_mutex_unlock(mutex);
}

void Task::set_assigned_to(RESOURCE_ID in) {
	assigned_to = in;
}

RESOURCE_ID Task::get_assigned_to() {
	return assigned_to;
}

bool Task::Done() {
	return done;
}

unsigned int Task::getID() {
	return taskID;
}

void Task::setID() {
	taskID = __sync_fetch_and_add(&taskID_incrementor, 1);
}

void Task::add_data_chunk(data_chunk* dc, PERMISSION_T p) {


	data_chunks->insert(taskAssociated_chunk(dc, p));


}

/**
 * In the case of associating a domain already assocaited, associated the WRITE domains in last. This domain is the one pointed by the chunk.
 */
void Task::associate_domain(Domain_* dmn) {

	domains->insert(dmn);

}

void Task::associate_domain(Task* t) {

	for (ListNode<Domain_*>* __it = t->domains->getIterator();t->domains->canStep(__it);t->domains->step(__it)) {
		domains->insert(domains->getCursorValue(__it));
	}

}

void Task::associate_kernel(RESOURCE_TYPE t, kernel_function_ptr func) {

	kernels[t % 100] = func;

}

void Task::associate_kernel(Task* t) {

	kernels[0] = t->kernels[0];
	kernels[1] = t->kernels[1];


}

kernel_function_ptr Task::get_kernel(RESOURCE_TYPE t) {

	return kernels[t % 100];
}

/*
 * Return an array all the kernel arguments: data pointer + function args
 */
void Task::get_task_data_pointers(RESOURCE_ID dev) {

	//data_chunks->lock();
	unsigned int length = data_chunks->length;

	void** data_pointers_buf = new void*[length];

	int index = 0;


	for (ListNode<taskAssociated_chunk>* __it = data_chunks->getIterator();data_chunks->canStep(__it);data_chunks->step(__it)) {
	//for (data_chunks->gotoBeginning(); data_chunks->canStep(); data_chunks->step()) {
		data_chunk* j = data_chunks->getCursorValue(__it).first;
		data_pointers_buf[index] = j->getDataPointer(dev);
		index++;


	}
	//data_chunks->unlock();

	data_pointers = data_pointers_buf;



}

