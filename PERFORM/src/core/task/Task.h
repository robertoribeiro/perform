/*
 * Task.h
 *
 *  Created on: Nov 23, 2010
 *      Author: rr
 */

#ifndef TASK_H_
#define TASK_H_

#include <iostream>

#include "../performancemodel/PerformanceModel.h"
#include "../datalibrary/data_chunk.h"
#include "../datalibrary/Domain.h"
#include "core.h"
#include <stdlib.h>
#include <vector>
#include <map>

#include "../List.h"

class Task;

enum TASK_STATUS {
	INVALID = 0, VALID = 0xFFFFFFFF
};

using namespace std;

typedef void (*kernel_function_ptr)(Task*);

class taskAssociated_chunk {

public:
	data_chunk* first;
	PERMISSION_T second;

	__HD__
	taskAssociated_chunk(data_chunk* first_, PERMISSION_T second_) {
		first = first_;
		second = second_;

	}

	__HD__
	taskAssociated_chunk() {
	}

	__HD__
	~taskAssociated_chunk() {
	}

};

class func_table {

public:
	RESOURCE_TYPE first;
	kernel_function_ptr second;

	__HD__
	func_table(RESOURCE_TYPE first_, kernel_function_ptr second_) {
		first = first_;
		second = second_;

	}

	__HD__
	~func_table() {
	}

};

class ubkTask {
public:

	TASK_STATUS init;

	__H_D__
	ubkTask() {

		init = INVALID;

	}

	__H_D__
	virtual ~ubkTask() {
	}

};

class Task {

public:

	void** data_pointers;
	cudaStream_t stream;
	TASK_STATUS init;
	int kernel_id;
	RESOURCE_ID device_preference;
	void* parent_host_task;
	TASK_TYPE type;
	bool done;
	bool diced;
	bool diceable;
	int dice_lvl;
	PM_Metric_normalized dice_value;

	PM_Metric_normalized wl;

	float job_elements;


	pthread_mutex_t* mutex;
	pthread_cond_t* completion_wait_line;

	LList<Domain_*>* domains;
	LList<taskAssociated_chunk>* data_chunks;

public:
	kernel_function_ptr kernels[MAX_ARCHITECTURES];

	//	Task params;

private:
	unsigned int taskID;
	RESOURCE_ID assigned_to;

public:

	__H_D__
	Task() {

		diceable = true;
		diced = false;
		init = INVALID;
		done = false;
		assigned_to = NONE;
		device_preference = NONE;
		dice_lvl = 0;
		stream = NULL;
		wl = PM_Metric_normalized(1);
		//domains = new LList<Domain_*>();
		//data_chunks = new LList<taskAssociated_chunk>();
	}

	Task(TASK_TYPE t_) {

		diceable = false;
		diced = false;
		init = VALID;
		type = t_;
		done = false;
		assigned_to = NONE;
		device_preference = NONE;
		dice_lvl = 0;
		stream = NULL;
		wl = PM_Metric_normalized(1);

		domains = new LList<Domain_*> ();
		data_chunks = new LList<taskAssociated_chunk> ();

		mutex = new pthread_mutex_t();
		completion_wait_line = new pthread_cond_t();

		pthread_mutex_init(mutex, NULL);
		pthread_cond_init(completion_wait_line, NULL);

		setID();

	}

	__H_D__
	~Task() {
	}

	/**
	 * When the task is single executed in the device
	 */
	void setTaskDone(RESOURCE_ID in);

	void set_assigned_to(RESOURCE_ID in);

	RESOURCE_ID get_assigned_to();

	/**
	 * When the task is executed underneath a task set
	 */
	void setTaskDone();
	void wait_completion();

	bool Done();

	unsigned int getID();

	void setID();

	void add_data_chunk(data_chunk* dc, PERMISSION_T p);

	void associate_domain(Domain_* dmn);

	void associate_domain(Task* t);

	void associate_kernel(RESOURCE_TYPE t, kernel_function_ptr func);

	void associate_kernel(Task* t);

	kernel_function_ptr get_kernel(RESOURCE_TYPE t);

	/*
	 * Return an array all the kernel arguments: data pointer + function args
	 */
	void get_task_data_pointers(RESOURCE_ID dev);

	virtual int dice(Task**& new_tasks_, PM_Metric_normalized m) {
		return 0;
	}

	virtual int diceTo(ubkTask*& new_tasks_, int f) {
		return 0;
	}

	/**
	 * used by the user inside the kernels. It associated the correct device data pointer when invoked
	 */
	template<typename T>
	__H_D__
	void getDomain(int index, Domain<T>& shell) {

		if (domains->length == 0) {
			printf("ERROR: domain count 0\n");
			return;
		}

		int it = 0;
		ListNode<Domain_*>* __it = domains->getIterator();
		for (; domains->canStep(__it); domains->step(__it)) {
			//for (domains->gotoBeginning(); domains->canStep(); domains->step()) {
			if (it == index)
				break;
			it++;
		}

		if (it <= index) {

			Domain_* a = domains->getCursorValue(__it);
			a->setDeviceDataPointer(data_pointers[index]);
			Domain<T>* d = (Domain<T>*) (a);

			shell = *d;
		}

	}

	/*
	 * used for dice purpose
	 */
	template<typename T>
	__H_D__
	void getDomain(int index, Domain<T>*& shell) {

		//domains->lock();
		if (domains->length == 0) {
			printf("ERROR: domain count 0\n");
			return;
		}

		int it = 0;
		ListNode<Domain_*>* __it = domains->getIterator();
			for (; domains->canStep(__it); domains->step(__it)) {
		//for (domains->gotoBeginning(); domains->canStep(); domains->step()) {
			if (it == index)
				break;
			it++;
		}

		if (it <= index) {
			Domain<T>* d = (Domain<T>*) (domains->getCursorValue(__it));
			shell = d;
		}
		//domains->unlock();

	}

};

#endif /* TASK_H_ */
