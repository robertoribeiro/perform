/*
 * TaskMapperFIFO.h
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#ifndef SCHEDULERDD_DYN_H_
#define SCHEDULERDD_DYN_H_

#include "TaskMapper.h"
#include <map>
#include <utility>
#include <iostream>
#include <pthread.h>

using namespace std;



class TaskMapper_DD_DYN : public TaskMapper {
public:

	float INITIAL_DICE;
	uint DD_FACTOR;

	ConcurrentQueue<SystemMessage*>* task_requests;

	map<RESOURCE_ID, ConcurrentQueue<Task*>* > work_queues;

	pthread_t thread;

	int work_steal_round_r;

	bool kill_requested;


	TaskMapper_DD_DYN(TaskContainer* taskfeeder, uint initial) : TaskMapper(DD_DYN,taskfeeder){

		DD_FACTOR = initial;
		INITIAL_DICE = 1/float(initial);
		kill_requested = false;
		task_requests = new ConcurrentQueue<SystemMessage*> (true);
		work_steal_round_r=0;

		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = resource_manager->controllers.begin(); i != resource_manager->controllers.end(); i++) {
			work_queues[i->first] = new ConcurrentQueue<Task*> (false);
		}

	}
	virtual ~TaskMapper_DD_DYN();


	void start() {
			kill_requested = false;

			pthread_create(&thread, NULL, &TaskMapper_DD_DYN::start_thread, this);
		}

	void kill() {
			kill_requested = true;
			task_feeder->kill();
			pthread_join(thread, 0);
		}

	static void* start_thread(void* p) {
			reinterpret_cast<TaskMapper_DD_DYN *> (p)->run_loop();
			return NULL;
		}

	void add_operation(SystemMessage* w) {
			task_requests->push(w);

	}

	Task* work_steal();


private:
	void run_loop();


};

#endif /* SCHEDULER_H_ */
