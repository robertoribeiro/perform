#include "TaskMapper_WRR_DUM.h"
#include <math.h>

TaskMapper_WRR_DUM::~TaskMapper_WRR_DUM() {
	// TODO Auto-generated destructor stub
}

void TaskMapper_WRR_DUM::run_loop() {

	LOG(1,cout << "Mapper loop thread running..." << endl;)

	RESOURCE_ID target;
	while (!kill_requested) {
		Task* task = task_feeder->getTask();
		if (task != NULL) {
			if (task->dice_lvl == 0 && task->diceable != 0) {
				Task** v;
				int task_count = task->dice(v, task->dice_value);
				if (task_count > 1) {
					task_feeder->addTasks(v, task_count);
					task->done = true;
					add_tasks_to_execute(-1);
				} else {
					if (map_task(task, target)) {

						//if (task->device_preference != "") target = task->device_preference;
						if (task->device_preference != NONE)
							target = task->device_preference;
						//target="GPU_0";
						LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << target << endl;)

						resource_manager->addWork(task, target);
						task->set_assigned_to(target);
						// Analyze moment where task splitting is done and implemented?
					} else {
						//sleep this copy_thread or something
					}
				}
			} else {
				if (map_task(task, target)) {

					//if (task->device_preference != "") target = task->device_preference;
					if (task->device_preference != NONE)
						target = task->device_preference;
					//target="GPU_0";
					LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << target << endl;)

					resource_manager->addWork(task, target);
					task->set_assigned_to(target);
					// Analyze moment where task splitting is done and implemented?
				} else {
					//sleep this copy_thread or something
				}
			}
		}
	}

	int* y = new int();
	*y = 1001;
	pthread_exit((void*) y);
}

bool TaskMapper_WRR_DUM::map_task(Task* task, RESOURCE_ID& res) {

	if (rounds.size() == 0) {

		float ratio = 1 / resource_manager->resources_info.systemTPFS.value;

		std::map<RESOURCE_ID, ResourceInfo*>::iterator item;

		float min = resource_manager->resources.begin()->second->TPFs.value;
		for (item = resource_manager->resources.begin(); item != resource_manager->resources.end(); item++) {
			if (!item->second->idle) {
				if (item->second->TPFs.value < min)
					min = item->second->TPFs.value;

			}
		}

		min = min * ratio;

		for (item = resource_manager->resources.begin(); item != resource_manager->resources.end(); item++) {
			if (!item->second->idle) {
				pair<int, int> a = pair<int, int> (
						(int) floor(((item->second->TPFs).value * ratio) / min), 0);

				rounds.insert(pair<RESOURCE_ID, pair<int, int> > (item->first, a));

			}
		}

		i = rounds.begin();

	}

	res = i->first;
	i->second.second++;

	if (i->second.second == i->second.first) {
		next_round_voting++;
		i->second.second = -1;
	}

	if (next_round_voting == rounds.size()) {
		next_round_voting = 0;
		std::map<RESOURCE_ID, pair<int, int> >::iterator item;
		for (item = rounds.begin(); item != rounds.end(); item++) {
			item->second.second = 0;

		}

	}

	i++;
	if (i == rounds.end()) {
		i = rounds.begin();
	}

	while (i->second.second == -1) {
		i++;
		if (i == rounds.end()) {
			i = rounds.begin();
		}

	}

	return true;
}
