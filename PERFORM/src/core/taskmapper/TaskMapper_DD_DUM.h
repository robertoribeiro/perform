/*
 * TaskMapperFIFO.h
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#ifndef SCHEDULERDD_DUM_H_
#define SCHEDULERDD_DUM_H_

#include "TaskMapper.h"
#include <map>
#include <utility>
#include <iostream>
#include <pthread.h>


using namespace std;



class TaskMapper_DD_DUM : public TaskMapper {
public:

	ConcurrentQueue<SystemMessage*>* task_requests;

	uint DD_FACTOR;

	pthread_t thread;

	bool kill_requested;


	TaskMapper_DD_DUM(TaskContainer* taskfeeder, uint initial) : TaskMapper(DD_DUM,taskfeeder){
		DD_FACTOR = initial;

		kill_requested = false;
		task_requests = new ConcurrentQueue<SystemMessage*> (true);

	}
	virtual ~TaskMapper_DD_DUM();

	void inline submit(Task* task,	RESOURCE_ID device,SystemMessage* op);

	void start() {
			kill_requested = false;

			pthread_create(&thread, NULL, &TaskMapper_DD_DUM::start_thread, this);
		}

	void kill() {
			kill_requested = true;
			task_feeder->kill();
			pthread_join(thread, 0);
		}

	static void* start_thread(void* p) {
			reinterpret_cast<TaskMapper_DD_DUM *> (p)->run_loop();
			return NULL;
		}

	void add_operation(SystemMessage* w) {
			task_requests->push(w);

	}


private:
	void run_loop();


};

#endif /* SCHEDULER_H_ */
