#include "TaskMapper_RR_DUM.h"
#include <math.h>

TaskMapper_RR_DUM::~TaskMapper_RR_DUM() {
	// TODO Auto-generated destructor stub
}

void TaskMapper_RR_DUM::run_loop() {

	LOG(1,cout << "Mapper loop thread running..." << endl;)

	RESOURCE_ID target;
	while (!kill_requested) {
		Task* task = task_feeder->getTask();
		if (task != NULL) {

			if (task->dice_lvl == 0 && task->diceable != 0) {
				Task** v;
				int task_count = task->dice(v, task->dice_value);
				if (task_count > 1) {
					task_feeder->addTasks(v, task_count);
					task->done = true;
					add_tasks_to_execute(-1);
				} else {
					if (map(task, target)) {

						if (task->device_preference != NONE)
							target = task->device_preference;
						LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << target << endl;)

						resource_manager->addWork(task, target);
						task->set_assigned_to(target);
					}
				}
			} else {
				if (map(task, target)) {

					if (task->device_preference != NONE)
						target = task->device_preference;
					LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << target << endl;)

					resource_manager->addWork(task, target);
					task->set_assigned_to(target);
				}
			}
		}
	}

}

bool TaskMapper_RR_DUM::map(Task* task, RESOURCE_ID& res) {

	bool assign = true;

	res = i->first;

	if (i->second->idle) {
		i++;
		if (i == resource_manager->resources.end()) {
			i = resource_manager->resources.begin();
		}
		res = i->first;
	}

	i++;
	if (i == resource_manager->resources.end()) {
		i = resource_manager->resources.begin();
	}

	return assign;

}
