/*
 * TaskMapper.h
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#ifndef TASKMAPPER_H_
#define TASKMAPPER_H_
#include <utility>

#include "../taskcontainer/TaskContainer.h"
#include "../resourcemanager/ResourceManager.h"
#include "core.h"
#include "ConcurrentQueue.h"

extern ResourceManager* resource_manager;

class TaskMapper {
public:

	SCH_T map_type;

	TaskContainer* task_feeder;

	TaskMapper(SCH_T t,TaskContainer* taskfeeder) : map_type(t){
		task_feeder=taskfeeder;

	}

	virtual ~TaskMapper();

	virtual void start()=0;


	virtual void kill()=0;

	bool hasWork(){
		return task_feeder->hasWork();
	}

	virtual void add_operation(SystemMessage* w)=0;

private:
	virtual void run_loop()=0;


};

#endif /* TASKMAPPER_H_ */
