#include "TaskMapper_WRAND_DUM.h"
#include <math.h>

TaskMapper_WRAND_DUM::~TaskMapper_WRAND_DUM() {
	// TODO Auto-generated destructor stub
}

void TaskMapper_WRAND_DUM::run_loop() {

	LOG(1,cout << "Mapper loop thread running..." << endl;)

	RESOURCE_ID target;
	while (!kill_requested) {
		Task* task = task_feeder->getTask();
		if (task != NULL) {
			if (task->dice_lvl == 0 && task->diceable != 0) {
				Task** v;
				int task_count = task->dice(v, task->dice_value);
				task_feeder->addTasks(v, task_count);
				task->done = true;
				add_tasks_to_execute(-1);
			} else {
				if (map(task, target)) {

					//if (task->device_preference != "") target = task->device_preference;
					if (task->device_preference != NONE)
						target = task->device_preference;
					//target="GPU_0";
					LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << target << endl;)

					resource_manager->addWork(task, target);
					task->set_assigned_to(target);
					// Analyze moment where task splitting is done and implemented?
				} else {
					//sleep this copy_thread or something
				}
			}
		}
	}


}

bool TaskMapper_WRAND_DUM::map(Task* task, RESOURCE_ID& res) {

	int res_idx;

	if (resource_manager->resources_info.weights.size() == 0) {

		float ratio = 100.0 / resource_manager->resources_info.max_TPFS.value;
		std::map<RESOURCE_ID, ResourceInfo*>::iterator item;
		for (item = resource_manager->resources.begin(); item != resource_manager->resources.end(); item++) {
			if (!item->second->idle) {
				pair<RESOURCE_ID, float> v = pair<RESOURCE_ID, float> (item->first, (item->second->TPFs).value * ratio);
				resource_manager->resources_info.weights.push_back(v);

			}
		}
	}

	int count = resource_manager->resources_info.weights.size();
	float sum_of_weight = 0;
	for (int i = 0; i < count; i++) {
		sum_of_weight += resource_manager->resources_info.weights[i].second;
	}

	float rnd = fmod(rand(), sum_of_weight);
	for (int i = 0; i < count; i++) {
		if (rnd < resource_manager->resources_info.weights[i].second) {
			res_idx = i;
			break;
		}
		rnd -= resource_manager->resources_info.weights[i].second;
	}

	bool assign = false;

	//	std::map<RESOURCE_ID, ResourceInfo*>::iterator item1 = resource_manager->resources.begin();
	//	std::advance(item1, res_idx);
	res = resource_manager->resources_info.weights[res_idx].first;
	assign = true;

	return assign;
}
