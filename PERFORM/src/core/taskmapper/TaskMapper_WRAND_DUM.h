#ifndef WRAND_DUM_H_
#define WRAND_DUM_H_

#include "TaskMapper.h"
#include <map>
#include <utility>
#include <iostream>
#include <pthread.h>

using namespace std;



class TaskMapper_WRAND_DUM : public TaskMapper {
public:

	pthread_t thread;

	bool kill_requested;


	TaskMapper_WRAND_DUM(TaskContainer* taskfeeder) : TaskMapper(WRAND_DUM,taskfeeder){
		kill_requested = false;
	}
	virtual ~TaskMapper_WRAND_DUM();


	void start() {
			kill_requested = false;

			pthread_create(&thread, NULL, &TaskMapper_WRAND_DUM::start_thread, this);
		}

	void kill() {
			kill_requested = true;
			task_feeder->kill();
			pthread_join(thread, 0);
		}

	static void* start_thread(void* p) {
			reinterpret_cast<TaskMapper_WRAND_DUM *> (p)->run_loop();
			return NULL;
		}

	void add_operation(SystemMessage* w) {

	}
private:
	void run_loop();

	bool map(Task* task,RESOURCE_ID& res);

};

#endif /* SCHEDULER_H_ */
