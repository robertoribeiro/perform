#ifndef WRR_DUM_H_
#define WRR_DUM_H_

#include "TaskMapper.h"
#include <map>
#include <utility>
#include <iostream>
#include <pthread.h>

using namespace std;



class TaskMapper_WRR_DUM : public TaskMapper {
public:

	pthread_t thread;

	bool kill_requested;

	int next_round_voting;
	map<RESOURCE_ID, pair<int,int> > rounds;

	std::map<RESOURCE_ID, pair<int,int> >::iterator i;


	TaskMapper_WRR_DUM(TaskContainer* taskfeeder) : TaskMapper(WRR_DUM,taskfeeder){
		kill_requested = false;
		next_round_voting=0;
	}
	virtual ~TaskMapper_WRR_DUM();


	void start() {
			kill_requested = false;

			pthread_create(&thread, NULL, &TaskMapper_WRR_DUM::start_thread, this);
		}

	void kill() {
			kill_requested = true;
			task_feeder->kill();

			void * ret;

					pthread_join(thread, &ret);

					//int nm = *(int*) ret;
					//printf("RETURN VALUE: %d\n", nm);

			//printf("map thread exited\n");

		}

	static void* start_thread(void* p) {
			reinterpret_cast<TaskMapper_WRR_DUM *> (p)->run_loop();
			return NULL;
		}

	void add_operation(SystemMessage* w) {

	}
private:
	void run_loop();

	bool map_task(Task* task,RESOURCE_ID& res);

};

#endif /* SCHEDULER_H_ */
