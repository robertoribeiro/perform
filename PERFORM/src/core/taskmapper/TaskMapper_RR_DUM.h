
#ifndef RAND_DUM_H_
#define RAND_DUM_H_

#include "TaskMapper.h"
#include <map>
#include <utility>
#include <iostream>
#include <pthread.h>

using namespace std;



class TaskMapper_RR_DUM : public TaskMapper {
public:

	pthread_t thread;

	bool kill_requested;


	std::map<RESOURCE_ID, ResourceInfo*>::iterator i;

	TaskMapper_RR_DUM(TaskContainer* taskfeeder) : TaskMapper(RR_DUM,taskfeeder){
		kill_requested = false;
		i = resource_manager->resources.begin();
	}
	virtual ~TaskMapper_RR_DUM();


	void start() {
			kill_requested = false;

			pthread_create(&thread, NULL, &TaskMapper_RR_DUM::start_thread, this);
		}

	void kill() {
			kill_requested = true;
			task_feeder->kill();
			pthread_join(thread, 0);
		}

	static void* start_thread(void* p) {
			reinterpret_cast<TaskMapper_RR_DUM *> (p)->run_loop();
			return NULL;
		}

	void add_operation(SystemMessage* w) {

	}

private:
	void run_loop();

	bool map(Task* task,RESOURCE_ID& res);

};

#endif /* SCHEDULER_H_ */
