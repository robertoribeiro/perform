/*
 * TaskMapperFIFO.cpp
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#include "../performancemodel/PerformanceModel.h"
#include "TaskMapper_DD_DYN.h"
#include <math.h>

extern PerformanceModel* performPM;

TaskMapper_DD_DYN::~TaskMapper_DD_DYN() {
	// TODO Auto-generated destructor stub
}

/**
 * Uses a round robin fashion to choose the queue
 * Used to search until it founds it
 */
Task* TaskMapper_DD_DYN::work_steal() {

	std::map<RESOURCE_ID, ConcurrentQueue<Task*>*>::iterator i = work_queues.begin();
	std::advance(i, work_steal_round_r++ % work_queues.size());
	return i->second->pop();
}

void TaskMapper_DD_DYN::run_loop() {
	LOG(1,cout << "Mapper loop thread running..." << endl;)

	RESOURCE_ID device;
	Task* task = NULL;
	PM_Metric_normalized deviceCC;
	SystemMessage* op = NULL;
	int index = 0;
	float dice_value;
	//const int nd  = resource_manager->computing_devices;
	const int nd  = 4;
	float idd = 1/(float(DD_FACTOR * nd));
	Task * remaining = NULL;
	while (!kill_requested) {

		dice_value=1;

		op = task_requests->pop();

		switch (op->type) {

		case DISPATCH_TASK:

			//task = NULL;

			device = *(RESOURCE_ID*) (op->argument);

//			deviceCC = resource_manager->getAbsoluteDeviceComputeCapability(device);
			deviceCC = resource_manager->getDeviceComputeCapability(device);


			if (task == NULL)
				task = task_feeder->getTask();

			if (task != NULL) {

				if (task->device_preference != device && task->device_preference != NONE) {
					task_feeder->remapTasks(task);
					task_requests->push(op);
				} else {

					Task** v;
					//float dice_value;
					//if (DD_FACTOR >1 ) dice_value = deviceCC.value / ((float(DD_FACTOR * resource_manager->computing_devices)));
					if (DD_FACTOR >=1 ) dice_value = deviceCC.value *idd ;

					int task_count = task->dice(v, PM_Metric_normalized(dice_value));
	
				// only 1 or 2 tasks come out
					if (task_count > 1) {
						task->done = true;
						//task_feeder->remapTasks(v[1]);
						add_tasks_to_execute(1);
						remaining = v[1];
					}

					task = v[0];

					LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << device << endl;)
					resource_manager->addWork(task, device);
					task->set_assigned_to(device);

					task = remaining;
					remaining = NULL;
				}
			}

		default:
			break;
		}

	}
}

/*
 * Can be seen as a two level dicing scheduler. Dices equally the task by INITIAL_DICE, then dices again according to
 * device. Stores the first dice tasks in main queues ans second level tasks in device queue.
 * If no tasks in device queue, if no task e main q and if steal failed block in main q.
 */
//void TaskMapper_DD_DYN::run_loop() {
//
//	LOG(1,cout << "Mapper loop thread running..." << endl;)
//
//	RESOURCE_ID device;
//	Task* task = NULL;
//	PM_Metric_normalized deviceCC;
//	SystemMessage* op = NULL;
//	int index = 0;
//	while (!kill_requested) {
//
//		op = task_requests->pop();
//
//		switch (op->type) {
//
//		case DISPATCH_TASK:
//
//			task = NULL;
//
//			device = *(RESOURCE_ID*) (op->argument);
//
//			deviceCC = resource_manager->getDeviceComputeCapability(device);
//
//			task = work_queues[device]->pop();
//
//			if (task == NULL)
//				task = task_feeder->getTaskNonBlocking();
//
//			index = 0;
//			while (task == NULL && index < work_queues.size()) {
//				task = work_steal();
//				index++;
////				if (task != NULL) {
////					printf("%s stealed a task of %f load!\n", get_enum_string(device),
////							task->wl.value);
////				}
//			}
//
//			//block waiting
//			if (task == NULL)
//				task = task_feeder->getTask();
//
//			if (task != NULL) {
//
//				if (task->device_preference != device && task->device_preference != NONE) {
//					task_feeder->remapTasks(task);
//					task_requests->push(op);
//				} else {
//
//					if (task->dice_lvl == 0 && task->diceable) {
//						Task** v;
//						int task_count = task->dice(v, PM_Metric_normalized(INITIAL_DICE));
//
//						if (task_count > 1) {
//							task_feeder->addTasks(v + 1, task_count - 1);
//
//							/*
//							 * ignore old diced, use new
//							 */
//							//add_tasks_to_execute(1);
//							//add_tasks_to_execute(-1);
//							task->done = true;
//						}
//						task = v[0];
//					}
//
//					if (task->dice_lvl == 1 && deviceCC.value != 1) {
//
//						Task** v;
//						int task_count = task->dice(v, PM_Metric_normalized(deviceCC.value));
//
//						//							printf("%s with %f cc diced task with %f load into %d tasks of %f load!\n",
//						//									get_enum_string(device),deviceCC.value,task->wl.value,task_count,v[0]->wl.value);
//
//						for (int i = 1; i < task_count; i++) {
//							work_queues[device]->push(v[i]);
//							add_tasks_to_execute(1);
//
//						}
//						/*
//						 * ignore old diced, use new
//						 */
//						//add_tasks_to_execute(1);
//						//add_tasks_to_execute(-1);
//						if (task_count > 1)
//							task->done = true;
//						task = v[0];
//
//					}
//
//					LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << device << endl;)
//					resource_manager->addWork(task, device);
//					task->set_assigned_to(device);
//				}
//			}
//
//		default:
//			break;
//		}
//
//	}
//
//}
