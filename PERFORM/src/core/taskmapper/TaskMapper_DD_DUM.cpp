/*
 * TaskMapperFIFO.cpp
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#include "TaskMapper_DD_DUM.h"
#include <math.h>

TaskMapper_DD_DUM::~TaskMapper_DD_DUM() {
	// TODO Auto-generated destructor stub
}

void inline TaskMapper_DD_DUM::submit(Task* task, RESOURCE_ID device, SystemMessage* op) {
	if (task->device_preference == device || task->device_preference == NONE) {

		LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << device << endl;)
		resource_manager->addWork(task, device);
		task->set_assigned_to(device);
	} else {
		task_feeder->remapTasks(task);
		task_requests->push(op);
	}
}

void TaskMapper_DD_DUM::run_loop() {

	LOG(1,cout << "Mapper loop thread running..." << endl;)

	RESOURCE_ID device;
	Task* task = NULL;
	PM_Metric_normalized deviceCC;
	SystemMessage* op = NULL;
	int index = 0;
	float dice_value = 0;
	Task * remaining = NULL;
	while (!kill_requested) {

		op = task_requests->pop();

		switch (op->type) {

		case DISPATCH_TASK:

			//task = NULL;

			device = *(RESOURCE_ID*) (op->argument);

			deviceCC = resource_manager->getAbsoluteDeviceComputeCapability(device);

			if (task == NULL)
				task = task_feeder->getTask();

			if (task != NULL) {

				if (task->device_preference != device && task->device_preference != NONE) {
					task_feeder->remapTasks(task);
					task_requests->push(op);
				} else {

					Task** v;
					float dice_value;
					if (DD_FACTOR > 1)
						dice_value = (1/float(resource_manager->computing_devices))/float(DD_FACTOR * resource_manager->computing_devices);

					int task_count = task->dice(v, PM_Metric_normalized(dice_value));
					// only 1 or 2 tasks come out
					if (task_count > 1) {
						task->done = true;
						//task_feeder->remapTasks(v[1]);
						add_tasks_to_execute(1);
						remaining = v[1];
					}

					task = v[0];

					LOG(0.5,cout << "task " << task->getID() << " assigned to resource: " << device << endl;)
					resource_manager->addWork(task, device);
					task->set_assigned_to(device);

					task = remaining;
					remaining = NULL;
				}
			}

		default:
			break;
		}

	}
}
