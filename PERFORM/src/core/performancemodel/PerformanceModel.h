/*
 * PerformanceModel.h
 *
 *  Created on: Sep 5, 2011
 *      Author: rr
 */

#ifndef PERFORMANCEMODEL_H_
#define PERFORMANCEMODEL_H_

#include "core.h"



class Metric {
public:
	Metric() {

	}
	~Metric() {

	}
};

class PM_Metric_normalized: public Metric {
public:

	float value;

	__H_D__
	PM_Metric_normalized() {

	}
	__H_D__
	PM_Metric_normalized(float v) {
		value = v;
	}

	__H_D__
	~PM_Metric_normalized() {

	}
};

class PerformanceModel {
public:

	//virtual PM_Metric_normalized normalize(Metric)=0;
	virtual bool match(PM_Metric_normalized, PM_Metric_normalized)=0;

	PerformanceModel() {

	}
	~PerformanceModel() {

	}
};

///////////////////////// USERSIDE ///////////////

class FLOPS: public Metric {
public:

	float value;

	FLOPS() {

	}

	FLOPS(float v) {
		value = v;
	}

	~FLOPS() {

	}
};

class FLOP: Metric {
public:

	float value;

	FLOP() {

	}
	~FLOP() {

	}
};

/**
 * uses the accumulated system FLOPS to normalize the device compute capabilty
 */
class FLOPSPerformanceModel: public PerformanceModel {
public:

	FLOPS max_device;
	FLOPS total_system_cc;

	PM_Metric_normalized normalize(FLOPS f) {
		return PM_Metric_normalized(f.value / max_device.value);
	}

	bool match(PM_Metric_normalized m1, PM_Metric_normalized m2) {
		return (m1.value <= m2.value);
	}

	FLOPSPerformanceModel();

	virtual ~FLOPSPerformanceModel();
};

#endif /* PERFORMANCEMODEL_H_ */
