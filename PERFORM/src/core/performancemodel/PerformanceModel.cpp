#include "PerformanceModel.h"

#include "../resourcemanager/ResourceManager.h"
extern ResourceManager* resource_manager;


FLOPSPerformanceModel::~FLOPSPerformanceModel() {

}

FLOPSPerformanceModel::FLOPSPerformanceModel() {
		max_device = FLOPS((resource_manager->resources_info.max_TPFS.value));
		total_system_cc = FLOPS(resource_manager->resources_info.systemTPFS.value);

		std::map<RESOURCE_ID, ResourceInfo*>::iterator item;
		for (item = resource_manager->resources.begin(); item != resource_manager->resources.end(); item++) {
			item->second->cc = normalize(item->second->TPFs);
			item->second->absolute_cc = PM_Metric_normalized(item->second->TPFs.value / total_system_cc.value);
		}
	}
