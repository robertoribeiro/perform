/*
 * kernels_raw.cpp
 *
 *  Created on: Dec 27, 2010
 *      Author: rr
 */

#include <iostream>
#include "perform_utils.h"
#include "core.h"
#include "task/Task.h"


using namespace std;

void k_CPU_test(void** data_pointers, void** args, void* stream) {

	//parallel_for(blocked_range<size_t>(0,6), test());

	char* matrix = (char*) *data_pointers;

	matrix[0] = 'c';
	matrix[1] = 'p';
	matrix[2] = 'u';

	cout << "Kernel done " << endl;

}

//void k_CPU_sleep(void** data_pointers, void** args, void* stream) {
//	float s = get_random_float_zero_to(3);
//	cout << "Sleeping in CPU for " << s << endl;
//	sleep(1 + s);
//	cout << "Kernel done " << endl;
//
//}

void k_CPU_light_work_gen_test(Task a) {

	double* arr = (double*) a.data_pointers[0];

	//	for (int i = 0 ; i< a.N;i++){
	//		printf("%.1f ",arr[i]);
	//	}

	printf("\nReached\n");

}


