#include <iostream>

#include "perform_utils.h"
#include "core.h"
#include "perform.h"

using namespace std;


__global__ void work_gen(float * arr, int N) {

	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i == 0) {

		for (int i = 0; i < N; i++) {
			printf("%.2f  ", arr[i]);
		}

	}

	//printf("\nWORK GENERATED by thread %d! in block %d, global thID %d\n",threadIdx.x,blockIdx.x,blockIdx.x*blockDim.x + threadIdx.x);

}

void k_CUDA_light_work_gen_test(Task t) {

//	float* arr = (float*) t.data_pointers[0];

	//work_gen<<<1,1,0,t.stream>>>(arr,t.N);

}


__global__ void k_subCUDA_test(char* m, char arg1) {

	m[0] = 'g';
	m[1] = 'p';
	m[2] = 'u';
	m[3] = '_';
	m[4] = arg1;

}

void k_CUDA_streamed_test(void** data_pointers, void** args, void* stream) {

	//dim3 dimBlock(1,1);
	//dim3 dimGrid(1,1);
	char arg1;
	if (stream == 0) {
		arg1 = 's';
	} else
		arg1 = 'a';
	//cout << "Execution stream: " << stream << endl;
	k_subCUDA_test<<<1,1 ,0, (cudaStream_t)stream>>>((char*)*data_pointers,arg1);

	checkCUDAError();
	cout << "Kernel submitted" << endl;

}

void k_CUDA_test(void** data_pointers, void** args, void* stream) {

	//dim3 dimBlock(1,1);
	//dim3 dimGrid(1,1);

	char arg1 = 'a';

	k_subCUDA_test<<<1,1 >>>((char*)*data_pointers,arg1);
	checkCUDAError();
	cout << "Kernel done in non-streamed mode" << endl;

}

__global__ void k_subCUDA_sleep() {

}

//void k_CUDA_sleep(void** data_pointers, void** args, void* stream) {
//
//	//float s = get_random_float_zero_to(3);
//	k_subCUDA_sleep<<<1,1,0,(cudaStream_t)stream>>>();
//	cout << "Sleeping in CUDA for " << s << endl;
//	sleep(1 + s);
//	checkCUDAError();
//	cout << "Kernel submitted" << endl;
//}
