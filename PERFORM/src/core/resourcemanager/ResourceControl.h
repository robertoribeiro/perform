/*
 * ResourceControl.h
 *
 *  Created on: Dec 30, 2010
 *      Author: rr
 */

#ifndef RESOURCECONTROL_H_
#define RESOURCECONTROL_H_

#include "task/Task.h"

class SystemMessage;

class ResourceControl {
public:

	//bool flushOperations;

	ResourceControl();
	virtual ~ResourceControl();

	virtual void addWork(Task* w)=0;
	virtual void start()=0;
	//virtual void flushAllOperations()=0;
	virtual void kill()=0;
	virtual void join()=0;
	virtual void add_operation(SystemMessage* w)=0;
	virtual void wake()=0;
	virtual void printQueue()=0;
	virtual void printOpsQueue()=0;
	virtual void checkAndWaitAsyncOps()=0;
	virtual void checkAsyncOps()=0;
	virtual bool allDone()=0;
	virtual bool check_if_memory_available(data_chunk* dc)=0;

};


class SystemMessage {

public:

	OPERATION_T type;
	void* argument;
	volatile bool done;

	ResourceControl* assigned_to;

	pthread_mutex_t mutex;
	pthread_cond_t completion_wait_line;

	SystemMessage(OPERATION_T type_, void* argument_, ResourceControl* assd_ = NULL) :
		type(type_), argument(argument_),assigned_to(assd_) {
		pthread_mutex_init(&mutex,NULL);
		pthread_cond_init(&completion_wait_line,NULL);
		done=false;
	}

	/**
	 * hacked. For some reason controller goes asleep and dont terminates the operation.
	 * Concurrency problem in run method
	 */
	void wait_completion() {
		//pthread_mutex_lock(&mutex);
		 //if (!done) pthread_cond_wait(&completion_wait_line,&mutex);



		 while (!done) {
			if (assigned_to != NULL) assigned_to->wake();
			 pthread_yield();
		 }
		//pthread_mutex_unlock(&mutex);
	}

	void setDone() {
		//pthread_mutex_lock(&mutex);
		done=true;
	//	pthread_mutex_unlock(&mutex);
		//pthread_cond_broadcast(&completion_wait_line);
	}



};


#endif /* RESOURCECONTROL_H_ */
