/*
 * ResourceManager.cpp
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#include "ResourceManager.h"
#include "perform_utils.h"
#include <cmath>
#include "../taskmapper/TaskMapper.h"
#include "../perform.h"

extern TaskMapper* performMAPPER;

void ResourceManager::addWork(Task *t, RESOURCE_ID id) {
	controllers[id]->addWork(t);
}

ResourceManager::~ResourceManager() {
	//killControllers();
	// TODO Auto-generated destructor stub
}

void ResourceManager::addResource(ResourceInfo* ri, RESOURCE_ID dev, DeviceAPI* api) {
	resources.insert(pair<RESOURCE_ID, ResourceInfo*> (dev, ri));
	resources_keys.push_back(dev);

	ResourceControl* a;

	if (ri->type == GPU)
		a = new ResourceControl_GPU(api, dev);
	else if (ri->type == CPU)
		a = new ResourceControl_CPU(api, dev);

	controllers.insert(pair<RESOURCE_ID, ResourceControl*> (dev, a));
	stats.device_tasks_exected[dev] = 0;
	stats.device_load_executed[dev] = 0.0;
	device_count++;


	if (!ri->idle) computing_devices++;

	LOG(2,
			//ri->print();
			cout << "Added!" << endl;
			cout << "Total devices: " << device_count << endl;)
}

void ResourceManager::startControllers() {
	std::map<RESOURCE_ID, ResourceControl*>::iterator i;
	for (i = controllers.begin(); i != controllers.end(); i++) {
		i->second->start();
	}

	LOG(0.1,cout << "PERFORM controllers started..." << endl;)
}

/*
 * TODO: use the operations queue
 */
void ResourceManager::killControllers() {// TODO
	std::map<RESOURCE_ID, ResourceControl*>::iterator i;
	for (i = controllers.begin(); i != controllers.end(); i++) {
		//delete i->second;
		//(i->second)->kill();

		SystemMessage* b = new SystemMessage(KILL, NULL,NULL);
		//resource_manager->controllers[(*i).first]->add_operation(b);

		i->second->add_operation(b);
		i->second->join();
		//printf("contoller thread exited\n");

	}

}

ResourceInfo* ResourceManager::getResourceInfo(RESOURCE_ID id) {
	return resources[id];

}

void ResourceManager::stats_taskExecuted(RESOURCE_ID in, PM_Metric_normalized wl) {
	__sync_fetch_and_add(&(stats.device_tasks_exected[in]), 1);
	stats.device_load_executed[in]+=wl.value;
}

void ResourceManager::stats_setIdleTime(RESOURCE_ID in, tick_count::interval_t v) {
	stats.T_last[in] = v;
}

void ResourceManager::stats_setTotalIdleTime(RESOURCE_ID in, tick_count::interval_t v,tick_count::interval_t v2) {
	stats.TotalIdleTimes[in] = v;
	stats.TotalExecTimes[in] = v2;

}

void ResourceManager::stats_setVolumeDataTransf(RESOURCE_ID in, long long vol){
	stats.data_volume_transf[in]= vol;
}


void ResourceManager::stats_printAssignedTaskCount()
{
    std::map<RESOURCE_ID,int>::iterator i;
	fprintf(stdout,"\n-------------------------  STATS -------------------------\n");
	fprintf(stdout,"Total tasks executed(dice included):\n");
    for(i = stats.device_tasks_exected.begin();i != stats.device_tasks_exected.end();i++){
    	fprintf(stdout,"%s: %d\n", get_enum_string(i->first), i->second);
    }
}

void ResourceManager::stats_print() {

	LOG(0.1,stats_printAssignedTaskCount();)

    //	std::map<RESOURCE_ID, tick_count::interval_t>::iterator it;
//	printf("\nDevice idle time:\n");
//	for (it = stats.T_last.begin(); it != stats.T_last.end(); it++) {
//		printf("%s: %.1lf%\n", get_enum_string(it->first), (it->second.seconds()*100)/stats.total_time);
//	}
//
//	printf("\nTotal device idle time:\n");
//	for (it = stats.TotalIdleTimes.begin(); it != stats.TotalIdleTimes.end(); it++) {
//		printf("%s: %.1lf%\n", get_enum_string(it->first), (it->second.seconds()*100)/stats.total_time);
//	}



#ifdef TEST_IDLES
	fprintf(stdout,"\n%.2lfs ",stats.total_time);
	std::map<RESOURCE_ID, ResourceInfo*>::iterator it;
	for (it = resources.begin(); it != resources.end(); it++) {
		if (it->first == CPU_0 && it->second->idle) continue;
		fprintf(stdout,"%s: ",get_enum_string(it->first));
		fprintf(stdout,"%.2lfs ",stats.TotalExecTimes[it->first].seconds());
		fprintf(stdout,"%.2lfms ",stats.TotalIdleTimes[it->first].seconds()*1000);
		fprintf(stdout,"%.2lfms ",stats.T_last[it->first].seconds()*1000);
		fprintf(stdout,"%.2f%% ", stats.device_load_executed[it->first]*100);
		//float tf= float(stats.data_volume_transf[it->first]/1024.0f/1024.0f);
		//fprintf(stderr,"%.1f ",tf);
        //cerr << stats.data_volume_transf[it->first]/1024/1024 << " ";

	}
#endif





//	int n, j, y;
//	double _sum = 0, variance;
//	y = stats.TotalIdleTimes.size();
//	double max[y];
//	n=0;
//	for (it = stats.TotalIdleTimes.begin(); it != stats.TotalIdleTimes.end(); it++) {
//		max[n] = (it->second.seconds()*100)/stats.total_time;
//		_sum += max[n];
//		n++;
//	}
//
//	double mean = _sum / n;
//	n = 0;
//	j = 0;
//	_sum = 0;
//	for (j = 0; j <= y - 1; j++) {
//		max[n] = pow((max[n] - mean), 2);
//		_sum += max[n];
//		n++;
//	}
//	variance = _sum / (n - 1);
//	cout << "Variance is: " << variance << endl;
//	cout << "Standard Deviation is: " << sqrt(variance) << "\n\n";

}

void ResourceManager::print(RESOURCE_ID dev) {
	resources[dev]->print();
}
;
