/*
 * ResourceManager.h
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include <iostream>
#include <map>
#include <utility>

#include "ResourceInfo.h"
#include "ResourceControl.h"
#include "core.h"
#include "ResourceControl_CPU.h"
#include "ResourceControl_GPU.h"
#include "device_apis/DeviceAPI.h"
#include "tbb/tick_count.h"

using namespace tbb;

struct sresources_info {
	FLOPS max_TPFS;
	vector<pair<RESOURCE_ID, float> > weights;
	FLOPS systemTPFS;
};

struct s_stats {
	double total_time;
	std::map<RESOURCE_ID, int> device_tasks_exected;
	std::map<RESOURCE_ID, float> device_load_executed;
	std::map<RESOURCE_ID, tick_count::interval_t> TotalIdleTimes;
	std::map<RESOURCE_ID, tick_count::interval_t> TotalExecTimes;
	std::map<RESOURCE_ID, tick_count::interval_t> T_last;
	std::map<RESOURCE_ID, long long> data_volume_transf;


};

class ResourceManager {

private:
	struct s_stats stats;
	void stats_printAssignedTaskCount();
public:
	int device_count;
	int computing_devices;
	std::map<RESOURCE_ID, ResourceInfo*> resources;
	std::vector<RESOURCE_ID> resources_keys;
	std::map<RESOURCE_ID, ResourceInfo*>::iterator iterator;
	std::map<RESOURCE_ID, ResourceControl*> controllers;
	struct sresources_info resources_info;
	ResourceManager() {
		device_count = 0;
		computing_devices=0;
		resources_info.max_TPFS = 0;
		resources_info.systemTPFS = 0;
	}

	virtual ~ResourceManager();
	void addResource(ResourceInfo *ri, RESOURCE_ID dev, DeviceAPI *api);
	void startControllers();
	void killControllers();
	void wakeWorkers() {
		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {
			i->second->wake();
		}
	}

	void addWork(Task *t, RESOURCE_ID id);
	ResourceInfo *getResourceInfo(RESOURCE_ID id);
	RESOURCE_ID get_random_resource() {
		std::map<RESOURCE_ID, ResourceInfo*>::iterator item = resources.begin();
		std::advance(item, rand() % resources.size());
		return (*item).first;
	}

	void checkAndWaitAsyncOps() {
		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {
			i->second->checkAndWaitAsyncOps();
		}
	}

	void checkAsyncOps() {
		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {
			i->second->checkAsyncOps();
		}
	}

	bool checkFlushFinished() {
		bool flushing = false;
		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {
		}
		return !flushing;

	}

	void print(RESOURCE_ID dev);

	void printAllQueues() {

		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {

			i->second->printQueue();

		}
	}

	void printAllOpsQueues() {

		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {

			i->second->printOpsQueue();

		}
	}

	PM_Metric_normalized getDeviceComputeCapability(RESOURCE_ID d) {

		return resources[d]->cc;

	}

	PM_Metric_normalized getAbsoluteDeviceComputeCapability(RESOURCE_ID d) {

			return resources[d]->absolute_cc;
	}

	bool allDone() {

		bool done = true;
		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = controllers.begin(); i != controllers.end(); i++) {
			if (!(i->second->allDone())) {
				done = false;
				break;
			}
		}
		return done;

	}

	void stats_taskExecuted(RESOURCE_ID in,PM_Metric_normalized wl);
	void stats_setIdleTime(RESOURCE_ID in, tick_count::interval_t v);
	void stats_setTotalIdleTime(RESOURCE_ID in, tick_count::interval_t v, tick_count::interval_t v2);
	void stats_print();
	void stats_setVolumeDataTransf(RESOURCE_ID in, long long vol);

	void stats_setTotalTime(double t) {
		stats.total_time = t;
	}

};

#endif /* RESOURCEMANAGER_H_ */
