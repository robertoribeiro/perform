/*
 * ResourceControl.cpp
 *
 *  Created on: Nov 30, 2010
 *      Author: rr
 */

#include "ResourceControl_GPU.h"
#include "../device_apis/CUDA/DeviceAPI_CUDA.h"

#include "../datalibrary/DataLibrary.h"
#include "../taskcontainer/TaskContainer.h"
#include "../taskmapper/TaskMapper.h"

extern DataLibrary* performDL;
extern TaskContainer* performQ;
extern TaskMapper* performMAPPER;
//extern ResourceManager* resource_manager;

void ResourceControl_GPU::kill() {

	//	printf("Set task data ready%f\n", PROF_SET_TASK_DATA_READY.seconds());
	//	printf("Generate task data chunks %f\n", PROF_GENERATE_TASK_CHUNKS.seconds());
	//	printf(" PROF_PREPARE_TASK %f\n", PROF_PREPARE_TASK.seconds());

	kill_requested = true;
	work->kill_requested = true;
	//api->shutdown();

	pthread_cond_broadcast(&(work->cond_empty));

//	cudaDeviceReset();

	cudaThreadExit();

	//int r =10;
	//pthread_exit(&r);
	//pthread_join(thread, 0);

}

void ResourceControl_GPU::getTask() {
	RESOURCE_ID* t = new RESOURCE_ID();
	*t = resource_id;
	SystemMessage* op = new SystemMessage(DISPATCH_TASK, (void*) t);
	performMAPPER->add_operation(op);
}

void inline ResourceControl_GPU::check_for_signals() {
	//pthread_mutex_lock(&critical_section);
	SystemMessage* op = system_operations->pop(); //non blocking pop
	if (op) {
		execute_operation(op);
		if (system_operations->size() == 0)
			work->skip = false; // after executing an op if there is no more ops, pop a task
	}

	//dont want to block if any events still pending, but still want to pop tasks
	DeviceAPI_CUDA_streamed* ap = (DeviceAPI_CUDA_streamed*) api;
	if (ap->pending_events() > 0) {
		work->temp_no_block = true;
	} else
		work->temp_no_block = false;

	//pthread_mutex_unlock(&critical_section);
}

void inline ResourceControl_GPU::ready_up_task(Task*& pocket,
		bool& pocketReady) {

	bool mem_released = false;
	//tick_count t0 = tick_count::now();
	performDL->generate_task_data_chunks(pocket);
	//tick_count t1 = tick_count::now();
	//tick_count::interval_t elapsed = (t1 - t0);
	//PROF_GENERATE_TASK_CHUNKS += elapsed;

	//t0 = tick_count::now();

	//ask the system if task can run
	mem_released = false;

	//	if (!performDL->can_task_run(pocket, resource_id, mem_released)) {
	//		//performDL->print_registry();
	//		if (mem_released)
	//			work->push(pocket);
	//		else {
	//			performQ->remapTasks(pocket);
	//			getTask();
	//		}
	//		pocket = NULL;
	//
	//	} else {
	//LOG(2,performDL->print_registry();)

	//LOG(0.5,cout << "Processing task " << pocket->getID() << " in resource " << resource_id
	//		<< endl;)

	//tick_count t0 = tick_count::now();
	pocketReady = api->setTaskDataReady(pocket);

	//tick_count t1 = tick_count::now();
	//tick_count::interval_t elapsed = (t1 - t0);
	//PROF_SET_TASK_DATA_READY += elapsed;
	//LOG(1,performDL->print_registry();)

	if (!pocketReady) {

		mem_released = performDL->release_memory(resource_id);

		if (mem_released)
			work->push(pocket);
		else { // if the device can't release memory send task to re-shcedulling
			performQ->remapTasks(pocket);
			getTask();
		}
		pocket = NULL;

	}

	//}

//	t1 = tick_count::now();
//	elapsed = (t1 - t0);
//	PROF_PREPARE_TASK += elapsed;
}

void ResourceControl_GPU::run() {

	api->init();

	bool idle = resource_manager->resources[resource_id]->idle;

	bool stall = false;
	bool pocketReady = false;
	//uint pending=0;
	//bool idle = resource_manager->resources[resource_id]->idle;

	Task* pocket = NULL;

	LOG(1, cout << resource_id << " thread running..." << endl
	;
	)

	if (!idle)
		getTask();

	while (!kill_requested) {

		check_for_signals();

		if (api->pending_tasks() >= 1)
			stall = true;
		else
			stall = false;

		//one task in flight, another in pocket
		if (pocket == NULL) {
			pocket = work->pop();
			if (pocket != NULL && !idle)
				getTask();
			pocketReady = false;
		}

		if (!pocketReady && pocket != NULL && !stall) {
			ready_up_task(pocket, pocketReady);
		}

		if (!stall && pocket != NULL && pocketReady) {

			performDL->unmark_queried(pocket, resource_id);
			api->executeTask(pocket);
			resource_manager->stats_taskExecuted(resource_id, pocket->wl);
			pocket = NULL;
		} else if (stall) {
			//while (api->pending_tasks())
			pthread_yield();
		}

	}

	api->shutdown();

	pthread_exit(0);

}

void ResourceControl_GPU::execute_operation(SystemMessage* op) {

	data_chunk* dc;
	SystemMessage* op2;
	SystemMessage* op3;
	switch (op->type) {
	case CHECK:
		api->checkAsyncOps(false);
		break;
	case CHECK_AND_WAIT:
		api->checkAsyncOps(true);
		break;
		/*case FLUSH:
		 api->flushDeviceOperations(true);
		 flushOperations = false;
		 break;*/
	case RETRIEVE_TO_HOST:
		dc = (data_chunk*) (op->argument);
		if (!(performDL->device_has_a_valid_copy(hostID, dc)))
			api->retrieve_data_chunk((data_chunk*) (op->argument), OW);
		//if (dc->getLocation() != hostID)	api->retrieve_data_chunk((data_chunk*) (op->second));

		break;
	case RETRIEVE_TO_HOST_BLOCKING:
		dc = (data_chunk*) (op->argument);
		if (!(performDL->device_has_a_valid_copy(hostID, dc)))
			api->retrieve_data_chunk((data_chunk*) (op->argument), OW, true);
		op->setDone();
		break;
	case RETRIEVE_TO_HOST_BLOCKING_OP:
		dc = (data_chunk*) (op->argument);
		api->retrieve_data_chunk((data_chunk*) (op->argument), ADD_UINT64,
				true);
		op->setDone();
		break;
	case FREE_CHUNK:

		dc = (data_chunk*) (op->argument);

		api->free_chunk(dc);
		op->setDone();
		break;
	case RETRIEVE_TO_HOST_AND_FREE:
		dc = (data_chunk*) (op->argument);
		if (!(performDL->device_has_a_valid_copy(hostID, dc)))
			api->retrieve_data_chunk((data_chunk*) (op->argument), OW, true);
		api->free_chunk(dc);
		op->setDone();
		break;
	case KILL:
		kill();
		break;
	case TASKS_FINISHED:
		op2 = new SystemMessage(TASKS_FINISHED, NULL);
		((DeviceAPI_CUDA_streamed*) api)->executor->add_operation(op2);
		//	op2->wait_completion();
		op->setDone();
		break;
	case JOB_FINISHED:
		op3 = new SystemMessage(JOB_FINISHED, NULL);
		((DeviceAPI_CUDA_streamed*) api)->executor->add_operation(op3);
		resource_manager->stats_setVolumeDataTransf(resource_id,
				api->bytes_transfered);

		op3->wait_completion();
		op->setDone();
		break;
	case JOB_STARTED:
		((DeviceAPI_CUDA_streamed*) api)->executor->add_operation(op);
		break;
	default:
		break;
	}
}
