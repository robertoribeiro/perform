/*
 * ResourceControl.h
 *
 *  Created on: Nov 30, 2010
 *      Author: rr
 */

#ifndef RESOURCECONTROL_GPU_H_
#define RESOURCECONTROL_GPU_H_

#include <iostream>
#include <stdlib.h>
#include <pthread.h>
#include "ConcurrentQueue.h"
#include "core.h"
#include "../device_apis/DeviceAPI.h"
#include "ResourceControl.h"
#include "task/Task.h"
#include "kernels.h"
#include "tbb/tick_count.h"

using namespace std;
using namespace tbb;

class ResourceControl_GPU: public ResourceControl {
public:

	RESOURCE_ID resource_id;
	pthread_t thread;
	ConcurrentQueue<Task*>* work;
	DeviceAPI* api;

	ConcurrentQueue<SystemMessage*>* system_operations;

	pthread_mutex_t critical_section;

	volatile bool kill_requested;

	//tick_count::interval_t PROF_GENERATE_TASK_CHUNKS;
	//tick_count::interval_t PROF_SET_TASK_DATA_READY;
	//tick_count::interval_t PROF_PREPARE_TASK_;

	ResourceControl_GPU(DeviceAPI* api_, RESOURCE_ID dev) {

		resource_id = dev;
		api = api_;
		work = new ConcurrentQueue<Task*> (true);
		system_operations = new ConcurrentQueue<SystemMessage*> (false);

		pthread_mutex_init(&critical_section, NULL);

//		PROF_GENERATE_TASK_CHUNKS = tick_count::interval_t();
//		PROF_SET_TASK_DATA_READY = tick_count::interval_t();
//		PROF_PREPARE_TASK = tick_count::interval_t();
	}

	virtual ~ResourceControl_GPU() {

	}

	void kill();

	void join(){
			void* ret;
			pthread_join(thread,&ret);
		}

	void start() {
		kill_requested = false;

		pthread_create(&thread, NULL, &ResourceControl_GPU::start_thread, this);
	}

	static void* start_thread(void* p) {
		reinterpret_cast<ResourceControl_GPU *> (p)->run();
		return NULL;
	}

	void run();

	void inline check_for_signals() ;
	void inline ready_up_task(Task*& pocket,bool& pocketReady);


	void execute_operation(SystemMessage* op);

	void addWork(Task* w) {
		work->push(w);
		LOG(2,cout << "work added" << endl;)
	}

	void wake() {
		pthread_cond_signal(&(work->cond_empty));
	}

	void add_operation(SystemMessage* w) {
		pthread_mutex_lock(&critical_section);
		system_operations->push(w);
		work->skip = true;
		LOG(2,cout << "operation added" << endl;)
		//std::cout << "operation added " << pthread_self() << std::endl;
		pthread_cond_broadcast(&(work->cond_empty));
		pthread_mutex_unlock(&critical_section);
	}

	void printQueue() {
		cout << "ResourceID " << resource_id << " queue has " << work->size() << " tasks" << endl;
	}

	void printOpsQueue() {
		cout << "ResourceID " << resource_id << " ops queue has " << system_operations->size()
				<< " operations" << endl;
	}
	/**
	 * Invoked externaly
	 * The only purpose for now is to check if the requested copies to host throw the cuda stream have completed
	 */
	//void flushAllOperations() {

	//api->flushDeviceOperations();
	//Task* params = new Task();
	//params->associate_kernel()
	//addWork(params);
	//}

	void checkAndWaitAsyncOps() {

		add_operation(new SystemMessage(CHECK_AND_WAIT, NULL, this));

	}
	void checkAsyncOps() {

		add_operation(new SystemMessage(CHECK, NULL, this));

	}

	void getTask();

	bool allDone() {

		bool res;
		pthread_mutex_lock(&critical_section);
		if (system_operations->size() == 0 && work->size() == 0 && api->opsLeft() == 0)
			res = true;
		else
			res = false;
		pthread_mutex_unlock(&critical_section);
		return res;

	}

	bool check_if_memory_available(data_chunk* dc) {
		return api->check_if_memory_available(dc);
	}
};

#endif /* RESOURCECONTROL_SQ_H_ */
