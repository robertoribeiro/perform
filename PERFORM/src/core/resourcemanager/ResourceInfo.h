/*
 * ResourceInfo.h
 *
 *  Created on: Nov 29, 2010
 *      Author: rr
 */

#ifndef RESOURCEINFO_H_
#define RESOURCEINFO_H_

#include <iostream>
#include "core.h"
#include "../performancemodel/PerformanceModel.h"

using namespace std;
class ResourceInfo {
public:


	RESOURCE_ID resourceID; /* static for now */
	RESOURCE_TYPE type; /* will allow a more specific type definition e.g GPU_fermi */
	int physical_threads;/* Analyze notion of physical copy_thread */
	int memory; /* mbytes, Attention memory reserved for the aplication, explicit specified by the programmer? */
	FLOPS TPFs; /* theoretical peak GFLOPS */

	int running_task;

	bool idle;

	PM_Metric_normalized cc;
	PM_Metric_normalized absolute_cc;


	ResourceInfo(RESOURCE_TYPE t,RESOURCE_ID id,int pt = 1,int mem = 1024){

			resourceID= id;
			physical_threads=pt;
			memory=mem;
			type = t;
			running_task=0;
			idle =false;


		};

	void print(){
		cout << endl;
		cout << "resourceID = " << resourceID << endl;
		cout << "physical_threads = " << physical_threads<< endl;
		cout << "memory = " << memory<< endl;
		cout << "type = " <<  type<< endl;
		cout << "running_task = " << running_task<< endl;
	}
	virtual ~ResourceInfo();
};

#endif /* RESOURCEINFO_H_ */
