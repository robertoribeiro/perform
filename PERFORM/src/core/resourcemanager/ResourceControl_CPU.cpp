/*
 * ResourceControl.cpp
 *
 *  Created on: Nov 30, 2010
 *      Author: rr
 */

#include "ResourceControl_CPU.h"

#include "datalibrary/DataLibrary.h"
#include "taskmapper/TaskMapper.h"
//#include "../taskmapper/TaskMapperDD.h"
#include <sys/time.h>

#include "perform_utils.h"

#include "tbb/tick_count.h"
//#include <mkl.h>

extern DataLibrary* performDL;
extern TaskMapper* performMAPPER;

void ResourceControl_CPU::kill() {

	kill_requested = true;
	work->kill_requested = true;
	pthread_cond_broadcast(&(work->cond_empty));




	//int r =10;
	//pthread_join(thread, 0);

}

void ResourceControl_CPU::getTask() {
	RESOURCE_ID* t = new RESOURCE_ID();
	*t = resource_id;
	SystemMessage* op = new SystemMessage(DISPATCH_TASK, (void*) t);
	performMAPPER->add_operation(op);
}

void inline ResourceControl_CPU::check_for_signals() {
	pthread_mutex_lock(&critical_section);
		SystemMessage* op = system_operations->pop();
		if (op) {
			execute_operation(op);
			if (system_operations->size() == 0)
				work->skip = false; // after executing an op if there is more ops, do not pop tasks
		}
		pthread_mutex_unlock(&critical_section);
}



void inline ResourceControl_CPU::ready_up_task(Task*& t) {

	bool mem_released = false;

	performDL->generate_task_data_chunks(t);

//	if (!performDL->can_task_run(t, resource_id, mem_released)) {
//		work->push(t);
//		t = NULL;
//	} else {

		LOG(2,performDL->print_registry();)

		LOG(0.5,cout << "Processing task " << t->getID() << " in resource " << resource_id
				<< endl;)

		api->setTaskDataReady(t);

		performDL->unmark_queried(t, resource_id);

		//performDL->print_registry();

		tick_count t0 = tick_count::now();
			api->executeTask(t);
		tick_count t1 = tick_count::now();
		tick_count::interval_t elapsed = (t1 - t0);
		TotalExecTime+=elapsed;

		resource_manager->stats_taskExecuted(resource_id,t->wl);
		//printf("Ecutexd a task of %f\n",t->wl.value);


	//}


}


void ResourceControl_CPU::run() {

	api->init();
	bool idle=resource_manager->resources[resource_id]->idle;

	LOG(1,cout << resource_id << " thread running..." << endl;)

	if (!idle)
		getTask();

	while (!kill_requested) {

		tick_count t0 = tick_count::now();
		check_for_signals();
		Task* t = work->pop();
		tick_count t1 = tick_count::now();
		tick_count::interval_t elapsed = (t1 - t0);
		TotalIdleTime += elapsed;
		//last_pop_wait = elapsed;

		if (!idle && t != NULL)
			getTask();

		if (t != NULL) {
			ready_up_task(t);
			time_last_finished = tick_count::now();
		}

	}

	api->shutdown();

	  int* y = new int();
	  *y = 1000;
	  pthread_exit((void*)y);

}

void ResourceControl_CPU::execute_operation(SystemMessage* op) {

	data_chunk* dc;
	switch (op->type) {
	case CHECK:
		api->checkAsyncOps(false);
		break;
	case CHECK_AND_WAIT:
		api->checkAsyncOps(true);
		break;
		/*case FLUSH:
		 api->flushDeviceOperations(true);
		 flushOperations = false;
		 break;*/
	case RETRIEVE_TO_HOST:
		dc = (data_chunk*) (op->argument);
		//if (!(performDL->device_has_a_valid_copy(hostID, dc)))
			api->retrieve_data_chunk((data_chunk*) (op->argument), OW);
		//if (dc->getLocation() != hostID)	api->retrieve_data_chunk((data_chunk*) (op->second));

		break;
	case RETRIEVE_TO_HOST_BLOCKING:
		dc = (data_chunk*) (op->argument);
		//if (!(performDL->device_has_a_valid_copy(hostID, dc)))
			api->retrieve_data_chunk((data_chunk*) (op->argument),OW, true);
		break;
	case RETRIEVE_TO_HOST_BLOCKING_OP:
			dc = (data_chunk*) (op->argument);
			//if (!(performDL->device_has_a_valid_copy(hostID, dc)))
				api->retrieve_data_chunk((data_chunk*) (op->argument),OW, true);
			break;
	case FREE_CHUNK:
		dc = (data_chunk*) (op->argument);
			api->free_chunk(dc);
		op->setDone();
		break;
	case KILL:
		kill();
		break;
	case TASKS_FINISHED:
		resource_manager->stats_setIdleTime(resource_id,tick_count::now()- time_last_finished);
		break;
	case JOB_FINISHED:
		resource_manager->stats_setTotalIdleTime(resource_id, TotalIdleTime,TotalExecTime);
		resource_manager->stats_setVolumeDataTransf(resource_id, api->bytes_transfered);
		op->setDone();
		break;
	case JOB_STARTED:
		TotalIdleTime = tick_count::interval_t();
		break;
	default:
		break;
	}

	op->setDone();
}
