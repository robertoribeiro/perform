/*
 * myUtil.h
 *
 *  Created on: Dec 22, 2010
 *      Author: rr
 */

#ifndef MYUTIL_H_
#define MYUTIL_H_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <cmath>
#include <sstream>

using namespace std;

#include "../device_apis/CUDA/cuda_utils.h"


static double fib(double n)
{
	double a = 1, b = 1;
	for (int i = 3; i <= n; i++)
	{
		double c = a + b;
		a = b;
		b = c;
	}
	return a;
}

static uint _something(uint x) {

	volatile uint acc = 0;

	volatile uint garbage = 0;
	while (garbage < 1000) {
		acc += x + 1;
		garbage += 1;
		//__threadfence_block();
	}

	return acc;
}


void printMatrixFloat(float *A, int rows, int cols);



bool checkMatrices(float *A, float *B, const int rows, const int cols);
void initMatrixRandom(float *A, const int rows, const int cols);
void initMatrixIdentity(float *A, const int rows, const int cols);
void setMatrixToZero(char *A, const int rows, const int cols);
void printMatrixChar(char *A, int rows, int cols);

template<class T>
std::string to_string(T t, std::ios_base & (*f)(std::ios_base&)) {
	std::ostringstream oss;
	oss << f << t;
	return oss.str();
}
template<typename T>
bool checkMatrices(T *A, T *B, const int rows, const int cols) {

	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			if (A[ii * cols + jj] != B[ii * cols + jj]) {
				fprintf(stderr,"\nMatrices are different!\n");
				//printMatrixFloat(A,rows,cols);
				return false;
			}
	fprintf(stderr,"\nMatrices are equal!\n");
	return true;
}
template<typename T>
void initMatrixIdentity(T *A, const int rows, const int cols) {

	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			if (ii == jj)
				A[ii * cols + jj] = 1.f;
			else
				A[ii * cols + jj] = 0.f;
}


template<typename T>
void initMatrix(T *A, const int rows, const int cols, T value) {
	for (int ii = 0; ii < rows; ii++)
		for (int jj = 0; jj < cols; jj++)
			A[ii * cols + jj] = value;
}

double clocks_to_sec(double clocks);
double StandardDeviation(int* list, int number_of_items);

double get_random_float_zero_to(int max);

double getGEMMFlops(double N, double time);

class PERFORM_acc_timer {
public:

	timeval t;
	timeval global_start;

	double duration;

	PERFORM_acc_timer() {
		duration = 0;
	}

	~PERFORM_acc_timer() {
	}

	void start() {
		gettimeofday(&t, NULL);
	}

	void stop() {
		timeval end;
		gettimeofday(&end, NULL);
		duration += get_interval(t, end) / 1000.0; //seconds
	}

	/**
	 * returns ms
	 */
	static double get_interval(timeval start, timeval end) {
		double v = (double) (end.tv_sec * 1000.0 + end.tv_usec / 1000.0 - start.tv_sec * 1000.0
				- start.tv_usec / 1000.0 + 0.5);
		return v;

	}

	char* print() {
		char* buffer = new char[32];
		snprintf(buffer, 32, "%g", duration);
		return buffer;
	}

};

class PERFORM_timer {
public:
	vector<double> values;
	timeval t;
	timeval global_start;
	double duration;

	vector<double> finals;

	PERFORM_timer() {
		duration = 0;
	}

	~PERFORM_timer() {
	}

	void start() {
		gettimeofday(&t, NULL);
		gettimeofday(&global_start, NULL);
		//global_start= params;
	}

	void stop() {
		timeval end;
		gettimeofday(&end, NULL);
		duration += get_interval(global_start, end) / 1000.0;
	}

	void save_and_restart() {
		timeval t2;
		gettimeofday(&t2, NULL);
		values.push_back(get_interval(t, t2));
		t = t2;
	}

	void append_and_restart(int pos = -1) {
		timeval t2;
		gettimeofday(&t2, NULL);
		double v = get_interval(t, t2);
		if (values.size() > pos)
			values.at(pos) += v;
		else
			values.insert(values.begin() + pos, v);
		t = t2;
	}

	/**
	 * returns ms
	 */
	static double get_interval(timeval start, timeval end) {
		double v = (double) (end.tv_sec * 1000.0 + end.tv_usec / 1000.0 - start.tv_sec * 1000.0
				- start.tv_usec / 1000.0 + 0.5);
		return v;

	}

	void getMinResult(double time, int executions) {
		finals.push_back(time);
		if (finals.size() >= executions) {
			vector<double>::iterator i;
			double min = 0;
			min = finals[0];
			for (i = finals.begin(); i != finals.end(); i++) {
				if (*i < min)
					min = *i;
			}
			printf("%.3f ", min);
		}

	}

	void getMinResult(double time, double N, uint executions) {
		finals.push_back(time);
		if (finals.size() >= executions) {
			vector<double>::iterator i;
			double min = 0;
			min = finals[0];
			for (i = finals.begin(); i != finals.end(); i++) {
				if (*i < min)
					min = *i;
			}
			//printf("%.2f", getGEMMFlops(N,min));
		}

	}

	double get(int p) {
		return values[p];
	}

	void print() {
		int a =values.size();
		for (int i = 0; i < a; i++) {
			cout << values[i] << " ";
		}
		printf("\n");
	}

};

#endif /* MYUTIL_H_ */

