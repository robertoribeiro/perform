/*
 * DataLibary.h
 *
 *  Created on: Jan 18, 2011
 *      Author: rr
 */

#ifndef DATALIBARY_H_
#define DATALIBARY_H_

#include "core.h"
#include "task/Task.h"
#include "resourcemanager/ResourceManager.h"

#include "MSI_Table.h"
#include <string.h>


class DataLibrary {

private:
	int data_chunkID_incrementor; // Attention: race condition
public:

	int device_count;

	MSI_Table* chunk_registry;

	//tick_count::interval_t PROF_GENERATE_TASK_CHUNKS;

	//long long bytes_transfered;

	DataLibrary(int device_count_);

	~DataLibrary() {

		//printf("Generate task data chunks %f\n",PROF_GENERATE_TASK_CHUNKS.seconds());

		std::map<RESOURCE_ID, ResourceControl*>::iterator i;
		for (i = resource_manager->controllers.begin(); i != resource_manager->controllers.end(); i++) {
			erase_memory(i->first);
		}

		//bytes_transfered=0;

	}

	void register_data_chunk(data_chunk* dc);

	void unregister_data_chunk(data_chunk* dc);

	vector<RESOURCE_ID>* dc_latest_version_locations(int dc_id);

	void register_location_update(data_chunk* dc, RESOURCE_ID dev_to, PERMISSION_T p, void* data);

	void register_write_request(data_chunk* dc, RESOURCE_ID dev_to);

	void register_share_request(data_chunk* dc, RESOURCE_ID dev_to);

	bool can_task_run(Task* t, RESOURCE_ID dev, bool& mem_released);

	bool device_has_a_valid_copy(RESOURCE_ID dev, data_chunk* dc);

	bool device_has_chunk(RESOURCE_ID dev, data_chunk* dc);

	void retrieve_off_host_chunks();

	RESOURCE_ID get_first_valid_copy(int dc_id);

	void print_registry();

	void register_task_completion(Task* t, RESOURCE_ID at);

	void generate_task_data_chunks(Task* t);

	void process_domain(Task* t, Domain_* domain);

	void update_chunk(Domain_* dst, Domain_* src);

	void splice(Domain_* domain, Domain_* elder_chunk_dmn = NULL, OPERATION_T op = RETRIEVE_TO_HOST_BLOCKING);

	void retrieve_off_host_chunk(data_chunk* dc, bool block, OPERATION_T op);

	void delete_domain(Domain_* domain);

	bool release_memory(RESOURCE_ID in);

	void erase_memory(RESOURCE_ID in);

	void mark_queried(data_chunk* dc, RESOURCE_ID dev_to);

	void unmark_queried(Task* t, RESOURCE_ID dev_to);

	void invalidate_domain_copies(Domain_* domain);

	void revertTaskDataReady(Task*,RESOURCE_ID in);

};
#endif
