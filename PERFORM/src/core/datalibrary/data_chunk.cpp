/*
 * data_chunk.cpp
 *
 *  Created on: Jan 21, 2011
 *      Author: rr
 */

#include "data_chunk.h"
#include "../datalibrary/DataLibrary.h"

extern DataLibrary* performDL;

data_chunk::data_chunk(void* original_host_pointer_, float n_bytes_, uint element_size_, uint line_length_, Domain_* d) {
	performDL->register_data_chunk(this);
	n_bytes = n_bytes_;
	element_size = element_size_;
	pitch = line_length_;
	hook_pointer = original_host_pointer_;
	divided = false;
	domain=d;

	 parent_chunk = NULL;



	performDL->register_location_update(this, hostID, R, original_host_pointer_);
}


/**
 * update it actually
 */
void data_chunk::addLocation(RESOURCE_ID dev, void* data_) {

//	locations.lock();
//	//for (ListNode<res_void>* __it = locations.getIterator();locations.canStep(__it);locations.step(__it)) {
//	for (locations.gotoBeginning(); locations.canStep(); locations.step()) {
//		res_void a = locations.getCursor();
//		if (a.first == dev) {
//			locations.removeCursor();
//			locations.stopStep();
//		}
//	}
//	locations.unlock();
//
//	locations.insert(res_void(dev, data_));

	locations[dev] = data_;


}
