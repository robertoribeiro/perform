/*
 * DataLibary.cpp
 *
 *  Created on: Jan 18, 2011
 *      Author: rr
 */

#include "DataLibrary.h"
extern ResourceManager* resource_manager;
#include "perform_utils.h"

DataLibrary::DataLibrary(int device_count_) {

	if (hostID == NONE) {
		cout << "Host ID not specified, one host is required" << endl;
		exit(0);
	}
	data_chunkID_incrementor = 0;
	device_count = device_count_;

	chunk_registry = new MSI_Table();

	//PROF_GENERATE_TASK_CHUNKS = tick_count::interval_t();
}

void DataLibrary::register_data_chunk(data_chunk* dc) {

	chunk_registry->register_chunk(dc,
			__sync_fetch_and_add(&data_chunkID_incrementor, 1));

	//return data_chunkID_incrementor;
}

void DataLibrary::unregister_data_chunk(data_chunk* dc) {

	//FREE CHUNK

	std::map<RESOURCE_ID, ResourceControl*>::iterator i;
	for (i = resource_manager->controllers.begin();
			i != resource_manager->controllers.end(); i++) {
		if (chunk_registry->device_has_chunk(i->first, dc->id)) {
			SystemMessage* b = new SystemMessage(FREE_CHUNK, (void*) dc,
					i->second);
			i->second->add_operation(b);
		}
	}

	chunk_registry->unregister(dc->id);
}

vector<RESOURCE_ID>* DataLibrary::dc_latest_version_locations(int dc_id) {

	vector<RESOURCE_ID>* res = new vector<RESOURCE_ID>();

	chunk_registry->dc_latest_version_locations(dc_id, res);

	return res;

}
/**
 * it assumes that there is no copy in M state for this chunk
 */
void DataLibrary::register_location_update(data_chunk* dc, RESOURCE_ID dev_to,
		PERMISSION_T p, void* data) {

	std::map<RESOURCE_ID, MSI_T>::iterator i;
	if (p == R) {
		chunk_registry->register_a_share(dc->id, dev_to);
	} else if (p == RW || p == W) {

		chunk_registry->register_a_write(dc->id, dev_to);
	} else if (p == WD) {

		chunk_registry->register_a_write_duplicate(dc->id, dev_to);
		//LOG(0, print_registry()	;

	}

	dc->addLocation(dev_to, data);

}

/**
 * For the CUDA case, we can have a task executing and another on hold with the data ready. When the tasks completes it
 * will mark the S chunks as SaP, but potentially the task on hold will need these chunks. The Q mark will prevent this.
 */
void DataLibrary::mark_queried(data_chunk* dc, RESOURCE_ID dev_to) {

	chunk_registry->mark_queried(dc->id, dev_to);
}

void DataLibrary::unmark_queried(Task* t, RESOURCE_ID dev_to) {

//t->data_chunks->lock();
	for (ListNode < taskAssociated_chunk > *__it = t->data_chunks->getIterator();
			t->data_chunks->canStep(__it); t->data_chunks->step(__it)) {
		//for (t->data_chunks->gotoBeginning(); t->data_chunks->canStep(); t->data_chunks->step()) {
		data_chunk* i = t->data_chunks->getCursorValue(__it).first;
		PERMISSION_T i_p = t->data_chunks->getCursorValue(__it).second;
		if (i_p == R)
			chunk_registry->unmark_queried(i->id, dev_to);
	}
//t->data_chunks->unlock();

}

void DataLibrary::register_write_request(data_chunk* dc, RESOURCE_ID dev_to) {
	chunk_registry->register_a_write(dc->id, dev_to);
}

void DataLibrary::register_share_request(data_chunk* dc, RESOURCE_ID dev_to) {
	chunk_registry->register_a_share(dc->id, dev_to);
}

/*
 * Checks if there is enough free memory
 */
bool DataLibrary::can_task_run(Task* t, RESOURCE_ID dev, bool& mem_released) {

	bool able = false;
//t->data_chunks->lock();
	for (ListNode < taskAssociated_chunk > *__it = t->data_chunks->getIterator();
			t->data_chunks->canStep(__it); t->data_chunks->step(__it)) {

		data_chunk* dc = (t->data_chunks->getCursorValue(__it)).first;
		//vector<RESOURCE_ID>* r = dc_latest_version_locations(dc->id);
		//if (r->size() != 0) {
		//if (!resource_manager->controllers[dev]->check_if_memory_available(dc)) {
		//	mem_released = release_memory(dev);
		//	able = false;
		//	break;
		//} else {
		able = true;
		//	}
		//} else {
		//	able = false;
		//	break;
		//}
	}
//t->data_chunks->unlock();

	return able;
}

bool DataLibrary::device_has_a_valid_copy(RESOURCE_ID dev, data_chunk* dc) {

	bool res;
	res = chunk_registry->device_has_a_valid_copy(dev, dc->id);

	return res;

}

bool DataLibrary::device_has_chunk(RESOURCE_ID dev, data_chunk* dc) {
	bool res;
	res = chunk_registry->device_has_chunk(dev, dc->id);

	return res;

}

void DataLibrary::retrieve_off_host_chunks() {

	vector<pair<RESOURCE_ID, data_chunk*> >::iterator it;
	vector<pair<RESOURCE_ID, data_chunk*> > res = vector<
			pair<RESOURCE_ID, data_chunk*> >();

	chunk_registry->get_off_host_registred_chunks(&res);

	for (it = res.begin(); it != res.end(); it++) {
		SystemMessage* b = new SystemMessage(RETRIEVE_TO_HOST_BLOCKING,
				(void*) (*it).second,
				resource_manager->controllers[(*it).first]);
		resource_manager->controllers[(*it).first]->add_operation(b);
	}

}

RESOURCE_ID DataLibrary::get_first_valid_copy(int dc_id) {

	RESOURCE_ID res = chunk_registry->get_first_valid_copy(dc_id);
	return res;

}

void DataLibrary::print_registry() {
	chunk_registry->print();
}

void DataLibrary::register_task_completion(Task* t, RESOURCE_ID at) {

//t->data_chunks->lock();
	for (ListNode < taskAssociated_chunk > *__it = t->data_chunks->getIterator();
			t->data_chunks->canStep(__it); t->data_chunks->step(__it)) {
		//for (t->data_chunks->gotoBeginning(); t->data_chunks->canStep(); t->data_chunks->step()) {
		data_chunk* dc = t->data_chunks->getCursorValue(__it).first;
		PERMISSION_T perm = t->data_chunks->getCursorValue(__it).second;

		if (perm == RW || perm == W) {
			chunk_registry->register_a_share(dc->id, at);
		} else {
			chunk_registry->park_chunk(dc->id, at);
		}
	}
//t->data_chunks->unlock();

}

void DataLibrary::invalidate_domain_copies(Domain_* domain) {
	if (domain->phy_chunk != NULL)
		chunk_registry->invalidate_chunk_copies(domain->phy_chunk->id);

//domain->childs.lock();
	for (ListNode<Domain_*>* __it = domain->childs->getIterator();
			domain->childs->canStep(__it); domain->childs->step(__it)) {
		//for (domain->childs.gotoBeginning(); domain->childs.canStep(); domain->childs.step()) {
		invalidate_domain_copies(domain->childs->getCursorValue(__it));
	}
//domain->childs.unlock();
}

void DataLibrary::generate_task_data_chunks(Task* t) {

//tick_count t0 = tick_count::now();
	if (t->data_chunks->length == 0) {
		for (ListNode<Domain_*>* __it = t->domains->getIterator();
				t->domains->canStep(__it); t->domains->step(__it)) {

			//		t->domains->lock();
			//for (t->domains->gotoBeginning(); t->domains->canStep(); t->domains->step()) {
			process_domain(t, t->domains->getCursorValue(__it));
		}
		//	t->domains->unlock();

	}

//tick_count t1 = tick_count::now();
//tick_count::interval_t elapsed = (t1 - t0);
//PROF_GENERATE_TASK_CHUNKS += elapsed;

}

void DataLibrary::process_domain(Task* t, Domain_* domain) {

	domain->lock();

	if (!(domain->chunked)) {

		Domain_* parent = domain->parent;
		data_chunk* parent_chunk;
		data_chunk* dc;
		bool found = false;
		while (!found && parent != NULL) {
			if (parent->phy_chunk != NULL && parent->chunked == true)
				found = true;
			else
				parent = parent->parent;
		}
		if (found) {

			parent_chunk = parent->phy_chunk;
			dc = new data_chunk();
			int elements = 1;

			domain->X.length() == 0 ? : elements *= domain->X.length();
			domain->Y.length() == 0 ? : elements *= domain->Y.length();

			dc->n_bytes = elements * parent_chunk->element_size;

			//			void* allocation = (void*) PERFORM_alloc(elements * parent_chunk->element_size);
			//
			//			if (domain->permissions != W) {
			//
			//				__sync_fetch_and_add(&bytes_transfered, dc->n_bytes);
			//				//printf("moved %.2lfmb\n",dc->n_bytes/1024/1024);
			//
			//				if (domain->n_dim_space > 1) {
			//
			//					int num_lines = domain->X.length();
			//					for (int i = 0; i < num_lines; i++) {
			//						byte_address data = (byte_address) (parent_chunk->hook_pointer);
			//
			//						int offset_line = (((domain->X.start - parent->X.start) + i)
			//								* (parent_chunk->pitch * parent_chunk->element_size));
			//						int offset_col = ((domain->Y.start - parent->Y.start)
			//								* parent_chunk->element_size);
			//						void* src = data + offset_line + offset_col;
			//
			//						offset_line = i * parent_chunk->element_size * domain->Y.length();
			//						void* dst = ((byte_address) allocation) + offset_line;
			//
			//						int length = domain->Y.length();
			//						memcpy(dst, src, length * parent_chunk->element_size);
			//
			//					}
			//
			//				} else {
			//					byte_address data = (byte_address) (parent_chunk->hook_pointer);
			//					void* src;
			//					int offset_line = (((domain->X.start - parent->X.start)) * (parent_chunk->pitch
			//							* parent_chunk->element_size));
			//
			//					if (parent->n_dim_space > 1) {
			//						int offset_col = ((domain->Y.start - parent->Y.start) * parent_chunk->element_size);
			//						src = data + offset_line + offset_col;
			//					} else {
			//						src = data + (domain->X.start - parent->X.start) * parent_chunk->element_size;
			//					}
			//
			//					//memcpy(&allocation, &src, domain->dim_lengths[0] * parent_chunk->element_size);
			//					int length = domain->X.length() * parent_chunk->element_size;
			//
			//					memcpy(allocation, src, length);
			//				}
			//			}

			int offset_line = (((domain->X.start - parent->X.start))
					* (parent_chunk->pitch * parent_chunk->element_size));

			int offset_col = ((domain->Y.start - parent->Y.start)
					* parent_chunk->element_size);

			dc->hook_pointer = (byte_address) parent_chunk->hook_pointer
					+ offset_line + offset_col;
			dc->element_size = parent_chunk->element_size;

			domain->n_dim_space > 1 ?
					dc->pitch = domain->Y.length() :
					dc->pitch = domain->X.length();

			domain->phy_chunk = dc;
			domain->chunked = true;

			dc->divided = false;
			dc->domain = domain;
			dc->parent_chunk = parent_chunk;

			parent_chunk->divided = true;

			//if (domain->permissions != R)
			//chunk_registry->invalidate_chunk(parent_chunk->id);

			register_data_chunk(dc);

			//register_location_update(dc, hostID, R, dc->hook_pointer);

			t->add_data_chunk(dc, domain->permissions);

			//printMatrixFloat((float*)params->get_task_data_pointers(hostID)[0], domain->dim_lengths[0], domain->dim_lengths[1]);

		} else {
			printf("ups\n");
		}

	} else {
		t->add_data_chunk(domain->phy_chunk, domain->permissions);
	}

	domain->unlock();

}
/**
 * TODO Inneficient
 */

void DataLibrary::update_chunk(Domain_* dst, Domain_* src) {

	data_chunk* dc_src = src->phy_chunk;
	data_chunk* dc_dst = dst->phy_chunk;

//__sync_fetch_and_add(&bytes_transfered, dc_src->n_bytes);

	if (src->n_dim_space > 1) {

		int num_lines = src->X.length();
		for (int i = 0; i < num_lines; i++) {
			byte_address data = (byte_address) (dc_dst->hook_pointer);

			int offset_line = ((src->X.start + i)
					* (dc_dst->pitch * dc_dst->element_size));
			int offset_col = (src->Y.start * dc_dst->element_size);
			void* dst_ptr = data + offset_line + offset_col;

			offset_line = i * dc_dst->element_size * src->Y.length();
			void* src_ptr = ((byte_address) dc_src->hook_pointer) + offset_line;

			int length = src->Y.length();

			memcpy(dst_ptr, src_ptr, length * dc_dst->element_size);
		}

	} else {

		byte_address data = (byte_address) (dc_dst->hook_pointer);
		void* dst_ptr;
		if (dst->n_dim_space > 1) {
			int offset_col = (src->Y.start * dc_src->element_size);
			int offset_line = ((dst->X.start)
					* (dc_dst->pitch * dc_dst->element_size));
			dst_ptr = data + offset_line + offset_col;
		} else {
			dst_ptr = data + src->X.start * dc_src->element_size;
			void* src_ptr = ((byte_address) dc_src->hook_pointer);

			int length = src->X.length();

			memcpy(dst_ptr, src_ptr, length * dc_dst->element_size);
		}

	}
}

/**
 * Very tricky method. Considers domain hierarchy, where domains can have phy_chunk or not, if so they can be divided or not.
 */
void DataLibrary::splice(Domain_* domain, Domain_* elder_chunk_dmn,
		OPERATION_T op) {
	if (elder_chunk_dmn == NULL)
		elder_chunk_dmn = domain;

	if (domain->phy_chunk != NULL) {
		if (domain->phy_chunk->divided) {
			for (ListNode<Domain_*>* __it = domain->childs->getIterator();
					domain->childs->canStep(__it); domain->childs->step(__it)) {
				Domain_* d = domain->childs->getCursorValue(__it);
				splice(d, elder_chunk_dmn);
				if (elder_chunk_dmn->phy_chunk != d->phy_chunk) {
					delete_domain(d);
				}
			}
		} else {
			retrieve_off_host_chunk(domain->phy_chunk, true, op);
		}
	} else if (domain->has_childs()) {
		for (ListNode<Domain_*>* __it = domain->childs->getIterator();
				domain->childs->canStep(__it); domain->childs->step(__it)) {
			Domain_ *d = domain->childs->getCursorValue(__it);
			splice(d, elder_chunk_dmn);
			if (elder_chunk_dmn->phy_chunk != d->phy_chunk) {
				delete_domain(d);
			}
		}
	}

//wrong, do only if all the sub-chunks are copied
//delete &domain->childs;
	domain->childs = new LList<Domain_*>();

// top of the hierarchy
	if (domain == elder_chunk_dmn) {
		register_location_update(elder_chunk_dmn->phy_chunk, hostID, R,
				elder_chunk_dmn->phy_chunk->hook_pointer);
		domain->phy_chunk->divided = false;
	}

}

void DataLibrary::retrieve_off_host_chunk(data_chunk* dc, bool block,
		OPERATION_T op) {



	if (op == RETRIEVE_TO_HOST_BLOCKING_OP) {
		msi_line* line;
		chunk_registry->get_and_lock_line(dc->id, &line);
		chunk_registry->unlock_line(dc->id);
		RESOURCE_ID res = NONE;

		std::map<RESOURCE_ID, MSI_T>::iterator i;

		i = line->begin();
		i++;
		for (; i != line->end(); i++) {

			if (i->second == MS) {
				res = i->first;

				//print_registry();

				SystemMessage* b = new SystemMessage(op, (void*) dc,
						resource_manager->controllers[res]);

				resource_manager->controllers[res]->add_operation(b);

				if (block)
					b->wait_completion();

			}

		}

	} else {

		RESOURCE_ID r = get_first_valid_copy(dc->id);

		if (r == NONE) {
			printf("No copy available in chunk retrival\n");
			exit(0);
		}

		SystemMessage* b = new SystemMessage(op, (void*) dc,
				resource_manager->controllers[r]);

		resource_manager->controllers[r]->add_operation(b);

		if (block)
			b->wait_completion();

	}
}
/**
 * Erases a domain form the system, its dc assocaited and all child domains.
 * Its assumed that if this is requested by the user , the domain is a top one.
 * Could use delete operator, but it needs access to datalibrary and since this is a cuda-capable class it's not a good idea.
 * it also assumes that the user is agnostic to dcs
 */
void DataLibrary::delete_domain(Domain_* domain) {
	if (domain->phy_chunk != NULL)
		unregister_data_chunk(domain->phy_chunk);

	for (ListNode<Domain_*>* __it = domain->childs->getIterator();
			domain->childs->canStep(__it); domain->childs->step(__it)) {
		//for (domain->childs->gotoBeginning(); domain->childs->canStep(); domain->childs->step()) {
		delete_domain(domain->childs->getCursorValue(__it));
	}
//domain->childs->unlock();

}

/**
 * Searches for invalid chunk in the MSI for a device, and releases them
 */
bool DataLibrary::release_memory(RESOURCE_ID in) {

	vector<data_chunk*> res;
	chunk_registry->get_releasable_chunks(in, res);
	OPERATION_T t = FREE_CHUNK;

// FILL chunk array to release

	int r = res.size();
	if (r == 0) {
		fprintf(stderr, "WARNING: device %s memory exhausted\n",
				get_enum_string(in));
		//chunk_registry->get_releasable_chunks2(in, res);
		if (res.size() == 0) {
			//print_registry();
			//checkCUDAmemory("release");

			//fprintf(stderr, "WARNING: device %s memory busted\n", get_enum_string(in));
			return false;
		}
		t = RETRIEVE_TO_HOST_AND_FREE;

	}

//send signals to controllers

	vector<data_chunk*>::iterator i;
	for (i = res.begin(); i != res.end(); i++) {
		//asyncronous cause the thread maybe be the same that request and frees the chunk
		SystemMessage* b = new SystemMessage(t, (void*) *i,
				resource_manager->controllers[in]);
		resource_manager->controllers[in]->add_operation(b);
		chunk_registry->free_chunk((*i)->id, in);
		//printf("Freeing chunk %d\n", (*i)->id);
		//b->wait_completion();
	}

	return true;
}

void DataLibrary::erase_memory(RESOURCE_ID in) {

	vector<data_chunk*> res;
	chunk_registry->get_releasable_chunks(in, res);

	chunk_registry->get_releasable_chunks2(in, res);

	vector<data_chunk*>::iterator i;
	for (i = res.begin(); i != res.end(); i++) {

		SystemMessage* b = new SystemMessage(FREE_CHUNK, (void*) *i,
				resource_manager->controllers[in]);
		resource_manager->controllers[in]->add_operation(b);
		chunk_registry->free_chunk((*i)->id, in);

	}

}

void DataLibrary::revertTaskDataReady(Task*t, RESOURCE_ID in) {
	chunk_registry->revertTaskDataReady(t, in);

}

