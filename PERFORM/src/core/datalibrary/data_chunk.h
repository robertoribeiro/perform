/*
 * data_chunk.h
 *
 *  Created on: Jan 21, 2011
 *      Author: rr
 */

#ifndef DATA_CHUNK_H_
#define DATA_CHUNK_H_

#include "core.h"
//#include "Domain_.h"
#include "../List.h"
#include <stdio.h>
#include <string.h>

using namespace std;

class Domain_;

struct res_void {
	RESOURCE_ID first;
	void* second;

	__H_D__ res_void (RESOURCE_ID first_,void* second_){
		first = first_;
		second = second_;
	}

	__H_D__ res_void (){

		}
};

class data_chunk {

private:
	//LList<res_void> locations;

	address locations[MAX_DEVICES];

public:
	int id;
	float n_bytes;
	uint element_size;
	uint pitch; // default =1;
	address hook_pointer;
	bool divided;
	Domain_* domain;
	data_chunk* parent_chunk;

		 __H_D__ data_chunk() {
			 memset(locations,0,sizeof(address)*MAX_DEVICES);
			 parent_chunk = NULL;
	 };
	 data_chunk(void* original_host_pointer_, float n_bytes_, uint element_size_, uint line_length_,Domain_* d);

	 __H_D__ ~data_chunk(){};

	//RESOURCE_ID getLocation(){
	//return location;
	//}


	 __H_D__ void addLocation(RESOURCE_ID dev, void* data_);
	//void removeLocation(RESOURCE_ID dev);

	 __HD__ void* getDataPointer(RESOURCE_ID dev) {
//
//		 void* r= NULL;
//		// locations.lock();
//		for (ListNode<res_void>* __it = locations.getIterator();locations.canStep(__it);locations.step(__it)) {
//		 //for(locations.gotoBeginning();locations.canStep();locations.step()){
//			 if (locations.getCursorValue(__it).first == dev) {
//				 r = locations.getCursorValue(__it).second;
//				 locations.stopStep();
//			 }
//		 }
		// locations.unlock();

		return (locations[dev]);
	}
};

#endif /* DATA_CHUNK_H_ */
