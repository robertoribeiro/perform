/*
 * MSI_Table.h
 *
 *  Created on: Feb 21, 2011
 *      Author: rr
 */

#ifndef MSI_TABLE_H_
#define MSI_TABLE_H_

#include "core.h"
#include "../resourcemanager/ResourceManager.h"
#include <iterator>
extern ResourceManager* resource_manager;

typedef char lock;
typedef map<RESOURCE_ID, MSI_T> msi_line;
typedef pair<lock, msi_line*> msi_line_lockfree;

/**
 * SaP chuncks after a task execution that were in S state. Which also means that there a device with a copy of this chunk
 */
class MSI_Table {
private:

	map<int, msi_line_lockfree> table_lock_free;

	map<int, data_chunk*> chunk_list; /** registry of all the chunks pointers */
	int inside_count; /** This is to allow insertions in a lock free structure,
	 if someone is inside the inserter waits */
	lock insertion_lock; /** The lock used for insertion. When inserting all the structure is blocked.
	 Also used for print() */

public:
	MSI_Table() {
		table_lock_free = map<int, msi_line_lockfree> ();
		//chunk_list= map<int, data_chunk*>();
		inside_count = 0;
		insertion_lock = 0;

	}
	virtual ~MSI_Table();

	msi_line* init_MSI_line() {

		//new pair<RESOURCE_ID,MSI_T>(device_count, NA>)
		msi_line* line = new msi_line();
		vector<RESOURCE_ID> v = resource_manager->resources_keys;
		line->insert(pair<RESOURCE_ID, MSI_T> (hostID, NA));

		for (uint i = 0; i < v.size(); i++) {
			if (v[i] != hostID)
				line->insert(pair<RESOURCE_ID, MSI_T> (v[i], NA));
		}
		return line;
	}
//private:
	bool get_and_lock_line(int dc_id, msi_line** line) {

		while (!__sync_bool_compare_and_swap(&insertion_lock, 0, 1)) {
			pthread_yield();
		}
		/** we could just check if the insertion lock is activated but after this c
		 * check a thread might try to insert. So we lock the strut just for increment the insertions
		 */
		__sync_fetch_and_add(&inside_count, 1);

		while (!__sync_bool_compare_and_swap(&insertion_lock, 1, 0)) {
			pthread_yield();
		}

		if (table_lock_free.find(dc_id) != table_lock_free.end()) {

			lock* l = &((table_lock_free[dc_id]).first);

			while (!__sync_bool_compare_and_swap(l, 0, 1)) {
				pthread_yield();
			}

			//pthread_mutex_lock(&((table_lock_free[dc_id]).first));
			//cout << "line locked:" << dc_id << endl;
			*line = table_lock_free[dc_id].second;

			return true;
		} else {
			print();
			return false;
		}

	}

	void unlock_line(int dc_id) {
		//	lock* l = &((table_lock_free[dc_id]).first);
		//cout << "line unlocked:" << dc_id << endl;


		lock* l = &((table_lock_free[dc_id]).first);
		while (!__sync_bool_compare_and_swap(l, 1, 0)) {
			pthread_yield();
		}

		__sync_fetch_and_sub(&inside_count, 1);

		//pthread_mutex_unlock(&((table_lock_free[dc_id]).first));
		/*
		 while(*l != 0){
		 __sync_val_compare_and_swap(l, 1,0);
		 }
		 */
	}
	void unlock_line() {

		__sync_fetch_and_sub(&inside_count, 1);

	}
public:
	void register_chunk(data_chunk* dc, int pos) {

		lock l;
		msi_line* line;
		msi_line_lockfree line_l;

		while (!__sync_bool_compare_and_swap(&inside_count, 0, 0)) {
			pthread_yield();
		}

		while (!__sync_bool_compare_and_swap(&insertion_lock, 0, 1)) {
			pthread_yield();
		}

		line = init_MSI_line();

		//pthread_mutex_init(&l, NULL);
		l = 0;
		line_l = pair<lock, msi_line*> (l, line);

		if (table_lock_free.find(pos) != table_lock_free.end()) {
			cout << "element already exists" << endl;
		}
		table_lock_free.insert(pair<int, msi_line_lockfree> (pos, line_l));

		//	system_data_chunks.insert(pair<int, data_chunk*> (dc->id, dc));

		chunk_list.insert(pair<int, data_chunk*> (pos, dc));
		dc->id = pos;

		LOG(3,cout << "Chunk " << pos << " registred " << endl;)

		while (!__sync_bool_compare_and_swap(&insertion_lock, 1, 0)) {
			pthread_yield();
		}
		//print();
	}

	void unregister(int dc) {

		msi_line* line;
		get_and_lock_line(dc, &line);

		if (line != NULL) {

			table_lock_free.erase(dc);
			//chunk_list.erase(dc);
		}
		LOG(2,cout << "Number of chunks in msi: " << table_lock_free.size() << endl;)
		unlock_line();
		//cout << "Number of chunks in list: " << chunk_list.size() << endl;
	}

	void dc_latest_version_locations(int dc_id, vector<RESOURCE_ID>* res) {

		msi_line* line;
		get_and_lock_line(dc_id, &line);
		std::map<RESOURCE_ID, MSI_T>::iterator i;
		for (i = line->begin(); i != line->end(); i++) {
			//delete i->second;

			switch (i->second) {
			case M:
				unlock_line(dc_id);
				return;

			case S:
				res->push_back(i->first);
				break;
			case SaP:
				res->push_back(i->first);
				break;
			case Q:
				res->push_back(i->first);
				break;
			default:
				break;
			}
		}

		unlock_line(dc_id);

	}

	void register_a_share(int dc, RESOURCE_ID dev_to) {

		msi_line* line;
		get_and_lock_line(dc, &line);

		if (line == NULL) {
			cout << "chunk not registred" << endl;
		} else
			(*line)[dev_to] = S;

		unlock_line(dc);

	}

	void mark_queried(int dc, RESOURCE_ID dev_to) {

		msi_line* line;
		get_and_lock_line(dc, &line);

		if (line == NULL) {
			cout << "chunk not registred" << endl;
		} else
			(*line)[dev_to] = Q;

		unlock_line(dc);

	}

	void unmark_queried(int dc, RESOURCE_ID dev_to) {

		msi_line* line;
		get_and_lock_line(dc, &line);

		if (line == NULL) {
			cout << "chunk not registred" << endl;
		} else
			(*line)[dev_to] = S;

		unlock_line(dc);

	}

	void park_chunk(int dc, RESOURCE_ID dev_to) {
		msi_line* line;
		get_and_lock_line(dc, &line);

		if (line == NULL) {
			cout << "chunk not registred" << endl;
		} else {
			if ((*line)[dev_to] == S) {
				(*line)[dev_to] = SaP;

			}

		}

		unlock_line(dc);
	}
	/**
	 * Theoretically, the chunk is never in Q state.
	 */
	void register_a_write(int dc, RESOURCE_ID dev_to) {

		msi_line* line;
		get_and_lock_line(dc, &line);
		std::map<RESOURCE_ID, MSI_T>::iterator i;
		for (i = line->begin(); i != line->end(); i++) {

			if ((i->second == S || i->second == SaP) && i->first != dev_to)
				i->second = I;
			else if (i->first == dev_to)
				i->second = M;

		}

		unlock_line(dc);

	}

	void register_a_write_duplicate(int dc, RESOURCE_ID dev_to) {

			msi_line* line;
			get_and_lock_line(dc, &line);
			std::map<RESOURCE_ID, MSI_T>::iterator i;
			for (i = line->begin(); i != line->end(); i++) {

				if (i->first == dev_to || i->first == hostID)
					i->second = MS;

			}

			unlock_line(dc);

		}


	void free_chunk(int dc, RESOURCE_ID d) {

		msi_line* line;
		get_and_lock_line(dc, &line);

		line->at(d) = NA;

		unlock_line(dc);
		LOG(3,print();)

	}

	/**
	 * Used in dice/domain_chunking contexts
	 */
	void invalidate_chunk(int dc) {

		msi_line* line;
		get_and_lock_line(dc, &line);
		std::map<RESOURCE_ID, MSI_T>::iterator i;
		for (i = line->begin(); i != line->end(); i++) {

			if (i->second == S || i->second == SaP || i->second == Q)
				i->second = I;

		}

		unlock_line(dc);

		LOG(3,print();)
	}

	void invalidate_chunk_copies(int dc) {

		msi_line* line;
		get_and_lock_line(dc, &line);
		std::map<RESOURCE_ID, MSI_T>::iterator i;
		for (i = line->begin(); i != line->end(); i++) {

			if ((i->second == S || i->second == SaP || i->second == Q) && i->first != hostID)
				i->second = I;

		}

		unlock_line(dc);

		LOG(3,print();)
	}

	bool device_has_a_valid_copy(RESOURCE_ID dev, int dc) {

		bool res;
		msi_line* line;
		get_and_lock_line(dc, &line);
		if ((*line)[dev] == S || (*line)[dev] == SaP || (*line)[dev] == Q)
			res = true;
		else
			res = false;

		unlock_line(dc);

		return res;
	}

	bool device_has_chunk(RESOURCE_ID dev, int dc) {

		bool res;
		msi_line* line;
		get_and_lock_line(dc, &line);
		if ((*line)[dev] != NA)
			res = true;
		else
			res = false;

		unlock_line(dc);

		return res;
	}

	RESOURCE_ID get_first_valid_copy(int dc_id) {

		msi_line* line;
		get_and_lock_line(dc_id, &line);
		RESOURCE_ID res = NONE;

		std::map<RESOURCE_ID, MSI_T>::iterator i;

		for (i = line->begin(); i != line->end(); i++) {

			if (i->second == S || i->second == SaP || i->second == Q) {
				res = i->first;
				break;
			}

		}
		unlock_line(dc_id);
		return res;

	}

	void get_off_host_registred_chunks(vector<pair<RESOURCE_ID, data_chunk*> >* res) {

		map<int, msi_line_lockfree>::iterator it;

		for (it = table_lock_free.begin(); it != table_lock_free.end(); it++) {

			RESOURCE_ID r = get_first_valid_copy(it->first);
			if (r != hostID && r != NONE) {
				data_chunk* dc = chunk_list[it->first];
				if (!(dc->divided))
					res->push_back(pair<RESOURCE_ID, data_chunk*> (r, dc));
			}
		}
	}

	/**
	 * Should never be called in CPU.
	 * Gets only the invalid and SaP chunks
	 */
	void get_releasable_chunks(RESOURCE_ID dev, vector<data_chunk*>& res) {

		map<int, msi_line_lockfree>::iterator it;

		for (it = table_lock_free.begin(); it != table_lock_free.end(); it++) {
			msi_line* line;
			get_and_lock_line(it->first, &line);
			if ((*line)[dev] == I || (*line)[dev] == SaP) {

				res.push_back(chunk_list[it->first]);
			}
			unlock_line(it->first);
		}
	}

	/**
	 * Get the chunks that can be copied to the host and freed
	 * i.e. all S chunks, possible?
	 */
	void get_releasable_chunks2(RESOURCE_ID dev, vector<data_chunk*>& res) {

		map<int, msi_line_lockfree>::iterator it;

		for (it = table_lock_free.begin(); it != table_lock_free.end(); it++) {
			msi_line* line;
			get_and_lock_line(it->first, &line);
			if ((*line)[dev] == S /*&& (*line)[hostID] == I*/) {
				res.push_back(chunk_list[it->first]);
			}
			unlock_line(it->first);
		}
	}
	__HD__
	void revertTaskDataReady(Task*t, RESOURCE_ID dev) {

		msi_line* line;
		//t->data_chunks->lock();
		for (ListNode<taskAssociated_chunk>* __it = t->data_chunks->getIterator();t->data_chunks->canStep(__it);t->data_chunks->step(__it)) {
		//for (t->data_chunks->gotoBeginning(); t->data_chunks->canStep(); t->data_chunks->step()) {
			data_chunk* i = t->data_chunks->getCursorValue(__it).first;
			//PERMISSION_T i_p = t->data_chunks->getCursor().second;

			get_and_lock_line(i->id, &line);
			if ((*line)[dev] == M || (*line)[dev] == Q) {
				(*line)[dev] = S;
			}
			unlock_line(i->id);

		}
		//t->data_chunks->unlock();
	}

	void print() {

		map<int, msi_line_lockfree>::iterator i;
		msi_line::iterator j;

		while (!__sync_bool_compare_and_swap(&insertion_lock, 0, 1)) {
			pthread_yield();
		}

		if (table_lock_free.size() == 0)
			cout << "No chunks registred" << endl;

		int index = 0;
		printf("\n");
		for (i = table_lock_free.begin(); i != table_lock_free.end(); i++) {
			//cout << "lock status:" << i->first << endl;

			msi_line* line;
			//get_and_lock_line(i->first, &line);
			line = table_lock_free[i->first].second;
			//cout << "Line:" << index << " " << "dc:" << i->first << " ";
			printf("Line %1d dc:%3d| ", index, i->first);
			for (j = line->begin(); j != line->end(); j++) {
				printf("%3s ", get_enum_string(j->second));
				//cout << get_enum_string(j->second) << " ";

			}
			cout << endl;
			index++;

		}

		while (!__sync_bool_compare_and_swap(&insertion_lock, 1, 0)) {
			pthread_yield();
		}
		cout << endl;

	}

};

#endif /* MSI_TABLE_H_ */
