/*
 * Domain_.h
 *
 *  Created on: Feb 14, 2011
 *      Author: rr
 */

#ifndef DOMAIN_H_
#define DOMAIN_H_

#include "data_chunk.h"
#include "../List.h"
#include "core.h"

//class data_chunk;

typedef struct s_dim_sapce {

	int start, end;

	__H_D__
	s_dim_sapce(int start_ = 0, int end_ = 0) :
		start(start_), end(end_) {
	}

	__H_D__
	int length() {
		int l = end - start;
		return (l <= 0) ? 1 : l;
	}

} dim_space;

/**
 *
 * The user must be aware that if a task is diceable, not all the elements are availabe
 */
class Domain_ {
public:

	void* device_data_buffer; //used only in device side.Updated when task assigned to device.
	pthread_mutex_t m_lock;

	Domain_* parent;
	PERMISSION_T permissions;

	uint n_dim_space;
	uint element_size;

	dim_space X; // in 1 dimension is the array length, in 2 is the number of lines
	dim_space Y;// in 1 dimension is 0, in 2 is the number of columns

	//const uint* parent_starts;
	//const uint* dim_lengths;

	data_chunk* phy_chunk;

	bool chunked;
	bool reusable;

	//vector<Domain_*> childs;
	LList<Domain_*>* childs;

	__H_D__
	Domain_() {
#ifndef __CUDACC__
		pthread_mutex_init(&m_lock, NULL);
		childs = new LList<Domain_*>();
#endif

	}

	__H_D__
	Domain_(uint dim_space_, PERMISSION_T intention_, void* original_host_pointer_,
			uint element_size_, dim_space x, dim_space y = dim_space(), bool reusable_ = true) {

		chunked = true;
		n_dim_space = dim_space_;
		element_size = element_size_;
		X = x;
		Y = y;

		float n_bytes_ = (X.length() * (Y.length() != 0 ? Y.length() : 1)) * element_size;

		phy_chunk = new data_chunk(original_host_pointer_, n_bytes_, element_size, Y.length(),this);


		permissions = intention_;

		//it's not a sub-domain
		parent = NULL;
		reusable = reusable_;

		pthread_mutex_init(&m_lock, NULL);
		childs = new LList<Domain_*>();

	}

	/*
	 * dim_space are the hook point relative to the parent domain
	 */
	__H_D__
	Domain_(Domain_* parent_, uint dim_space_, PERMISSION_T intention_, dim_space x,
			dim_space y = dim_space(), bool reusable_ = true) {

		//it's a sub-domain
		chunked = false;
		phy_chunk = parent_->phy_chunk;
		parent = parent_;
		n_dim_space = dim_space_;

		X = x;
		Y = y;

		permissions = intention_;
		parent_->childs->insert(this);
		reusable = reusable_;

		pthread_mutex_init(&m_lock, NULL);
		childs = new LList<Domain_*>();

	}

	__H_D__
	~Domain_() {

	}

	//	template <typename T>
	//	__H_D__
	//	T& at(int x,int y, int z)  {
	//
	//		return ((T*)device_data_buffer)[x][y][z];
	//
	//	}

	void lock() {
#ifndef __CUDACC__
		pthread_mutex_lock(&m_lock);
#endif
	}

	void unlock() {
#ifndef __CUDACC__
		pthread_mutex_unlock(&m_lock);
#endif
	}

	__H_D__
	void setDeviceDataPointer(void* pt) {
		device_data_buffer = pt;
	}

	__H_D__
	bool has_childs() {
		if (childs->length == 0)
			return false;
		else
			return true;
	}
};

template<typename T>
class Domain: public Domain_ {
public:
	__H_D__
	Domain(uint dim_space_, PERMISSION_T intention_, T* original_host_pointer_, dim_space x,
			dim_space y = dim_space(), bool reusable_ = true) :
		Domain_(dim_space_, intention_, (void*) original_host_pointer_, sizeof(T), x, y, reusable_) {
	}

	/*
	 * dim_space are the hook point relative to the parent domain
	 */
	__H_D__
	Domain(Domain_* parent_, uint dim_space_, PERMISSION_T intention_, dim_space x,
			dim_space y = dim_space(), bool reusable_ = true) :
		Domain_(parent_, dim_space_, intention_, x, y) {

	}

	__H_D__
	Domain() {
	}

	__H_D__
	~Domain() {

	}

	/**
	 * The user need to limit the element access usign the limits defined in the domain.
	 * This calculus maps the global iterators provided by the user to the local iterators for the sub-domain
	 */
	__H_D__
	T& at(int x) {

		int local_offset = x - X.start;

		return ((T*) device_data_buffer)[local_offset];

	}

	__H_D__
	T& at(int x, int y) {

		T* m = (T*) device_data_buffer;

		int local_offset_x = x - X.start;
		int local_offset_y = y - Y.start;

		//m[x*X.length()+ y]
		return m[local_offset_x * Y.length() + local_offset_y];

	}

};

#endif /* DOMAIN_H_ */
