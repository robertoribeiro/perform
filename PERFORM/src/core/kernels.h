/*
 * kernels.h
 *
 *  Created on: Dec 27, 2010
 *      Author: rr
 */

#ifndef KERNELS_H_
#define KERNELS_H_


void k_CPU_test(void** data_pointers,void** args,void* stream);

void k_CUDA_test(void** data_pointers,void** args,void* stream);

void k_CPU_sleep(void** data_pointers,void** args,void* stream);

void k_CUDA_sleep(void** data_pointers,void** args,void* stream);

void k_CUDA_streamed_test(void** data_pointers,void** args,void* stream);

void k_CUDA_work_gen_test(void** data_pointers,void** args,void* stream);

void k_CUDA_light_work_gen_test(Task t);

void k_CPU_light_work_gen_test(Task t);

/**
 * User responsibility too serialize the array of tasks
 */

//void k_CPU_GEMM(void** data_pointers,void** args,void* stream);

//void k_CUDA_GEMM(void** data_pointers,void** args,void* stream);


void k_CUDA_init(Task* t_);

void k_CPU_init(Task* t_) ;

#endif /* KERNELS_H_ */
