#ifndef QUEUE_H_
#define QUEUE_H_

#include <deque>
#include <iostream>
#include <stdlib.h>
#include <pthread.h>

template<class T>
class ConcurrentQueue {
protected:
	std::deque<T> c; // container for the elements

public:
	pthread_mutex_t mutex;
	pthread_cond_t cond_empty;


	volatile bool kill_requested;
	volatile bool skip; // used to prioritize other opertaions
	volatile bool blocking;
	volatile bool temp_no_block;

	ConcurrentQueue(bool blocking_) {
		temp_no_block=false;
		blocking = blocking_;
		kill_requested = false;
		skip = false;
		pthread_mutex_init(&mutex, NULL);
		if (blocking) pthread_cond_init(&cond_empty, NULL);

	}

	// number of elements
	typename std::deque<T>::size_type size() {
		//std::cout << pthread_self() << "entered" << std::endl;
		pthread_mutex_lock(&mutex);
		int s = c.size();
		pthread_mutex_unlock(&mutex);
		//std::cout << pthread_self() << "exited" << std::endl;
		return s;
	}

	// is queue empty?
	bool empty() {
		pthread_mutex_lock(&mutex);
		bool b = c.empty();
		pthread_mutex_unlock(&mutex);
		return b;
	}

	// insert element into the queue
	void push(const T& elem) {
		pthread_mutex_lock(&mutex);
		c.push_back(elem);
		pthread_mutex_unlock(&mutex);
		if (blocking) pthread_cond_broadcast(&cond_empty);
	}

	// read element from the queue and return its value
	T pop() {
		T elem = NULL;
		pthread_mutex_lock(&mutex);
		if (blocking) {
			while (c.size() == 0 && !kill_requested && !skip && !temp_no_block) {
				//std::cout << "empy cond " << pthread_self() << std::endl;
				pthread_cond_wait(&cond_empty, &mutex);

			}
		}
		if (!skip) {
			if (c.size() != 0) {
				elem = c.front();
				c.pop_front();
			}

		}
		pthread_mutex_unlock(&mutex);
		return elem;
	}

	// return value of next element
	T& front() {
		pthread_mutex_lock(&mutex);
		if (blocking) while (c.size() == 0)	pthread_cond_wait(&cond_empty, &mutex);
		T& res = c.front();
		pthread_mutex_unlock(&mutex);
		return res;
	}
};

#endif /* QUEUE_H_ */
