#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <limits>


using namespace std;


inline double mass() {
	double m = 0;
	while (m == 0 || m != m || m <= 0)
		m = (1 / double(rand() % 1000 + 2)) * pow(10.0, (20.0 + double(rand() % 10 - 5)));
	return m;
}

inline double mass2(int n) {
	return 0.00000023 * n;
}

void balanced() {
	cout << "---[ balanced set ]---" << endl;
	ofstream o;
	o.open("input.debug.txt");
	//int r = 50000, n=1000000, i=0;
	int n = 10000, i = 0;
	//double r=2.5e11-1;
	double r = 5e7;
	double x = 0, y = 0, z = 0, m = 0;
	o.precision(numeric_limits<double>::digits10);
	cout.precision(numeric_limits<double>::digits10);

	o << r << endl;
	o << n << endl;

	r = 5e6;

	for (; i < n; i++) {
		x = rand() * (r * 2) / RAND_MAX - r;// + 1.0/double(rand()%1000+2); //the 1.0/ unnecessary?
		y = rand() * (r * 2) / RAND_MAX - r;// + 1.0/double(rand()%1000+2);
		z = rand() * (r * 2) / RAND_MAX - r;// + 1.0/double(rand()%1000+2);
		m = mass();
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 1000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}
	o.close();
}

void oppcorners() {
	cout << "---[ oppcorners set ]---" << endl;
	ofstream o;
	o.open("input.ub1,oppcorners.txt");
	int r = 50000, n = 1000000, i = 0;
	double x = 0, y = 0, z = 0, m = 0;
	o << r << endl;
	o << n << endl;

	//(x,y,z) = (-,-,-)
	int r1 = 10000 - 1;
	for (i = 0; i < 200000; i++) {
		x = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);
		y = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);
		z = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	//(x,y,z) = (+,+,+)
	for (; i < 400000; i++) {
		x = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);
		y = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);
		z = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	for (; i < n; i++) {
		x = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}
	o.close();
}

void center() {
	cout << "---[ center set ]---" << endl;
	ofstream o;
	o.open("input.ub2,center1.txt");
	int r = 50000, n = 1000000, i = 0;
	double x = 0, y = 0, z = 0, m = 0;
	o << r << endl;
	o << n << endl;

	r = 10000 - 1;
	for (; i < 400000; i++) {
		x = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	r = 50000 - 1;
	for (; i < n; i++) {
		x = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}
	o.close();
}

void my_center() {
	cout << "---[ center set ]---" << endl;
	ofstream o;
	o.open("/home/rr/work/perform/PERFORM/data/my_center.txt");
	int r;
	int n = 1000000, i = 0;
	double x = 0, y = 0, z = 0, m = 0;
	o << r << endl;
	o << n << endl;

	for (; i < n / 15; i++) {

		r = 15 - 1;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;

		r = 25 - 1;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;

		r = 50 - 1;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		r = 200 - 1;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

		r = 500 - 1;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);
		o << x << " " << y << " " << z << " " << m << endl;

	}
	o.close();
}

void samecorner() {
	cout << "---[ samecorner set ]---" << endl;
	ofstream o;
	o.open("/home/rr/work/perform/PERFORM/data/samecorner.txt");
	int r = 500, n = 1000000, i = 0;
	double x = 0, y = 0, z = 0, m = 0;
	o << r << endl;
	o << n << endl;

	int r1;
	int yy;
	for (; i < n / 15; i++) {
		//(x,y,z) = (-,-,-)
		r1 = 50 - 1;
		 yy = 400;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;


		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		r1 = 100 - 1;
			yy = 300;
		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;
		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;



		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;


		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		r1 = 200 - 1;
		yy = 100;

		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;
		x = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		y = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);
		z = -rand() % (r1 * 2) - yy + 1.0 / double(rand() % 1 + 2);

		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;

		x = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		y = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		z = rand() % (r * 2) - r + 1.0 / double(rand() % 1 + 2 * (rand() % 7 + 1));
		m = mass2(1000);

		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}
	o.close();
}

void star() {
	cout << "---[ star set ]---" << endl;
	ofstream o;
	o.open("input.ub4,star.txt");
	int r = 50000, n = 1000000, i = 0;
	double x = 0, y = 0, z = 0, m = 0;
	o << r << endl;
	o << n << endl;

	//(x,y,z) = (-,-,-)
	int r1 = 10000 - 1;
	for (i = 0; i < 200000; i++) {
		x = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);
		y = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);
		z = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	//(x,y,z) = (+,+,+)
	for (; i < 400000; i++) {
		x = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);
		y = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);
		z = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	//(x,y,z) = (+,+,+)
	for (; i < 600000; i++) {
		x = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);
		y = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);
		z = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	//(x,y,z) = (+,+,+)
	for (; i < 800000; i++) {
		x = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);
		y = rand() % (r1 * 2) + 30000 + 1.0 / double(rand() % 100 + 2);
		z = -rand() % (r1 * 2) - 30000 + 1.0 / double(rand() % 100 + 2);

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}

	for (; i < 1000000; i++) {
		x = rand() % (r1 * 2) - r1 + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		y = rand() % (r1 * 2) - r1 + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));
		z = rand() % (r1 * 2) - r1 + 1.0 / double(rand() % 100 + 2 * (rand() % 7 + 1));

		m = mass();
		if (i % 5000 == 0)
			m = m * (100 + 1 << (rand() % 5 + 1));
		o << x << " " << y << " " << z << " " << m << endl;
		if (i % 50000 == 0)
			cout << "i:" << i << "\t" << x << " " << y << " " << z << " " << m << endl;
	}
	o.close();
}





int main(int args, char *argc[]) {
	srand(time(NULL));
	//	balanced();
	//	oppcorners();
	//center();
	//my_center();
	//samecorner();
	//	star();




}
