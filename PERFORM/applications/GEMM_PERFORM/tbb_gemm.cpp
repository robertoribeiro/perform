#include <iostream>
#include "perform_utils.h"
#include "core.h"
#include "tbb/blocked_range2d.h"

#include "dgemm.h"

void multiply(const blocked_range2d<int>& r, Domain<float> A, Domain<float> B, Domain<float> C) {

	float* AA = (float*) (A.device_data_buffer);
	float*BB = (float*) (B.device_data_buffer);
	float*CC = (float*) (C.device_data_buffer);

	float staticA;

	for (int k = A.Y.start; k < A.Y.end; k++) {
		for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
			staticA = AA[i * A.Y.length() + k];
			for (int j = r.cols().begin(); j != r.cols().end(); ++j) {
				CC[i * C.Y.length() + j] += staticA * BB[k * B.Y.length() + j];
				}
		}
	}

	//		for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
	//			for (int j = r.cols().begin(); j != r.cols().end(); ++j) {
	//				float acc = 0;
	//				for (int k = A.Y.start; k < A.Y.end; k++) {
	//					acc += A.at(i, k) * B.at(k, j);
	//				}
	//				C.at(i, j) = acc;
	//			}
	//		}
}

void k_CPU_GEMM(Task* t_) {

	Domain<float> A;
	t_->getDomain(0, A);

	Domain<float> B;
	t_->getDomain(1, B);

	Domain<float> C;
	t_->getDomain(2, C);

	int sub_sub_block;

	if (C.X.length() >= 32)
		sub_sub_block = 32;
	else
		sub_sub_block = C.X.length();

	//tbb::parallel_for ( blocked_range2d<int>(0, C.X.length(), sub_sub_block, 0, C.Y.length(), sub_sub_block),	MM(A,B,C) );
	tbb::parallel_for(blocked_range2d<int> (0, C.X.length(), sub_sub_block,0, C.X.length(),sub_sub_block), MM(A, B, C));


}
