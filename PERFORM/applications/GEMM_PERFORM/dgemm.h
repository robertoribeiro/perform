/*
 * dgemm.h
 *
 *  Created on: Apr 6, 2011
 *      Author: rr
 */

#ifndef DGEMM_H_
#define DGEMM_H_

#define GEMM

#include "../../perform.h"


typedef double GEMM_TYPE;

#include "user_defined_task.h"
#include "tbb/blocked_range2d.h"

using namespace tbb;
using namespace std;



void k_CPU_GEMM(Task* t);

void k_CUDA_GEMM(Task* t);

void multiply(const blocked_range2d<int>& r,Domain<GEMM_TYPE> A, Domain<GEMM_TYPE> B, Domain<GEMM_TYPE> C);

double job_GEMM(int argc, char *argv[], int N );

#endif /* DGEMM_H_ */
