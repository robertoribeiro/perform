#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"
#include <math.h>
#include "dgemm.h"

class Task_MM: public Task {
public:
	__H_D__
	Task_MM() :
		Task() {
	}

	Task_MM(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		Domain<GEMM_TYPE>* A;
		getDomain(0, A);

		Domain<GEMM_TYPE>* B;
		getDomain(1, B);

		Domain<GEMM_TYPE>* C;
		getDomain(2, C);

		int task_count = 0;

		float len1 = floor(job_elements * m.value);

		if (C->X.length() <= len1 || len1 == 0) {
			Task_MM** new_tasks = new Task_MM*[1];
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			return 1;
		}

		Domain<GEMM_TYPE>* subA[2];

		dim_space XA = dim_space(A->X.start, A->X.start + len1);
		dim_space YA = dim_space(A->Y.start, A->Y.end);

		subA[0] = new Domain<GEMM_TYPE> (A, 2, R, XA, YA, false);

		XA = dim_space(A->X.start + len1, A->X.end);
		YA = dim_space(A->Y.start, A->Y.end);

		subA[1] = new Domain<GEMM_TYPE> (A, 2, R, XA, YA, false);

		Task_MM** new_tasks = new Task_MM*[2];
		//task0
		Task_MM* t = new Task_MM(REGULAR);

		dim_space X = dim_space(C->X.start, C->X.start + len1);
		dim_space Y = dim_space(C->Y.start, C->Y.end);
		Domain<GEMM_TYPE>* subdomainC = new Domain<GEMM_TYPE> (C, 2, C->permissions, X, Y, false);

		t->associate_domain(subA[0]);
		t->associate_domain(B);
		t->associate_domain(subdomainC);
		t->device_preference = device_preference;
		t->associate_kernel(this);
		t->wl.value = len1 / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->job_elements = job_elements;

		new_tasks[task_count++] = t;
		//task1
		t = new Task_MM(REGULAR);

		X = dim_space(C->X.start + len1, C->X.end);
		Y = dim_space(C->Y.start, C->Y.end);
		subdomainC = new Domain<GEMM_TYPE> (C, 2, C->permissions, X, Y, false);

		t->associate_domain(subA[1]);
		t->associate_domain(B);
		t->associate_domain(subdomainC);
		t->device_preference = device_preference;
		t->associate_kernel(this);
		t->wl.value = (C->X.length() - len1) / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->job_elements = job_elements;

		new_tasks[task_count++] = t;

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

	int dice1(Task**& new_tasks_, PM_Metric_normalized m) {

		Domain<float>* A;
		getDomain(0, A);

		Domain<float>* B;
		getDomain(1, B);

		Domain<float>* C;
		getDomain(2, C);

		int task_count = 0;

		float N = C->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);
		//printf("dicing in %f\n",m.value);

		if (N < 32 * divide_task_by)
			divide_task_by = N / 32;

		if (divide_task_by < 2) {
			printf("error in dice\n");
			return 0;
		}

		Domain<float>* subA[divide_task_by];
		//Domain<float>* subB[divide_task_by];

		int line_step = C->X.length() / divide_task_by;

		for (int i = 0; i < divide_task_by; i++) {

			dim_space XA = dim_space(i * line_step + A->X.start, A->X.start + (i + 1) * line_step);
			dim_space YA = dim_space(A->Y.start, A->Y.end);

			subA[i] = new Domain<float> (A, 2, R, XA, YA, false);

		}

		Task_MM** new_tasks = new Task_MM*[divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {

			Task_MM* t = new Task_MM(REGULAR);

			dim_space X = dim_space(i * line_step + C->X.start, C->X.start + (i + 1) * line_step);
			dim_space Y = dim_space(C->Y.start, C->Y.end);

			Domain<float>* subdomainC = new Domain<float> (C, 2, C->permissions, X, Y, false);

			t->associate_domain(subA[i]);
			t->associate_domain(B);
			t->associate_domain(subdomainC);

			t->device_preference = device_preference;
			t->associate_kernel(this);
			t->wl.value = wl.value / (float) divide_task_by;
			t->dice_lvl = dice_lvl + 1;

			new_tasks[task_count++] = t;

		}
		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

	int dice2(Task**& new_tasks_, PM_Metric_normalized m) {

		Domain<GEMM_TYPE>* A;
		getDomain(0, A);

		Domain<GEMM_TYPE>* B;
		getDomain(1, B);

		Domain<GEMM_TYPE>* C;
		getDomain(2, C);

		int task_count = 0;

		float N = C->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);
		//printf("dicing in %f\n",m.value);

		//	if (N < 32 * divide_task_by)
		//		divide_task_by = N / 32;

		if (divide_task_by < 2) {
			Task_MM** new_tasks = new Task_MM*[1];
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			return 1;

		}

		Domain<GEMM_TYPE>* subA[divide_task_by];
		Domain<GEMM_TYPE>* subB[divide_task_by];

		if (C->X.length() % divide_task_by != 0)
			printf("PROBLEM\n");
		int block_step = C->X.length() / divide_task_by;

		for (int i = 0; i < divide_task_by; i++) {

			dim_space XA =
					dim_space(i * block_step + A->X.start, A->X.start + (i + 1) * block_step);
			dim_space YA = dim_space(A->Y.start, A->Y.end);

			dim_space XB = dim_space(B->X.start, B->X.end);
			dim_space YB =
					dim_space(i * block_step + B->Y.start, B->Y.start + (i + 1) * block_step);

			subA[i] = new Domain<GEMM_TYPE> (A, 2, R, XA, YA, false);
			subB[i] = new Domain<GEMM_TYPE> (B, 2, R, XB, YB, false);
		}

		Task_MM** new_tasks = new Task_MM*[divide_task_by * divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {
			for (int j = 0; j < divide_task_by; j++) {

				Task_MM* t = new Task_MM(REGULAR);

				dim_space X = dim_space(i * block_step + C->X.start,
						C->X.start + (i + 1) * block_step);
				dim_space Y = dim_space(j * block_step + C->Y.start,
						C->Y.start + (j + 1) * block_step);

				Domain<GEMM_TYPE>* subdomainC = new Domain<GEMM_TYPE> (C, 2, C->permissions, X, Y,
						false);

				t->associate_domain(subA[i]);
				t->associate_domain(subB[j]);
				t->associate_domain(subdomainC);

				t->device_preference = device_preference;
				t->associate_kernel(this);
				t->wl = PM_Metric_normalized(N / (block_step * wl.value));
				t->dice_lvl = dice_lvl + 1;

				new_tasks[task_count++] = t;

			}
		}
		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

};

#endif /* UBK_TASK_H_ */
