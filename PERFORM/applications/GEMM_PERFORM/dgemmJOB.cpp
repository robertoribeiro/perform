#include <iostream>

//#include "utils.h"
#include "core.h"
#include "mkl.h"

#include "dgemm.h"



double job_GEMM(int argc, char *argv[], int N) {

	// just a single cublas, is faster with 8
	float divide_task_by = atoi(argv[3]) == 0 ? 0.0 : atoi(argv[3]);
	//float divide_task_by = 8;


	GEMM_TYPE *matrixA = (GEMM_TYPE*) (PERFORM_alloc(sizeof(GEMM_TYPE) * N * N));
	GEMM_TYPE *matrixB = (GEMM_TYPE*) (PERFORM_alloc(sizeof(GEMM_TYPE) * N * N));
	GEMM_TYPE *matrixC = (GEMM_TYPE*) (PERFORM_alloc(sizeof(GEMM_TYPE) * N * N));

	initMatrix<GEMM_TYPE>(matrixA, N, N, 5);
	initMatrixIdentity<GEMM_TYPE>(matrixB, N, N);
	initMatrix<GEMM_TYPE>(matrixC, N, N, 0);

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();

	fprintf(stderr,"Job started...\n");
	//PERFORM_signal_JobStarted();
	timer->start();

	Domain<GEMM_TYPE>* A = new Domain<GEMM_TYPE> (2, R, matrixA, dim_space(0, N), dim_space(0, N));
	Domain<GEMM_TYPE>* B = new Domain<GEMM_TYPE> (2, R, matrixB, dim_space(0, N), dim_space(0, N));
	Domain<GEMM_TYPE>* C = new Domain<GEMM_TYPE> (2, RW, matrixC, dim_space(0, N), dim_space(0, N));

	Task_MM *t = new Task_MM(REGULAR);

	t->associate_domain(A);
	t->associate_domain(B);
	t->associate_domain(C);

	t->associate_kernel(CPU, &k_CPU_GEMM);
	t->associate_kernel(GPU, &k_CUDA_GEMM);
	t->dice_value= 1/divide_task_by;
	t->job_elements= N;
	if (divide_task_by) t->diceable = true;


	performQ->addTasks(t);

//	Task** v;
//	int task_count = t->dice(v, divide_task_by);
//	performQ->addTasks(v, task_count);

	wait_for_all_tasks();
	//PERFORM_signal_taskSubmissionEnded();

	performDL->splice(C);
	timer->stop();
	//PERFORM_signal_JobFinished(timer->duration);

	//printMatrixFloat((float*) (matrixC), N, N);


	checkMatrices<GEMM_TYPE>(matrixC, matrixA, N, N);


//	PERFORM_free(matrixA);
//	PERFORM_free(matrixB);
//	PERFORM_free(matrixC);

//#ifdef TEST_TIME
	fprintf(stdout, "Simulation done in %s\t", timer->print());
//#endif


	return timer->duration;

}
