#include "../../perform.h"
#include "dgemm.h"
#include "mkl.h"

int main(int argc, char *argv[]) {

	LOG(0.1, cerr << "Run-time started" << endl
	;
	)

	//	//

	int scheduler, dev, initial_dice, cpu_flops, size;

	scheduler = 3;

	printf("Argc %d\n", argc);

	if (argc < 4) {
		dev = 1;
		initial_dice = 16;
		cpu_flops = 400;
		size = 6144;

		//	8192
		//	10240
		//	12288 14336
	} else {
		dev = atoi(argv[1]);
		initial_dice = atoi(argv[2]);
		cpu_flops = atoi(argv[3]);
		size = atoi(argv[4]);
	}

	initPERFORM(scheduler, dev, initial_dice, cpu_flops);

	job_GEMM(argc, argv, size);

	shutdownPERFORM();

	LOG(0.1, cerr << "\n\nRun-time terminated!\n" << endl
	;
	)

	//exit(EXIT_SUCCESS);

	return 0;
}

