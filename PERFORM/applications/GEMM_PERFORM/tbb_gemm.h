
#ifndef TBBDGEMM_H_
#define TBBDGEMM_H_

#define GEMM

#include "dgemm.h"
#include "tbb/blocked_range2d.h"

using namespace tbb;
using namespace std;

class MM {
public:

	Domain<float> A;
	Domain<float> B;
	Domain<float> C;



	MM(Domain<float> a, Domain<float> b, Domain<float> c) :
		A(a), B(b), C(c) {
	}



	void operator()(const blocked_range2d<int>& r) const {
		multiply(r,A,B,C);
	}

};

#endif /* DGEMM_H_ */
