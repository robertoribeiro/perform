#include <iostream>
#include "perform_utils.h"
#include "core.h"

#include "dgemm.h"

#include "kernels.h"

using namespace std;

#define BLOCK_SIZE 32

__global__ void matMultiply(Domain<float> A, Domain<float> B, Domain<float> C) {



	float* AA = (float*) (A.device_data_buffer);
	float*BB = (float*) (B.device_data_buffer);
	float*CC = (float*) (C.device_data_buffer);


	int TX = threadIdx.x + blockIdx.x * blockDim.x;
	int TY = threadIdx.y + blockIdx.y * blockDim.y;

	if (TX < C.Y.length() && TY < C.X.length()) {

		float acc = 0;

		for (int k = 0; k < A.Y.length(); ++k) {
			float Melement = AA[TY * A.Y.length() + k];
			float Nelement = BB[k * B.Y.length() + TX];
			acc += Melement * Nelement;
		}

		CC[TY * C.Y.length() + TX] = acc;
	}

//	int uWM = A.Y.length();
//	int uWN = B.Y.length();
//
//	__shared__
//	float MS[BLOCK_SIZE][BLOCK_SIZE];
//	__shared__
//	float NS[BLOCK_SIZE][BLOCK_SIZE];
//
//	int tx = threadIdx.x, ty = threadIdx.y, bx = blockIdx.x, by = blockIdx.y;
//	int rowM = ty + by * BLOCK_SIZE;
//	int colN = tx + bx * BLOCK_SIZE;
//	float Pvalue = 0;
//
//	for (int m = 0; m < uWM / BLOCK_SIZE; ++m) {
//		MS[ty][tx] = AA[rowM * uWM + (m * BLOCK_SIZE + tx)];
//		NS[ty][tx] = BB[colN + uWN * (m * BLOCK_SIZE + ty)];
//		__syncthreads();
//
//		for (int k = 0; k < BLOCK_SIZE; k++)
//			Pvalue += MS[ty][k] * NS[k][tx];
//		__syncthreads();
//		CC[rowM * C.Y.length() + colN] = Pvalue;
//	}

}

void k_CUDA_GEMM(Task* t_) {

	Domain<float> A;
	t_->getDomain(0, A);

	Domain<float> B;
	t_->getDomain(1, B);

	Domain<float> C;
	t_->getDomain(2, C);

	//float* AA = (float*) (A.phy_chunk->original_host_pointer);
	//float*BB = (float*) (B.phy_chunk->original_host_pointer);
	//float*CC = (float*) (C.phy_chunk->original_host_pointer);

	Task_MM* t = (Task_MM*) t_;


	//gridDIM = dim3(ceil(block_size / blockDIM.x),ceil( block_size / blockDIM.x);


	//printf("blockDIM.x : %d  blockDIM.y: %d,block_size: %d \n",blockDIM.x,blockDIM.y,block_size);


	dim3 blockDIM = dim3(BLOCK_SIZE, BLOCK_SIZE);
	dim3 gridDIM =  dim3(ceil((float)C.Y.length() / (float)blockDIM.x),ceil( (float)C.X.length() / (float)blockDIM.y));


	matMultiply<<<gridDIM,blockDIM,0,t->stream>>>(A, B, C);

//	if (block_size == 64) {
//
//		float *matrix = (float*) (PERFORM_alloc(sizeof(float) * A.X.length() *  A.Y.length()));
//
//		initMatrix(matrix, A.X.length(), A.Y.length(), 5);
//
//		checkMatrices(matrix, AA,A.X.length(), A.Y.length());
//
//	}


	checkCUDAError();

}

void k_CUDA_wrapper_uberkernel(Task* t, Task** task_prms, int child_task_count) {
}

