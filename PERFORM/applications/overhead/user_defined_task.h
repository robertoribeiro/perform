#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"
#include "math.h"


//#define OVERHEAD_MEM

class ubkSIMDTask_pathtracerSIMD32 /*: public ubkTask*/{
public:

	__H_D__
	ubkSIMDTask_pathtracerSIMD32() {

	}

};

class Task_overhead: public Task {
public:

	float arg;
	uint nroots;

	__H_D__
	Task_overhead() :
		Task() {
	}
	__H_D__
	Task_overhead(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

#ifdef OVERHEAD_MEM
		Domain<int>* A;
		getDomain(0, A);
#endif

		int task_count = 0;

		float len1 = floor(job_elements * m.value);

		if (nroots <= len1 || len1 == 0) {
			Task_overhead** new_tasks = new Task_overhead*[1];
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			return 1;
		}

#ifdef OVERHEAD_MEM
		Domain<int>* subA[2];

		dim_space XA = dim_space(A->X.start, A->X.start + len1);

		subA[0] = new Domain<int> (A,1, R, XA, false);

		XA = dim_space(A->X.start + len1, A->X.end);

		subA[1] = new Domain<int> (A, 1, R, XA, false);
#endif


		Task_overhead** new_tasks = new Task_overhead*[2];

		//task0
		Task_overhead* t = new Task_overhead(REGULAR);

#ifdef OVERHEAD_MEM
		t->associate_domain(subA[0]);
#endif

		t->device_preference = device_preference;
		t->associate_kernel(this);
		t->wl.value = len1 / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->job_elements = job_elements;

		t->arg = arg;
		t->nroots = len1;

		new_tasks[task_count++] = t;
		//task1
		t = new Task_overhead(REGULAR);

#ifdef OVERHEAD_MEM
		t->associate_domain(subA[1]);
#endif

		t->device_preference = device_preference;
		t->associate_kernel(this);
		t->wl.value = (nroots - len1) / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->job_elements = job_elements;

		t->arg = arg;
		t->nroots = nroots - len1;

		new_tasks[task_count++] = t;

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

//	int dice2(Task**& new_tasks_, PM_Metric_normalized m) {
//
//		int task_count = 0;
//
//		Domain<RGB>* pixelsRadiance;
//		getDomain(0, pixelsRadiance);
//
//		Domain<char>* geometry_chunk;
//		getDomain(1, geometry_chunk);
//
//		Domain<BVHAccelArrayNode>* bvh;
//		getDomain(2, bvh);
//
//		Domain<prePath>* paths;
//		getDomain(3, paths);
//
//		uint shuffle_size = 30;
//
//		if (((4 * shuffle_size) % CONFIG.STRATA) != 0) {
//			printf("ERROR: strata must be divisor of 128\n");
//			exit(0);
//		}
//
//		uint shuffled_chunks = (paths->X.length() / (CONFIG.STRATA * CONFIG.WIDTH)) / (4
//				* shuffle_size);
//
//		int d = floor(1 / m.value);
//
//		if (d >= shuffled_chunks)
//			d = shuffled_chunks;
//
//		while ((shuffled_chunks % d) != 0 && d > 1)
//			d--;
//
//		int resulting_tasks = d;
//
//		if (resulting_tasks < 2 || m.value > 0.5) {
//			Task_pathtracer** new_tasks = new Task_pathtracer*[1];
//			dice_lvl++;
//			new_tasks[0] = this;
//			new_tasks_ = (Task**) new_tasks;
//			return 1;
//		}
//
//		//int divide_task_by = floor(shuffled_chunks/fst_length);
//
//		int block_step = (shuffled_chunks / resulting_tasks) * (4 * shuffle_size);
//
//		Task_pathtracer** new_tasks = new Task_pathtracer*[resulting_tasks];
//
//		for (int i = 0; i < resulting_tasks; i++) {
//
//			Task_pathtracer* t = new Task_pathtracer(IRREGULAR);
//
//			dim_space bX = dim_space(
//					paths->X.start + (i * block_step) * (CONFIG.STRATA * CONFIG.WIDTH),
//					paths->X.start + ((i * block_step) + block_step) * (CONFIG.STRATA
//							* CONFIG.WIDTH));
//
//			//dim_space bY = dim_space(paths->Y.start,paths->Y.end);
//
//
//			dim_space a = dim_space(
//					pixelsRadiance->X.start + (i * (block_step / CONFIG.STRATA)) * CONFIG.WIDTH,
//					pixelsRadiance->X.start + (i * (block_step / CONFIG.STRATA)) * CONFIG.WIDTH
//							+ (block_step / CONFIG.STRATA) * CONFIG.WIDTH);
//
//			Domain<RGB>* sub_pixelsRadiance = new Domain<RGB> (pixelsRadiance, 1, W, a);
//			Domain<prePath>* sub_paths = new Domain<prePath> (paths, 1, R, bX);
//
//			t->associate_domain(sub_pixelsRadiance);
//			t->associate_domain(geometry_chunk);
//			t->associate_domain(bvh);
//			t->associate_domain(sub_paths);
//
//			t->camera = camera;
//			t->geometry = geometry;
//			t->ray_count = ray_count;
//			t->CONFIG = CONFIG;
//
//			t->device_preference = device_preference;
//			t->parent_host_task = (void*) this;
//			//t->wl = PM_Metric_normalized((block_step * wl.value) / N);
//			t->dice_lvl = dice_lvl + 1;
//
//			t->associate_kernel(this);
//
//			new_tasks[task_count++] = t;
//
//		}
//
//		new_tasks_ = (Task**) new_tasks;
//		return task_count;
//	}
//
//	int diceTo(ubkTask*& new_tasks_, int n_bwu) {
//
//		//PERFORM_acc_timer* timer = new PERFORM_acc_timer();
//
//		//timer->start();
//
//		int task_count = 0;
//
//		Domain<prePath>* paths;
//		getDomain(3, paths);
//
//		float N = paths->X.length();// *  paths->Y.length();
//
//		int divide_task_by = N / n_bwu;
//
//		ubkSIMDTask_pathtracerSIMD32* new_tasks = new ubkSIMDTask_pathtracerSIMD32[divide_task_by];
//
//		RGBe throughput[32];
//		for (int i = 0; i < 32; i++)
//			throughput[i] = RGBe(RGB(1.f, 1.f, 1.f));
//
//		//RGB throughput[32];
//		//for (int i =0; i < 32 ; i++) throughput[i] = RGB(1.f, 1.f, 1.f);
//
//		//for (int i = 0,j =0; i < N ; i+=n_bwu, j++) {
//
//		int stepY = 4;
//		int stepX = 8;
//		float strat = CONFIG.STRATA;
//
//		int j = 0;
//
//		int nlines = paths->X.length() / (CONFIG.WIDTH * CONFIG.STRATA);
//		int ncols = (CONFIG.WIDTH * CONFIG.STRATA);
//
//		for (int y = 0; y < nlines; y += stepY) {
//			for (int x = 0; x < ncols; x += stepX) {
//
//				ubkSIMDTask_pathtracerSIMD32* t = new_tasks + j++;
//
//				t->p_target_start[0] = y;
//				t->p_target_start[1] = x;
//
//				memset(&(t->depth), 0, sizeof(short) * 32);
//				memset(&(t->done), 0, sizeof(bool) * 32);
//
//				t->live_threads = 32;
//				memcpy(t->throughput, throughput, sizeof(RGBe) * 32);
//				task_count++;
//			}
//		}
//
//		new_tasks_ = (ubkTask*) new_tasks;
//
//		//timer->stop();
//		//fprintf(stderr, "Dice time: %s\n", timer->print());
//
//		return task_count;
//	}
//
//	int diceTo2(ubkTask*& new_tasks_, int n_bwu) {
//
//		int task_count = 0;
//
//		Domain<prePath>* paths;
//		getDomain(3, paths);
//
//		float N = paths->X.length();
//
//		int divide_task_by = N / n_bwu;
//
//		int block_step = n_bwu;
//
//		ubkSIMDTask_pathtracerSIMD32* new_tasks = new ubkSIMDTask_pathtracerSIMD32[divide_task_by];
//
//		RGBe throughput[32];
//		for (int i = 0; i < 32; i++)
//			throughput[i] = RGBe(RGB(1.f, 1.f, 1.f));
//
//		//RGB throughput[32];
//		//for (int i =0; i < 32 ; i++) throughput[i] = RGB(1.f, 1.f, 1.f);
//
//		for (int i = 0, j = 0; i < N; i += n_bwu, j++) {
//
//			ubkSIMDTask_pathtracerSIMD32* t = new_tasks + j;
//
//			//t->p_target_start = /*p_particles_new->X.start + */i;
//
//			//#ifdef WARP_RR
//			//			//t->depth = 0;
//			//			memset(&(t->depth), 0, sizeof(short) * 32);
//			//
//			//#else
//			memset(&(t->depth), 0, sizeof(short) * 32);
//			//#endif
//			memset(&(t->done), 0, sizeof(bool) * 32);
//			t->live_threads = 32;
//			memcpy(t->throughput, throughput, sizeof(RGBe) * 32);
//			task_count++;
//		}
//
//		new_tasks_ = (ubkTask*) new_tasks;
//		return task_count;
//	}

};

#endif /* UBK_TASK_H_ */
