#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "user_defined_task.h"

#include "perform.h"
#include <time.h>

#include <omp.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <time.h>

using namespace tbb;

void k_CUDA_overhead(Task* t_);

void k_CPU_overhead(Task* t_) {

#ifdef OVERHEAD_MEM
	Domain<int> work;
	t_->getDomain(0, work);
#endif

	Task_overhead* t = (Task_overhead*) t_;
//
//	volatile float sum = 0;
//
//	const int f1 = 0;
//	const int f2 = t->nroots;

	//#ifndef __DEBUG
	//	#pragma omp parallel for
	//#endif
//	for (int i = f1; i < f2; i++) {
//		sum = sqrt(t->arg); // about 13 clocks
//		//work.at(i) = sum;
//	}

	long ms = 1000000*int(t->arg);
	nanosleep((struct timespec[]){{0,ms}}, NULL);

	}

int job(int argc, char *argv[]) {

	int N;
	N = 2 * atoi(argv[5]);

	//int *array = (int*) (PERFORM_alloc(sizeof(int) * N));
	//for (int i = 0; i < N; i++) array[i] = 1;



	//cudaSetDevice(0);
	cudaDeviceSynchronize();

	int dev_config = atoi(argv[2]);
	int ndev = 1;
	switch (dev_config) {
	case 0:
		ndev = 1;
		break;
	case 1:
		ndev = 1;
		break;
	case 2:
		ndev = 2;
		break;
	case 3:
		ndev = 2;
		break;
	case 4:
		ndev = 3;
		break;
	case 5:
		ndev = 3;
		break;
	case 6:
		ndev = 4;
		break;

	default:
		break;
	}

	int dice_v = atoi(argv[5]) / (ndev * ndev);

	PERFORM_signal_JobStarted();
	tick_count start = tick_count::now();

#ifdef OVERHEAD_MEM
	Domain<int>* work = new Domain<int> (1, R, array, dim_space(0, N));
#endif

	Task_overhead* t;

	for (int i = 0; i < 1; i++) {

		t = new Task_overhead(REGULAR);
#ifdef OVERHEAD_MEM
		t->associate_domain(work);
#endif

		t->diceable = dice_v == 0 ? false : true;
		t->dice_value = 1 / float(dice_v);
		t->job_elements = N;

		t->arg = 10.0;
		t->nroots = N;

		t->associate_kernel(CPU, &k_CPU_overhead);
		t->associate_kernel(GPU, &k_CUDA_overhead);

		performQ->addTasks(t);
	}

	wait_for_all_tasks();
	PERFORM_signal_taskSubmissionEnded();


	tick_count end = tick_count::now();

	tick_count::interval_t elapsed = end - start;

	PERFORM_signal_JobFinished(toNanoseconds(elapsed));


	tick_count a;
	tick_count::interval_t elapsede = end - a;
	tick_count::interval_t elapseds = start - a;


	long long *clocks = (long long*) &elapsed;

	//printf("%.3f\n", (*clocks) * 1E-6);

	return EXIT_SUCCESS;
}
