//#include "math.h"
//#include "utils.h"

#include "user_defined_task.h"

//#include "device_apis/CUDA/my_cutil_math.h"
//#include "math_functions.h"

__global__ void kernel(float arg, int n	) {

	int tID = blockIdx.x * blockDim.x + threadIdx.x;

	if ((tID >= 0) && (tID < n)) {

		volatile float sum = 0;

		sum = sqrtf(arg);


	}

}

void k_CUDA_overhead(Task* t_) {

#ifdef OVERHEAD_MEM
	Domain<int> work;
	t_->getDomain(0, work);
#endif

	Task_overhead* t = (Task_overhead*) t_;

//	dim3 blockDIM = dim3(512);
//	dim3 gridDIM = dim3(ceil((float)t->nroots / (float) blockDIM.x));

	//kernel<<<gridDIM,blockDIM,0,(cudaStream_t)(t->stream) >>>(t->arg, t->nroots);

	long ms = 1000000*int(t->arg);
		nanosleep((struct timespec[]){{0,ms}}, NULL);

}
