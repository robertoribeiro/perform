#include <stdio.h>
#include <stdlib.h>
#include "perform.h"
#include "core.h"

int job(int argc, char *argv[]);

int main(int argc, char *argv[]) {


	argv[1] = "3";//sch
	argv[2] = "4";//dev
	argv[3] = "0";//calculated dice
	argv[4] = "0";//ndevices
	argv[5] = "4096";//ntasks


	int dev_config = atoi(argv[2]);
	int ndev = 1;

	switch (dev_config) {
	case 0:
		ndev = 1;
		break;
	case 1:
		ndev = 1;
		break;
	case 2:
		ndev = 2;
		break;
	case 3:
		ndev = 2;
		break;
	case 4:
		ndev = 3;
		break;
	case 5:
		ndev = 3;
		break;
	case 6:
		ndev = 4;
		break;

	default:
		break;
	}

	int dd = atoi(argv[5]) / (ndev * ndev);

	initPERFORM(atoi(argv[1]), atoi(argv[2]), dd, 1350);

	double time = job(argc, argv);

	shutdownPERFORM();

	LOG(0.1,cerr << "\n\nRun-time terminated. Job time: " << time << "s." << endl;)

	exit(0);

	return 0;
}
