#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "BarnesHut_seq.h"

#include "../../myUtil.h"
#include "../../common.h"

using namespace std;

void load_data_set(particle* particles, char *filename, int nbodies, int& timesteps,
		BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol) {
	BH_TYPE vx, vy, vz;
	register FILE *f;

	int f_nbodies;
	int f_timesteps;

	f = fopen(filename, "r+t");
	if (f == NULL) {
		fprintf(stderr, "file not found: %s\n", filename);
		exit(-1);
	}

	fscanf(f, "%d", &f_nbodies);
	fscanf(f, "%d", &f_timesteps);
	fscanf(f, "%f", &dtime);
	fscanf(f, "%f", &eps);
	fscanf(f, "%f", &tol);

	//particles = new particle[nbodies];

	if (f_nbodies < nbodies)
		nbodies = f_nbodies;

	for (int i = 0; i < (nbodies); i++) {
		fscanf(f, "%fE", &(particles[i].mass_pos.mass));
		fscanf(f, "%fE", &(particles[i].mass_pos.x));
		fscanf(f, "%fE", &(particles[i].mass_pos.y));
		fscanf(f, "%fE", &(particles[i].mass_pos.z));
		fscanf(f, "%fE", &(particles[i].velx));
		fscanf(f, "%fE", &(particles[i].vely));
		fscanf(f, "%fE", &(particles[i].velz));
		particles[i].mass_pos.id = i;
		particles[i].lock = 0;

		particles[i].accx = 0.0;
		particles[i].accy = 0.0;
		particles[i].accz = 0.0;


		//bodies[i]->setVelocity(vx, vy, vz);
	}

	fclose(f);

}

void load_data_set2(particle* particles, char *filename, int nbodies, int& timesteps,
		BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol) {
	BH_TYPE vx, vy, vz;
	register FILE *f;

	int f_nbodies;
	int f_timesteps;

	f = fopen(filename, "r+t");
	if (f == NULL) {
		fprintf(stderr, "file not found: %s\n", filename);
		exit(-1);
	}


	fscanf(f, "%d", &f_nbodies);
	fscanf(f, "%d", &f_timesteps);
	fscanf(f, "%f", &dtime);
	fscanf(f, "%f", &eps);
	fscanf(f, "%f", &tol);
	eps = 0.0005;
	tol = 0.3;
	//particles = new particle[nbodies];

	if (f_nbodies < nbodies)
		printf("Not enough particles\n");

	for (int i = 0; i < (nbodies); i++) {

		fscanf(f, "%fE", &(particles[i].mass_pos.x));
		fscanf(f, "%fE", &(particles[i].mass_pos.y));
		fscanf(f, "%fE", &(particles[i].mass_pos.z));

		fscanf(f, "%fE", &(particles[i].mass_pos.mass));
		particles[i].mass_pos.mass=0.23;
		particles[i].velx = 0.0f;
		particles[i].vely = 0.0f;
		particles[i].velz = 0.0f;
		particles[i].mass_pos.id = i;
		particles[i].lock = 0;


		particles[i].accx = 0.0;
		particles[i].accy = 0.0;
		particles[i].accz = 0.0;
	}


	fclose(f);

}

void compute_bounding_box(particle* particles, const int nbodies, BH_TYPE &diameter,
		BH_TYPE &centerx, BH_TYPE &centery, BH_TYPE &centerz) {

	BH_TYPE minx, miny, minz;
	BH_TYPE maxx, maxy, maxz;
	BH_TYPE posx, posy, posz;

	minx = 0.;
	miny = 0.;
	minz = 0.;
	maxx = 0.;
	maxy = 0.;
	maxz = 0.;

	for (int i = 0; i < nbodies; i++) {
		posx = particles[i].mass_pos.x;
		posy = particles[i].mass_pos.y;
		posz = particles[i].mass_pos.z;

		if (minx > posx)
			minx = posx;
		if (miny > posy)
			miny = posy;
		if (minz > posz)
			minz = posz;

		if (maxx < posx)
			maxx = posx;
		if (maxy < posy)
			maxy = posy;
		if (maxz < posz)
			maxz = posz;
	}

	diameter = maxx - minx;
	if (diameter < (maxy - miny))
		diameter = (maxy - miny);
	if (diameter < (maxz - minz))
		diameter = (maxz - minz);

	centerx = (maxx + minx) * 0.5;
	centery = (maxy + miny) * 0.5;
	centerz = (maxz + minz) * 0.5;
}

int build_tree_array(cell* cells, BH_TYPE radius, particle* particles, int nbodies,
		int& tree_height) {

	BH_TYPE x, y, z, r;
	BH_TYPE p_new_x, p_new_y, p_new_z;
	BH_TYPE parent_x, parent_y, parent_z;
	BH_TYPE new_x, new_y, new_z;
	BH_TYPE rh;

	int child, child1, parent;
	BH_TYPE rootx, rooty, rootz;
	int cell_count = 1;
	int particle_pos = 0;
	int tmp_height;

	//system center
	rootx = cells[0].mass_pos.x;
	rooty = cells[0].mass_pos.y;
	rootz = cells[0].mass_pos.z;

	//For all particles in the system
	while (particle_pos < nbodies) {

		tmp_height = 0;
		child = 0;
		x = 0;
		y = 0;
		z = 0;

		//particle to insert position
		p_new_x = particles[particle_pos].mass_pos.x;
		p_new_y = particles[particle_pos].mass_pos.y;
		p_new_z = particles[particle_pos].mass_pos.z;

		//r reinitialized to system radius
		r = radius;

		//get voxel where the particle will be positioned
		if (rootx < p_new_x) {
			child = 1;
			x = r;
		}

		if (rooty < p_new_y) {
			child += 2;
			y = r;
		}
		if (rootz < p_new_z) {
			child += 4;
			z = r;
		}

		//if child is a cell find and follow until a particle/leaf or nothing is found
		parent = 0;
		while (cells[parent].child_types[child] == CELL) {
			tmp_height++;
			r *= 0.5f; //go down in the octree
			//the child cell is in the cells array in this position
			parent = cells[parent].child_indexes[child];
			parent_x = cells[parent].mass_pos.x;
			parent_y = cells[parent].mass_pos.y;
			parent_z = cells[parent].mass_pos.z;
			child = 0;
			x = 0;
			y = 0;
			z = 0;
			//find the child voxel
			if (parent_x < p_new_x) {
				child = 1;
				x = r;
			}
			if (parent_y < p_new_y) {
				child += 2;
				y = r;
			}
			if (parent_z < p_new_z) {
				child += 4;
				z = r;
			}
		}

		if (cells[parent].child_types[child] == NILL) { //if there is no particle in the octant, add the particle

			cells[parent].child_types[child] = PARTICLE; //the child of the octant found is now a particle
			cells[parent].child_indexes[child] = particle_pos; //the particle is in this index of the particles array

		} else if (cells[parent].child_types[child] == PARTICLE) {//if a particle is already in the octant, devide the octant

			int particle = cells[parent].child_indexes[child]; //particle that was there
			BH_TYPE p_old_x, p_old_y, p_old_z;
			p_old_x = particles[particle].mass_pos.x;
			p_old_y = particles[particle].mass_pos.y;
			p_old_z = particles[particle].mass_pos.z;

			if (!(p_old_x == p_new_x && p_old_y == p_new_y && p_old_z == p_new_z)){



				// the particles may be in the same octant again so multiple divisions are required, stop where they are in different octants
				do {

					cells[parent].child_types[child] = CELL;
					cells[parent].child_indexes[child] = cell_count;

					cell new_cell;
					for (int k = 0; k < 8; k++)
						new_cell.child_types[k] = NILL;
					for (int k = 0; k < 8; k++)
						new_cell.child_indexes[k] = -1;

					rh = 0.5 * r;
					parent_x = cells[parent].mass_pos.x;
					parent_y = cells[parent].mass_pos.y;
					parent_z = cells[parent].mass_pos.z;

					//the new child bb
					new_x = parent_x - rh + x;
					new_y = parent_y - rh + y;
					new_z = parent_z - rh + z;

					new_cell.mass_pos.x = new_x;
					new_cell.mass_pos.y = new_y;
					new_cell.mass_pos.z = new_z;

					//the r,x,y,z will be used in the next iteration
					r *= 0.5f;
					child = 0;
					x = 0;
					y = 0;
					z = 0;

					//find the new voxel child where the particle that was already there will be inserted
					if (new_x < p_old_x) {
						child = 1;
						x = r;
					}
					if (new_y < p_old_y) {
						child += 2;
						y = r;
					}
					if (new_z < p_old_z) {
						child += 4;
						z = r;
					}

					child1 = child; //child1 is the child for the new particle, child is for the old one
					child = 0;

					//find the new octet child where the particle that we are adding will be inserted
					if (new_x < p_new_x)
						child = 1;
					if (new_y < p_new_y)
						child += 2;
					if (new_z < p_new_z)
						child += 4;

					new_cell.parent_cell_index = parent;

					if (tmp_height++ >= tree_height)
						tree_height++;

					parent = cell_count;
					cells[cell_count++] = new_cell;

				} while (child == child1); //repeat if the child is the same


				//child is the child for the new particle, child1 is for the old one
				cells[cell_count - 1].child_types[child] = PARTICLE;
				cells[cell_count - 1].child_indexes[child] = particle_pos;

				//child is the child for the new particle, child1 is for the old one
				cells[cell_count - 1].child_types[child1] = PARTICLE;
				cells[cell_count - 1].child_indexes[child1] = particle;

			}
			else {
				//printf("ignoring particle %d\n",particle_pos);
			}

		}
		particle_pos++;
	}
	//cout << "voxel count:" << cell_count << " body count: " << nbodies << std::endl;

	return cell_count;
}

void compute_center_of_mass(cell* cells, particle* particles, int cell_count, int tree_height) {

	BH_TYPE m, px = 0.0, py = 0.0, pz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;
	int p_t_count = 0;
	while (!(stack_top == 0 && child == 8)) {
		if (child == 8) {

			m = 0;
			px = 0.0;
			py = 0.0;
			pz = 0.0;

			for (int i = 0; i < 8; i++) {
				mass_position mass_pos;
				if (cells[current].child_types[i] == PARTICLE)
					mass_pos = particles[cells[current].child_indexes[i]].mass_pos;
				else if (cells[current].child_types[i] == CELL)
					mass_pos = cells[cells[current].child_indexes[i]].mass_pos;
				else
					continue;

				m = mass_pos.mass;
				cells[current].mass_pos.mass += m;

				px += mass_pos.x * m;
				py += mass_pos.y * m;
				pz += mass_pos.z * m;

			}

			m = 1.0 / cells[current].mass_pos.mass;
			cells[current].mass_pos.x = px * m;
			cells[current].mass_pos.y = py * m;
			cells[current].mass_pos.z = pz * m;

			/*	printf("Center of mass:  ");
			 PrintBH_TYPE(cells[current].mass_pos.x);
			 printf(" ");
			 PrintBH_TYPE(cells[current].mass_pos.y);
			 printf(" ");
			 PrintBH_TYPE(cells[current].mass_pos.z);
			 printf("\n");*/

			current = cells[current].parent_cell_index;
			child = stack[--stack_top];
			child++;
		} else {
			if (cells[current].child_types[child] == CELL) {
				current = cells[current].child_indexes[child];
				stack[stack_top++] = child;
				child = 0;
			} else if (cells[current].child_types[child] == PARTICLE) {
				child++;
			} else if (cells[current].child_types[child] == NILL) {
				child++;
			}
		}
	}

	// Center of mass of the root
	m = 0;
	px = 0.0;
	py = 0.0;
	pz = 0.0;

	for (int i = 0; i < 8; i++) {
		mass_position mass_pos;
		if (cells[current].child_types[i] == PARTICLE)
			mass_pos = particles[cells[current].child_indexes[i]].mass_pos;
		else if (cells[current].child_types[i] == CELL)
			mass_pos = cells[cells[current].child_indexes[i]].mass_pos;
		else
			continue;

		m = mass_pos.mass;
		cells[current].mass_pos.mass += m;

		px += mass_pos.x * m;
		py += mass_pos.y * m;
		pz += mass_pos.z * m;

	}

	m = 1.0 / cells[current].mass_pos.mass;
	cells[current].mass_pos.x = px * m;
	cells[current].mass_pos.y = py * m;
	cells[current].mass_pos.z = pz * m;

	/* printf("Center of mass:  ");
	 PrintBH_TYPE(cells[current].mass_pos.x);
	 printf(" ");
	 PrintBH_TYPE(cells[current].mass_pos.y);
	 printf(" ");
	 PrintBH_TYPE(cells[current].mass_pos.z);
	 printf("\n");

	 cout << "center of mass done" << endl;  */
}

uint _something( uint x, float y, double z) {

	volatile uint acc;

	int garbage = 1;
	while (garbage < 1000) {
		acc += z + y + x;
		garbage += 1;
	}

	return acc;
}

void sequential_compute_force(cell* cells, particle* particles, int particle, int tree_height, BH_TYPE dtime,
		BH_TYPE epssq, BH_TYPE dsq, int step) {

	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
	BH_TYPE ax, ay, az;

	ax = particles[particle].accx;
	ay = particles[particle].accy;
	az = particles[particle].accz;

	particles[particle].accx = 0.0;
	particles[particle].accy = 0.0;
	particles[particle].accz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;

	dsq *= 0.25;

	mass_position mass_pos_p = particles[particle].mass_pos;

	while (!(stack_top == 0 && child == 8)) {

		if (child == 8) {
			current = cells[current].parent_cell_index;
			dsq *= 4;
			child = stack[--stack_top];
			child++;

		} else {

			mass_position mass_pos;
			if (cells[current].child_types[child] == PARTICLE)
				mass_pos = particles[cells[current].child_indexes[child]].mass_pos;
			else if (cells[current].child_types[child] == CELL)
				mass_pos = cells[cells[current].child_indexes[child]].mass_pos;
			else {
				child++;
				continue;
			}

			//volatile uint acc=0;
			_something(2, 2.2f, 2.2);


			drx = mass_pos.x - mass_pos_p.x;
			dry = mass_pos.y - mass_pos_p.y;
			drz = mass_pos.z - mass_pos_p.z;

			drsq = drx * drx + dry * dry + drz * drz;
			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
			if (drsq < dsq) {
				if (cells[current].child_types[child] == CELL) {
					dsq *= 0.25;
					current = cells[current].child_indexes[child];
					stack[stack_top++] = child;
					child = 0;
					//printf("going down\n");

				} else if (cells[current].child_types[child] == PARTICLE) {
					if (mass_pos.id != mass_pos_p.id) {
						drsq += epssq;
						idr = 1 / sqrt(drsq);
						nphi = mass_pos.mass * idr;
						scale = nphi * idr * idr;
						particles[particle].accx += drx * scale;
						particles[particle].accy += dry * scale;
						particles[particle].accz += drz * scale;

						//if (particle == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);


					}
					child++;
				} else if (cells[current].child_types[child] == NILL) {
					child++;
				}
			} else {
				//printf("Aproximating\n");
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = mass_pos.mass * idr;
				scale = nphi * idr * idr;
				particles[particle].accx += drx * scale;
				particles[particle].accy += dry * scale;
				particles[particle].accz += drz * scale;

				//if (particle == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);

				child++;
			}

		}
	}

	BH_TYPE dthf = 0.5 * dtime;

	if (step > 0) {
		particles[particle].velx += (particles[particle].accx - ax) * dthf;
		particles[particle].vely += (particles[particle].accy - ay) * dthf;
		particles[particle].velz += (particles[particle].accz - az) * dthf;
	}

	if (particle ==0){
		//printf("ACCt_x: %.15f\n",ax);
		//printf("ACCx: %.15f\n",particles[particle].accx);
	}


	/*	PrintBH_TYPE(particles[particle].velx);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].vely);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].velz);
	 printf("\n");*/

}

void sequential_update_positions(particle* particles, int part, BH_TYPE dtime) {

	BH_TYPE dthf = 0.5 * dtime;

	BH_TYPE dvelx, dvely, dvelz;
	BH_TYPE velhx, velhy, velhz;

	particle* p = &(particles[part]);

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	//printf("%.10lf\n",velhx * dtime);

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;

}

void initBARNES_HUT(int argc, char *argv[], particle*& particles, int &nbodies, int& timesteps,
		char *& filename) {

	filename = argv[4];

	nbodies = atoi(argv[6]);
	timesteps = atoi(argv[5]);

	particles = new particle[nbodies];

}

void PrintParticles(particle* particles, int nbodies) {

	for (int i = 0; i < nbodies; i++) { // print result
		printf("p %d: ", i);
		PrintBH_TYPE(particles[i].mass_pos.x);
		printf("   ");
		PrintBH_TYPE(particles[i].mass_pos.y);
		printf("   ");
		PrintBH_TYPE(particles[i].mass_pos.z);
		printf("\n");
	}
}


void PrintParticles(particle* particles, int ns) {

	printf("p %d: ", i);
		PrintBH_TYPE(particles[ns].mass_pos.x);
		printf("   ");
		PrintBH_TYPE(particles[ns].mass_pos.y);
		printf("   ");
		PrintBH_TYPE(particles[ns].mass_pos.z);
		printf("\n");
}


void PrintBH_TYPE(BH_TYPE d) {
	register int i;
	char str[16];

	sprintf(str, "%.4lE", d);

	i = 0;
	while ((i < 16) && (str[i] != 0)) {
		if ((str[i] == 'E') && (str[i + 1] == '-') && (str[i + 2] == '0') && (str[i + 3] == '0')) {
			printf("E00");
			i += 3;
		} else if (str[i] != '+') {
			printf("%c", str[i]);
		}
		i++;
	}
}

void jobBARNES_HUT_check(int argc, char *argv[]) {

	printf("Checking results...\n");

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	char* filename;

	PERFORM_timer* timer = new PERFORM_timer();
	particle* particles;

	initBARNES_HUT(argc, argv, particles, nbodies, timesteps, filename);

	load_data_set2(particles, filename, nbodies, timesteps, dtime, eps, tol);

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	for (int step = 0; step < timesteps; step++) {

		BH_TYPE diameter, centerx, centery, centerz;

		/**
		 * Compute the center and diameter of the all system
		 */
		compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);

		/**
		 * Create the the octree root
		 */

		/**
		 * based in the burtsher code ???
		 */
		int nnodes = nbodies * 2;
		if (nnodes < 1024 * 16)
			nnodes = 1024 * 20;

		//tree_cells_array nodes = tree_cells_array();

		cell* nodes = new cell[nnodes];

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)	root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)	root.child_indexes[k] = -1;

		nodes[0] = root;
		//nodes.push_back(root);

		const BH_TYPE radius = diameter * 0.5;

		build_tree_array(nodes, radius, particles, nbodies, tree_height);

		compute_center_of_mass(nodes, particles, 0, tree_height);

		BH_TYPE dsq = diameter * diameter * itolsq;

		PERFORM_acc_timer times[nbodies];

		double min = 100.0;
		double max = 0.0;



		for (int i = 0; i < nbodies; i+=1){
			times[i].start();
			sequential_compute_force(nodes, particles, i, tree_height, dtime, epssq, dsq, step);
			times[i].stop();

			if (times[i].duration > max) max = times[i].duration;
			if (times[i].duration < min) min = times[i].duration;

			//printf("%s\n", times[i].print());
		}

		printf("MAX: %lf MIN: %lf DIF: %lf PERC %.2f \n",max,min, max-min,(max-min)/max);


		int below=0;
		for (int i = 0; i < nbodies; i+=1){
			if (times[i].duration < ((max-min)/2 + min)) below++;
		}

		printf("Particle below middle %d%\n",(below*100)/nbodies);



		//		PERFORM_acc_timer times;
		//		int i = 92864;
		//	//	for (int i = 0; i < nbodies; i++){
		//			times.start();
		//			compute_force_(nodes, particles, i, tree_height, dtime, epssq, dsq, step);
		//			times.stop();
		//			printf("SINGLE PARTICLE: %s\n", times.print());
		//	//	}


		for (int i = 0; i < nbodies; i++) sequential_update_positions(particles, i, dtime);

	}

	//PrintParticles((particle*)particles, nbodies-1023);


}

