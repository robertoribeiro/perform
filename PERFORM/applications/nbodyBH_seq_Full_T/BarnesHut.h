#ifndef BARNESHUT_H_
#define BARNESHUT_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "../../perform.h"

using namespace std;
using namespace tbb;



//#define TASK_FORCE_TYPE_IRREGULAR

#ifdef TASK_FORCE_TYPE_IRREGULAR
#define TASK_FORCE_TYPE IRREGULAR
#else
#define TASK_FORCE_TYPE REGULAR
#endif

//#include "BarnesHut_seq.h"
#include "../nbodyBH_common/BarnesHut_common.h"


double jobBARNES_HUT(int argc, char *argv[]);

#endif
