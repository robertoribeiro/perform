#include "mcml-cuda.H"

#include "device_apis/CUDA/global_sync.h"

#ifdef MCML_FLUOR
#define MAX_SEC_TASK 2
#else
#define MAX_SEC_TASK 1
#endif

#define HOPS_PER_TASK 512

#define WARPSIZE 32

#define DEQ_MULTIPLIER 32 // <32
//#define N_SM 0

#define WARPS 8

#define N_BLOCKS (WARPS*N_SM)

//#define N_BLOCKS 15

/*Se muito alto e muitas tarefas produzidas,
 *  falha no espaço disponivel da out o que causa force
 e pode tb falhar nas slots de tasks*/
#define TASKS_PER_LOOP (DEQ_MULTIPLIER)

#define UBK_INBOX_SIZE 30000000

/* se local inbox muito grande, rouba muito trabalho pa apenas uma SM.
 calcular em funçao do child_task_count?*/
#define LOCAL_INBOX_QUEUE_SIZE (128)
//se local outboc muito pequeno força muito o lock
#define LOCAL_OUTBOX_QUEUE_SIZE (1024*20)
// PE is a warp or thread 350
#define MEM_MEM_CHUNKS_PER_PE (1024)
#define THR_PER_BLOCK (WARPSIZE)
#define QUEUE_FULL_HANDICAP 32
#define MEM_CHUNKS_PER_SM (MEM_MEM_CHUNKS_PER_PE)
#define MEM_CHUNK_SIZE sizeof(ubkSpecificTask)
#define MEM_TOTAL (MEM_CHUNKS_PER_SM * MEM_CHUNK_SIZE)
//Extra open slots on each SM frees q
#define LOCAL_FREES_QUEUE_SIZE (MEM_CHUNKS_PER_SM + 1024*30)
#define TIDX (threadIdx.x)
#define THREAD0 (threadIdx.x == 0)
#define WARP0 (threadIdx.x < 32)
#define LANE0 ((threadIdx.x & 31)==0)    // or warp lane index
#define THREADG0 (tID == 0)

#include "device_apis/CUDA/workqueue.h"

__device__ CudaConcurrentQueue<ubkTask_ptr>* inbox;

__device__ ubkTask_ptr* local_inbox_buffer;
__device__ ubkTask_ptr* local_outbox_buffer;
__device__ ubkTask_ptr* local_frees_buffer;

__device__ CudaLocalQueue<ubkTask_ptr>* heads_outbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_inbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_frees;

__device__ volatile int out_votes[N_BLOCKS];

__device__
__forceinline void k_CUDA_MCML(ubkSpecificTask& task, SimState d_state,
		UINT32 number_of_photons, volatile uint& elements_bag,
		ubkTask_ptr bag[], CudaLocalQueue<ubkTask_ptr>* local_frees,
		UINT64* d_hop_counters, UINT32* d_sm_counters) {

	volatile __shared__ bool r[32];
	r[TIDX] = false;

	if (!task.done[0]) {

		uint tid = blockIdx.x * blockDim.x + TIDX;

		UINT32 hit;
		FLOAT s;
		// photon structure stored in registers
		PhotonStruct photon = task.photons[0];

		if (!photon.fluor && photon.w == 0) {
			LaunchPhoton(&photon);
#ifdef MEASURE_IMBALANCE2
			d_sm_counters[task.p_target_start] = blockIdx.x;
#endif
		}

		UINT64 rnd_x = d_state.x[tid];
		UINT32 rnd_a = d_state.a[tid];

		uint hop_count = 0;

#ifdef ITERATION_DELAY

		int NOT_DELAYED = 1;

#ifdef MAJ_VOTE

		const int THREASH = 16;
#endif

#ifdef RR_VOTE

		// branch direction period : 0 ,0 ,0 ,1
		// 1 means the IF branch path
		// 0 means the ELSE branch path
		const int num_zeros = 3, num_ones = 1;
		const int period = num_zeros + num_ones;
		int counter = -1;
#endif

#endif

		while (hop_count < HOPS_PER_TASK) {

#ifdef ITERATION_DELAY

			if (NOT_DELAYED) {

#endif
			//>>>>>>>>> StepSizeInTissue() in MCML
			s = ComputeStepSize(&photon, &rnd_x, &rnd_a);

//			if (photon.fluor && photon.hop_count==0)
//				printf("%llu %u\n",rnd_x,rnd_a);

			//>>>>>>>>> HitBoundary() in MCML
			hit = HitBoundary(&photon, s);
#ifdef ITERATION_DELAY

		}

		int COND_FOR_ALL;

#ifdef MAJ_VOTE
		int go_in_count = __popc(__ballot(hit));
		int active_threads = __popc(__ballot(1));

		if (active_threads < THREASH) {
			COND_FOR_ALL = hit;
			NOT_DELAYED = 1;
		} else {
			COND_FOR_ALL = go_in_count >= THREASH;
			NOT_DELAYED = (COND_FOR_ALL == hit);
		}
#endif

#ifdef RR_VOTE

		// update the direction
		if (++counter == period)
		counter = 0;

		COND_FOR_ALL = (counter >= num_zeros);
		// remove idle iteration
		//COND_FOR_ALL = COND_FOR_ALL ? __any(hit) : __all(hit);

		NOT_DELAYED = (COND_FOR_ALL == hit);

#endif

		if (NOT_DELAYED) {

#endif
			Hop(&photon, s);
			hop_count++;

#ifdef MEASURE_IMBALANCE
			//if (photon.fluor && hit)
			AtomicAddULL(&d_hop_counters[blockIdx.x], 1);
#endif

#ifdef ITERATION_DELAY
			if (COND_FOR_ALL) {
#else
			FLOAT absorved = 0.f;
			if (hit) {
#endif
				FastReflectTransmit(&photon, &d_state, &rnd_x, &rnd_a);
			} else {
				absorved = Drop(0, &photon, d_state.A_rz);

				Spin(d_layerspecs[photon.layer].g, &photon, &rnd_x, &rnd_a);
			}

			/***********************************************************
			 *  >>>>>>>>> Roulette()
			 *  If the photon weight is small, the photon packet tries
			 *  to survive a roulette.
			 ****/

			r[TIDX] = 1;
			if (photon.w < WEIGHT) {
				FLOAT rand = rand_MWC_co(&rnd_x, &rnd_a);

				// This photon survives the roulette.
				if (photon.w != MCML_FP_ZERO && rand < CHANCE)
					photon.w *= (FP_ONE / CHANCE);
				// This photon is terminated.
				else {

#ifndef NO_ATOMIC_COUNTERS
					if (!photon.fluor)
					atomicSub(d_state.n_photons_left, 1);
#ifdef MCML_FLUOR
					else {
						atomicAdd(d_state.fluor_spawnned, 1);
					}
#endif
#endif
					task.done[0] = 1;
					r[TIDX] = 0;
					hop_count = HOPS_PER_TASK;
					//printf("Photon left %d hops: %d\n", *d_state.n_photons_left,
					//		photon.hop_count);

				}
			}
#ifdef ITERATION_DELAY
		}
#endif

#ifdef MCML_FLUOR

			if (!photon.fluor && absorved != 0) {

				FLOAT rand = rand_MWC_co(&rnd_x, &rnd_a);

				if (rand < FLUOR_CHANCE) {
					// photon structure stored in registers

					ubkTask_ptr current_new_task;
					if (!local_frees->dequeue(current_new_task))
						printf("Error dequeuing frees\n");

					//	current_new_task->p_target_start = task.p_target_start;
					current_new_task->done[0] = false;

					PhotonStruct& pfluor = current_new_task->photons[0];

					LaunchPhoton(&pfluor);

					//fluor.w *= (FP_ONE / FLUOR_CHANCE);

					pfluor.fluor = true;

					pfluor.x = photon.x;
					pfluor.y = photon.y;
					pfluor.z = photon.z;

					// Monte carlo model
					pfluor.w = absorved * FLUOR_QY * (FP_ONE / FLUOR_CHANCE);

					//Fluorescence
					//pfluor.w = photon.w * FLUOR_QY;

					bag[TIDX * MAX_SEC_TASK + elements_bag++] =
							current_new_task;

					hop_count = HOPS_PER_TASK;

				}
			}
#endif
		}

		d_state.x[tid] = rnd_x;
		d_state.a[tid] = rnd_a;

		task.photons[0] = photon;

	}

	if (r[TIDX]) {

		bag[TIDX * MAX_SEC_TASK + elements_bag++] = &task;
//		ubkTask_ptr current_new_task;
//		if (!local_frees->dequeue(current_new_task))
//			printf("Error dequeuing frees\n");
//
//		ubkSpecificTask& c = *current_new_task;
//		c = task;

	} else {
		if (!local_frees->isFull())
			local_frees->enqueue(&task);
	}

}

__device__
__forceinline void flush_to_global(CudaLocalQueue<ubkTask_ptr> *& q) {

	ubkTask_ptr t = NULL;
	while (!q->isEmpty() && !inbox->isFull()) {

#ifdef __DEBUG
		if (q->dequeue(t) && !inbox->enqueue(t)) {
			printf("Error 4\n");
			//__trap();

		}
#else
		if (q->dequeue(t))
		inbox->enqueue(t);
#endif
	}

}

__device__
__forceinline void try_donate(CudaLocalQueue<ubkTask_ptr> *& local_outbox) {

	if (!inbox->isFull() && !local_outbox->isEmpty() && inbox->tryLock()) {
		flush_to_global(local_outbox);
		inbox->Release();
	}

}

//__device__
//__forceinline bool check_all_isEmpty() {
//
//#if N_BLOCKS > WARPSIZE
//
//	register int yy = 0;
//	register int t1 = N_BLOCKS - WARPSIZE;
//	register int t2 = (N_BLOCKS & 31);
//
//	for (yy = 0; (yy < t1); yy += WARPSIZE) {
//		if (!__all(heads_inbox[TIDX + yy].isEmpty()) || !__all(heads_outbox[TIDX + yy].isEmpty()))
//			return false;
//	}
//
//	if (!__all(heads_inbox[(TIDX % t2) + yy].isEmpty()) || !__all(
//			heads_outbox[(TIDX % t2) + yy].isEmpty()))
//		return false;
//#else
//	if (!__all(heads_inbox[(TIDX % N_BLOCKS)].isEmpty()) || !__all(
//					heads_outbox[(TIDX % N_BLOCKS)].isEmpty()))
//	return false;
//#endif
//
//	return true;
//}

__device__
__forceinline void vote_out(CudaLocalQueue<ubkTask_ptr>* local_inbox,
		CudaLocalQueue<ubkTask_ptr>* local_outbox) {

	if (inbox->isEmpty() && local_inbox->isEmpty() && local_outbox->isEmpty()) {
		out_votes[blockIdx.x] = 1;
	} else
		out_votes[blockIdx.x] = 0;

}

__device__
__forceinline bool check_all_isEmpty() {

	register int yy = 0;
	register int t1 = N_BLOCKS / WARPSIZE;
	register int t2 = (N_BLOCKS & 31);

	for (yy = 0; yy < t1 * WARPSIZE; yy += WARPSIZE) {
		if (__any(!out_votes[TIDX + yy]))
			return false;
	}

	for (yy = 0; yy < t2; yy++) {
		if (out_votes[yy] != 1)
			return false;
	}

	return true;
}

/**
 * pkernel
 */

__global__ void pkernel(SimState d_state, UINT32 number_of_photons,
		ubkTask_ptr* inbox_buffer, void* mem_buffer, void* qs_buffer,
		int child_task_count, UINT64* d_hop_counters, UINT32* d_sm_counters) {

	const int tID = blockIdx.x * blockDim.x + threadIdx.x;

	if (tID == 0) {

		local_inbox_buffer = (ubkTask_ptr*) qs_buffer;
		local_outbox_buffer = local_inbox_buffer
				+ LOCAL_INBOX_QUEUE_SIZE * N_BLOCKS;

		local_frees_buffer = local_outbox_buffer
				+ LOCAL_OUTBOX_QUEUE_SIZE * N_BLOCKS;

		heads_inbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
		heads_outbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
		heads_frees = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];

		inbox = new CudaConcurrentQueue<ubkTask_ptr>(inbox_buffer,
				child_task_count, UBK_INBOX_SIZE);

	}

	__gpu_sync(N_BLOCKS);

	CudaLocalQueue<ubkTask_ptr>* local_inbox = heads_inbox + blockIdx.x;
	CudaLocalQueue<ubkTask_ptr>* local_outbox = heads_outbox + blockIdx.x;
	CudaLocalQueue<ubkTask_ptr>* local_frees = heads_frees + blockIdx.x;

	if (THREAD0) {

		local_inbox->setBuffer(
				(ubkTask_ptr*) (local_inbox_buffer
						+ blockIdx.x * LOCAL_INBOX_QUEUE_SIZE), 0,
				LOCAL_INBOX_QUEUE_SIZE);
		local_outbox->setBuffer(
				(ubkTask_ptr*) (local_outbox_buffer
						+ blockIdx.x * LOCAL_OUTBOX_QUEUE_SIZE), 0,
				LOCAL_OUTBOX_QUEUE_SIZE);

		local_frees->setBuffer(
				(ubkTask_ptr*) (local_frees_buffer
						+ blockIdx.x * LOCAL_FREES_QUEUE_SIZE), 0,
				LOCAL_FREES_QUEUE_SIZE);

		local_inbox->clear();
		local_outbox->clear();
		local_frees->clear();

		memset((int*) out_votes, 0, sizeof(int) * N_BLOCKS);

	}

	ubkSpecificTask* warp_chunk = (ubkSpecificTask*) ((char*) mem_buffer
			+ blockIdx.x * MEM_TOTAL);

	for (int yy = 0; yy < MEM_MEM_CHUNKS_PER_PE; yy += WARPSIZE) {
#ifdef __DEBUG
		if (!local_frees->enqueue(warp_chunk + yy + TIDX)) {
			printf("Error here\n");
		}
#else
		local_frees->enqueue(warp_chunk + yy + TIDX);
#endif
	}

	volatile __shared__ ubkTask_ptr v_work_bag[TASKS_PER_LOOP];

	__shared__ ubkTask_ptr* work_bag;
	work_bag = (ubkTask_ptr*) v_work_bag;

	volatile __shared__ uint elements_bag[TASKS_PER_LOOP];

	__shared__ ubkTask_ptr out_bag[MAX_SEC_TASK * TASKS_PER_LOOP];

	__gpu_sync(N_BLOCKS);

	while (!check_all_isEmpty()) {

		/** Se a inbox esta vazia e a outbox tem trabalho, despeja na inbox
		 * ate ficar cheia ou não haver mais trabalho
		 */
		while (!local_inbox->isFull() && !local_outbox->isEmpty()) {
			ubkTask_ptr t_item = NULL;

			if (local_outbox->dequeue(t_item) && t_item != NULL) {
#ifdef __DEBUG

				local_inbox->enqueue(t_item) ? : printf("Error 0\n");
#else
				local_inbox->enqueue(t_item);
#endif
			}

		}

		//TODO empty by one element is not empty
		//Se a inbox estiver vazia tenta roubar trabalho na global
		if (local_inbox->isEmpty()) {

			inbox->Acquire();
			ubkTask_ptr t_item = NULL;

			while (!local_inbox->isFull() && !inbox->isEmpty()) {
				if (inbox->dequeue(t_item) && (t_item != NULL)) {
#ifdef __DEBUG
					if (!local_inbox->enqueue(t_item)) {
						printf("Error 1\n");
						//__brkpt();
					}
#else
					local_inbox->enqueue(t_item);
#endif
				}
			}
			inbox->Release();

		}

		// work donation using global inbox
		//try_donate(local_outbox);

		if (TIDX < DEQ_MULTIPLIER) {
			work_bag[TIDX] = NULL;
			elements_bag[TIDX] = 0;

			for (int i = 0; i < MAX_SEC_TASK; i++)
				out_bag[TIDX * MAX_SEC_TASK + i] = 0;
		}

		if (TIDX < DEQ_MULTIPLIER)
			if (!local_inbox->isEmpty())
				local_inbox->dequeue(work_bag[TIDX]);

//		if (tID == 0)
//			printf("Elements in local_inbox %u \n", local_inbox->getSize());

		//for (int l = 0; l < DEQ_MULTIPLIER; l++) {
		if (work_bag[TIDX] != NULL) {
			k_CUDA_MCML(*work_bag[TIDX], d_state, number_of_photons,
					elements_bag[TIDX], out_bag, local_frees, d_hop_counters,
					d_sm_counters);
		}

//		if ((TIDX < DEQ_MULTIPLIER) && work_bag[TIDX] != NULL
//				&& !local_frees->isFull()) {
//#ifdef __DEBUG
//			if (!local_frees->enqueue(work_bag[TIDX]))
//				printf("Error 2\n");
//#else
//			local_frees->enqueue(work_bag[TIDX]);
//#endif
//		}

		if (TIDX < DEQ_MULTIPLIER) {
			if (work_bag[TIDX] != NULL) {
				for (int i = 0; i < elements_bag[TIDX]; i++)
					local_outbox->enqueue(out_bag[TIDX * MAX_SEC_TASK + i]);
			}
		}

//		//TODO empty by one element is not empty
//		//Se a inbox estiver vazia tenta roubar trabalho na global
//		if (inbox->tryLock()) {
//			ubkTask_ptr t_item = NULL;
//
//			while (!local_inbox->isFull() && !inbox->isEmpty()) {
//				if (inbox->dequeue(t_item) && (t_item != NULL)) {
//#ifdef __DEBUG
//					if (!local_inbox->enqueue(t_item)) {
//						printf("Error 1\n");
//						//__brkpt();
//					}
//#else
//					local_inbox->enqueue(t_item);
//#endif
//				}
//			}
//			inbox->Release();
//
//		}

		__threadfence_system();
		//vote_out(local_inbox, local_outbox);
		//__threadfence_system();


		if (inbox->isEmpty() && local_inbox->isEmpty() && local_outbox->isEmpty()) {
				break;
			}

	}

	__gpu_sync(N_BLOCKS);

#ifdef __DEBUG

	if (THREAD0)
		printf("Elements in local_inbox %u \n", local_inbox->getSize());

	if (THREAD0)
		printf("Elements in local_outbox %u \n", local_outbox->getSize());

	if (tID == 0) {

		printf("Elements in inbox %u \n", inbox->getSize());

	}
#endif
//	if (THREAD0)
//		printf("Elements in local_frees %u \n", local_frees->getSize());
}

void k_CUDA_wrapper_uberkernel(Task* t, ubkTask* tasks_, int task_count) {

	Task_MCML* t_ = (Task_MCML*) t;

#ifdef __DEBUG

	cudaStreamSynchronize(t->stream);

	checkCUDAmemory("start");

	fprintf(stdout, "Task size: %d\n", (int) sizeof(ubkSpecificTask));
#endif

	Domain<UINT32> rng_a;
	t_->getDomain(0, rng_a);

	Domain<UINT64> rng_x;
	t_->getDomain(1, rng_x);

	Domain<UINT64> A_rz;
	t_->getDomain(2, A_rz);

	Domain<UINT64> Rd_ra;
	t_->getDomain(3, Rd_ra);

	Domain<UINT64> Tt_ra;
	t_->getDomain(4, Tt_ra);

	UINT32 number_of_photons = t_->photons_todo;

	SimState DeviceMem;

	DeviceMem.a = (UINT32*) rng_a.device_data_buffer;
	DeviceMem.x = (UINT64*) rng_x.device_data_buffer;
	DeviceMem.A_rz = (UINT64*) A_rz.device_data_buffer;
	DeviceMem.Rd_ra = (UINT64*) Rd_ra.device_data_buffer;
	DeviceMem.Tt_ra = (UINT64*) Tt_ra.device_data_buffer;

#ifndef NO_ATOMIC_COUNTERS

	cudaMalloc((void**) &DeviceMem.n_photons_left, sizeof(UINT32));
	cudaMemcpy(DeviceMem.n_photons_left, &number_of_photons, sizeof(UINT32),
			cudaMemcpyHostToDevice);

	cudaMalloc((void**) &DeviceMem.fluor_spawnned, sizeof(UINT32));
	cudaMemset(DeviceMem.fluor_spawnned, 0, sizeof(UINT32));

#endif

//TODO doing it twice?
//ubkSIMDTask_mcml* tasks = new ubkSIMDTask_mcml[task_count];

	ubkSpecificTask* d_tasks;
	cudaMalloc((void**) &d_tasks, task_count * sizeof(ubkSpecificTask));

#ifdef __DEBUG
	fprintf(stderr, "UBK: Allocated %d for primary tasks\n",
			(int) (task_count * sizeof(ubkSpecificTask)) / 1024 / 1024);
#endif

	cudaMemcpy(d_tasks, tasks_, (task_count) * sizeof(ubkSpecificTask),
			cudaMemcpyHostToDevice);

//Breaking tasks' array into pointers
//why host side?
	ubkSpecificTask** tasks_ptrs = new ubkSpecificTask*[task_count];

	for (int f = 0; f < task_count; f++)
		tasks_ptrs[f] = d_tasks + f;

	ubkSpecificTask** d_inbox_buffer;
	cudaMalloc((void**) &d_inbox_buffer,
			UBK_INBOX_SIZE * sizeof(ubkSpecificTask*));

	void* d_mem_buffer;
	cudaMalloc((void**) &d_mem_buffer, MEM_TOTAL * N_BLOCKS);
	__E(cudaMemset(d_mem_buffer, 0, MEM_TOTAL * N_BLOCKS));

#ifdef __DEBUG
	fprintf(stdout, "UBK: Allocated %d for d_inbox_buffer\n",
			(int) (UBK_INBOX_SIZE * sizeof(ubkSpecificTask*)) / 1024 / 1024);
#endif

	cudaMemset(d_inbox_buffer, 0, UBK_INBOX_SIZE * sizeof(ubkSpecificTask*));

	cudaMemcpy(d_inbox_buffer, tasks_ptrs,
			(task_count) * sizeof(ubkSpecificTask*), cudaMemcpyHostToDevice);

	void* d_queues_buffers;
	cudaMalloc((void**) &d_queues_buffers,
			N_BLOCKS * sizeof(ubkTask_ptr)
					* (LOCAL_INBOX_QUEUE_SIZE + LOCAL_OUTBOX_QUEUE_SIZE
							+ LOCAL_FREES_QUEUE_SIZE));

#ifdef __DEBUG
	fprintf(stdout, "UBK: allocated %d for queues\n",
			N_BLOCKS * sizeof(ubkTask_ptr)
					* (LOCAL_INBOX_QUEUE_SIZE + LOCAL_OUTBOX_QUEUE_SIZE
							+ LOCAL_FREES_QUEUE_SIZE)/ 1024 / 1024
					);

			checkCUDAmemory
	("before launch");
#endif

	Task_MCML* gtask_d;
	cudaMalloc((void**) &gtask_d, sizeof(Task_MCML));
	cudaMemcpy(gtask_d, t_, sizeof(Task_MCML), cudaMemcpyHostToDevice);

	UINT64* d_hop_counters = NULL;
#ifdef MEASURE_IMBALANCE
	cudaMalloc((void**) &d_hop_counters, N_BLOCKS * sizeof(UINT64));
	cudaMemset(d_hop_counters, 0, N_BLOCKS * sizeof(UINT64));
#endif

	UINT32* d_sm_counters = NULL;
#ifdef MEASURE_IMBALANCE2
	cudaMalloc((void**) &d_sm_counters, task_count * sizeof(UINT32));
	cudaMemset(d_sm_counters, 0, task_count * sizeof(UINT32));
#endif

	fprintf(stdout, "Launching pkernel (%d blocks) for %d tasks in GPU%d...\n", N_BLOCKS,
			task_count, t->get_assigned_to());
	checkCUDAError();

	pkernel<<<N_BLOCKS,32,0,t->stream>>>(DeviceMem, number_of_photons,
			d_inbox_buffer, d_mem_buffer, d_queues_buffers,
			task_count, d_hop_counters, d_sm_counters);

	checkCUDAError();

// ATTENTION
	cudaStreamSynchronize(t->stream);

#ifdef MEASURE_IMBALANCE
	UINT64* hop_counters = new UINT64[N_BLOCKS];
	cudaMemcpy(hop_counters, d_hop_counters, N_BLOCKS * sizeof(UINT64),
			cudaMemcpyDeviceToHost);

	UINT64 total = 0;
	for (int i = 0; i < N_BLOCKS; i++) {
		//	printf("%d %llu\n", i, hop_counters[i]);
		total += hop_counters[i];
	}
	printf("Total hops: %llu\n", total);
	printf("Relative standard deviation: %.2f%\n",
			relative_standard_deviation(hop_counters, N_BLOCKS));

#endif

#ifndef NO_ATOMIC_COUNTERS

	UINT32 n_photons_left;

// Copy the number of photons left from device to host.
	CUDA_SAFE_CALL(
			cudaMemcpy(&n_photons_left, DeviceMem.n_photons_left,
					sizeof(unsigned int), cudaMemcpyDeviceToHost));

	UINT32 fluor_spawnned;
	CUDA_SAFE_CALL(
			cudaMemcpy(&fluor_spawnned, DeviceMem.fluor_spawnned,
					sizeof(unsigned int), cudaMemcpyDeviceToHost));

	printf("[GPU%d] Number of photons left %10u\n", t->get_assigned_to(),
			n_photons_left);

	printf("[GPU%d] Number of fluor photons spawned %10u\n",
			t->get_assigned_to(), fluor_spawnned);
#endif

#ifdef MEASURE_IMBALANCE2
	UINT32* sm_photon = new UINT32[task_count];
	cudaMemcpy(sm_photon, d_sm_counters, task_count * sizeof(UINT32),
			cudaMemcpyDeviceToHost);

	cudaMemcpy(tasks_, d_tasks, (task_count) * sizeof(ubkSpecificTask),
			cudaMemcpyDeviceToHost);

	ubkSIMDTask_mcml* tt= (ubkSIMDTask_mcml*)tasks_;
	for (int i = 0; i < task_count; i++) {
		printf("%d %d %u\n", i, sm_photon[i],
				tt[i].photons[0].hop_count);
	}
#endif

	cudaFree(d_tasks);
	cudaFree(d_inbox_buffer);
	cudaFree(d_queues_buffers);

//delete tasks;
	delete tasks_ptrs;
	delete (ubkSpecificTask*) tasks_;

	checkCUDAError();

}

