#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"
#include "mcml.h"

#include "uberkernel.h"

void k_CPU_MCM_FULLT(Task* t_);
void k_CUDA_MCM_FULLT(Task* t_);

#ifndef __FULLT

class MY_ALIGN(16) ubkSIMDTask_mcml /*: public ubkTask*/{
public:

	int p_target_start;
	bool done[SIMD];
	PhotonStruct photons[SIMD];

//	// cartesian coordinates of the photon [cm]
//	FLOAT x[32];
//	FLOAT y[32];
//	FLOAT z[32];
//
//	// directional cosines of the photon
//	FLOAT ux[32];
//	FLOAT uy[32];
//	FLOAT uz[32];
//
//	FLOAT w[32];            // photon weight
//
//	FLOAT s[32];            // step size [cm]
//	FLOAT sleft[32];        // leftover step size [cm]
//
//	// index to layer where the photon resides
//	UINT32 layer[32];

// flag to indicate if photon hits a boundary
//UINT32 hit;

//UINT32 hop_count;

	__H_D__ ubkSIMDTask_mcml() {

	}

};

#endif

class Task_MCML: public Task {
public:

	UINT32 photons_todo;

	Task_MCML(TASK_TYPE t_) :
			Task(t_) {

	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		Domain<UINT32>* rng_a;
		getDomain(0, rng_a);

		Domain<UINT64>* rng_x;
		getDomain(1, rng_x);

		Domain<UINT64>* A_rz;
		getDomain(2, A_rz);

		Domain<UINT64>* Rd_ra;
		getDomain(3, Rd_ra);

		Domain<UINT64>* Tt_ra;
		getDomain(4, Tt_ra);

		float len1 = ceil(job_elements * m.value);

		Task_MCML** new_tasks = new Task_MCML*[2];

		if (photons_todo <= len1 || len1 == 0) {
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			return 1;
		}

#ifdef __FULLT

		Domain<UINT32>* rng_a_t1 = new Domain<UINT32>(rng_a, 1, R,
				dim_space(rng_a->X.start, rng_a->X.start + len1));

		Domain<UINT64>* rng_x_t1 = new Domain<UINT64>(rng_x, 1, R,
				dim_space(rng_x->X.start, rng_x->X.start + len1));
#else
#if defined(__PKERNEL) || defined(__PKERNELSIMD32)

		Domain<UINT32>* rng_a_t1 = rng_a;
		Domain<UINT64>* rng_x_t1 = rng_x;
#endif
#endif

#ifdef __FULLT
		Task_MCML* t1 = new Task_MCML(REGULAR);
#else
#if defined(__PKERNEL) || defined(__PKERNELSIMD32)
		Task_MCML* t1 = new Task_MCML(IRREGULAR);
#endif
#endif
		t1->associate_domain(rng_a_t1);
		t1->associate_domain(rng_x_t1);
		t1->associate_domain(A_rz);
		t1->associate_domain(Rd_ra);
		t1->associate_domain(Tt_ra);

		t1->associate_kernel(CPU, &k_CPU_MCM_FULLT);

#ifdef __FULLT
		t1->associate_kernel(GPU, &k_CUDA_MCM_FULLT);
#endif

		t1->photons_todo = len1;
		t1->wl.value = len1 / job_elements;
		t1->dice_lvl = dice_lvl + 1;
		t1->job_elements = job_elements;
#ifdef __FULLT

		Domain<UINT32>* rng_a_t2 = new Domain<UINT32>(rng_a, 1, R,
				dim_space(rng_a->X.start + len1, rng_a->X.end));

		Domain<UINT64>* rng_x_t2 = new Domain<UINT64>(rng_x, 1, R,
				dim_space(rng_x->X.start + len1, rng_x->X.end));

#else
#if defined(__PKERNEL) || defined(__PKERNELSIMD32)

		Domain<UINT32>* rng_a_t2 = rng_a;
		Domain<UINT64>* rng_x_t2 = rng_x;
#endif
#endif

#ifdef __FULLT
		Task_MCML* t2 = new Task_MCML(REGULAR);
#else
#if defined(__PKERNEL) || defined(__PKERNELSIMD32)
		Task_MCML* t2 = new Task_MCML(IRREGULAR);
#endif
#endif
		t2->associate_domain(rng_a_t2);
		t2->associate_domain(rng_x_t2);
		t2->associate_domain(A_rz);
		t2->associate_domain(Rd_ra);
		t2->associate_domain(Tt_ra);

		t2->associate_kernel(CPU, &k_CPU_MCM_FULLT);

#ifdef __FULLT
		t2->associate_kernel(GPU, &k_CUDA_MCM_FULLT);
#endif

		t2->photons_todo = photons_todo - len1;
		t2->wl.value = (photons_todo - len1) / job_elements;
		t2->dice_lvl = dice_lvl + 1;
		t2->job_elements = job_elements;

		new_tasks[0] = t1;
		new_tasks[1] = t2;

		new_tasks_ = (Task**) new_tasks;

		return 2;

	}

#ifndef __FULLT
	int diceTo(ubkTask*& new_tasks_, int n_bwu) {

		int task_count = 0;

		Domain<UINT32>* rng_a;
		getDomain(0, rng_a);

		float N = photons_todo;

		//float A = ceil(N / float(n_bwu));
		int divide_task_by = ceil(N / float(n_bwu));

		ubkSIMDTask_mcml* new_tasks = new ubkSIMDTask_mcml[divide_task_by];

		for (int i = 0, j = 0; i < N; i += n_bwu, j++) {

			ubkSIMDTask_mcml* t = new_tasks + j;

			t->p_target_start = /*p_particles_new->X.start + */i;

			//memset(&(t->photons), 0, sizeof(PhotonStruct) * 32);
			//memset(&(t->done), 0, sizeof(bool) * 32);

//
//			//#ifdef WARP_RR
//			//			//t->depth = 0;
//			//			memset(&(t->depth), 0, sizeof(short) * 32);
//			//
//			//#else
//			memset(&(t->depth), 0, sizeof(short) * 32);
//			//#endif
//			memset(&(t->done), 0, sizeof(bool) * 32);
//			t->live_threads = 32;
//			memcpy(t->throughput, throughput, sizeof(RGBe) * 32);
			task_count++;
		}

		new_tasks_ = (ubkTask*) new_tasks;
		return task_count;
	}

#endif

};

#endif /* UBK_TASK_H_ */
