#ifndef UBERKERNEL_H
#define UBERKERNEL_H


#define MCML

void k_CUDA_wrapper_uberkernel(Task* t,ubkTask* task_prms, int child_task_count);

#ifdef __FULLT


#endif

#ifdef __PKERNEL

#define UBK_IMPLEMENTED

#define SIMD 1

class ubkSIMDTask_mcml;

typedef ubkSIMDTask_mcml ubkSpecificTask;
typedef ubkSpecificTask* ubkTask_ptr;

#endif

#ifdef __PKERNELSIMD32

#define UBK_IMPLEMENTED

#define SIMD 32

class ubkSIMDTask_mcml;

typedef ubkSIMDTask_mcml ubkSpecificTask;
typedef ubkSpecificTask* ubkTask_ptr;

#endif




#endif

