#include "mcml-cuda.H"

//#define ITERATION_DELAY
//#define MAJ_VOTE
//#define RR_VOTE

#define STACK_SIZE 200

__global__ void MCMLKernel(SimState d_state, UINT32 number_of_photons,
		UINT64* d_hop_counters) {

	//UINT32 tid = blockIdx.x * NUM_THREADS_PER_BLOCK + threadIdx.x;
	UINT32 tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (tid >= number_of_photons)
		return;

#ifdef MCML_FLUOR
	PhotonStruct stack[STACK_SIZE];
	int stack_top = 0;
#endif

	// photon structure stored in registers
	PhotonStruct photon;

	LaunchPhoton(&photon);

#ifdef MCML_FLUOR
	stack[stack_top++] = photon;
#endif

	// random number seeds

	UINT64 rnd_x;
	UINT32 rnd_a;
	rnd_x = d_state.x[tid];
	rnd_a = d_state.a[tid];

#ifdef MCML_FLUOR
	while (!(stack_top == 0)) {

		// photon structure stored in registers
		PhotonStruct photon = stack[--stack_top];
#endif
		// Flag to indicate if this thread is active
		UINT32 is_active = 1;

		UINT32 hit;
		FLOAT s;

#ifdef ITERATION_DELAY

		int NOT_DELAYED = 1;

#ifdef MAJ_VOTE

		const int THREASH = 16;
#endif

#ifdef RR_VOTE

		// branch direction period : 0 ,0 ,0 ,1
		// 1 means the IF branch path
		// 0 means the ELSE branch path
		const int num_zeros = 6, num_ones = 1;
		const int period = num_zeros + num_ones;
		int counter = -1;
#endif

#endif

		while (is_active) {
			// Only process photon if the thread is active.

#ifdef ITERATION_DELAY

			if (NOT_DELAYED) {

#endif
			//>>>>>>>>> StepSizeInTissue() in MCML
			s = ComputeStepSize(&photon, &rnd_x, &rnd_a);
//			if (photon.fluor && photon.hop_count == 0)
//				printf("%lu\n", rnd_x);

			//>>>>>>>>> HitBoundary() in MCML
			hit = HitBoundary(&photon, s);

#ifdef ITERATION_DELAY

		}

		int COND_FOR_ALL;

#ifdef MAJ_VOTE
		int go_in_count = __popc(__ballot(hit));
		int active_threads = __popc(__ballot(1));

		if (active_threads < THREASH) {
			COND_FOR_ALL = hit;
			NOT_DELAYED = 1;
		} else {
			COND_FOR_ALL = go_in_count >= THREASH;
			NOT_DELAYED = (COND_FOR_ALL == hit);
		}
#endif

#ifdef RR_VOTE

		// update the direction
		if (++counter == period)
		counter = 0;

		COND_FOR_ALL = (counter >= num_zeros);
		// remove idle iteration
		//COND_FOR_ALL = COND_FOR_ALL ? __any(hit) : __all(hit);

		NOT_DELAYED = (COND_FOR_ALL == hit);

#endif

		if (NOT_DELAYED) {

#endif

			Hop(&photon, s);

#ifdef MEASURE_IMBALANCE
			//if (photon.fluor && hit)
			AtomicAddULL(&d_hop_counters[blockIdx.x], 1);
#endif

#ifdef ITERATION_DELAY
			if (COND_FOR_ALL) {
#else
			FLOAT absorved = 0.f;
			if (hit) {
#endif
				FastReflectTransmit(&photon, &d_state, &rnd_x, &rnd_a);
			} else {
				absorved = Drop(0, &photon, d_state.A_rz);

				Spin(d_layerspecs[photon.layer].g, &photon, &rnd_x, &rnd_a);
			}

			if (photon.w < WEIGHT) {
				FLOAT rand = rand_MWC_co(&rnd_x, &rnd_a);

				// This photon survives the roulette.
				if (photon.w != MCML_FP_ZERO && rand < CHANCE)
					photon.w *= (FP_ONE / CHANCE);
				// This photon is terminated.
				else {

#ifndef NO_ATOMIC_COUNTERS
					if (!photon.fluor)
					atomicSub(d_state.n_photons_left, 1);
#ifdef MCML_FLUOR
					else {
						atomicAdd(d_state.fluor_spawnned, 1);
					}
#endif

#endif
					is_active = 0;
					//printf("Photon done %d\n", photon.hop_count);

				}
			}
#ifdef MCML_FLUOR

			if (!photon.fluor && absorved != 0) {

				FLOAT rand = rand_MWC_co(&rnd_x, &rnd_a);

				if (rand < FLUOR_CHANCE) {
					// photon structure stored in registers

					PhotonStruct pfluor;

					LaunchPhoton(&pfluor);
					//printf("%f\n", rand);

					pfluor.fluor = true;

					pfluor.x = photon.x;
					pfluor.y = photon.y;
					pfluor.z = photon.z;

					// Monte carlo model
					pfluor.w = absorved * FLUOR_QY * (FP_ONE / FLUOR_CHANCE);

					//Fluorescence

					if (stack_top < STACK_SIZE)
						stack[stack_top++] = pfluor;
					else {
						//printf("stack overflow\n");
					}

				}
			}
#endif

			//////////////////////////////////////////////////////////////////////

#ifdef ITERATION_DELAY
		}
#endif

		} // end of the main loop

#ifdef MCML_FLUOR
	}
#endif

}

void k_CUDA_MCM_FULLT(Task* t_) {

	Task_MCML t = *(Task_MCML*) t_;

	Domain<UINT32> rng_a;
	t.getDomain(0, rng_a);

	Domain<UINT64> rng_x;
	t.getDomain(1, rng_x);

	Domain<UINT64> A_rz;
	t.getDomain(2, A_rz);

	Domain<UINT64> Rd_ra;
	t.getDomain(3, Rd_ra);

	Domain<UINT64> Tt_ra;
	t.getDomain(4, Tt_ra);

	UINT32 number_of_photons = rng_a.X.length();
	SimState DeviceMem;

//	cudaGetSymbolAddress(&(GPU_MCML::consts[dev]),GPU_MCML::d_simparam );
//
//    printf("Address of sum label in memory: %p\n", &GPU_MCML::consts[dev]);

	DeviceMem.a = (UINT32*) rng_a.device_data_buffer;
	DeviceMem.x = (UINT64*) rng_x.device_data_buffer;
	DeviceMem.A_rz = (UINT64*) A_rz.device_data_buffer;
	DeviceMem.Rd_ra = (UINT64*) Rd_ra.device_data_buffer;
	DeviceMem.Tt_ra = (UINT64*) Tt_ra.device_data_buffer;

#ifndef NO_ATOMIC_COUNTERS

	cudaMalloc((void**) &DeviceMem.n_photons_left, sizeof(UINT32));
	cudaMemcpy(DeviceMem.n_photons_left, &number_of_photons, sizeof(UINT32),
			cudaMemcpyHostToDevice);

	cudaMalloc((void**) &DeviceMem.fluor_spawnned, sizeof(UINT32));
	cudaMemset(DeviceMem.fluor_spawnned, 0, sizeof(UINT32));

#endif

	int numBlocks, numThreads;
	ComputeGridSize(number_of_photons, 512, numBlocks, numThreads);

	//	dim3 dimBlock(NUM_THREADS_PER_BLOCK);
	//	dim3 dimGrid(NUM_BLOCKS);

	dim3 dimBlock(numThreads);
	dim3 dimGrid(numBlocks);

	UINT64* d_hop_counters = NULL;

#ifdef MEASURE_IMBALANCE
	cudaMalloc((void**) &d_hop_counters, numBlocks * sizeof(UINT64));
	cudaMemset(d_hop_counters, 0, numBlocks * sizeof(UINT64));
#endif

	fprintf(stdout, "Launching fullt for %d photons in GPU%d...\n",
			number_of_photons, t.get_assigned_to());

	MCMLKernel<<<dimGrid, dimBlock>>>(DeviceMem,number_of_photons,
			d_hop_counters);

	// Check if there was an error
	cudaError_t cudastat;
	cudastat = cudaGetLastError();
	if (cudastat) {
		fprintf(stderr, "[GPU%d] failure in MCMLKernel (%i): %s.\n",
				t.get_assigned_to(), cudastat, cudaGetErrorString(cudastat));
		exit(1);
	}

#ifdef MEASURE_IMBALANCE
	UINT64* hop_counters = new UINT64[numBlocks];
	cudaMemcpy(hop_counters, d_hop_counters, numBlocks * sizeof(UINT64),
			cudaMemcpyDeviceToHost);

	UINT64 total = 0;
	for (int i = 0; i < numBlocks; i++) {
		//printf("%d: %llu\n", i, hop_counters[i]);
		total += hop_counters[i];
	}
	printf("Total hops: %llu\n", total);
	printf("Relative standard deviation: %.2f%\n",
			relative_standard_deviation(hop_counters, numBlocks));

#endif

#ifndef NO_ATOMIC_COUNTERS

	UINT32 n_photons_left;

	// Copy the number of photons left from device to host.
	CUDA_SAFE_CALL(
			cudaMemcpy(&n_photons_left, DeviceMem.n_photons_left,
					sizeof(unsigned int), cudaMemcpyDeviceToHost));

	UINT32 fluor_spawnned;
	CUDA_SAFE_CALL(
			cudaMemcpy(&fluor_spawnned, DeviceMem.fluor_spawnned,
					sizeof(unsigned int), cudaMemcpyDeviceToHost));

	printf("[GPU%d] Number of photons left %10u\n", t.get_assigned_to(),
			n_photons_left);

	printf("[GPU%d] Number of fluor photons spawned %10u\n",
			t.get_assigned_to(), fluor_spawnned);

#endif
}

