/*****************************************************************************
 *
 *   Kernel code for GPUMCML
 *   =========================================================================
 *   Features:
 *   1) Backwards compatible with older graphics card (compute capability 1.1)
 *      using emulated atomicAdd (AtomicAddULL) enabled by EMULATED_ATOMIC flag
 *      See <gpumcml_kernel.h>.
 *   2) Simplified function parameter list with PhotonStructGPU
 *   3) Simplified Drop code (no atomic access optimizations)
 ****************************************************************************/
/*	 
 *   This file is part of GPUMCML.
 *
 *   GPUMCML is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   GPUMCML is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with GPUMCML.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GPUMCML_KERNEL_CPU_
#define _GPUMCML_KERNEL_CPU_

#include "../../perform.h"

#include "user_defined_task.h"
#include "atomic.h"
#include "math.h"
#include "float.h"
#include "mcml_cpu.h"
#include "mcml.h"
#include <omp.h>

#include <fenv.h>       /* fesetround, FE_* */

namespace CPU_MCML {

#define STACK_SIZE 200

SimParamDevice d_simparam;
LayerStructDevice d_layerspecs[MAX_LAYERS];

//////////////////////////////////////////////////////////////////////////////
//   Initialize Device Constant Memory with read-only data
//////////////////////////////////////////////////////////////////////////////
int InitConstantCPUMem(SimulationStruct *sim) {
	// Make sure that the number of layers is within the limit.
	UINT32 n_layers = sim->n_layers + 2;
	if (n_layers > MAX_LAYERS)
		return 1;

	CPU_MCML::d_simparam.num_layers = sim->n_layers;  // not plus 2 here
	CPU_MCML::d_simparam.init_photon_w = sim->start_weight;
	CPU_MCML::d_simparam.dz = sim->det.dz;
	CPU_MCML::d_simparam.dr = sim->det.dr;
	CPU_MCML::d_simparam.na = sim->det.na;
	CPU_MCML::d_simparam.nz = sim->det.nz;
	CPU_MCML::d_simparam.nr = sim->det.nr;

	for (UINT32 i = 0; i < n_layers; ++i) {
		CPU_MCML::d_layerspecs[i].z0 = sim->layers[i].z_min;
		CPU_MCML::d_layerspecs[i].z1 = sim->layers[i].z_max;
		FLOAT n1 = sim->layers[i].n;
		CPU_MCML::d_layerspecs[i].n = n1;

		// TODO: sim->layer should not do any pre-computation.
		FLOAT rmuas = sim->layers[i].mutr;
		CPU_MCML::d_layerspecs[i].muas = FP_ONE / rmuas;
		CPU_MCML::d_layerspecs[i].rmuas = rmuas;
		CPU_MCML::d_layerspecs[i].mua_muas = sim->layers[i].mua * rmuas;

		CPU_MCML::d_layerspecs[i].g = sim->layers[i].g;

		if (i == 0 || i == n_layers - 1) {
			CPU_MCML::d_layerspecs[i].cos_crit0 = MCML_FP_ZERO;
			CPU_MCML::d_layerspecs[i].cos_crit1 = MCML_FP_ZERO;
		} else {
			FLOAT n2 = sim->layers[i - 1].n;
			CPU_MCML::d_layerspecs[i].cos_crit0 =
					(n1 > n2) ?
							sqrtf(FP_ONE - n2 * n2 / (n1 * n1)) : MCML_FP_ZERO;
			n2 = sim->layers[i + 1].n;
			CPU_MCML::d_layerspecs[i].cos_crit1 =
					(n1 > n2) ?
							sqrtf(FP_ONE - n2 * n2 / (n1 * n1)) : MCML_FP_ZERO;
		}
	}

	return 0;
}

float __logf(float x) {
	return logf(x);
}

float __fdividef(float x, float y) {
	return (x / y);
}

float rsqrtf(float x) {
	return 1 / sqrt(x);
}

void __sincosf(float x, float *sptr, float *cptr) {
	return sincosf(x, sptr, cptr);
}

/**
 * \ingroup CUDA_MATH_INTRINSIC_CAST
 * \brief Convert an unsigned integer to a float in round-towards-zero mode.
 *
 * Convert the unsigned integer value \p x to a single-precision floating point value
 * in round-towards-zero mode.
 * \return Returns converted value.
 */
//extern __device__ __device_builtin__ float  __uint2float_rz(unsigned int x);
float __uint2float_rz(unsigned int x) {

	float a = float(x);

	return a;
}

//////////////////////////////////////////////////////////////////////////////
//   Generates a random number between 0 and 1 [0,1)
//////////////////////////////////////////////////////////////////////////////
FLOAT rand_MWC_co(UINT64* x, UINT32* a) {
	*x = (*x & 0xffffffffull) * (*a) + (*x >> 32);

	//fesetround(FE_TOWARDZERO);
	float d = __uint2float_rz((UINT32) (*x));
	//fesetround(FE_TONEAREST);

	float s = (FLOAT) 0x100000000;

	float dd = __fdividef(d, s);

	return dd;
	// The typecast will truncate the x so that it is 0<=x<(2^32-1),
	// __uint2FLOAT_rz ensures a round towards zero since 32-bit FLOATing point
	// cannot represent all integers that large.
	// Dividing by 2^32 will hence yield [0,1)

}

//////////////////////////////////////////////////////////////////////////////sim

//   Generates a random number between 0 and 1 (0,1]
//////////////////////////////////////////////////////////////////////////////
FLOAT rand_MWC_oc(UINT64* x, UINT32* a) {
	return 1.0f - rand_MWC_co(x, a);
}

//////////////////////////////////////////////////////////////////////////////
//   AtomicAdd for Unsigned Long Long (ULL) data type
//   Note: 64-bit atomicAdd to global memory are only supported 
//   in graphics card with compute capability 1.2 or above
//////////////////////////////////////////////////////////////////////////////
void AtomicAddULL(UINT64* address, UINT32 add) {

	//AtomicAdd(address, (UINT64) add);
	UINT64 v = (UINT64) add;

	__sync_fetch_and_add(address, v);

}

//////////////////////////////////////////////////////////////////////////////
//   Initialize photon position (x, y, z), direction (ux, uy, uz), weight (w), 
//   step size remainder (sleft), and current layer (layer) 
//   Note: Infinitely narrow beam (pointing in the +z direction = downwards)
//////////////////////////////////////////////////////////////////////////////
void LaunchPhoton(PhotonStruct *photon) {
	photon->x = photon->y = photon->z = MCML_FP_ZERO;
	photon->ux = photon->uy = MCML_FP_ZERO;
	photon->uz = FP_ONE;
	photon->w = d_simparam.init_photon_w;
	photon->sleft = MCML_FP_ZERO;
	photon->layer = 1;
	photon->hop_count = 0;
	photon->fluor = 0;

}

//////////////////////////////////////////////////////////////////////////////
//   Compute the step size for a photon packet when it is in tissue
//   If sleft is 0, calculate new step size: -log(rnd)/(mua+mus).
//   Otherwise, pick up the leftover in sleft.
//////////////////////////////////////////////////////////////////////////////
FLOAT ComputeStepSize(PhotonStruct *photon, UINT64 *rnd_x, UINT32 *rnd_a) {
	// Make a new step if no leftover.
	FLOAT s;
	if (photon->sleft == MCML_FP_ZERO) {
		FLOAT rand = rand_MWC_oc(rnd_x, rnd_a);
		s = -__logf(rand) * d_layerspecs[photon->layer].rmuas;
	} else {
		s = photon->sleft * d_layerspecs[photon->layer].rmuas;
		photon->sleft = MCML_FP_ZERO;
	}

	return s;
}

//////////////////////////////////////////////////////////////////////////////
//   Check if the step size calculated above will cause the photon to hit the 
//   boundary between 2 layers.
//   Return 1 for a hit, 0 otherwise.
//   If the projected step hits the boundary, the photon steps to the boundary
//   and the remainder of the step size is stored in sleft for the next iteration
//////////////////////////////////////////////////////////////////////////////
int HitBoundary(PhotonStruct *photon, FLOAT& s) {
	/* step size to boundary. */
	FLOAT dl_b;

	/* Distance to the boundary. */
	FLOAT z_bound =
			(photon->uz > MCML_FP_ZERO) ?
					d_layerspecs[photon->layer].z1 :
					d_layerspecs[photon->layer].z0;
	dl_b = __fdividef(z_bound - photon->z, photon->uz);     // dl_b > 0

	UINT32 hit_boundary = (photon->uz != MCML_FP_ZERO) && (s > dl_b);
	if (hit_boundary) {
		// No need to multiply by (mua + mus), as it is later
		// divided by (mua + mus) anyways (in the original version).
		photon->sleft = (s - dl_b) * d_layerspecs[photon->layer].muas;
		s = dl_b;
	}

	return hit_boundary;
}

//////////////////////////////////////////////////////////////////////////////
//   Move the photon by step size (s) along direction (ux,uy,uz) 
//////////////////////////////////////////////////////////////////////////////
void Hop(PhotonStruct *photon, FLOAT& s) {
	photon->x += s * photon->ux;
	photon->y += s * photon->uy;
	photon->z += s * photon->uz;
	photon->hop_count += 1;

}

//////////////////////////////////////////////////////////////////////////////
//   Drop a part of the weight of the photon to simulate absorption
//////////////////////////////////////////////////////////////////////////////
FLOAT Drop(UINT32 ignoreDetection, PhotonStruct *photon, UINT64 *g_A_rz) {

	FLOAT dwa = photon->w * d_layerspecs[photon->layer].mua_muas;
	photon->w -= dwa;

	// If scoring of absorption is specified (no -A flag in command line)
	if (ignoreDetection == 0) {
		UINT32 iz = __fdividef(photon->z, d_simparam.dz);
		UINT32 ir = __fdividef(
				sqrtf(photon->x * photon->x + photon->y * photon->y),
				d_simparam.dr);

		// Only record if photon is not at the edge!!
		// This will be ignored anyways.
		if (iz < d_simparam.nz && ir < d_simparam.nr) {
			UINT32 addr = ir * d_simparam.nz + iz;

			// Write to the global memory.
			AtomicAddULL(&g_A_rz[addr], (UINT64) (dwa * WEIGHT_SCALE));
		}
	}

	return dwa;

}

//////////////////////////////////////////////////////////////////////////////
//   UltraFast version (featuring reduced divergence compared to CPU-MCML)
//   If a photon hits a boundary, determine whether the photon is transmitted
//   into the next layer or reflected back by computing the internal reflectance
//////////////////////////////////////////////////////////////////////////////
void FastReflectTransmit(PhotonStruct *photon, SimState *d_state_ptr,
		UINT64 *rnd_x, UINT32 *rnd_a) {
	/* Collect all info that depend on the sign of "uz". */
	FLOAT cos_crit;
	UINT32 new_layer;
	if (photon->uz > MCML_FP_ZERO) {
		cos_crit = d_layerspecs[photon->layer].cos_crit1;
		new_layer = photon->layer + 1;
	} else {
		cos_crit = d_layerspecs[photon->layer].cos_crit0;
		new_layer = photon->layer - 1;
	}

	// cosine of the incident angle (0 to 90 deg)
	FLOAT ca1 = fabsf(photon->uz);

	// The default move is to reflect.
	photon->uz = -photon->uz;

	// Moving this check down to "RFresnel = MCML_FP_ZERO" slows down the
	// application, possibly because every thread is forced to do
	// too much.
	if (ca1 > cos_crit) {
		/* Compute the Fresnel reflectance. */

		// incident and transmit refractive index
		FLOAT ni = d_layerspecs[photon->layer].n;
		FLOAT nt = d_layerspecs[new_layer].n;
		FLOAT ni_nt = __fdividef(ni, nt);   // reused later

		FLOAT sa1 = sqrtf(FP_ONE - ca1 * ca1);
		if (ca1 > COSZERO)
			sa1 = MCML_FP_ZERO;
		FLOAT sa2 = fminf(ni_nt * sa1, FP_ONE);
		FLOAT uz1 = sqrtf(FP_ONE - sa2 * sa2);    // uz1 = ca2

		FLOAT ca1ca2 = ca1 * uz1;
		FLOAT sa1sa2 = sa1 * sa2;
		FLOAT sa1ca2 = sa1 * uz1;
		FLOAT ca1sa2 = ca1 * sa2;

		// normal incidence: [(1-ni_nt)/(1+ni_nt)]^2
		// We ensure that ca1ca2 = 1, sa1sa2 = 0, sa1ca2 = 1, ca1sa2 = ni_nt
		if (ca1 > COSZERO) {
			sa1ca2 = FP_ONE;
			ca1sa2 = ni_nt;
		}

		FLOAT cam = ca1ca2 + sa1sa2; /* c- = cc + ss. */
		FLOAT sap = sa1ca2 + ca1sa2; /* s+ = sc + cs. */
		FLOAT sam = sa1ca2 - ca1sa2; /* s- = sc - cs. */

		FLOAT rFresnel = __fdividef(sam, sap * cam);
		rFresnel *= rFresnel;
		rFresnel *= (ca1ca2 * ca1ca2 + sa1sa2 * sa1sa2);

		// In this case, we do not care if "uz1" is exactly 0.
		if (ca1 < COSNINETYDEG || sa2 == FP_ONE)
			rFresnel = FP_ONE;

		FLOAT rand = rand_MWC_co(rnd_x, rnd_a);

		if (rFresnel < rand) {
			// The move is to transmit.
			photon->layer = new_layer;

			// Let's do these even if the photon is dead.
			photon->ux *= ni_nt;
			photon->uy *= ni_nt;
			photon->uz = -copysignf(uz1, photon->uz);

			if (photon->layer == 0 || photon->layer > d_simparam.num_layers) {
				// transmitted
				FLOAT uz2 = photon->uz;
				UINT64 *ra_arr = d_state_ptr->Tt_ra;
				if (photon->layer == 0) {
					// diffuse reflectance
					uz2 = -uz2;
					ra_arr = d_state_ptr->Rd_ra;
				}

				UINT32 ia = acosf(uz2) * FP_TWO * RPI * d_simparam.na;
				UINT32 ir = __fdividef(
						sqrtf(photon->x * photon->x + photon->y * photon->y),
						d_simparam.dr);
				if (ir >= d_simparam.nr)
					ir = d_simparam.nr - 1;

				AtomicAddULL(&ra_arr[ia * d_simparam.nr + ir],
						(UINT32) (photon->w * WEIGHT_SCALE));

				// Kill the photon.
				photon->w = MCML_FP_ZERO;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
//   Computing the scattering angle and new direction by 
//	 sampling the polar deflection angle theta and the
// 	 azimuthal angle psi.
//////////////////////////////////////////////////////////////////////////////
void Spin(FLOAT g, PhotonStruct *photon, UINT64 *rnd_x, UINT32 *rnd_a) {
	FLOAT cost, sint; // cosine and sine of the polar deflection angle theta
	FLOAT cosp, sinp; // cosine and sine of the azimuthal angle psi
	FLOAT psi;
	FLOAT SIGN;
	FLOAT temp;
	FLOAT last_ux, last_uy, last_uz;
	FLOAT rand;

	/***********************************************************
	 *	>>>>>>> SpinTheta
	 *  Choose (sample) a new theta angle for photon propagation
	 *	according to the anisotropy.
	 *
	 *	If anisotropy g is 0, then
	 *		cos(theta) = 2*rand-1.
	 *	otherwise
	 *		sample according to the Henyey-Greenstein function.
	 *
	 *	Returns the cosine of the polar deflection angle theta.
	 ****/

	rand = rand_MWC_co(rnd_x, rnd_a);

	cost = FP_TWO * rand - FP_ONE;

	if (g != MCML_FP_ZERO) {
		temp = __fdividef((FP_ONE - g * g), FP_ONE + g * cost);
		cost = __fdividef(FP_ONE + g * g - temp * temp, FP_TWO * g);
		cost = fmaxf(cost, -FP_ONE);
		cost = fminf(cost, FP_ONE);
	}
	sint = sqrtf(FP_ONE - cost * cost);

	/* spin psi 0-2pi. */
	rand = rand_MWC_co(rnd_x, rnd_a);

	psi = FP_TWO * PI_const * rand;
	__sincosf(psi, &sinp, &cosp);

	FLOAT stcp = sint * cosp;
	FLOAT stsp = sint * sinp;

	last_ux = photon->ux;
	last_uy = photon->uy;
	last_uz = photon->uz;

	if (fabsf(last_uz) > COSZERO)
	/* normal incident. */
	{
		photon->ux = stcp;
		photon->uy = stsp;
		SIGN = ((last_uz) >= MCML_FP_ZERO ? FP_ONE : -FP_ONE);
		photon->uz = cost * SIGN;
	} else
	/* regular incident. */
	{
		temp = rsqrtf(FP_ONE - last_uz * last_uz);
		photon->ux = (stcp * last_ux * last_uz - stsp * last_uy) * temp
				+ last_ux * cost;
		photon->uy = (stcp * last_uy * last_uz + stsp * last_ux) * temp
				+ last_uy * cost;
		photon->uz = __fdividef(-stcp, temp) + last_uz * cost;
	}
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//   Main Kernel for MCML (Calls the above inline device functions)
//////////////////////////////////////////////////////////////////////////////

void MCMLKernel(SimState d_state, UINT32 i) {

#ifdef MCML_FLUOR
	PhotonStruct stack[STACK_SIZE];
	int stack_top = 0;
#endif

	// photon structure stored in registers
	PhotonStruct photon;

	// random number seeds
	UINT64 rnd_x;
	UINT32 rnd_a;

	// Flag to indicate if this thread is active

	LaunchPhoton(&photon);

#ifdef MCML_FLUOR
	stack[stack_top++] = photon;
#endif

	rnd_x = d_state.x[i];
	rnd_a = d_state.a[i];

	FLOAT s;
	UINT32 hit;

#ifdef MCML_FLUOR
	while (!(stack_top == 0)) {

		// photon structure stored in registers
		PhotonStruct photon = stack[--stack_top];
#endif

		UINT32 is_active = 1;

		while (is_active) {
			// Only process photon if the thread is active.

			//>>>>>>>>> StepSizeInTissue() in MCML
			s = ComputeStepSize(&photon, &rnd_x, &rnd_a);

			//>>>>>>>>> HitBoundary() in MCML
			hit = HitBoundary(&photon, s);

			Hop(&photon, s);
			FLOAT absorved = 0.f;
			if (hit)
				FastReflectTransmit(&photon, &d_state, &rnd_x, &rnd_a);
			else {
				absorved = Drop(0, &photon, d_state.A_rz);

				Spin(d_layerspecs[photon.layer].g, &photon, &rnd_x, &rnd_a);
			}

			/***********************************************************
			 *  >>>>>>>>> Roulette()
			 *  If the photon weight is small, the photon packet tries
			 *  to survive a roulette.
			 ****/
			if (photon.w < WEIGHT) {
				FLOAT rand = rand_MWC_co(&rnd_x, &rnd_a);

				// This photon survives the roulette.
				if (photon.w != MCML_FP_ZERO && rand < CHANCE)
					photon.w *= (FP_ONE / CHANCE);
				// This photon is terminated.
				else {
#ifndef NO_ATOMIC_COUNTERS
					if (!photon.fluor)
					AtomicAdd(d_state.n_photons_left, -1);

#ifdef MCML_FLUOR
					else {
						AtomicAdd(d_state.fluor_spawnned, 1);
					}
#endif
#endif

					is_active = 0;

				}
			}

#ifdef MCML_FLUOR

			if (!photon.fluor && absorved != 0) {

				FLOAT rand = rand_MWC_co(&rnd_x, &rnd_a);

				if (rand < FLUOR_CHANCE) {
					// photon structure stored in registers
					PhotonStruct pfluor;

					LaunchPhoton(&pfluor);
					//printf("%f\n", rand);

					pfluor.fluor = true;

					pfluor.x = photon.x;
					pfluor.y = photon.y;
					pfluor.z = photon.z;

					// Monte carlo model
					pfluor.w = absorved * FLUOR_QY * (FP_ONE / FLUOR_CHANCE);

					//Fluorescence
					//pfluor.w = photon.w * FLUOR_QY;

					if (stack_top < STACK_SIZE)
						stack[stack_top++] = pfluor;
					else {
						printf("stack overflow\n");
					}

				}
			}
#endif
			//////////////////////////////////////////////////////////////////////
		} // end of the main loop

#ifdef MCML_FLUOR
	}
#endif

     d_state.x[i]=rnd_x;     
	d_state.a[i]=rnd_a;
}

}  // namespace CPU_MCML

void k_CPU_MCM_FULLT(Task* t_) {

	Task_MCML t = *(Task_MCML*) t_;

	Domain<UINT32> rng_a;
	t.getDomain(0, rng_a);

	Domain<UINT64> rng_x;
	t.getDomain(1, rng_x);

	Domain<UINT64> A_rz;
	t.getDomain(2, A_rz);

	Domain<UINT64> Rd_ra;
	t.getDomain(3, Rd_ra);

	Domain<UINT64> Tt_ra;
	t.getDomain(4, Tt_ra);

	UINT32 number_of_photons = t.photons_todo;
	UINT32 fluor_spawnned = 0;
	SimState DeviceMem;

	DeviceMem.n_photons_left = &number_of_photons;
	DeviceMem.fluor_spawnned = &fluor_spawnned;

	DeviceMem.a = (UINT32*) rng_a.device_data_buffer;
	DeviceMem.x = (UINT64*) rng_x.device_data_buffer;
	DeviceMem.A_rz = (UINT64*) A_rz.device_data_buffer;
	DeviceMem.Rd_ra = (UINT64*) Rd_ra.device_data_buffer;
	DeviceMem.Tt_ra = (UINT64*) Tt_ra.device_data_buffer;


	fprintf(stdout, "Launching cpu for %d photons...\n",
			number_of_photons);

	fesetround(FE_TOWARDZERO);


#pragma omp parallel for schedule(guided)
	for (int i = 0; i < number_of_photons; i++) {
		int id = omp_get_thread_num();
		CPU_MCML::MCMLKernel(DeviceMem,id );
		//CPU_MCML::MCMLKernel(DeviceMem,  i); 

	}

	fesetround(FE_TONEAREST);

#ifndef NO_ATOMIC_COUNTERS

	printf("[CPU] Number of photons left %10u\n", *DeviceMem.n_photons_left);
	printf("[CPU] Number of fluor photons spawned %10u\n", fluor_spawnned);

#endif

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
#endif  // _GPUMCML_KERNEL_CU_
