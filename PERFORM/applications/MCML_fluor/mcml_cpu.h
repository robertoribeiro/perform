/*****************************************************************************
 *
 *   Header file for GPU-related data structures and kernel configurations
 *
 ****************************************************************************/
/*	 
 *   This file is part of GPUMCML.
 * 
 *   GPUMCML is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   GPUMCML is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with GPUMCML.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GPUMCML_CPU_H_
#define _GPUMCML_CPU_H_

#include "mcml.h"

namespace CPU_MCML {

typedef struct
{
  FLOAT init_photon_w;      // initial photon weight

  FLOAT dz;                 // z grid separation.[cm]
  FLOAT dr;                 // r grid separation.[cm]

  UINT32 na;                // array range 0..na-1.
  UINT32 nz;                // array range 0..nz-1.
  UINT32 nr;                // array range 0..nr-1.

  UINT32 num_layers;        // number of layers.
} SimParamDevice;

typedef struct
{
  FLOAT z0, z1;             // z coordinates of a layer. [cm]
  FLOAT n;                  // refractive index of a layer.

  FLOAT muas;               // mua + mus
  FLOAT rmuas;              // 1/(mua+mus)
  FLOAT mua_muas;           // mua/(mua+mus)

  FLOAT g;                  // anisotropy.

  FLOAT cos_crit0, cos_crit1;
} LayerStructDevice;

extern CPU_MCML::SimParamDevice d_simparam;
extern CPU_MCML::LayerStructDevice d_layerspecs[MAX_LAYERS];

}



#endif // _GPUMCML_KERNEL_H_

