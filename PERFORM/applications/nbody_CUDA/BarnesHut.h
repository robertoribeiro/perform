#ifndef BARNESHUT_H_
#define BARNESHUT_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "../nbody_common/BarnesHut_common.h"
#include "../nbody_common/BarnesHut_common.h"

using namespace std;


void compute_force_cuda(cell* cells, particle* particles,int nbodies, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step);
void update_positions_cuda(particle* particles,int nbodies,  BH_TYPE dtime);
void jobBARNES_HUT(int argc, char *argv[]);


#endif
