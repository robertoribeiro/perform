#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>
#include "../../myUtil.h"

#include "BarnesHut.h"
#include "../../common.h"

using namespace std;


void jobBARNES_HUT(int argc, char *argv[]) {

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	char* filename;

	PERFORM_timer* timer = new PERFORM_timer();
	particle* particles;

	initBARNES_HUT(argc,argv,particles,nbodies,timesteps,filename);

	load_data_set(particles, filename, nbodies, timesteps, dtime, eps, tol);

	//fprintf(stderr, "configuration: %d bodies, %d time steps running in CUDA\n", nbodies, timesteps);

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	int device = 0;
	cudaSetDevice(device);

	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, device);

	//cout << "Using a device with compute capability: " << deviceProp.major << "." << deviceProp.minor << endl;

	timer->start();

	for (int step = 0; step < timesteps; step++) {

		//printf("timstep: %d\n",step);

		BH_TYPE diameter, centerx, centery, centerz;

		compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);

		timer->append_and_restart(0);

		int nnodes = nbodies * 2;
		if (nnodes < 1024 * 16)
			nnodes = 1024 * 16;

		cell* nodes = new cell[nnodes];

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)
			root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)
			root.child_indexes[k] = -1;

		nodes[0] = root;

		const BH_TYPE radius = diameter * 0.5;
		cell_count = build_tree_array(nodes, radius, particles, nbodies, tree_height);

		timer->append_and_restart(1);

		compute_center_of_mass(nodes, particles, 0, tree_height);

		timer->append_and_restart(2);

		cell* d_nodes;
		particle* d_particles;

		cudaMalloc(&d_nodes, cell_count * sizeof(struct cell_s));
		cudaMalloc(&d_particles, nbodies * sizeof(struct s_particle));

		cudaMemcpy(d_nodes, nodes, cell_count * sizeof(struct cell_s), cudaMemcpyHostToDevice);
		checkCUDAError();

		cudaMemcpy(d_particles, particles, nbodies * sizeof(struct s_particle), cudaMemcpyHostToDevice);
		checkCUDAError();

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO e xplaind or dodifferent

		compute_force_cuda(d_nodes, d_particles, nbodies, tree_height, dtime, epssq, dsq, step);
		checkCUDAError();

		cudaThreadSynchronize();

		timer->append_and_restart(3);

		update_positions_cuda(d_particles, nbodies, dtime);
		checkCUDAError();

		cudaMemcpy(particles, d_particles, nbodies * sizeof(struct s_particle), cudaMemcpyDeviceToHost);
		checkCUDAError();

		timer->append_and_restart(4);
		cudaThreadSynchronize();

	}

	timer->stop();
//	printf("using %d\n", sizeof(BH_TYPE));
//
		printf("%.1f\t",timer->duration);
#ifdef _DEBUG
		jobBARNES_HUT_check(argc,argv,particles);
#endif
		//PrintParticles(particles, nbodies);


}

