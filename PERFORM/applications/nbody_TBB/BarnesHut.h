#ifndef BARNESHUT_H_
#define BARNESHUT_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "../nbody_common/BarnesHut_common.h"
using namespace std;
using namespace tbb;


void compute_force(cell* cells, particle* particles, int particle, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step);
void update_positions(particle* particles, int part, BH_TYPE dtime);
void jobBARNES_HUT(int argc, char *argv[]);

class ComputeForceSet {
public:

	cell* cells;
	particle* particles;
	int tree_height;
	BH_TYPE dtime;
	BH_TYPE epssq;
	BH_TYPE dsq;
	int step;

	void operator()(const blocked_range<int>& range) const {
		for (int i = range.begin(); i != range.end(); i++) {
			compute_force(cells, particles, i, tree_height, dtime, epssq, dsq, step);
		}
	}

	ComputeForceSet(cell* cells_, particle* particles_, int tree_height_, BH_TYPE dtime_, BH_TYPE epssq_, BH_TYPE dsq, int step_) :
		cells(cells_), particles(particles_), tree_height(tree_height_), dtime(dtime_), epssq(epssq_), dsq(dsq), step(step_) {
	}
};

class UpdatePositionSet {
public:
	particle* particles;
	BH_TYPE dtime;

	void operator()(const blocked_range<int>& range) const {
		for (int i = range.begin(); i != range.end(); i++) {
			update_positions(particles, i, dtime);
		}
	}

	UpdatePositionSet(particle* particles_, BH_TYPE dtime_) :
		particles(particles_), dtime(dtime_) {
	}
};


#endif
