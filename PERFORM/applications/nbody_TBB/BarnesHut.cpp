#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>
#include "../../myUtil.h"

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut.h"

using namespace std;
using namespace tbb;


void compute_force(cell* cells, particle* particles, int particle, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {

	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
	BH_TYPE ax, ay, az;

	ax = particles[particle].accx;
	ay = particles[particle].accy;
	az = particles[particle].accz;

	particles[particle].accx = 0.0;
	particles[particle].accy = 0.0;
	particles[particle].accz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;

	dsq *= 0.25;

	while (!(stack_top == 0 && child == 8)) {

		if (child == 8) {
			current = cells[current].parent_cell_index;
			dsq *= 4;
			child = stack[--stack_top];
			child++;

		} else {

			mass_position mass_pos;
			if (cells[current].child_types[child] == PARTICLE)
				mass_pos = particles[cells[current].child_indexes[child]].mass_pos;
			else if (cells[current].child_types[child] == CELL)
				mass_pos = cells[cells[current].child_indexes[child]].mass_pos;
			else {
				child++;
				continue;
			}

			mass_position mass_pos_p = particles[particle].mass_pos;

			drx = mass_pos.x - mass_pos_p.x;
			dry = mass_pos.y - mass_pos_p.y;
			drz = mass_pos.z - mass_pos_p.z;

			drsq = drx * drx + dry * dry + drz * drz;
			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
			if (drsq < dsq) {
				if (cells[current].child_types[child] == CELL) {
					dsq *= 0.25;
					current = cells[current].child_indexes[child];
					stack[stack_top++] = child;
					child = 0;
				} else if (cells[current].child_types[child] == PARTICLE) {
					if (mass_pos.id != mass_pos_p.id) {
						drsq += epssq;
						idr = 1 / sqrt(drsq);
						nphi = mass_pos.mass * idr;
						scale = nphi * idr * idr;
						particles[particle].accx += drx * scale;
						particles[particle].accy += dry * scale;
						particles[particle].accz += drz * scale;

					}
					child++;
				} else if (cells[current].child_types[child] == NILL) {
					child++;
				}
			} else {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = mass_pos.mass * idr;
				scale = nphi * idr * idr;
				particles[particle].accx += drx * scale;
				particles[particle].accy += dry * scale;
				particles[particle].accz += drz * scale;
				child++;
			}

		}
	}

	BH_TYPE dthf = 0.5 * dtime;

	if (step > 0) {
		particles[particle].velx += (particles[particle].accx - ax) * dthf;
		particles[particle].vely += (particles[particle].accy - ay) * dthf;
		particles[particle].velz += (particles[particle].accz - az) * dthf;
	}

	/*PrintBH_TYPE(particles[particle].velx);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].vely);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].velz);
	 printf("\n");*/

}

void update_positions(particle* particles, int part, BH_TYPE dtime) {

	BH_TYPE dthf = 0.5 * dtime;

	BH_TYPE dvelx, dvely, dvelz;
	BH_TYPE velhx, velhy, velhz;

	particle* p = &(particles[part]);

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;
}

void jobBARNES_HUT(int argc, char *argv[]) {

	int n_tbb_threads = 8;

	task_scheduler_init init(task_scheduler_init::deferred);

	init.initialize(n_tbb_threads);

	//cout << "TBB initialized threads: " << n_tbb_threads << endl;

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	char* filename;

	PERFORM_timer* timer = new PERFORM_timer();
	particle* particles;

	initBARNES_HUT(argc,argv,particles,nbodies,timesteps,filename);

	load_data_set(particles, filename, nbodies, timesteps, dtime, eps, tol);

	//fprintf(stderr, "configuration: %d bodies, %d time steps running in TBB\n", nbodies, timesteps);

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	timer->start();

	for (int step = 0; step < timesteps; step++) {

		BH_TYPE diameter, centerx, centery, centerz;

		/**
		 * Compute the center and diameter of the all system
		 */
		compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);

		timer->append_and_restart(0);

		/**
		 * Create the the octree root
		 */

		/**
		 * based in the burtsher code ???
		 */
		int nnodes = nbodies * 2;
		if (nnodes < 1024 * 16)
			nnodes = 1024 * 16;

		//tree_cells_array nodes = tree_cells_array();

		cell* nodes = new cell[nnodes];

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)
			root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)
			root.child_indexes[k] = -1;

		nodes[0] = root;
		//nodes.push_back(root);

		const BH_TYPE radius = diameter * 0.5;
		build_tree_array(nodes, radius, particles, nbodies, tree_height);

		timer->append_and_restart(1);

		compute_center_of_mass(nodes, particles, 0, tree_height);

		timer->append_and_restart(2);

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different

		tbb::parallel_for(blocked_range<int> (0, nbodies), ComputeForceSet(nodes, particles, tree_height, dtime, epssq, dsq, step));

		timer->append_and_restart(3);

		tbb::parallel_for(blocked_range<int> (0, nbodies), UpdatePositionSet(particles, dtime));

		timer->append_and_restart(4);

	}

	timer->stop();

	printf("%.1f\t", timer->duration);

	//timer->print();

	//PrintParticles(particles, nbodies);

#ifdef _DEBUG
		jobBARNES_HUT_check(argc,argv,particles);
#endif

	init.terminate();

}

