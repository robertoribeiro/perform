#include "../../perform.h"
#include "fft.h"

int main(int argc, char *argv[]) {
//
	argv[1] = "0";//sch
	argv[2] = "2";//dev
	argv[3] = "2";//dice
	argv[4] = "/home/rr/work/perform/PERFORM/data/1024.jpg";

	PERFORM_timer *timer = new PERFORM_timer();

	timer->start();

	initPERFORM(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));

	double time = job_CONV(argc, argv);

#ifdef TEST_TIME
	timer->getMinResult(time, EXECUTIONS);
#endif

	shutdownPERFORM();

	timer->stop();

	LOG(0.1,cout << "\n\nRun-time terminated in " << timer->duration << "s." << endl;)

	return 0;
}
