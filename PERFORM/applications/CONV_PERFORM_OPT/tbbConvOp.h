/* System includes */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* TBB includes */
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/tick_count.h>

/* MKL includes */
#include <mkl_dfti.h>
#include <omp.h>

/* CUFFT includes */
#include </usr/local/cuda/include/cufft.h>

#include "fft.h"

/* Defines */
#define PI  3.141592654

using namespace tbb;

static void printMatrix(double *A, int rows, int cols);

// FFT on a signal


void operator_FREQMULT_Cplx16(blocked_range2d<int> r, Domain<MKL_Complex16> x,
		Domain<MKL_Complex16> y) {

	MKL_Complex16 tmp;
	for (int i = r.rows().begin(); i < r.rows().end(); i++) {
		for (int j = r.cols().begin(); j < r.cols().end(); j++) {
			tmp.real = (x.at(i, j).real * y.at(i, j).real) - (x.at(i, j).imag * y.at(i, j).imag);
			tmp.imag = (x.at(i, j).imag * y.at(i, j).real) + (x.at(i, j).real * y.at(i, j).imag);
			x.at(i, j).real = tmp.real;
			x.at(i, j).imag = tmp.imag;
		}
	}

}

class FREQMULT_Cplx16 {

	Domain<MKL_Complex16> x;
	Domain<MKL_Complex16> y;

public:

	FREQMULT_Cplx16(Domain<MKL_Complex16> xIn, Domain<MKL_Complex16> yIn) {
		x = xIn;
		y = yIn;
	}

	void operator ()(blocked_range2d<int> r) const {
		operator_FREQMULT_Cplx16(r, x, y);
	}

};

void k_CPU_FREQMULT_Cplx16(Task* t_) {

	Domain<MKL_Complex16> x;
	t_->getDomain(0, x);

	Domain<MKL_Complex16> y;
	t_->getDomain(1, y);

	int sub_sub_block;
	if (x.X.length() >= 32)
		sub_sub_block = 32;
	else
		sub_sub_block = x.X.length();

	parallel_for(
			blocked_range2d<int> (x.X.start, x.X.end, sub_sub_block, x.Y.start, x.Y.end,
					sub_sub_block), FREQMULT_Cplx16(x, y));

}

void k_CPU_FFT_MKL(Task* t_) {

	Task_FFT* t = (Task_FFT*) t_;

	int dir = t->direction;

	Domain<MKL_Complex16> input;
	t_->getDomain(0, input);
	int lines = input.X.length();
	int length = input.Y.length();

	/*
	 * SETUP
	 */

	// 1.Allocate a fresh descriptor for the problem
	DFTI_DESCRIPTOR_HANDLE mklDescHandle;
	DftiCreateDescriptor( &mklDescHandle, DFTI_DOUBLE, DFTI_COMPLEX, 1, length );

	// 2.Optionally adjust the descriptor configuration
	DftiSetValue(mklDescHandle, DFTI_PLACEMENT, DFTI_INPLACE);
	DftiSetValue(mklDescHandle, DFTI_INPUT_DISTANCE, length);
	DftiSetValue(mklDescHandle, DFTI_NUMBER_OF_TRANSFORMS, lines); // DFTI_NUMBER_OF_TRANSFORMS
	DftiSetValue(mklDescHandle, DFTI_BACKWARD_SCALE, 1.0 / (double) length); // DFTI_BACKWARD_SCALE

	// 3.Commit the descriptor
	DftiCommitDescriptor(mklDescHandle);

	if (t->direction == 1) {
		DftiComputeForward(mklDescHandle, (MKL_Complex16*) (input.device_data_buffer));
	} else {
		DftiComputeBackward(mklDescHandle, (MKL_Complex16*) (input.device_data_buffer));
	}

	// 5.Deallocate the descriptor
	DftiFreeDescriptor(&mklDescHandle);

}

void
operator_transpose_Cplx16(blocked_range2d<int> , Domain<MKL_Complex16> , Domain<MKL_Complex16> );

void
operator_transpose_Cplx16_(blocked_range2d<int> , Domain<MKL_Complex16> , Domain<MKL_Complex16> );

class CPU_TRANSPOSE_Cplx16 {

	Domain<MKL_Complex16> input;
	Domain<MKL_Complex16> output;

public:

	CPU_TRANSPOSE_Cplx16(Domain<MKL_Complex16> in, Domain<MKL_Complex16> out) {
		input = in;
		output = out;

	}

	void operator()(blocked_range2d<int> r) const {

		operator_transpose_Cplx16(r, input, output);

	}

};

class CPU_TRANSPOSE_Cplx16_ {

	Domain<MKL_Complex16> input;
	Domain<MKL_Complex16> output;

public:

	CPU_TRANSPOSE_Cplx16_(Domain<MKL_Complex16> in, Domain<MKL_Complex16> out) {
		input = in;
		output = out;

	}

	void operator()(blocked_range2d<int> r) const {

		operator_transpose_Cplx16_(r, input, output);

	}

};

void k_CPU_TRANSPOSE_Cplx16(Task* t_) {

	Domain<MKL_Complex16> input;
	t_->getDomain(0, input);

	Domain<MKL_Complex16> output;
	t_->getDomain(1, output);

	parallel_for(
			blocked_range2d<int> (input.X.start, input.X.end, 1, input.Y.start, input.Y.end,
					input.Y.length()), CPU_TRANSPOSE_Cplx16(input, output));

}

//void k_CPU_TRANSPOSE_Cplx16_(Task* t_) {
//
//	Domain<MKL_Complex16> input;
//	t_->getDomain(0, input);
//
//	Domain<MKL_Complex16> output;
//	t_->getDomain(1, output);
//
//	parallel_for(
//			blocked_range2d<int> (0, input.X.length(), 1, 0, input.Y.length(),
//					input.Y.length()), CPU_TRANSPOSE_Cplx16_(input, output));
//
//}

