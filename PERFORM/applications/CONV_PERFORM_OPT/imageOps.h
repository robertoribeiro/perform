/* MKL includes*/
#include <mkl_dfti.h>


int getImageSize(char*);
int readImage(double*, char*);
int writeImage(double*, char*, int);
int saveImage(double*, char*, int);
int writeImage_WLIBS(MKL_Complex16*, char*, int);
//int getNextPower( int ,int);
