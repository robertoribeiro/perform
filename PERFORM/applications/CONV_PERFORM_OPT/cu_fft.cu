#include "fft.h"
#include "user_defined_task.h"
#include <mkl.h>

/* CUFFT includes */
#include </usr/local/cuda/include/cufft.h>

__global__ void k_Normalize(double2 *x, int w, int h) {

	int i = blockIdx.x * blockDim.x + threadIdx.x;
	int j = blockIdx.y * blockDim.y + threadIdx.y;

	if (i < w && j < h) {
		double tmpx;
		double tmpy;

		tmpx = x[j* w + i].x * (double) (1.0 / w);
		tmpy = x[j* w + i ].y * (double) (1.0 / w);

		x[j* w + i].x = tmpx;
		x[j* w + i].y = tmpy;
	}
}

void k_GPU_FFT_CUFFT(Task* t_) {

	Task_FFT* t = (Task_FFT*) t_;
	Domain<MKL_Complex16> input;
	t_->getDomain(0, input);
	int lines = input.X.length();
	int length = input.Y.length();

	/*
	 * SETUP
	 */
	cufftHandle plan;
	cufftPlan1d(&plan, // Pointer to a cufftHandle object
			length, // The transform size (e.g., 256 for a 256 point FFT)
			CUFFT_Z2Z, // The transform data type (e.g., CUFFT_C2C for complex to complex)
			lines //  Number of transforms of size nx
	);

	/*
	 * COMPUTE INPUT IMAGE FFT
	 */
	if (t->direction == 1)
		cufftExecZ2Z(plan, (cufftDoubleComplex *) (input.device_data_buffer),
				(cufftDoubleComplex *) (input.device_data_buffer), CUFFT_FORWARD);
	else {

		cufftExecZ2Z(plan, (cufftDoubleComplex *) (input.device_data_buffer),
				(cufftDoubleComplex *) (input.device_data_buffer), CUFFT_INVERSE);

		dim3 transBlockSize = dim3(16, 16, 1);
		dim3 transGridSize = dim3( length / 16 + 1,lines / 16 + 1, 1);

		//cudaThreadSynchronize();

		k_Normalize<<<transGridSize, transBlockSize,0,t->stream>>>((double2 *) (input.device_data_buffer), length,lines);

		//cudaThreadSynchronize();

	}

}

__global__ void CUDA_TRANSPOSE_Cplx16(Domain<MKL_Complex16> x, Domain<MKL_Complex16> y) {

	int i = blockIdx.x * blockDim.x + threadIdx.x + x.X.start;
	int j = blockIdx.y * blockDim.y + threadIdx.y + x.Y.start;

	if (i < x.X.end && j < x.Y.end) {
		y.at(j, i) = x.at(i, j);
	}
}

void k_CUDA_TRANSPOSE_Cplx16(Task* t_) {

	Domain<MKL_Complex16> input;
	t_->getDomain(0, input);

	Domain<MKL_Complex16> output;
	t_->getDomain(1, output);

	int h = input.X.length();
	int w = input.Y.length();

	dim3 transBlockSize = dim3(16, 16, 1);
	dim3 transGridSize = dim3(h / 16 +1, w / 16 +1, 1);

	CUDA_TRANSPOSE_Cplx16<< <transGridSize, transBlockSize,0,t_->stream>> >(input, output);
}

__global__ void FREQMUL_Complex16(Domain<MKL_Complex16> x, Domain<MKL_Complex16> y) {

	int i = blockIdx.x * blockDim.x + threadIdx.x + x.X.start;
	int j = blockIdx.y * blockDim.y + threadIdx.y + x.Y.start;

	MKL_Complex16 tmpx;

	if (i < x.X.end && j < x.Y.end) {
		// do frequency multiplication: zReal = xReal*yReal - xImag*yImag;
		tmpx.real = (x.at(i, j).real * y.at(i, j).real) - (x.at(i, j).imag * y.at(i, j).imag);

		// do frequency multiplication: zImag = xImag*yReal + xReal*yImag;
		tmpx.imag = (x.at(i, j).imag * y.at(i, j).real) + (x.at(i, j).real * y.at(i, j).imag);

		x.at(i, j) = tmpx;
	}
}

void k_CUDA_FREQMULT_Cplx16(Task* t_) {

	Domain<MKL_Complex16> x;
	t_->getDomain(0, x);

	Domain<MKL_Complex16> y;
	t_->getDomain(1, y);

	int h = x.X.length();
	int w = x.Y.length();

	dim3 transBlockSize = dim3(16, 16, 1);
	dim3 transGridSize = dim3(h / 16+ 1, w / 16+1, 1);

	FREQMUL_Complex16<< <transGridSize, transBlockSize,0,t_->stream >> >(x,y);
}

