/*
 * dgemm.h
 *
 *  Created on: Apr 6, 2011
 *      Author: rr
 */

#ifndef FFT_H_
#define FFT_H_

#include "../../perform.h"

//#include "user_defined_task.h"



using namespace std;

void k_CUDA_FFT(Task* t_);

void k_CUDA_TRANSPOSE(Task* t_);

void k_CUDA_FREQMULT(Task* t_) ;

void k_CUDA_FREQMULT_Cplx16(Task* t_);

void k_CUDA_TRANSPOSE_Cplx16(Task* t_);

void k_GPU_FFT_CUFFT(Task* t_) ;


void butterfly(int dir,int offset,Domain<double> x, Domain<double> y)  ;

double job_CONV(int argc, char *argv[]);

#endif
