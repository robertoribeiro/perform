#include <stdio.h>

// Square Matrix Multiplication
void matrixMul(double  *a, double  *b, double  *c, int N){
    
    int tmp;

    for(int i = 0; i<N; i++){
        for(int j=0; j<N; j++){
            c[i*N+j]=0;
            for(int k=0; k<N; k++){
                c[i*N+j] += a[i*N+k] * b[k*N+j];
            }
        }
    }    
}

// Print a square matrix
void printMatrix(double  *x, int N) {

    if(N<33){
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                printf("%4.0f ", x[i * N + j]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

// Matrix transpose
void matrixTranspose(double  *x, int N) {

    double  temp;
    for (int i = 0; i < N; i++) {
        for (int j = i+1; j < N; j++) {
            temp = x[i * N + j];
            x[i * N + j] = x[j * N + i];
            x[j * N + i] = temp;
        }
    }
}

int checkResult(double *x, double *y, int N){

    N=N*N;
    for(int i=0; i<N; i++)
        if(x[i] != y[i]){
            printf("ERROR!!!!!!!!!!!!!!!!!!!!!\n");
            return 0;
        }

    return 1;

}
