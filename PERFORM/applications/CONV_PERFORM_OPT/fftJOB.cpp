#include "fft.h"
#include "user_defined_task.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <tbb/tick_count.h>
#include "tbbConvOp.h"
#include "matrix_ops.h"
#include "imageOps.h"

using namespace tbb;
using namespace std;

void k_CUDA_wrapper_uberkernel(Task * t, Task ** task_prms, int child_task_count) {
}

struct IMAGE {
	double *data;
};

char *inamex, *inamey;

static void printMatrix(double *A, int rows, int cols) {
	printf("\n\n");
	int ii, jj;
	for (ii = 0; ii < rows; ii++) {
		for (jj = 0; jj < cols; jj++)
			printf("%6.1lf ", A[ii * cols + jj]);
		//cout << A[ii * cols + jj] << " ";
		printf("\n");
	}
}

double FFT1, TRANS1, TRANS2, FFT2, TRANS3, TRANS4, FFT3, TRANS5, TRANS6, FFT4, TRANS7, TRANS8,
		FREQMULTI, FFT5, TRANS9, TRANS10, FFT6, TRANS11, TRANS12, TOTAL;

void operator_transpose_Cplx16(blocked_range2d<int> r, Domain<MKL_Complex16> input,
		Domain<MKL_Complex16> output) {
	for (int i = r.rows().begin(); i < r.rows().end(); i++) {
		for (int j = r.cols().begin(); j < r.cols().end(); j++) {
			//			printf("input.X.start:%d input.X.end:%d input.Y.start:%d input.Y.end:%d i:%d j:%d\n",
			//					input.X.start,input.X.end,input.Y.start,input.Y.end,i,j);
			//			printf("output.X.start:%d output.X.end:%d output.Y.start:%d output.Y.end %d\n\n",
			//					output.X.start,output.X.end,output.Y.start,output.Y.end);
			//output.at(j, i) = input.at(i, j);
			output.at(j, i) = input.at(i, j);
		}
	}

}

//void operator_transpose_Cplx16_(blocked_range2d<int> r, Domain<MKL_Complex16> input,
//		Domain<MKL_Complex16> output) {
//
//	MKL_Complex16* in = (MKL_Complex16*)input.device_data_buffer;
//	MKL_Complex16* out = (MKL_Complex16*)output.device_data_buffer;
//
//	for (int i = r.rows().begin(); i < r.rows().end(); i++) {
//		for (int j = r.cols().begin(); j < r.cols().end(); j++) {
//			//			printf("input.X.start:%d input.X.end:%d input.Y.start:%d input.Y.end:%d i:%d j:%d\n",
//			//					input.X.start,input.X.end,input.Y.start,input.Y.end,i,j);
//			//			printf("output.X.start:%d output.X.end:%d output.Y.start:%d output.Y.end %d\n\n",
//			//					output.X.start,output.X.end,output.Y.start,output.Y.end);
//			//output.at(j, i) = input.at(i, j);
//			out[j*output.Y.length() + i].real = in[j*input.Y.length() + i].real;
//		}
//	}
//
//}

void butterfly(int dir, int offset, Domain<double> x, Domain<double> y) {

	int i, j, k, n1, n2, N = x.Y.length(); //r.grainsize();
	double c, s, w, a, t1, t2;
	j = 0;
	n2 = N / 2;
	for (i = 1; i < N - 1; i++) {
		n1 = n2;
		while (j >= n1) {
			j = j - n1;
			n1 = n1 / 2;
		}
		j = j + n1;
		if (i < j) {
			t1 = x.at(offset, i);
			x.at(offset, i) = x.at(offset, j);
			x.at(offset, j) = t1;
			t1 = y.at(offset, i);
			y.at(offset, i) = y.at(offset, j);
			y.at(offset, j) = t1;
			//                t1 = x[i + offset];
			//                x[i + offset] = x[j + offset];
			//                x[j + offset] = t1;
			//                t1 = y[i + offset];
			//                y[i + offset] = y[j + offset];
			//                y[j + offset] = t1;
		}
	}

	/* Second Step: FFT */
	n1 = 0;
	N = 1;
	int stages = ceil(log2(x.Y.length()));
	int n = x.Y.length();
	// For each stage
	for (i = 0; i < stages; i++) {
		n1 = N; // tamanho de casa asa
		N = N + N; // tamanho de cada borboleta
		w = dir * (-6.283185307179586 / N);
		a = 0.0;
		for (j = 0; j < n1; j++) {
			// para cada asa
			c = cos(a);
			s = sin(a);
			a = a + w;
			for (k = j; k < n; k = k + N) {
				t1 = c * x.at(offset, k + n1) - s * y.at(offset, k + n1);
				t2 = s * x.at(offset, k + n1) + c * y.at(offset, k + n1);
				x.at(offset, k + n1) = x.at(offset, k) - t1;
				y.at(offset, k + n1) = y.at(offset, k) - t2;
				x.at(offset, k) = x.at(offset, k) + t1;
				y.at(offset, k) = y.at(offset, k) + t2;
			}
		}

	}

	if (dir < 0) {
		for (i = 0; i < n; i++) {
			x.at(offset, i) = x.at(offset, i) / n;
			y.at(offset, i) = y.at(offset, i) / n;
		}
	}

}

// padd image to the next power of 2
// return the new N
int getNextPower(int Ndata, int Nkernel) {
	// calc new size
	int newN;
	newN = Ndata + Nkernel - 1;
	int power = ceil(log2(Ndata));
	int newSize = pow(2, power + 1);
	return (newSize);
}

void init_MKL(int Nx, struct IMAGE & xReal, struct IMAGE & yReal, int Ny, int & paddedN) {

	struct IMAGE tmp;
	struct IMAGE tmp2;

	/* Read input images ********************************************************* */

	xReal.data = (double*) (PERFORM_alloc(sizeof(double) * Nx * Nx));
	yReal.data = (double*) (PERFORM_alloc(sizeof(double) * Ny * Ny));
	readImage(xReal.data, inamex);
	// Edge detection kernel
	yReal.data[0] = -1;
	yReal.data[1] = -1;
	yReal.data[2] = -1;
	yReal.data[3] = -1;
	yReal.data[4] = 8;
	yReal.data[5] = -1;
	yReal.data[6] = -1;
	yReal.data[7] = -1;
	yReal.data[8] = -1;

	/* Padd input images ******************************************************** */
	paddedN = getNextPower(Nx, Ny);

	int n = -1;
	tmp.data = (double*) (PERFORM_alloc(sizeof(double) * paddedN * paddedN));
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= Nx || n >= Nx)
				tmp.data[i * paddedN + j] = 0;

			else
				tmp.data[i * paddedN + j] = xReal.data[n * Nx + j];

		}
	}

	tmp2.data = xReal.data;
	xReal.data = tmp.data;
	PERFORM_free((void*) (tmp2.data));
	tmp.data = (double*) (PERFORM_alloc(sizeof(double) * paddedN * paddedN));
	n = -1;
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= Ny || n >= Ny)
				tmp.data[i * paddedN + j] = 0;

			else
				tmp.data[i * paddedN + j] = yReal.data[n * Ny + j];

		}
	}

	tmp2.data = yReal.data;
	yReal.data = tmp.data;
	PERFORM_free((void*) (tmp2.data));
	return;
}

// Stage 1 With Libs
void stage1_WLIBS(Domain<MKL_Complex16> *d_x, RESOURCE_ID & dev_p, Domain<MKL_Complex16> *d_tmp,
		bool& dice, float dice_value) {




	Task_FFT *t1 = new Task_FFT(REGULAR);
	t1->direction = 1;
	t1->associate_domain(d_x);
	t1->associate_kernel(CPU, &k_CPU_FFT_MKL);
	t1->associate_kernel(GPU, &k_GPU_FFT_CUFFT);
	t1->device_preference = dev_p;
	t1->diceable = dice;
	t1->dice_value = dice_value;
	performQ->addTasks(t1);
	wait_for_all_tasks();
	performDL->splice(d_x);

	d_x->permissions = R;
	d_tmp->permissions = W;


	Task_TRANSPOSE* t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_x);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE_Cplx16);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE_Cplx16);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value = dice_value;
	performQ->addTasks(t_transpose);
	wait_for_all_tasks();
	performDL->splice(d_x);
	performDL->splice(d_tmp);



	d_tmp->permissions = RW;

	t1 = new Task_FFT(REGULAR);
	t1->direction = 1;
	t1->associate_domain(d_tmp);
	t1->associate_kernel(CPU, &k_CPU_FFT_MKL);
	t1->associate_kernel(GPU, &k_GPU_FFT_CUFFT);
	t1->device_preference = dev_p;
	t1->diceable = dice;
	t1->dice_value = dice_value;
	performQ->addTasks(t1);
	wait_for_all_tasks();
	performDL->splice(d_tmp);

	d_tmp->permissions = R;
	d_x->permissions = W;



	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_domain(d_x);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE_Cplx16);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE_Cplx16);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value = dice_value;
	performQ->addTasks(t_transpose);
	wait_for_all_tasks();
	performDL->splice(d_tmp);
	performDL->splice(d_x);



}

// Stage 1 With Libs
void stage1_WLIBS_inverse(Domain<MKL_Complex16> *d_x, RESOURCE_ID & dev_p,
		Domain<MKL_Complex16> *d_tmp, bool& dice, float dice_value) {

	d_x->permissions = RW;

	Task_FFT *t1 = new Task_FFT(REGULAR);
	t1->direction = 0;
	t1->associate_domain(d_x);
	t1->associate_kernel(CPU, &k_CPU_FFT_MKL);
	t1->associate_kernel(GPU, &k_GPU_FFT_CUFFT);
	t1->device_preference = dev_p;
	t1->diceable = dice;
	t1->dice_value = dice_value;
	performQ->addTasks(t1);
	wait_for_all_tasks();
	performDL->splice(d_x);

	d_tmp->permissions = W;
	d_x->permissions = R;

	Task_TRANSPOSE* t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_x);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE_Cplx16);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE_Cplx16);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value = dice_value;
	performQ->addTasks(t_transpose);
	wait_for_all_tasks();
	performDL->splice(d_x);
	performDL->splice(d_tmp);

	d_tmp->permissions = RW;

	t1 = new Task_FFT(REGULAR);
	t1->direction = 0;
	t1->associate_domain(d_tmp);
	t1->associate_kernel(CPU, &k_CPU_FFT_MKL);
	t1->associate_kernel(GPU, &k_GPU_FFT_CUFFT);
	t1->device_preference = dev_p;
	t1->diceable = dice;
	t1->dice_value = dice_value;
	performQ->addTasks(t1);
	wait_for_all_tasks();
	performDL->splice(d_tmp);

	d_tmp->permissions = RW;
	d_x->permissions = RW;

	//performDL->print_registry();


//	for (int i = 0; i < d_x->X.length(); ++i) {
//		for (int j = 0; j < d_x->Y.length(); ++j) {
//			printf("%.1f ", ((MKL_Complex16*)d_tmp->phy_chunk->hook_pointer)[i*d_tmp->Y.length() + j].real);
//		}
//		printf("\n");
//	}
//	printf("\n");

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_domain(d_x);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE_Cplx16);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE_Cplx16);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value = dice_value;
	performQ->addTasks(t_transpose);
	wait_for_all_tasks();

	PERFORM_signal_taskSubmissionEnded();

	performDL->splice(d_tmp);
	performDL->splice(d_x);

//	for (int i = 0; i < d_x->X.length(); ++i) {
//		for (int j = 0; j < d_x->Y.length(); ++j) {
//			printf("%5.1f ", ((MKL_Complex16*)d_tmp->phy_chunk->hook_pointer)[i*d_tmp->Y.length() + j].real);
//		}
//		printf("\n");
//	}
//	printf("\n");

//	for (int i = 0; i < d_x->X.length(); ++i) {
//			for (int j = 0; j < d_x->Y.length(); ++j) {
//				printf("%.1f ", ((MKL_Complex16*)d_x->phy_chunk->hook_pointer)[i*d_x->Y.length() + j].real);
//			}
//			printf("\n");
//		}
//		printf("\n");


}

void stage3_WLIBS(Domain<MKL_Complex16>*d_x, Domain<MKL_Complex16>*d_y, RESOURCE_ID & dev_p,
		bool& dice, float dice_value) {

	d_x->permissions = RW;
	d_y->permissions = RW;

	Task_FREQMULT *t = new Task_FREQMULT(REGULAR);
	t->associate_domain(d_x);
	t->associate_domain(d_y);
	t->associate_kernel(CPU, &k_CPU_FREQMULT_Cplx16);
	t->associate_kernel(GPU, &k_CUDA_FREQMULT_Cplx16);
	t->device_preference = dev_p;
	t->diceable = dice;
	t->dice_value = dice_value;
	performQ->addTasks(t);
	wait_for_all_tasks();
	performDL->splice(d_x);
	performDL->splice(d_y);
}

void finish_WLIBS(int N, int paddedN, MKL_Complex16 *output) {

	MKL_Complex16 *tmp = new MKL_Complex16[N * N];

	N = getImageSize(inamex);
	int n = -1;
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= N || n >= N)
				continue;

			else
				tmp[n * N + j].real = output[i * paddedN + j].real;

		}
	}

	writeImage_WLIBS(tmp, "Convolution_TBB", N);

}

// This job is the description of the convolution operation of two images
double job_CONV(int argc, char *argv[]) {

	inamex = argv[4];
	inamey = argv[4];

	float divide_task_by = atoi(argv[3]) == 0 ? 0.0 : atoi(argv[3]);

	tbb::tick_count timeBefore, timeAfter;

	int Nx = getImageSize(inamex);
	int Ny = 3;
	struct IMAGE xReal, yReal;
	int paddedN = 0;
	init_MKL(Nx, xReal, yReal, Ny, paddedN);
	int N = paddedN;

	MKL_Complex16 *x, *y, *z, *tmp;

	x = new MKL_Complex16[N * N];
	y = new MKL_Complex16[N * N];
	z = new MKL_Complex16[N * N];
	tmp = new MKL_Complex16[N * N];
	for (int i = 0; i < N * N; i++) {
		x[i].real = xReal.data[i];
		x[i].imag = 0;
		y[i].real = yReal.data[i];
		y[i].imag = 0;
		z[i].real = z[i].imag = 0;
		tmp[i].real = tmp[i].imag = 0;
	}

	Domain<MKL_Complex16> *d_x = new Domain<MKL_Complex16> (2, RW, x, dim_space(0, N),
			dim_space(0, N));
	Domain<MKL_Complex16> *d_y = new Domain<MKL_Complex16> (2, RW, y, dim_space(0, N),
			dim_space(0, N));
	Domain<MKL_Complex16> *d_tmp = new Domain<MKL_Complex16> (2, RW, tmp, dim_space(0, N),
			dim_space(0, N));

	//RESOURCE_ID dev_p = GPU1;
	RESOURCE_ID dev_p = NONE;

	PERFORM_timer *timer = new PERFORM_timer();
	timer->start();

	bool dice = divide_task_by == 0.0 ? false : true;


	stage1_WLIBS(d_x, dev_p, d_tmp, dice, 1/divide_task_by);
	stage1_WLIBS(d_y, dev_p, d_tmp, dice, 1/divide_task_by);
	stage3_WLIBS(d_x, d_y, dev_p, dice, 1/divide_task_by);
	stage1_WLIBS_inverse(d_x, dev_p, d_tmp, dice, 1/divide_task_by);

	timer->stop();
	PERFORM_signal_JobFinished(timer->duration);

	finish_WLIBS(N, paddedN, (MKL_Complex16*) (d_x->phy_chunk->hook_pointer));

	return timer->duration;
}
