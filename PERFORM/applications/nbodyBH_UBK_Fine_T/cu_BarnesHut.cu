#include "BarnesHut_ubk.h"




__global__ void k_compute_force(Domain<cell> cells, Domain<particle> particles, Domain<particle> particles_new, int tree_height, BH_TYPE dtime, BH_TYPE epssq,
		BH_TYPE dsq, int step) {

	int p = blockIdx.x * blockDim.x + threadIdx.x + particles_new.X.start;

	if (p < particles_new.X.end && p >= particles_new.X.start) {

		BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
		BH_TYPE ax, ay, az;

		particle x = particles.at(p);


		ax = x.accx;
		ay = x.accy;
		az = x.accz;

		x.accx = 0.0;
		x.accy = 0.0;
		x.accz = 0.0;

		char stack[100];
		//register char* stack= new char[tree_height];
		int stack_top = 0;

		int current = 0;
		int child = 0;

		dsq *= 0.25;

		mass_position mass_pos_p = x.mass_pos;

		while (!(stack_top == 0 && child == 8)) {

			//printf("Stack pointer: %d\n",stack_top);

			if (child == 8) {
				current = cells.at(current).parent_cell_index;
				dsq *= 4;
				child = stack[--stack_top];
				child++;

			} else {

				mass_position mass_pos;
				if (cells.at(current).child_types[child] == PARTICLE)
					mass_pos = particles.at(cells.at(current).child_indexes[child]).mass_pos;
				else if (cells.at(current).child_types[child] == CELL)
					mass_pos = cells.at(cells.at(current).child_indexes[child]).mass_pos;
				else {
					child++;
					continue;
				}

				drx = mass_pos.x - mass_pos_p.x;
				dry = mass_pos.y - mass_pos_p.y;
				drz = mass_pos.z - mass_pos_p.z;

				drsq = drx * drx + dry * dry + drz * drz;
				//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
				if (drsq < dsq) {
					if (cells.at(current).child_types[child] == CELL) {
						//if (p==0) printf("%lf\n",dsq);
						dsq *= 0.25;
						current = cells.at(current).child_indexes[child];
						stack[stack_top++] = child;
						child = 0;
					} else if (cells.at(current).child_types[child] == PARTICLE) {
						if (mass_pos.id != mass_pos_p.id) {
							drsq += epssq;
							//idr = rsqrtf(drsq);
							idr = 1/sqrtf(drsq);
							nphi = mass_pos.mass * idr;
							scale = nphi * idr * idr;
							x.accx += drx * scale;
							x.accy += dry * scale;
							x.accz += drz * scale;

							//if (p==0) printf("%.15f\n",scale);


						}
						child++;
					} else if (cells.at(current).child_types[child] == NILL) {
						child++;
					}
				} else {
					drsq += epssq;
					//idr = rsqrtf(drsq);
					idr = 1/sqrtf(drsq);
					nphi = mass_pos.mass * idr;
					scale = nphi * idr * idr;
					x.accx += drx * scale;
					x.accy += dry * scale;
					x.accz += drz * scale;
					child++;

					//if (p==0) printf("%.15f\n",scale);

				}

			}
		}

		BH_TYPE dthf = 0.5 * dtime;

		if (step > 0) {
			x.velx += (x.accx - ax) * dthf;
			x.vely += (x.accy - ay) * dthf;
			x.velz += (x.accz - az) * dthf;
		}
		particles_new.at(p) = x;

	}

}

__global__ void k_update_vel_positions(Domain<particle> particles, BH_TYPE dtime, int step) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x + particles.X.start;

	if (pt < particles.X.end && pt >= particles.X.start) {

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;
		BH_TYPE dthf = 0.5 * dtime;
		particle* p = &(particles.at(pt));

		if (pt == 0){
			printf("ACCt_x: %.15f\n",p->t_accx);
			printf("ACCx: %.15f\n",p->accx);
		}

		if (step > 0) {
				p->velx += (p->accx - p->t_accx) * dthf;
				p->vely += (p->accy - p->t_accy) * dthf;
				p->velz += (p->accz - p->t_accz) * dthf;
			}

		dvelx = p->accx * dthf;
		dvely = p->accy * dthf;
		dvelz = p->accz * dthf;

		velhx = p->velx + dvelx;
		velhy = p->vely + dvely;
		velhz = p->velz + dvelz;

		p->mass_pos.x += velhx * dtime;
		p->mass_pos.y += velhy * dtime;
		p->mass_pos.z += velhz * dtime;

		p->velx = velhx + dvelx;
		p->vely = velhy + dvely;
		p->velz = velhz + dvelz;

	}

}

__global__ void k_update_positions(Domain<particle> particles, BH_TYPE dtime) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x + particles.X.start;

	if (pt < particles.X.end && pt >= particles.X.start) {

		BH_TYPE dthf = 0.5 * dtime;

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;

		particle* p = &(particles.at(pt));

		dvelx = p->accx * dthf;
		dvely = p->accy * dthf;
		dvelz = p->accz * dthf;

		velhx = p->velx + dvelx;
		velhy = p->vely + dvely;
		velhz = p->velz + dvelz;

		p->mass_pos.x += velhx * dtime;
		p->mass_pos.y += velhy * dtime;
		p->mass_pos.z += velhz * dtime;

		p->velx = velhx + dvelx;
		p->vely = velhy + dvely;
		p->velz = velhz + dvelz;
	}

}


void k_CUDA_compute_force(Task* t_) {

//	size_t free, total;
//	cuMemGetInfo(&free, &total);
//	printf("cuda forces free mem start %d total %d\n", free / 1024 / 1024, total / 1024 / 1024);

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(0, particles);

	Domain<cell> nodes;
	t->getDomain(1, nodes);

	Domain<particle> particles_new;
	t->getDomain(2, particles_new);

	int nbodies = particles_new.X.length();

	dim3 blockDIM = dim3(1024, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	//printf("blockDIM %d gridDIM %d",blockDIM.x,gridDIM.x );

	k_compute_force<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(nodes, particles, particles_new, t->tree_height, t->dtime, t->epssq, t->dsq, t->step);
	checkCUDAError();

	//cudaStreamSynchronize((cudaStream_t) (t->stream));

	k_update_positions<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(particles_new, t->dtime);
	checkCUDAError();

}

void k_CUDA_update_vel_positions(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(2, particles);

	int nbodies = particles.X.length();

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	k_update_vel_positions<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(particles, t->dtime,t->step);
	checkCUDAError();

	cudaDeviceSynchronize();

	//PrintParticles((particle*)particles.phy_chunk->original_host_pointer, nbodies-1023);


}


void k_CUDA_update_positions(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(2, particles);

	int nbodies = particles.X.length();

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	k_update_positions<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(particles, t->dtime);
	checkCUDAError();

	cudaDeviceSynchronize();

	//PrintParticles((particle*)particles.phy_chunk->original_host_pointer, nbodies-1023);


}
