#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut_ubk.h"

using namespace std;
using namespace tbb;

void compute_force(Domain<cell> cells, Domain<particle> particles, Domain<particle> particles_new,
		int p, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {

	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
	BH_TYPE ax, ay, az;

	particle x = particles.at(p);

	ax = x.accx;
	ay = x.accy;
	az = x.accz;

	x.accx = 0.0;
	x.accy = 0.0;
	x.accz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;

	dsq *= 0.25;

	mass_position mass_pos_p = x.mass_pos;

	while (!(stack_top == 0 && child == 8)) {

		if (child == 8) {
			//current = cells[current].parent_cell_index;
			current = cells.at(current).parent_cell_index;
			dsq *= 4;
			child = stack[--stack_top];
			child++;

		} else {

			mass_position mass_pos;
			if (cells.at(current).child_types[child] == PARTICLE)
				mass_pos = particles.at(cells.at(current).child_indexes[child]).mass_pos;
			else if (cells.at(current).child_types[child] == CELL)
				mass_pos = cells.at(cells.at(current).child_indexes[child]).mass_pos;
			else {
				child++;
				continue;
			}

			drx = mass_pos.x - mass_pos_p.x;
			dry = mass_pos.y - mass_pos_p.y;
			drz = mass_pos.z - mass_pos_p.z;

			drsq = drx * drx + dry * dry + drz * drz;
			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
			if (drsq < dsq) {
				if (cells.at(current).child_types[child] == CELL) {
					dsq *= 0.25;
					current = cells.at(current).child_indexes[child];
					stack[stack_top++] = child;
					child = 0;
				} else if (cells.at(current).child_types[child] == PARTICLE) {
					if (mass_pos.id != mass_pos_p.id) {
						drsq += epssq;
						idr = 1 / sqrt(drsq);
						nphi = mass_pos.mass * idr;
						scale = nphi * idr * idr;
						x.accx += drx * scale;
						x.accy += dry * scale;
						x.accz += drz * scale;

					}
					child++;
				} else if (cells.at(current).child_types[child] == NILL) {
					child++;
				}
			} else {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = mass_pos.mass * idr;
				scale = nphi * idr * idr;
				x.accx += drx * scale;
				x.accy += dry * scale;
				x.accz += drz * scale;
				child++;
			}

		}
	}

	BH_TYPE dthf = 0.5 * dtime;

	if (step > 0) {
		x.velx += (x.accx - ax) * dthf;
		x.vely += (x.accy - ay) * dthf;
		x.velz += (x.accz - az) * dthf;
	}

	particles_new.at(p) = x;

	/*PrintBH_TYPE(particles[particle].velx);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].vely);
	 printf("   ");
	 PrintBH_TYPE(par ticles[particle].velz);
	 printf("\n");*/

}

void k_TBB_compute_force(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(0, particles);

	Domain<cell> nodes;
	t->getDomain(1, nodes);

	Domain<particle> particles_new;
	t->getDomain(2, particles_new);

	tbb::parallel_for(
			blocked_range<int> (particles_new.X.start, particles_new.X.end),
			ComputeForceSet(nodes, particles, particles_new, t->tree_height, t->dtime, t->epssq,
					t->dsq, t->step));

	tbb::parallel_for(blocked_range<int> (particles_new.X.start, particles_new.X.end),
			UpdatePositionSet(particles_new, t->dtime));

}


void update_positions(Domain<particle> particles, int part, BH_TYPE dtime) {

	BH_TYPE dthf = 0.5 * dtime;

	BH_TYPE dvelx, dvely, dvelz;
	BH_TYPE velhx, velhy, velhz;

	particle* p = &(particles.at(part));

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;
}

double jobBARNES_HUT(int argc, char *argv[]) {

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	char* filename;

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();

	particle* particles;

	initBARNES_HUT(argc, argv, particles, nbodies, timesteps, filename);

	load_data_set2(particles, filename, nbodies, timesteps, dtime, eps, tol);

	//PrintParticles(particles, nbodies-1023);


	int nnodes = nbodies * 2;
	if (nnodes < 1024 * 16)
		nnodes = 1024 * 20;

	cell* nodes;

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	Domain<struct s_particle> *domain_particles = new Domain<struct s_particle> (1, R, particles,
			dim_space(0, nbodies));

	particle * particles_new = new particle[nbodies];

	memcpy(particles_new, particles, nbodies * sizeof(particle));

	Domain<struct s_particle> *domain_new_particles = new Domain<struct s_particle> (1, RW,
			particles_new, dim_space(0, nbodies));

	float dice_v;

	dice_v = atoi(argv[3]) == 0 ? 0 : 1.0 / atoi(argv[3]);

	for (int step = 0; step < timesteps; step++) {

		particle * particles_buffer =
				(particle*) domain_particles->phy_chunk->original_host_pointer;
		BH_TYPE diameter, centerx, centery, centerz;

		compute_bounding_box(particles_buffer, nbodies, diameter, centerx, centery, centerz);

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)
			root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)
			root.child_indexes[k] = -1;

		nodes = new cell[nnodes];

		nodes[0] = root;

		const BH_TYPE radius = diameter * 0.5;

		cell_count = build_tree_array(nodes, radius, particles_buffer, nbodies, tree_height);

		compute_center_of_mass(nodes, particles_buffer, 0, tree_height);

		Domain<struct cell_s> *domain_nodes = new Domain<struct cell_s> (1, R, nodes,
				dim_space(0, nnodes));

		printf("UBK: Allocated %d for tree, elem: %d \n",(int)(domain_nodes->X.length() *sizeof(struct cell_s))/1024/1024,sizeof(struct cell_s));
		printf("UBK: Allocated %d for particles,elem: %d \n",(int)(domain_particles->X.length() *sizeof(struct s_particle))/1024/1024,sizeof(struct s_particle));
		printf("UBK: Allocated %d for particles_new\n",(int)(domain_particles->X.length() *sizeof(struct s_particle))/1024/1024);



		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different

		if (step == 0)
			PERFORM_signal_JobStarted();

		timer->start();

		Task_ForcesCalc* t;

		 t = new Task_ForcesCalc(IRREGULAR);
		 t->device_preference= GPU0;


		t->associate_domain(domain_particles);
		t->associate_domain(domain_nodes);
		t->associate_domain(domain_new_particles);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		t->parent_host_task = (void*) t;
		t->diceable = dice_v == 0 ? false : true;
		t->dice_value = PM_Metric_normalized(dice_v);

		t->associate_kernel(CPU, &k_TBB_compute_force);
		t->associate_kernel(GPU, &k_CUDA_compute_force);


		performQ->addTasks(t);

		wait_for_all_tasks();

		if (step + 1 == timesteps)
			PERFORM_signal_taskSubmissionEnded();

		performDL->splice(domain_new_particles);

	//	PrintParticles((particle*)domain_new_particles->phy_chunk->original_host_pointer, nbodies-1023);


		timer->stop();

		if (step + 1 == timesteps)
			PERFORM_signal_JobFinished(timer->duration);

		performDL->delete_domain(domain_nodes);

		Domain<struct s_particle> *tmp;
		tmp = domain_particles;
		domain_particles = domain_new_particles;
		domain_new_particles = tmp;

		domain_particles->intention = R;
		domain_new_particles->intention = RW;
	}

	printf("DURATION: %s\n",timer->print());

	//PrintParticles((particle*)domain_new_particles->phy_chunk->original_host_pointer, nbodies-1023);


//#ifdef _DEBUG
	jobBARNES_HUT_check(argc,argv,(particle*)domain_particles->phy_chunk->original_host_pointer);
//#endif

	return timer->duration;

}



//void jobBARNES_HUT(int argc, char *argv[]) {
//
//	int n_tbb_threads = 8;
//
//	task_scheduler_init init(task_scheduler_init::deferred);
//
//	init.initialize(n_tbb_threads);
//
//	//	cout << "TBB initialized threads: " << n_tbb_threads << endl;
//
//	BH_TYPE dtime; // length of one time step
//	BH_TYPE eps; // potential softening parameter
//	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error
//
//	int nbodies;
//	int timesteps;
//	int tree_height = 0;
//
//	PERFORM_timer* timer = new PERFORM_timer();
//
//	int leaf_count, cell_count;
//
//	char * filename;
//
//	//	if (argc > 1) {
//	//		switch (*argv[1]) {
//	//		case 'a':
//	//			filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inA";
//	//			break;
//	//		case 'b':harc
//	//			filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inB";
//	//			break;
//	//		case 'c':
//	filename = "/home/jbarbosa/workspace_perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inD";
//	//			break;
//	//		default:
//	//			exit(0);
//	//			break;
//	//		}
//	//	} else {
//	//		filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inA";
//	//	}
//
//	//nbodies = atoi(argv[2]);
//
//	//nbodies = 9984;
//	nbodies = 1024*15;
//	particle* particles = new particle[nbodies];
//
//	load_data_set(particles, filename, nbodies, timesteps, dtime, eps, tol);
//
//	//timesteps = atoi(argv[3]);
//
//	//fprintf(stderr, "configuration: %d bodies, %d time steps running in PERFORM\n", nbodies, timesteps);
//
//	/**
//	 * based in the burtsher code ???
//	 */
//	int nnodes = nbodies * 2;
//	if (nnodes < 1024 * 16)
//		nnodes = 1024 * 16;
//
//	cell* nodes = new cell[nnodes];
//
//	BH_TYPE itolsq = 1.0 / (tol * tol);
//	BH_TYPE epssq = eps * eps;
//
//	data_chunk* dc_particles = new data_chunk((void*) particles, nbodies * sizeof(struct s_particle), sizeof(struct s_particle), 1);
//
//	uint *a = new uint(1);
//	a[0] = nbodies;
//	Domain *domain_particles = new Domain(1, a, dc_particles, W);
//
//	data_chunk* dc_nodes = new data_chunk((void*) nodes, nnodes * sizeof(struct cell_s), sizeof(struct cell_s), 1);
//
//	a = new uint(1);
//	a[0] = nnodes;
//	Domain *domain_nodes = new Domain(1, a, dc_nodes, R);
//
//	timer->start();
//
//	timesteps = 1;
//	for (int step = 0; step < timesteps; step++) {
//
//		printf("timstep: %d\n",step);
//
//		BH_TYPE diameter, centerx, centery, centerz;
//
//		compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);
//
//		cell root;
//		root.mass_pos.x = centerx;
//		root.mass_pos.y = centery;
//		root.mass_pos.z = centerz;
//
//		for (int k = 0; k < 8; k++)
//			root.child_types[k] = NILL;
//
//		for (int k = 0; k < 8; k++)
//			root.child_indexes[k] = -1;
//
//		nodes[0] = root;
//
//		const BH_TYPE radius = diameter * 0.5;
//		int cell_count = build_tree_array(nodes, radius, particles, nbodies, tree_height);
//
//		compute_center_of_mass(nodes, particles, 0, tree_height);
//
//		timer->append_and_restart(0);
//
//		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different
//
//		Task_ForcesCalc* t = new Task_ForcesCalc(IRREGULAR);
//
//		t->associate_domain(domain_particles);
//		t->associate_domain(domain_nodes);
//
//		t->tree_height = tree_height;
//		t->dtime = dtime;
//		t->epssq = epssq;
//		t->dsq = dsq;
//		t->step = step;
//		t->start = 0;
//		t->end = nbodies - 1;
//		t->device_preference = GPU_0;
//		t->parent_host_task = (void*) t;
//		t->cell_count = cell_count;
//
//		t->associate_kernel(CPU, &k_TBB_compute_force);
//		t->associate_kernel(GPU, &k_CUDA_compute_force);
//
//		mainQ->addTasks(t);
//		t->wait_completion();
//
//		data_library->splice(domain_particles); /* required??? */
//
//		timer->append_and_restart(1);
//
//		Task_UpdatePos* t2 = new Task_UpdatePos(REGULAR);
//
//		t2->associate_domain(domain_particles);
//
//		t2->dtime = dtime;
//		t2->start = 0;
//		t2->end = nbodies - 1;
//		t2->device_preference = GPU_0;
//		t2->parent_host_task = (void*) t2;
//
//		t2->associate_kernel(CPU, &k_TBB_update_positions);
//		t2->associate_kernel(GPU, &k_CUDA_update_positions);
//
//		mainQ->addTasks(t2);
//		t2->wait_completion();
//
//		data_library->splice(domain_particles);
//
//		timer->append_and_restart(2);
//
//	}
//
//	timer->stop();
////	printf("SIZE %d\n",sizeof(Task_ForcesCalc));
////
////	printf("size of particles %d\n", sizeof(particle) );
////	printf("size of nodes %d\n\n", sizeof(struct cell_s));
//	//timer->print();
//
//	printf("%.1f\t", timer->duration);
//	printf("\n");
//	//printf("using %d\n", sizeof(BH_TYPE));
//	for (int i = nbodies -10	; i < nbodies; i++) { // print result
//		printf("p %d: ", i);
//		PrintDouble(particles[i].mass_pos.x);
//		printf("   ");
//		PrintDouble(particles[i].mass_pos.y);
//		printf("   ");
//		PrintDouble(particles[i].mass_pos.z);
//		printf("\n");
//	}
//
//	//	timer->print();
//
//	init.terminate();
//
//}

