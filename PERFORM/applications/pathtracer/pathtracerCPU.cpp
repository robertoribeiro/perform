/*
 * pathtracer.cpp
 *
 *  Created on: Mar 2, 2012
 *      Author: rr
 */

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "display/film.h"
#include "display/displayfunc.h"
#include "path.h"
#include "randomgen.h"
#include "geometry.h"
#include "geometry/bvhaccel.h"
#include "camera.h"
#include "config.h"
#include "perform.h"

#include "utils.h"
#include "user_defined_task.h"

#include <omp.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <boost/thread/mutex.hpp>

#include "pathtracer.hpp"

using namespace CPU_FUNTIONS;

namespace CPU_FUNTIONS {

bool TriangleIntersect(Triangle* tri, const Ray &ray, const Point *verts, RayHit *rayHit,const float minT, float maxT) {

	const Point &p0 = verts[tri->v[0]];
	const Point &p1 = verts[tri->v[1]];
	const Point &p2 = verts[tri->v[2]];

	const Vector e1 = p1 - p0;
	const Vector e2 = p2 - p0;
	const Vector s1 = Cross(ray.d, e2);

	const float divisor = Dot(s1, e1);
	if (divisor == 0.f)
		return false;

	const float invDivisor = 1.f / divisor;

	// Compute first barycentric coordinate
	const Vector d = ray.o - p0;
	const float b1 = Dot(d, s1) * invDivisor;
	if (b1 < 0.f)
		return false;

	// Compute second barycentric coordinate
	const Vector s2 = Cross(d, e1);
	const float b2 = Dot(ray.d, s2) * invDivisor;
	if (b2 < 0.f)
		return false;

	const float b0 = 1.f - b1 - b2;
	if (b0 < 0.f)
		return false;

	// Compute _t_ to intersection point
	const float t = Dot(e2, s2) * invDivisor;
	//if (t < ray.mint || t > ray.maxt)
		if (t < minT || t >maxT)
		return false;

	rayHit->t = t;
	rayHit->b1 = b1;
	rayHit->b2 = b2;

	return true;
}

RGB Sample_L(uint triIndex, const Geometry *objs, const Point &p, const Normal &N, const float u0,
		const float u1, float *pdf, Ray *shadowRay) {

	const Triangle &tri = objs->triangles[objs->meshLightOffset + triIndex];

	const float area = objs->lights[triIndex].area;

	Point samplePoint;
	float b0, b1, b2;

	tri.Sample(objs->vertices, u0, u1, &samplePoint, &b0, &b1, &b2);
	Normal sampleN = objs->vertNormals[tri.v[0]]; // Light sources are supposed to be flat

	Vector wi = samplePoint - p;
	const float distanceSquared = wi.LengthSquared();
	const float distance = sqrtf(distanceSquared);
	wi /= distance;

	const float SampleNdotMinusWi = Dot(sampleN, -wi);
	const float NdotMinusWi = Dot(N, wi);
	if ((SampleNdotMinusWi <= 0.f) || (NdotMinusWi <= 0.f)) {
		*pdf = 0.f;
		return RGB(0.f, 0.f, 0.f);
	}

	*shadowRay = Ray(p, wi, RAY_EPSILON, distance - RAY_EPSILON);
	*pdf = distanceSquared / (SampleNdotMinusWi * NdotMinusWi * area);

	// Return interpolated color
	return tri.InterpolateColor(objs->vertColors, b0, b1, b2);
}

bool BBox_IntersectP(BBox bbox, const Ray &ray, float *hitt0, float *hitt1) {

	float minT = RAY_EPSILON;
	float maxT = INFINITY;


	float t0 = minT, t1 =maxT;
	for (int i = 0; i < 3; ++i) {
		// Update interval for _i_th bounding box slab
		float invRayDir = 1.f / ray.d[i];
		float tNear = (bbox.pMin[i] - ray.o[i]) * invRayDir;
		float tFar = (bbox.pMax[i] - ray.o[i]) * invRayDir;
		// Update parametric interval from slab intersection $t$s
		if (tNear > tFar)
			swap(&tNear, &tFar);
		t0 = tNear > t0 ? tNear : t0;
		t1 = tFar < t1 ? tFar : t1;
		if (t0 > t1)
			return false;
	}
	if (hitt0)
		*hitt0 = t0;
	if (hitt1)
		*hitt1 = t1;
	return true;
}

bool Intersect(const Ray &ray, RayHit& rayHit, BVHAccelArrayNode* bvhTree, Triangle *triangles,
		Point *vertices) {

	rayHit.t = INFINITY;
	rayHit.index = 0xffffffffu;
	unsigned int currentNode = 0; // Root Node
	unsigned int stopNode = bvhTree[0].skipIndex; // Non-existent
	bool hit = false;
	RayHit triangleHit;

	float minT = ray.mint;
	float maxT = ray.maxt;

	while (currentNode < stopNode) {
		if (BBox_IntersectP(bvhTree[currentNode].bbox, ray, NULL, NULL)) {
			if (bvhTree[currentNode].primitive != 0xffffffffu) {
				//float tt, b1, b2;
				Triangle t = triangles[bvhTree[currentNode].primitive];
				if (TriangleIntersect(&t, ray, vertices, &triangleHit,minT, maxT)) {
					hit = true; // Continue testing for closer intersections
					if (triangleHit.t < rayHit.t) {
						rayHit.t = triangleHit.t;
						rayHit.b1 = triangleHit.b1;
						rayHit.b2 = triangleHit.b2;
						rayHit.index = bvhTree[currentNode].primitive;
					}
				}
			}

			currentNode++;
		} else
			currentNode = bvhTree[currentNode].skipIndex;
	}

	return hit;
}

bool Shade(Path* path, Geometry *geometry, BVHAccelArrayNode *bvh, const RayHit& rayHit,uint& ray_count) {

	uint tracedShadowRayCount;

	if (rayHit.index == 0xffffffffu) {
		return false;
	}

	// Something was hit
	unsigned int currentTriangleIndex = rayHit.index;
	RGB triInterpCol = geometry->triangles[currentTriangleIndex].InterpolateColor(
			geometry->vertColors, rayHit.b1, rayHit.b2);
	Normal shadeN = geometry->triangles[currentTriangleIndex].InterpolateNormal(
			geometry->vertNormals, rayHit.b1, rayHit.b2);

	// Calculate next step
	path->depth++;

	// Check if I have to stop
	if (path->depth >= CONFIG.MAX_PATH_DEPTH) {
		// Too depth, terminate the path
		return false;
	} else if (path->depth > 0.6*CONFIG.MAX_PATH_DEPTH) {

		// Russian Rulette, maximize cos
		const float p = min(1.f, triInterpCol.filter() * AbsDot(shadeN, path->pathRay.d));

		if (p > getFloatRNG(path->seed))
			path->throughput /= p;
		else {
			// Terminate the path
			return false;
		}
	}

	//--------------------------------------------------------------------------
	// Build the shadow ray
	//--------------------------------------------------------------------------

	// Check if it is a light source
	float RdotShadeN = Dot(path->pathRay.d, shadeN);
	if (geometry->IsLight(currentTriangleIndex)) {
		// Check if we are on the right side of the light source
		if ((path->depth == 1) && (RdotShadeN < 0.f))
			path->radiance += triInterpCol * path->throughput;

		// Terminate the path
		return false;
	}

	if (RdotShadeN > 0.f) {
		// Flip shade  normal
		shadeN = -shadeN;
	} else
		RdotShadeN = -RdotShadeN;

	path->throughput *= RdotShadeN * triInterpCol;

	// Trace shadow rays
	const Point hitPoint = path->pathRay.o + path->pathRay.d * rayHit.t; // = path->pathRay(rayHit.t);


	// path->pathRay.o + path->pathRay.d * t;

	tracedShadowRayCount = 0;
	const float lightStrategyPdf = static_cast<float> (SHADOWRAY)
			/ static_cast<float> (geometry->nLights);

	float lightPdf[SHADOWRAY];
	RGB lightColor[SHADOWRAY];
	Ray shadowRay[SHADOWRAY];

	for (unsigned int i = 0; i < SHADOWRAY; ++i) {
		// Select the light to sample
		const unsigned int currentLightIndex = geometry->SampleLights(getFloatRNG(path->seed));
		//	const TriangleLight &light = scene->lights[currentLightIndex];

		// Select a point on the surface
		lightColor[tracedShadowRayCount] = Sample_L(currentLightIndex, geometry, hitPoint, shadeN,
				getFloatRNG(path->seed), getFloatRNG(path->seed),
				&lightPdf[tracedShadowRayCount], &shadowRay[tracedShadowRayCount]);

		// Scale light pdf for ONE_UNIFORM strategy
		lightPdf[tracedShadowRayCount] *= lightStrategyPdf;

//		int x = (int) path->screenX;
//		int y = (int) path->screenY;
//		if (x == 240 && y == 240)
//			printf("%u\n",path->seed);



		// Using 0.1 instead of 0.0 to cut down fireflies
		if (lightPdf[tracedShadowRayCount] > 0.1f)
			tracedShadowRayCount++;
	}

	//RayHit* rh = new RayHit[tracedShadowRayCount];
	RayHit rh[SHADOWRAY];

	for (unsigned int i = 0; i < tracedShadowRayCount; ++i){
		__sync_fetch_and_add(&ray_count, 1);
		Intersect(shadowRay[i], rh[i], bvh, geometry->triangles, geometry->vertices);
	}

	if ((tracedShadowRayCount > 0)) {
		for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
			const RayHit *shadowRayHit = &rh[i];
			if (shadowRayHit->index == 0xffffffffu) {
				// Nothing was hit, light is visible
				path->radiance += path->throughput * lightColor[i] / lightPdf[i];
			}
		}
	}
	//delete rh;

	//--------------------------------------------------------------------------
	// Build the next vertex path ray
	//--------------------------------------------------------------------------

	// Calculate exit direction

	float r1 = 2.f * M_PI * getFloatRNG(path->seed);
	float r2 = getFloatRNG(path->seed);
	float r2s = sqrt(r2);
	const Vector w(shadeN);

	Vector u;
	if (fabsf(shadeN.x) > .1f) {
		const Vector a(0.f, 1.f, 0.f);
		u = Cross(a, w);
	} else {
		const Vector a(1.f, 0.f, 0.f);
		u = Cross(a, w);
	}
	u = Normalize(u);

	Vector v = Cross(w, u);

	Vector newDir = u * (cosf(r1) * r2s) + v * (sinf(r1) * r2s) + w * sqrtf(1.f - r2);
	newDir = Normalize(newDir);

	path->pathRay.o = hitPoint;
	path->pathRay.d = newDir;

	return true;
}


//void DistrurbeSample(Path *p) {
//
//	p->screenX += getFloatRNG(p->seed) / p->strat;
//	p->screenY += getFloatRNG(p->seed) / p->strat;
//
//}

} // namespace CPU


void initCONFIG(char *argv[]){
	CONFIG.HEIGHT = atoi(argv[4]);
	CONFIG.WIDTH = atoi(argv[5]);
	CONFIG.SPP = atoi(argv[6]);
	CONFIG.MAX_PATH_DEPTH = atoi(argv[7]);
	CONFIG.SCENEID = atoi(argv[8]);
	CONFIG.STRATA = sqrt(atoi(argv[6]));
}


void UpdateScreenBuffer(Film* film, uint width, uint height, RGB *pixelsRadiance, float *pixels) {

	const float weight = CONFIG.SPP;

	unsigned int count = width * height;
	for (unsigned int i = 0, j = 0; i < count; ++i) {

			if (weight == 0.f)
			j += 3;
		else {
			const float invWeight = 1.f / weight;

//			if (i >= 512*256 - 512 && (i < 512*256)
//					&& pixelsRadiance[i].b == 0.f && pixelsRadiance[i].g == 0.f && pixelsRadiance[i].r == 0.f)
//				printf("%d\n",i);


			pixels[j++] = film->Radiance2PixelFloat(pixelsRadiance[i].r * invWeight);
			pixels[j++] = film->Radiance2PixelFloat(pixelsRadiance[i].g * invWeight);
			pixels[j++] = film->Radiance2PixelFloat(pixelsRadiance[i].b * invWeight);
		}
	}
}

void ParseSceneFile(PerspectiveCamera*& camera, Geometry*& geometry) {

	cerr << "Reading scene: " << getScenePath(CONFIG.SCENEID) << endl;

	ifstream file;
	file.exceptions(ifstream::eofbit | ifstream::failbit | ifstream::badbit);
	file.open(getScenePath(CONFIG.SCENEID).c_str(), ios::in);

	//--------------------------------------------------------------------------
	// Read light position and radius
	//--------------------------------------------------------------------------

	RGB lightGain;
	file >> lightGain.r;
	file >> lightGain.g;
	file >> lightGain.b;

	cerr << "Light gain: (" << lightGain.r << ", " << lightGain.g << ", " << lightGain.b << ")"
			<< endl;

	//--------------------------------------------------------------------------
	// Read camera position and target
	//--------------------------------------------------------------------------

	Point o;
	file >> o.x;
	file >> o.y;
	file >> o.z;

	Point t;
	file >> t.x;
	file >> t.y;
	file >> t.z;





	//t.y = t.y - 4;
	//t.z = t.z + 2;

	// Translate backwards 6x
	//	Vector t2 = -6 * Normalize(t - o);
	//	o += t2;
	//	t += t2;

	cerr << "Camera postion: " << o << endl;
	cerr << "Camera target: " << t << endl;

	camera = new PerspectiveCamera(o, t, CONFIG.WIDTH, CONFIG.HEIGHT);

	//--------------------------------------------------------------------------
	// Read objects .ply file
	//--------------------------------------------------------------------------

	string plyFileName;
	file >> plyFileName;

	plyFileName = SCENE_FOLDER + plyFileName;

	cerr << "PLY objects file name: " << plyFileName << endl;

	Geometry objects;
	objects.Build(plyFileName);

	//--------------------------------------------------------------------------
	// Read lights .ply file
	//--------------------------------------------------------------------------

	file >> plyFileName;

	plyFileName = SCENE_FOLDER + plyFileName;

	cerr << "PLY lights file name: " << plyFileName << endl;

	Geometry meshLights;
	meshLights.Build(plyFileName);
	// Scale lights intensity
	for (unsigned int i = 0; i < meshLights.vertexCount; ++i)
		meshLights.vertColors[i] *= lightGain;

	//--------------------------------------------------------------------------
	// Join the ply objects
	//--------------------------------------------------------------------------

	geometry->Merge(objects, meshLights);
	geometry->meshLightOffset = objects.triangleCount;

	cerr << "Vertex count: " << geometry->vertexCount << " (" << (geometry->vertexCount
			* sizeof(Point) / 1024) << "Kb)" << endl;
	cerr << "Triangle count: " << geometry->triangleCount << " (" << (geometry->triangleCount
			* sizeof(Triangle) / 1024) << "Kb)" << endl;

	//--------------------------------------------------------------------------
	// Create light sources list
	//--------------------------------------------------------------------------

	geometry->nLights = geometry->triangleCount - geometry->meshLightOffset;
	geometry->lights = new TriangleLight[geometry->nLights];
	for (size_t i = 0; i < geometry->nLights; ++i)
		new (&(geometry->lights[i])) TriangleLight(i + geometry->meshLightOffset,
				&(geometry->triangles[i + geometry->meshLightOffset]), geometry->vertices);

}

void k_CUDA_pathtracer(Task* t_);

void k_CPU_pathtracer(Task* t_) {

	Domain<RGB> pixelsRadiance;
	t_->getDomain(0, pixelsRadiance);

	Domain<char> geometry_chunk;
	t_->getDomain(1, geometry_chunk);

	Domain<BVHAccelArrayNode> bvh;
	t_->getDomain(2, bvh);

	Domain<prePath> paths;
	t_->getDomain(3, paths);

	Task_pathtracer* t = (Task_pathtracer*) t_;

	char *vertices_c = (char*) geometry_chunk.device_data_buffer;
	char *vertNormals_c = vertices_c + t->geometry->vertexCount * sizeof(Point);
	char *vertColors_c = vertNormals_c + t->geometry->vertexCount * sizeof(Normal);
	char *triangles_c = vertColors_c + t->geometry->vertexCount * sizeof(RGB);
	char *lights_c = triangles_c + t->geometry->triangleCount * sizeof(Triangle);

	Point *vertices = (Point*) (vertices_c);
	Normal *vertNormals = (Normal*) (vertNormals_c);
	RGB *vertColors = (RGB*) (vertColors_c);
	Triangle *triangles = (Triangle*) (triangles_c);
	TriangleLight *lights = (TriangleLight*) (lights_c);

	boost::mutex radianceMutex;
	//printf("Rendering process started\n");

	uint ray_count =0;

	fprintf(stderr, "Launching CPU kernel for %d paths...\n",paths.X.length());


#ifndef __DEBUG
	#pragma omp parallel for schedule(dynamic)
#endif
	for (int ipath = paths.X.start; ipath < paths.X.end; ipath++) {

//		if (ipath!= 0 && (ipath - paths.X.start) % 1000000 == 0){
//			fprintf(stderr,"Processing at %.1f \n",((ipath - paths.X.start)*100.0)/(float)paths.X.length());
//		}

		bool not_done = false;

		Path p = Path(&paths.at(ipath));

		t->camera->GenerateRay(&p);
		// Process path

		do {

			Ray rb = Ray(p.pathRay.o,p.pathRay.d);
			RayHit hit;
			__sync_fetch_and_add(&ray_count, 1);
			Intersect(rb, hit, (BVHAccelArrayNode*) bvh.device_data_buffer, triangles, vertices);
			not_done = Shade(&p, t->geometry, (BVHAccelArrayNode*) bvh.device_data_buffer, hit,ray_count);

		} while (not_done);

		int x = (int) p.screenX;
		int y = (int) p.screenY;

		//if (y >= CONFIG.HEIGHT || x >= CONFIG.WIDTH) printf("error\n");

		const int offset = x + y * CONFIG.WIDTH;

	//	if (p.pathRay.o.x == 0.f)
	//		printf("error\n");

		radianceMutex.lock();

		if (offset < pixelsRadiance.X.start || offset >= pixelsRadiance.X.end)
			printf("error\n");
		pixelsRadiance.at(offset) += p.radiance;

		radianceMutex.unlock();
	}

	t->update_ray_count(ray_count);

}
