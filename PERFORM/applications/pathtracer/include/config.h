/*
 * config.h
 *
 *  Created on: Feb 13, 2012
 *      Author: rr
 */

#ifndef CONFIG_H_
#define CONFIG_H_

//// CONFIG ////


#include "shadowX.h"
//#define SHADOWRAY 8

/////////////

#define PIXEL_COUNT (CONFIG.WIDTH * CONFIG.HEIGHT)
#define PATH_COUNT (PIXEL_COUNT * CONFIG.SPP)


#define SCENE1 "scenes/luxball.scn"
#define SCENE2 "scenes/simple.scn"
#define SCENE3 "scenes/interp.scn"
#define SCENE4 "scenes/loft.scn" // SN
#define SCENE5 "scenes/kitchen.scn" //bad w/ >36spp
#define SCENE6 "scenes/loft2.scn" // SN
#define SCENE7 "scenes/interp_closed.scn" //SN

#define SCENE_FOLDER "/home/rr/work/perform/PERFORM/data/"
//#define SCENE_PATH (string(SCENE_FOLDER) + string(SCENE_FILE))


typedef struct {
	short WIDTH;
	short HEIGHT;
	short MAX_PATH_DEPTH;
	uint SPP;
	short SCENEID;
	short STRATA;
} PT_CONFIG;

static string getScenePath(short sc_id) {
	switch (sc_id) {
	case 1:
		return (string(SCENE_FOLDER) + string(SCENE1));

	case 2:
		return (string(SCENE_FOLDER) + string(SCENE2));

	case 3:
		return (string(SCENE_FOLDER) + string(SCENE3));

	case 4:
		return (string(SCENE_FOLDER) + string(SCENE4));

	case 5:
		return (string(SCENE_FOLDER) + string(SCENE5));

	case 6:
			return (string(SCENE_FOLDER) + string(SCENE6));

	case 7:
		return (string(SCENE_FOLDER) + string(SCENE7));

	default:
		printf("Scene id error\n");
		exit(0);
		break;
	}
}

extern PT_CONFIG CONFIG;

void initCONFIG(char *argv[]);

#endif /* CONFIG_H_ */
