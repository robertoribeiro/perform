/*
 * pathtracer.hpp
 *
 *  Created on: Mar 2, 2012
 *      Author: rr
 */

#ifndef PATHTRACER_HPP_
#define PATHTRACER_HPP_

/*
 * pathtracer.cpp
 *
 *  Created on: Mar 2, 2012
 *      Author: rr
 */

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "display/film.h"
#include "display/displayfunc.h"
#include "path.h"
#include "randomgen.h"
#include "geometry.h"
#include "geometry/bvhaccel.h"
#include "camera.h"
#include "config.h"
#include "perform.h"

#include "utils.h"
//#include "user_defined_task.h"

#include <omp.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <boost/thread/mutex.hpp>

namespace CPU_FUNTIONS {


bool TriangleIntersect(Triangle* tri, const Ray &ray, const Point *verts, RayHit *rayHit) ;

RGB Sample_L(uint triIndex, const Geometry *objs, const Point &p, const Normal &N, const float u0,
		const float u1, float *pdf, Ray *shadowRay);

bool BBox_IntersectP(BBox bbox, const Ray &ray, float *hitt0, float *hitt1);

bool Intersect(const Ray &ray, RayHit& rayHit, BVHAccelArrayNode* bvhTree, Triangle *triangles,
		Point *vertices);

bool Shade(Path* path, Geometry *geometry, BVHAccelArrayNode *bvh, const RayHit& rayHit);

void DistrurbeSample(Path *p);

}



void UpdateScreenBuffer(Film* film, uint width, uint height, RGB *pixelsRadiance, float *pixels);

void ParseSceneFile(PerspectiveCamera*& camera, Geometry*& geometry);

void k_CUDA_pathtracer(Task* t_);

void k_CPU_pathtracer(Task* t_);

#endif /* PATHTRACER_HPP_ */
