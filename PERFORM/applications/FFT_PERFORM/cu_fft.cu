#include "fft.h"
#include "user_defined_task.h"

__global__ void CUDA_TRANSPOSE(Domain<double> x, Domain<double> y) {

	int i = blockIdx.x * blockDim.x + threadIdx.x + x.X.start;
	int j = blockIdx.y * blockDim.y + threadIdx.y + x.Y.start;

	if (i == x.X.start && j == x.Y.start) {
		//printf("input.X.start:%d input.X.end:%d input.Y.start:%d input.Y.end:%d\n", x.X.start, x.X.end, x.Y.start, x.Y.end);
		//printf("output.X.start:%d output.X.end:%d output.Y.start:%d output.Y.end %d\n\n", y.X.start, y.X.end, y.Y.start, y.Y.end);
	}
	__syncthreads();
	if (i < x.X.end && j < x.Y.end) {
		//if (x.X.length() == 16) printf("i:%d j:%d\n", i, j);

		y.at(j, i) = x.at(i, j);
	}
}

__global__ void FREQMUL(Domain<double> xReal, Domain<double> xImag, Domain<double> yReal, Domain<double> yImag) {

	int i = blockIdx.x * blockDim.x + threadIdx.x + xReal.X.start;
	int j = blockIdx.y * blockDim.y + threadIdx.y + xReal.Y.start;

	double tmpx;
	double tmpy;

	if (i < xReal.X.end && j < xReal.Y.end) {
		// do frequency multiplication: zReal = xReal*yReal - xImag*yImag;
		tmpx = (xReal.at(i, j) * yReal.at(i, j)) - (xImag.at(i, j) * yImag.at(i, j));

		// do frequency multiplication: zImag = xImag*yReal + xReal*yImag;
		tmpy = (xImag.at(i, j) * yReal.at(i, j)) + (xReal.at(i, j) * yImag.at(i, j));

		xReal.at(i, j) = tmpx;
		xImag.at(i, j) = tmpy;
	}

}

__global__ void CUDA_FFT(Domain<double> x, Domain<double> y, int tSize, int dir, int powN) {

	int offset = /*tSize **/(blockIdx.x * blockDim.x + threadIdx.x) + x.X.start;

	if (offset < x.X.end) {
		//	printf("offset: %d\n",offset);

		int j, k, n1, n2, N = tSize;
		double c, s, w, a, t1, t2;

		// bit-reverse //
		j = 0;
		n2 = N / 2;
		for (int i = 1; i < N - 1; i++) {
			n1 = n2;
			while (j >= n1) {
				j = j - n1;
				n1 = n1 / 2;
			}
			j = j + n1;

			if (i < j) {
				t1 = x.at(offset, i);
				x.at(offset, i) = x.at(offset, j);
				x.at(offset, j) = t1;
				t1 = y.at(offset, i);
				y.at(offset, i) = y.at(offset, j);
				y.at(offset, j) = t1;
				//			t1 = x[i + offset];
				//			x[i + offset] = x[j + offset];
				//			x[j + offset] = t1;
				//			t1 = y[i + offset];
				//			y[i + offset] = y[j + offset];
				//			y[j + offset] = t1;
			}
		}

		// Second Step: FFT //
		n1 = 0;
		N = 1;
		int stages = powN;
		int n = tSize;

		// For each stage
		for (int i = 0; i < stages; i++) {
			n1 = N; // tamanho de casa asa
			N = N + N; // tamanho de cada borboleta
			w = dir * (-6.283185307179586 / N);
			a = 0.0;

			for (j = 0; j < n1; j++) { // Aumenta o tamanho da asa
				c = cos(a);
				s = sin(a);
				a = a + w;

				for (k = j; k < n; k = k + N) { // percorre todas as borboletas

					t1 = c * x.at(offset, k + n1) - s * y.at(offset, k + n1);
					t2 = s * x.at(offset, k + n1) + c * y.at(offset, k + n1);
					x.at(offset, k + n1) = x.at(offset, k) - t1;
					y.at(offset, k + n1) = y.at(offset, k) - t2;
					x.at(offset, k) = x.at(offset, k) + t1;
					y.at(offset, k) = y.at(offset, k) + t2;
					//				t1 = c * x[k + n1 + offset] - s * y[k + n1 + offset];
					//				t2 = s * x[k + n1 + offset] + c * y[k + n1 + offset];
					//				x[k + n1 + offset] = x[k + offset] - t1;
					//				y[k + n1 + offset] = y[k + offset] - t2;
					//				x[k + offset] = x[k + offset] + t1;
					//				y[k + offset] = y[k + offset] + t2;
				}
			}
		}

		if (dir < 0) {
			for (int i = 0; i < n; i++) {
				x.at(offset, i) = x.at(offset, i) / n;
				y.at(offset, i) = y.at(offset, i) / n;

				//			x[i + offset] = x[i + offset] / n;
				//			y[i + offset] = y[i + offset] / n;
			}
		}

	}

}

void k_CUDA_FFT(Task* t_) {

	Task_FFT* t = (Task_FFT*) t_;

	Domain<double> xReal;
	t_->getDomain(0, xReal);

	Domain<double> xImag;
	t_->getDomain(1, xImag);

	int N = xReal.Y.length();

	int powN = ceil(log2((double) N)); // define block size and grid size here

	dim3 fftBlockSize = dim3(512, 1, 1);
	dim3 fftGridSize = dim3(xReal.X.length() / fftBlockSize.x + 1, 1, 1);

	CUDA_FFT<< <fftGridSize, fftBlockSize,0,t->stream >>>(xReal, xImag, N, t->direction, powN);

}

void k_CUDA_TRANSPOSE(Task* t_) {

	Domain<double> xReal;
	t_->getDomain(0, xReal);

	Domain<double> tmp;
	t_->getDomain(1, tmp);

	int h = xReal.X.length();
	int w = xReal.Y.length();

	dim3 transBlockSize = dim3(16, 16, 1);
	dim3 transGridSize = dim3(h / 16 + 1, w / 16 + 1, 1);

	CUDA_TRANSPOSE<< <transGridSize, transBlockSize,0,t_->stream >> >(xReal, tmp);
}

void k_CUDA_FREQMULT(Task* t_) {

	Domain<double> xReal;
	t_->getDomain(0, xReal);

	Domain<double> yReal;
	t_->getDomain(1, yReal);

	Domain<double> xImag;
	t_->getDomain(2, xImag);

	Domain<double> yImag;
	t_->getDomain(3, yImag);

	int h = xReal.X.length();
	int w = xReal.Y.length();

	dim3 transBlockSize = dim3(16, 16, 1);
	dim3 transGridSize = dim3(h / 16 + 1, w / 16 + 1, 1);

	FREQMUL<< <transGridSize, transBlockSize,0,t_->stream >> >(xReal, xImag, yReal, yImag);

}

