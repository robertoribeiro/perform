#include "fft.h"
#include "user_defined_task.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <tbb/tick_count.h>
#include "tbbConvOp.h"
#include "matrix_ops.h"
#include "imageOps.h"

using namespace tbb;
using namespace std;

void k_CUDA_wrapper_uberkernel(Task * t, Task ** task_prms, int child_task_count) {
}

struct IMAGE {
	double *data;
};

char *inamex, *inamey;

static void printMatrix(double *A, int rows, int cols) {
	printf("\n\n");
	int ii, jj;
	for (ii = 0; ii < rows; ii++) {
		for (jj = 0; jj < cols; jj++)
			printf("%6.1lf ", A[ii * cols + jj]);
		//cout << A[ii * cols + jj] << " ";
		printf("\n");
	}
}
static void checkTranspose(double *A, double *B, int rows, int cols) {

	int ii, jj;
	for (ii = 0; ii < rows; ii++) {
		for (jj = 0; jj < cols; jj++)
			if (A[ii * cols + jj] != B[jj * cols + ii]) {
				printf("Transpose is wrong\n");
				return;
			}
	}

	//printf("Transpose is correct\n");

}

double FFT1, TRANS1, TRANS2, FFT2, TRANS3, TRANS4, FFT3, TRANS5, TRANS6, FFT4, TRANS7, TRANS8,
		FREQMULTI, FFT5, TRANS9, TRANS10, FFT6, TRANS11, TRANS12, TOTAL;

void operator_transpose(blocked_range2d<int> r, Domain<double> input, Domain<double> output) {
	for (int i = r.rows().begin(); i < r.rows().end(); i++) {
		for (int j = r.cols().begin(); j < r.cols().end(); j++) {
			//			printf("input.X.start:%d input.X.end:%d input.Y.start:%d input.Y.end:%d i:%d j:%d\n",
			//					input.X.start,input.X.end,input.Y.start,input.Y.end,i,j);
			//			printf("output.X.start:%d output.X.end:%d output.Y.start:%d output.Y.end %d\n\n",
			//					output.X.start,output.X.end,output.Y.start,output.Y.end);
			output.at(j, i) = input.at(i, j);
		}
	}

}

void butterfly(int dir, int offset, Domain<double> x, Domain<double> y) {

	int i, j, k, n1, n2, N = x.Y.length(); //r.grainsize();
	double c, s, w, a, t1, t2;
	j = 0;
	n2 = N / 2;
	for (i = 1; i < N - 1; i++) {
		n1 = n2;
		while (j >= n1) {
			j = j - n1;
			n1 = n1 / 2;
		}
		j = j + n1;
		if (i < j) {
			t1 = x.at(offset, i);
			x.at(offset, i) = x.at(offset, j);
			x.at(offset, j) = t1;
			t1 = y.at(offset, i);
			y.at(offset, i) = y.at(offset, j);
			y.at(offset, j) = t1;
			//                t1 = x[i + offset];
			//                x[i + offset] = x[j + offset];
			//                x[j + offset] = t1;
			//                t1 = y[i + offset];
			//                y[i + offset] = y[j + offset];
			//                y[j + offset] = t1;
		}
	}

	/* Second Step: FFT */
	n1 = 0;
	N = 1;
	int stages = ceil(log2(x.Y.length()));
	int n = x.Y.length();
	// For each stage
	for (i = 0; i < stages; i++) {
		n1 = N; // tamanho de casa asa
		N = N + N; // tamanho de cada borboleta
		w = dir * (-6.283185307179586 / N);
		a = 0.0;
		for (j = 0; j < n1; j++) {
			// para cada asa
			c = cos(a);
			s = sin(a);
			a = a + w;
			for (k = j; k < n; k = k + N) {
				t1 = c * x.at(offset, k + n1) - s * y.at(offset, k + n1);
				t2 = s * x.at(offset, k + n1) + c * y.at(offset, k + n1);
				x.at(offset, k + n1) = x.at(offset, k) - t1;
				y.at(offset, k + n1) = y.at(offset, k) - t2;
				x.at(offset, k) = x.at(offset, k) + t1;
				y.at(offset, k) = y.at(offset, k) + t2;
			}
		}

	}

	if (dir < 0) {
		for (i = 0; i < n; i++) {
			x.at(offset, i) = x.at(offset, i) / n;
			y.at(offset, i) = y.at(offset, i) / n;
		}
	}

}

// padd image to the next power of 2
// return the new N
int getNextPower(int Ndata, int Nkernel) {

	// calc new size
	int newN;
	newN = Ndata + Nkernel - 1;

	int power = ceil(log2(Ndata));
	//printf("N is %d. Power is %d\n", Ndata,power);

	int newSize = pow(2, power + 1);
	//printf("New power is %d\n", power+1);
	//printf("New size is %d\n", newSize);

	//printf("Return \n");
	return (newSize);

}

void init(int Nx, struct IMAGE &xReal, struct IMAGE &yReal, int Ny, int &paddedN,
		struct IMAGE &tmp, struct IMAGE &tmp2) {
	/* Read input images ********************************************************* */

	xReal.data = (double*) (PERFORM_alloc(sizeof(double) * Nx * Nx));
	yReal.data = (double*) (PERFORM_alloc(sizeof(double) * Ny * Ny));
	readImage(xReal.data, inamex);
	// Edge detection kernel
	yReal.data[0] = -1;
	yReal.data[1] = -1;
	yReal.data[2] = -1;
	yReal.data[3] = -1;
	yReal.data[4] = 8;
	yReal.data[5] = -1;
	yReal.data[6] = -1;
	yReal.data[7] = -1;
	yReal.data[8] = -1;
	//readImage(yReal.data, inamey);
	/* Padd input images ******************************************************** */
	paddedN = getNextPower(Nx, Ny);

	int n = -1;
	tmp.data = (double*) (PERFORM_alloc(sizeof(double) * paddedN * paddedN));
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= Nx || n >= Nx)
				tmp.data[i * paddedN + j] = 0;

			else
				tmp.data[i * paddedN + j] = xReal.data[n * Nx + j];

		}
	}

	tmp2.data = xReal.data;
	xReal.data = tmp.data;
	PERFORM_free((void*) (tmp2.data));
	tmp.data = (double*) (PERFORM_alloc(sizeof(double) * paddedN * paddedN));
	n = -1;
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= Ny || n >= Ny)
				tmp.data[i * paddedN + j] = 0;

			else
				tmp.data[i * paddedN + j] = yReal.data[n * Ny + j];

		}
	}

	tmp2.data = yReal.data;
	yReal.data = tmp.data;
	PERFORM_free((void*) (tmp2.data));
	return;
}

void stage1(Domain<double>*&d_xReal, Domain<double>*&d_xImag, RESOURCE_ID & dev_p,
		Domain<double>*&d_tmp, Domain<double>*&d_tmp2, Domain<double>*&d_aux_ptr, bool dice,
		float dice_value) {

	Task_FFT* t_FFT;
	Task_TRANSPOSE* t_transpose;

	d_xReal->intention = RW;
	d_xImag->intention = RW;

	t_FFT = new Task_FFT(REGULAR);
	t_FFT->direction = 1;
	t_FFT->associate_domain(d_xReal);
	t_FFT->associate_domain(d_xImag);
	t_FFT->associate_kernel(CPU, &k_CPU_FFT);
	t_FFT->associate_kernel(GPU, &k_CUDA_FFT);
	t_FFT->device_preference = dev_p;
	t_FFT->diceable = dice;
	t_FFT->dice_value=dice_value;
	performQ->addTasks(t_FFT);
	wait_for_all_tasks();
	performDL->splice(d_xReal);
	performDL->splice(d_xImag);

	d_xReal->intention = R;
	d_tmp->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_xReal);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	performQ->addTasks(t_transpose);

	d_xImag->intention = R;
	d_tmp2->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_xImag);
	t_transpose->associate_domain(d_tmp2);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	performQ->addTasks(t_transpose);

	wait_for_all_tasks();
	//	performDL->splice(d_xReal);
	performDL->splice(d_tmp);

#ifdef _DEBUG
	checkTranspose((double*) (d_xReal->phy_chunk->original_host_pointer),
			(double*) (d_tmp->phy_chunk->original_host_pointer),
			d_xReal->X.length(), d_xReal->Y.length());
#endif

	wait_for_all_tasks();
	//	performDL->splice(d_xImag);
	performDL->splice(d_tmp2);

#ifdef _DEBUG
	checkTranspose((double*) (d_xImag->phy_chunk->original_host_pointer),
			(double*) (d_tmp2->phy_chunk->original_host_pointer),
			d_xImag->X.length(), d_xImag->Y.length());
#endif
	d_tmp->intention = RW;
	d_tmp2->intention = RW;

	t_FFT = new Task_FFT(REGULAR);
	t_FFT->direction = 1;
	t_FFT->associate_domain(d_tmp);
	t_FFT->associate_domain(d_tmp2);
	t_FFT->associate_kernel(CPU, &k_CPU_FFT);
	t_FFT->associate_kernel(GPU, &k_CUDA_FFT);
	t_FFT->device_preference = dev_p;
	t_FFT->diceable = dice;
	t_FFT->dice_value=dice_value;
	performQ->addTasks(t_FFT);
	wait_for_all_tasks();
	performDL->splice(d_tmp);
	performDL->splice(d_tmp2);

	d_tmp->intention = R;
	d_xReal->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_domain(d_xReal);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	performQ->addTasks(t_transpose);

	d_tmp2->intention = R;
	d_xImag->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_tmp2);
	t_transpose->associate_domain(d_xImag);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	t_transpose->device_preference = dev_p;
	performQ->addTasks(t_transpose);

	wait_for_all_tasks();
	performDL->splice(d_xReal);
	//performDL->splice(d_tmp);
#ifdef _DEBUG
	checkTranspose((double*) (d_xReal->phy_chunk->original_host_pointer),
			(double*) (d_tmp->phy_chunk->original_host_pointer),
			d_xReal->X.length(), d_xReal->Y.length());
#endif
	wait_for_all_tasks();
	performDL->splice(d_xImag);
	//performDL->splice(d_tmp2);
#ifdef _DEBUG
	checkTranspose((double*) (d_xImag->phy_chunk->original_host_pointer),
			(double*) (d_tmp2->phy_chunk->original_host_pointer),
			d_xImag->X.length(), d_xImag->Y.length());
#endif
}

void stage3(Domain<double>*&d_xReal, Domain<double>*d_yReal, Domain<double>*d_xImag,
		Domain<double>*&d_yImag, RESOURCE_ID & dev_p, bool dice,
		float dice_value) {

	d_xReal->intention = RW;
	d_yReal->intention = R;

	d_xImag->intention = RW;
	d_yImag->intention = R;

	Task_FREQMULT *t = new Task_FREQMULT(REGULAR);
	t->associate_domain(d_xReal);
	t->associate_domain(d_yReal);
	t->associate_domain(d_xImag);
	t->associate_domain(d_yImag);
	t->associate_kernel(CPU, &k_CPU_FREQMULT);
	t->associate_kernel(GPU, &k_CUDA_FREQMULT);
	t->device_preference = dev_p;
	t->diceable = dice;
	t->dice_value=dice_value;

	performQ->addTasks(t);
	wait_for_all_tasks();
	performDL->splice(d_xReal);
	//performDL->splice(d_yReal);
	performDL->splice(d_xImag);
	//performDL->splice(d_yImag);


}

void stage4(Domain<double>*&d_zReal, Domain<double>*&d_zImag, RESOURCE_ID dev_p,
		Domain<double>*&d_tmp, Domain<double>*&d_tmp2, Domain<double>*d_aux_ptr,bool dice,
		float dice_value) {

	Task_FFT* t_FFT;
	Task_TRANSPOSE* t_transpose;

	d_zReal->intention = RW;
	d_zImag->intention = RW;

	t_FFT = new Task_FFT(REGULAR);
	t_FFT->direction = -1;
	t_FFT->associate_domain(d_zReal);
	t_FFT->associate_domain(d_zImag);
	t_FFT->associate_kernel(CPU, &k_CPU_FFT);
	t_FFT->associate_kernel(GPU, &k_CUDA_FFT);
	t_FFT->device_preference = dev_p;
	t_FFT->diceable = dice;
	t_FFT->dice_value=dice_value;
	performQ->addTasks(t_FFT);
	wait_for_all_tasks();
	performDL->splice(d_zReal);
	performDL->splice(d_zImag);

	d_zReal->intention = R;
	d_tmp->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_zReal);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	performQ->addTasks(t_transpose);

	d_zImag->intention = R;
	d_tmp2->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_zImag);
	t_transpose->associate_domain(d_tmp2);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	performQ->addTasks(t_transpose);

	wait_for_all_tasks();
	//	performDL->splice(d_zReal);
	performDL->splice(d_tmp);

#ifdef _DEBUG
	checkTranspose((double*) (d_zReal->phy_chunk->original_host_pointer),
			(double*) (d_tmp->phy_chunk->original_host_pointer),
			d_zReal->X.length(), d_zReal->Y.length());
#endif

	wait_for_all_tasks();
	//performDL->splice(d_zImag);
	performDL->splice(d_tmp2);
#ifdef _DEBUG
	checkTranspose((double*) (d_zImag->phy_chunk->original_host_pointer),
			(double*) (d_tmp2->phy_chunk->original_host_pointer),
			d_zReal->X.length(), d_zReal->Y.length());
#endif

	d_tmp->intention = RW;
	d_tmp2->intention = RW;

	t_FFT = new Task_FFT(REGULAR);
	t_FFT->direction = -1;
	t_FFT->associate_domain(d_tmp);
	t_FFT->associate_domain(d_tmp2);
	t_FFT->associate_kernel(CPU, &k_CPU_FFT);
	t_FFT->associate_kernel(GPU, &k_CUDA_FFT);
	t_FFT->device_preference = dev_p;
	t_FFT->diceable = dice;
	t_FFT->dice_value=dice_value;
	performQ->addTasks(t_FFT);
	wait_for_all_tasks();
	performDL->splice(d_tmp);
	performDL->splice(d_tmp2);

	d_tmp->intention = R;
	d_zReal->intention = W;

	t_transpose = new Task_TRANSPOSE(REGULAR);
	t_transpose->associate_domain(d_tmp);
	t_transpose->associate_domain(d_zReal);
	t_transpose->associate_kernel(CPU, &k_CPU_TRANSPOSE);
	t_transpose->associate_kernel(GPU, &k_CUDA_TRANSPOSE);
	t_transpose->device_preference = dev_p;
	t_transpose->diceable = dice;
	t_transpose->dice_value=dice_value;
	performQ->addTasks(t_transpose);
	wait_for_all_tasks();

	PERFORM_signal_taskSubmissionEnded();

	performDL->splice(d_zReal);
	//performDL->splice(d_tmp);
#ifdef _DEBUG
	checkTranspose((double*) (d_tmp->phy_chunk->original_host_pointer),
			(double*) (d_zReal->phy_chunk->original_host_pointer),
			d_zReal->X.length(), d_zReal->Y.length());
#endif
}

void finish(int N, int paddedN, struct IMAGE tmp, struct IMAGE &zReal, struct IMAGE tmp2) {

	N = getImageSize(inamex);
	int n = -1;
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= N || n >= N)
				continue;

			else
				tmp.data[n * N + j] = zReal.data[i * paddedN + j];

		}
	}

	tmp2.data = zReal.data;
	zReal.data = tmp.data;
	//PERFORM_free((void*) (tmp2.data));
	writeImage(tmp.data, "Convolution_TBB", N);
}

// This job is the description of the convolution operation of two images
double job_CONV(int argc, char *argv[]) {

	inamex = argv[4];
	inamey = argv[4];

	float divide_task_by = atoi(argv[3]) == 0 ? 0.0 : 1.0/atoi(argv[3]);

	int Nx = getImageSize(inamex);
	int Ny = 3;

	struct IMAGE xReal, xImag, yReal, yImag, zReal, zImag;
	struct IMAGE tmp;
	struct IMAGE tmp2;
	int paddedN = 0;

	init(Nx, xReal, yReal, Ny, paddedN, tmp, tmp2);

	int N = paddedN;

	xImag.data = (double*) (PERFORM_alloc(sizeof(double) * N * N));
	yImag.data = (double*) (PERFORM_alloc(sizeof(double) * N * N));
	zReal.data = (double*) (PERFORM_alloc(sizeof(double) * N * N));
	zImag.data = (double*) (PERFORM_alloc(sizeof(double) * N * N));
	tmp.data = (double*) (PERFORM_alloc(sizeof(double) * N * N));
	tmp2.data = (double*) (PERFORM_alloc(sizeof(double) * N * N));

	memset(xImag.data, '0', N * N * sizeof(double));
	memset(yImag.data, '0', N * N * sizeof(double));
	memset(zReal.data, '0', N * N * sizeof(double));
	memset(zImag.data, '0', N * N * sizeof(double));
	memset(tmp.data, '0', N * N * sizeof(double));
	memset(tmp2.data, '0', N * N * sizeof(double));

	//################################### DOMAINS ######################
	Domain<double>*d_xReal = new Domain<double> (2, RW, xReal.data, dim_space(0, N),
			dim_space(0, N));
	Domain<double>*d_yReal = new Domain<double> (2, RW, yReal.data, dim_space(0, N),
			dim_space(0, N));
	Domain<double>*d_xImag = new Domain<double> (2, RW, xImag.data, dim_space(0, N),
			dim_space(0, N));
	Domain<double>*d_yImag = new Domain<double> (2, RW, yImag.data, dim_space(0, N),
			dim_space(0, N));
	Domain<double>*d_tmp = new Domain<double> (2, RW, tmp.data, dim_space(0, N), dim_space(0, N));
	Domain<double>*d_tmp2 = new Domain<double> (2, RW, tmp2.data, dim_space(0, N), dim_space(0, N));

	Domain<double>*d_aux_ptr;
	//#############################################

	PERFORM_timer *timer = new PERFORM_timer();
	PERFORM_signal_JobStarted();
	timer->start();

	RESOURCE_ID dev_p = NONE;
	//RESOURCE_ID dev_p = CPU0;

	bool dice = divide_task_by == 0.0 ? false : true;

	stage1(d_xReal, d_xImag, dev_p, d_tmp, d_tmp2, d_aux_ptr, dice,divide_task_by);

	stage1(d_yReal, d_yImag, dev_p, d_tmp, d_tmp2, d_aux_ptr, dice,divide_task_by);

	stage3(d_xReal, d_yReal, d_xImag, d_yImag, dev_p, dice,divide_task_by);

	Domain<double>*d_zReal = new Domain<double> (2, RW,
			(double *) ((d_xReal->phy_chunk->original_host_pointer)), dim_space(0, N),
			dim_space(0, N));
	Domain<double>*d_zImag = new Domain<double> (2, RW,
			(double *) ((d_xImag->phy_chunk->original_host_pointer)), dim_space(0, N),
			dim_space(0, N));

	stage4(d_zReal, d_zImag, dev_p, d_tmp, d_tmp2, d_aux_ptr, dice,divide_task_by);

	timer->stop();
	PERFORM_signal_JobFinished(timer->duration);

	zReal.data = (double *) (d_zReal->phy_chunk->original_host_pointer);
	zImag.data = (double *) (d_zImag->phy_chunk->original_host_pointer);
	tmp.data = (double *) (d_tmp->phy_chunk->original_host_pointer);

	finish(N, paddedN, tmp, zReal, tmp2);

	// final clean
	//PERFORM_free((void*)(zReal.data));
	//PERFORM_free((void*)(zImag.data));

	return timer->duration;
}
