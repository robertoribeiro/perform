/* System includes */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* TBB includes */
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/tick_count.h>

#include "fft.h"

/* Defines */
#define PI  3.141592654

using namespace tbb;

static void printMatrix(double *A, int rows, int cols);

// FFT on a signal

class oneD_FFT {

	int dir;
	//double *x, *y;

	Domain<double> x;
	Domain<double> y;

public:

	oneD_FFT(int d, Domain<double> x_, Domain<double> y_) {

		x = x_;
		y = y_;
		dir = d;
	}

	void operator ()(const blocked_range2d<int> r) const {
		/* First step: bit-reversal sorting */
		int offset = r.rows().begin();
		butterfly(dir, offset, x, y);
	}

};


void operator_FREQMULT(blocked_range2d<int> r, Domain<double> xReal, Domain<double> yReal, Domain<double> xImag, Domain<double> yImag) {

	double tmpREAL, tmpIMG;
	for (int i = r.rows().begin(); i < r.rows().end(); i++) {
		for (int j = r.cols().begin(); j < r.cols().end(); j++) {
			tmpREAL = (xReal.at(i, j) * yReal.at(i, j)) - (xImag.at(i, j) * yImag.at(i, j));
			tmpIMG = (xImag.at(i, j) * yReal.at(i, j)) + (xReal.at(i, j) * yImag.at(i, j));
			xReal.at(i, j) = tmpREAL;
			xImag.at(i, j) = tmpIMG;
		}
	}

}

class FREQMULT {

	Domain<double> xReal;
	Domain<double> yReal;
	Domain<double> xImag;
	Domain<double> yImag;

public:

	FREQMULT(Domain<double> xReal_, Domain<double> yReal_, Domain<double> xImag_, Domain<double> yImag_) {
		xReal = xReal_;
		yReal = yReal_;
		xImag = xImag_;
		yImag = yImag_;
	}

	void operator ()(blocked_range2d<int> r) const {
		operator_FREQMULT(r, xReal, yReal, xImag, yImag);
	}

};

void k_CPU_FFT(Task* t_) {

	Task_FFT* t = (Task_FFT*) t_;

	Domain<double> xReal;
	t_->getDomain(0, xReal);

	Domain<double> xImag;
	t_->getDomain(1, xImag);

	int N = xReal.X.length();
	int dir = t->direction;
	parallel_for(blocked_range2d<int> (xReal.X.start, xReal.X.end, 1, xReal.Y.start, xReal.Y.end, xReal.Y.length()), oneD_FFT(dir, xReal, xImag),
			simple_partitioner());

}

void operator_transpose(blocked_range2d<int> r, Domain<double> in, Domain<double> out);

class CPU_TRANSPOSE {

	Domain<double> input;
	Domain<double> output;

public:

	CPU_TRANSPOSE(Domain<double> in, Domain<double> out) {
		input = in;
		output = out;

	}

	void operator()(blocked_range2d<int> r) const {

		operator_transpose(r, input, output);

	}

};

void k_CPU_TRANSPOSE(Task* t_) {

	Domain<double> input;
	t_->getDomain(0, input);

	Domain<double> output;
	t_->getDomain(1, output);

	parallel_for(blocked_range2d<int> (input.X.start, input.X.end, 1, input.Y.start, input.Y.end, input.Y.length()), CPU_TRANSPOSE(input, output));

}

void k_CPU_FREQMULT(Task* t_) {

	Domain<double> xReal;
	t_->getDomain(0, xReal);

	Domain<double> yReal;
	t_->getDomain(1, yReal);

	Domain<double> xImag;
	t_->getDomain(2, xImag);

	Domain<double> yImag;
	t_->getDomain(3, yImag);

	int sub_sub_block;
//	if (xReal.X.length() >= 32)
//		sub_sub_block = 32;
//	else
		sub_sub_block = xReal.X.length();

	parallel_for(blocked_range2d<int> (xReal.X.start, xReal.X.end, sub_sub_block, xReal.Y.start, xReal.Y.end, sub_sub_block),
			FREQMULT(xReal, yReal, xImag, yImag));

}

class TRANSPOSE {
	double *input, *output;
	int W, H;

public:

	TRANSPOSE(double *in, double *out, int h, int w) {
		input = in;
		output = out;
		W = w;
		H = h;
	}

	void operator()(blocked_range2d<int> r) const {
		for (int j = 0; j < W; j++) {
			for (int i = r.rows().begin(); i < r.cols().end(); i++) {
				output[j * H + i] = input[i * W + j];
			}
		}
	}

};

void k_TRANSPOSE(double **data_pointers, int *args, void* stream) {

	int W = args[0];
	int H = args[1];

	int nThreads = task_scheduler_init::default_num_threads();
	parallel_for(blocked_range2d<int> (0, W, W, 0, H, H / nThreads), TRANSPOSE(data_pointers[0], data_pointers[1], H, W), simple_partitioner());
}

