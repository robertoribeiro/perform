#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "../../task/Task.h"

#include <math.h>

class Task_FFT: public Task {
public:

	int direction;

	__H_D__
	Task_FFT() :
		Task() {
	}

	Task_FFT(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<double>* real;
		getDomain(0, real);

		Domain<double>* img;
		getDomain(1, img);

		float N = real->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);

		int block_step = real->X.length() / divide_task_by;

		Task_FFT** new_tasks = new Task_FFT*[divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {

			Task_FFT* t = new Task_FFT(REGULAR);

			Domain<double>* subReal =
					new Domain<double> (
							real,
							2,
							RW,
							dim_space(i * block_step + real->X.start,
									real->X.start + (i + 1) * block_step),
							dim_space(real->Y.start, real->Y.end));
			Domain<double>* subImg = new Domain<double> (img, 2, RW,
					dim_space(i * block_step + img->X.start, img->X.start + (i + 1) * block_step),
					dim_space(img->Y.start, img->Y.end));

			t->associate_domain(subReal);
			t->associate_domain(subImg);
			t->direction = direction;
			t->device_preference = device_preference;
			t->associate_kernel(this);

			t->wl = PM_Metric_normalized((block_step * wl.value) / N);
			t->dice_lvl = dice_lvl + 1;

			new_tasks[task_count++] = t;

		}
		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

};

class Task_TRANSPOSE: public Task {
public:
	__H_D__
	Task_TRANSPOSE() :
		Task() {
	}

	Task_TRANSPOSE(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<double>* A;
		getDomain(0, A);

		Domain<double>* B;
		getDomain(1, B);

		float N = A->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);

		int block_step = A->X.length() / divide_task_by;

		Task_TRANSPOSE** new_tasks = new Task_TRANSPOSE*[divide_task_by * divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {
			for (int j = 0; j < divide_task_by; j++) {

				Task_TRANSPOSE* t = new Task_TRANSPOSE(REGULAR);

				Domain<double>* subdomainA = new Domain<double> (A, 2, RW,
						dim_space(A->X.start + i * block_step, A->X.start + (i + 1) * block_step),
						dim_space(A->Y.start + j * block_step, A->Y.start + (j + 1) * block_step));

				Domain<double>* subdomainB = new Domain<double> (B, 2, RW,
						dim_space(B->X.start + j * block_step, B->X.start + (j + 1) * block_step),
						dim_space(B->Y.start + i * block_step, B->Y.start + (i + 1) * block_step));

				t->associate_domain(subdomainA);
				t->associate_domain(subdomainB);

				t->device_preference = device_preference;
				t->associate_kernel(this);

				t->wl = PM_Metric_normalized((block_step * wl.value) / N);
				t->dice_lvl = dice_lvl + 1;

				new_tasks[task_count++] = t;

			}
		}
		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}
};

class Task_FREQMULT: public Task {
public:
	__H_D__
	Task_FREQMULT() :
		Task() {
	}

	Task_FREQMULT(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<double>* d_xReal;
		getDomain(0, d_xReal);

		Domain<double>* d_yReal;
		getDomain(1, d_yReal);

		Domain<double>* d_xImag;
		getDomain(2, d_xImag);

		Domain<double>* d_yImag;
		getDomain(3, d_yImag);

		float N = d_xReal->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);

		int block_step = d_xReal->X.length() / divide_task_by;

		Task_FREQMULT** new_tasks = new Task_FREQMULT*[divide_task_by * divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {
			for (int j = 0; j < divide_task_by; j++) {

				Task_FREQMULT* t = new Task_FREQMULT(REGULAR);

				Domain<double>* subd_xReal = new Domain<double> (d_xReal, 2, RW,
						dim_space(d_xReal->X.start + i * block_step, d_xReal->X.start + (i + 1) * block_step),
						dim_space(d_xReal->Y.start + j * block_step,d_xReal->Y.start +  (j + 1) * block_step));

				Domain<double>* subd_yReal = new Domain<double> (d_yReal, 2, RW,
						dim_space(d_yReal->X.start + i * block_step,d_yReal->X.start +  (i + 1) * block_step),
						dim_space(d_yReal->Y.start + j * block_step, d_yReal->Y.start + (j + 1) * block_step));

				Domain<double>* subd_xImag = new Domain<double> (d_xImag, 2, RW,
						dim_space(d_xImag->X.start + i * block_step, d_xImag->X.start + (i + 1) * block_step),
						dim_space(d_xImag->Y.start + j * block_step, d_xImag->Y.start + (j + 1) * block_step));

				Domain<double>* subd_yImag = new Domain<double> (d_yImag, 2, RW,
						dim_space(d_yImag->X.start + i * block_step,d_yImag->X.start +  (i + 1) * block_step),
						dim_space(d_yImag->Y.start + j * block_step, d_yImag->Y.start + (j + 1) * block_step));

				t->associate_domain(subd_xReal);
				t->associate_domain(subd_yReal);
				t->associate_domain(subd_xImag);
				t->associate_domain(subd_yImag);

				t->device_preference = device_preference;
				t->associate_kernel(this);

				t->wl = PM_Metric_normalized((block_step * wl.value) / N);
				t->dice_lvl = dice_lvl + 1;

				new_tasks[task_count++] = t;

			}
		}
		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}
};

#endif /* UBK_TASK_H_ */
