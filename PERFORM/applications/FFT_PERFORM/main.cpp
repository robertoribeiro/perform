#include "../../perform.h"
#include "fft.h"

int main(int argc, char *argv[]) {
//
//	argv[1]="3";//SCH
//	argv[2]="3";//dev
//	argv[3]="8";//dice
//
//	argv[4]="/home/rr/work/perform/PERFORM/data/1024.jpg";

	fprintf(stderr,"args %s %s %s %s execs %d\n",argv[1],argv[2],argv[3],argv[4],EXECUTIONS);

	PERFORM_timer *timer = new PERFORM_timer();

	timer->start();

	for (int i = 0; i < EXECUTIONS; i++) {
		initPERFORM(atoi(argv[1]), atoi(argv[2]));

		double time = job_CONV(argc, argv);
#ifdef TEST_TIME
		timer->getMinResult(time, EXECUTIONS);
#endif
		shutdownPERFORM();
	}

	timer->stop();

	LOG(0.1,cout << "\n\nRun-time terminated in " << timer->duration << "s." << endl;)

	return 0;
}
