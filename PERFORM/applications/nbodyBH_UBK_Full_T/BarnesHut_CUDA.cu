#include "BarnesHut_job.h"


__global__ void k_update_vel_positions(Domain<float> domain_result, BH_TYPE dtime, int step,int nbodies) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x /*+ particles.X.start*/;

	FLOAT_ARRAY result_chunk = (FLOAT_ARRAY) domain_result.device_data_buffer;

	FLOAT_ARRAY r_posx = result_chunk;
	FLOAT_ARRAY	r_posy = r_posx + (nbodies);
	FLOAT_ARRAY r_posz = r_posy + (nbodies);
	FLOAT_ARRAY r_velx = r_posz + nbodies;
	FLOAT_ARRAY	r_vely = r_velx + (nbodies);
	FLOAT_ARRAY r_velz = r_vely + (nbodies);
	FLOAT_ARRAY r_accx = r_velz + nbodies;
	FLOAT_ARRAY	r_accy = r_accx + nbodies;
	FLOAT_ARRAY r_accz = r_accy + nbodies;
	FLOAT_ARRAY r_t_accx = r_accz + nbodies;
	FLOAT_ARRAY r_t_accy = r_t_accx + nbodies;
	FLOAT_ARRAY r_t_accz = r_t_accy + nbodies;


	//if (pt < particles.X.end && pt >= particles.X.start) {
		if (pt < nbodies && pt >= 0) {

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;
		BH_TYPE dthf = 0.5 * dtime;
		//particle* p = &(particles.at(pt));

//		if (pt == 0){
//			printf("ACCt_x: %.15f\n",p->t_accx);
//			printf("ACCx: %.15f\n",p->accx);
//		}

		if (step > 0) {
				r_velx[pt] += (r_accx[pt] - r_t_accx[pt]) * dthf;
				r_vely[pt] += (r_accy[pt] - r_t_accy[pt]) * dthf;
				r_velz[pt] += (r_accz[pt] - r_t_accz[pt]) * dthf;
			}

		dvelx = r_accx[pt] * dthf;
		dvely = r_accy[pt] * dthf;
		dvelz = r_accz[pt] * dthf;

		velhx = r_velx[pt] + dvelx;
		velhy = r_vely[pt] + dvely;
		velhz = r_velz[pt] + dvelz;

		r_posx[pt] += velhx * dtime;
		r_posy[pt] += velhy * dtime;
		r_posz[pt] += velhz * dtime;

		r_velx[pt] = velhx + dvelx;
		r_vely[pt] = velhy + dvely;
		r_velz[pt] = velhz + dvelz;

	}

}

__global__ void k_update_positions(Domain<particle> particles, BH_TYPE dtime) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x + particles.X.start;

	if (pt < particles.X.end && pt >= particles.X.start) {

		BH_TYPE dthf = 0.5 * dtime;

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;

		particle* p = &(particles.at(pt));

		dvelx = p->accx * dthf;
		dvely = p->accy * dthf;
		dvelz = p->accz * dthf;

		velhx = p->velx + dvelx;
		velhy = p->vely + dvely;
		velhz = p->velz + dvelz;

		p->mass_pos.x += velhx * dtime;
		p->mass_pos.y += velhy * dtime;
		p->mass_pos.z += velhz * dtime;

		p->velx = velhx + dvelx;
		p->vely = velhy + dvely;
		p->velz = velhz + dvelz;
	}

}

void k_CUDA_update_vel_positions(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<float> domain_result;
	t->getDomain(1, domain_result);

	int nbodies = t->nbodies;

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	k_update_vel_positions<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(domain_result, t->dtime,t->step,t->nbodies);
	checkCUDAError();

	cudaDeviceSynchronize();

	//PrintParticles((particle*)particles.phy_chunk->original_host_pointer, nbodies-1023);


}


void k_CUDA_update_positions(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(2, particles);

	int nbodies = particles.X.length();

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	k_update_positions<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(particles, t->dtime);
	checkCUDAError();

	cudaDeviceSynchronize();

	//PrintParticles((particle*)particles.phy_chunk->original_host_pointer, nbodies-1023);


}
