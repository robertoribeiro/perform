#include <iostream>

#include "BarnesHut_job.h"

#include "../../myUtil.h"
#include "../../common.h"
#include "../../perform.h"

#include "../../cuda_queue/cuda_utils.h"
#include "../../kernels.h"
#include "uberkernel.h"

using namespace std;

////// CONFIG

#define WARPSIZE 32

#define WARPS 1

#define N_BLOCKS 1

#define UBK_INBOX_SIZE 30000000

#define LOCAL_INBOX_QUEUE_SIZE (1024*20)           // se local inbox muito grande, rouba muito trabalho pa apenas uma SM. calcular em funçao do child_task_count?
#define LOCAL_OUTBOX_QUEUE_SIZE (1024*500)         //se local outboc muito pequeno força muito o lock
#define MEM_MEM_CHUNKS_PER_THREAD (1024*20) //Each SM has a frees queue. Initially each SM thread will have MEM_MEM_CHUNKS_PER_THREAD frees
///// MACROS

#define THR_PER_BLOCK (WARPSIZE*WARPS)
#define MEM_CHUNKS_PER_SM (THR_PER_BLOCK * MEM_MEM_CHUNKS_PER_THREAD)
#define MEM_CHUNK_SIZE sizeof(ubkTask_ForcesCalc)
#define MEM_TOTAL (MEM_CHUNKS_PER_SM * MEM_CHUNK_SIZE)
#define LOCAL_FREES_QUEUE_SIZE (MEM_CHUNKS_PER_SM + 1024*5) //Extra open slots on each SM frees q
#define TIDX (threadIdx.x)
#define LANEIDX (threadIdx.x & 31)                  // or warp lane index
#define THREAD0(X) \
		if ((threadIdx.x) == 0) { X } \

#define WARP0 (TIDX < 32)

#define THREADG0(X) \
		if (tID == 0) { X } \


//// PROF

#define N_BLOCK_SECTIONS 12
//#define __PROF


#include "../../cuda_queue/workqueue.h"

__device__ Task_ForcesCalc* global_task;
__device__ unsigned int task_exe_count_T;
__device__ unsigned int sub_task_exe_count_T;
__device__ unsigned int loop_count_T;

__device__ unsigned int * task_exe_count;
__device__ unsigned int * sub_task_exe_count;
__device__ unsigned int * loop_count;

__device__ unsigned int frees_left;

__device__ CudaConcurrentQueue<ubkTask_ptr>* inbox;

__device__ ubkTask_ptr* local_inbox_buffer;
__device__ ubkTask_ptr* local_outbox_buffer;
__device__ ubkTask_ptr* local_frees_buffer;

__device__ CudaLocalQueue<ubkTask_ptr>* heads_outbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_inbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_frees;

__device__ FLOAT_ARRAY posx;
__device__ FLOAT_ARRAY posy;
__device__ FLOAT_ARRAY posz;
__device__ FLOAT_ARRAY mass;
__device__ INT_ARRAY childs;
__device__ FLOAT_ARRAY accx;
__device__ FLOAT_ARRAY accy;
__device__ FLOAT_ARRAY accz;
__device__ FLOAT_ARRAY t_accx;
__device__ FLOAT_ARRAY t_accy;
__device__ FLOAT_ARRAY t_accz;
__device__ INT_ARRAY sort_order;

__device__ FLOAT_ARRAY r_accx;
__device__ FLOAT_ARRAY r_accy;
__device__ FLOAT_ARRAY r_accz;
__device__ FLOAT_ARRAY r_t_accx;
__device__ FLOAT_ARRAY r_t_accy;
__device__ FLOAT_ARRAY r_t_accz;

__device__
__forceinline void save_before(double before[N_BLOCK_SECTIONS][THR_PER_BLOCK], int thread,
		int code_block) {
#ifdef __PROF
	before[code_block][thread] = (double) clock();
#endif

}

__device__
__forceinline void save_elapsed(double before[N_BLOCK_SECTIONS][THR_PER_BLOCK],
		double times[N_BLOCK_SECTIONS][THR_PER_BLOCK], int thread, int code_block) {
#ifdef __PROF
	times[code_block][thread] += ((double) clock()) - before[code_block][thread];
#endif

}

//__device__ void k_CUDA_devptr_compute_force(ubkTask_ForcesCalc task,
//		CudaLocalQueue<ubkTask_ptr>* local_frees, ubkTask_ptr bag[], uint& elements_bag/*,
// double before[N_BLOCK_SECTIONS][THR_PER_BLOCK],
// double times[N_BLOCK_SECTIONS][THR_PER_BLOCK]*/) {
//
//	//ubkTask_ForcesCalc task;
//	ubkTask_ptr current_new_task = INIT_TASK;
//	int p;
//	float epssq;
//	float dsq;
//	int current;
//	float drx, dry, drz, nphi, scale, idr;
//	double drsq;
//	float ax, ay, az;
//	short child;
//	short target_slot = 0;
//
//	int nbodies = global_task->nbodies;
//
//	float pos_p_x;
//	float pos_p_y;
//	float pos_p_z;
//
//	//task = *(work_bag[threadIdx.x]);
//
//	atomicAdd((uint*) &(task_exe_count_T), 1);
//	int a = blockIdx.x;
//	//atomicAdd((uint*) &(task_exe_count[a]), 1);
//
//	epssq = global_task->epssq;
//	dsq = task.dsq;
//
//	for (int __it_target = 0; __it_target < task.target_count; __it_target++) {
//
//		atomicAdd((uint*) &sub_task_exe_count_T, 1);
//		//atomicAdd((uint*)&(sub_task_exe_count[a]), 1);
//
//		p = task.p_target[__it_target];
//
//
//		current = task.current_cell[__it_target];
//
//		//if (threadIdx.x == 0)		printf("Executing task %d %d %d %f\n",task.target_count, p, current, dsq);
//
//		if (task.tree_at_root == true) {
//
//			p = sort_order[task.p_target[__it_target]];
//
//			r_t_accx[p] = accx[p];
//			r_t_accy[p] = accy[p];
//			r_t_accz[p] = accz[p];
//
//			r_accx[p] = 0.0;
//			r_accy[p] = 0.0;
//			r_accz[p] = 0.0;
//
//		}
//
//		ax = 0.0;
//		ay = 0.0;
//		az = 0.0;
//
//		child = 0;
//		//mass_pos_p = particles.at(p).mass_pos;
//
//		pos_p_x = posx[p];
//		pos_p_y = posy[p];
//		pos_p_z = posz[p];
//
//		__PROF_START(9);
//
////#pragma unroll 8
//		while (child < 8) {
//
//			__PROF_START(8);
//
//			float pos_x;
//			float pos_y;
//			float pos_z;
//
//			if (childs[current * 8 + child] != -1) {
//				pos_x = posx[childs[current * 8 + child]];
//				pos_y = posy[childs[current * 8 + child]];
//				pos_z = posz[childs[current * 8 + child]];
//			} else {
//				child++;
//				continue;
//			}
//
//			drx = pos_x - pos_p_x;
//			dry = pos_y - pos_p_y;
//			drz = pos_z - pos_p_z;
//
//			// CRITICAL ACCURACY SECTION //
//
//			drsq = powf(drx, 2);
//			drsq += powf(dry, 2);
//			drsq += powf(drz, 2);
//
//			//
//			__PROF_END(8);
//
//			__PROF_START(10);
//
//			//drsq = drx * drx + dry * dry + drz * drz;
//			if (drsq < dsq) {
//				if (IS_CELL(childs[current*8 + child])) {
//
//					if (current_new_task == INIT_TASK) {
//
//						// only required here, only place with warp concurrency
//						local_frees->Acquire();
//						if (!local_frees->dequeue(current_new_task))
//							printf("Error: 9\n");
//						local_frees->Release();
//
//						if (current_new_task != INIT_TASK) {
//							current_new_task->dsq = dsq * 0.25;
//							current_new_task->tree_at_root = false;
//							//bag[elements_bag++] = current_new_task;
//
//
//						}
//					}
//
//					if (current_new_task != INIT_TASK) {
//
//						current_new_task->p_target[target_slot] = p;
//						current_new_task->current_cell[target_slot]	= childs[current*8 + child];
//						current_new_task->target_count = ++target_slot;
//
//						if (target_slot == MAX_P_PER_TASK) {
//							target_slot = 0;
//							current_new_task = INIT_TASK;
//						}
//					} else {
//						printf("Erro 8: tasks lost\n");
//						atomicAdd((int*) &frees_left, 1);
//					}
//
//					child++;
//
//				} else if (IS_PARTICLE(childs[current*8 + child])) {
//
//					if (childs[current*8 + child] != p) {
//
//						drsq += epssq;
//						idr = 1 / sqrtf(drsq);
//						nphi = mass[childs[current*8 + child]] * idr;
//						scale = nphi * idr * idr;
//
//						ax += drx * scale;
//						ay += dry * scale;
//						az += drz * scale;
//
//						//if (p == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);
//
//					}
//					child++;
//
//				} else {
//					child++;
//				}
//
//			} else {
//
//				drsq += epssq;
//				idr = 1 / sqrtf(drsq);
//				//nphi = mass_pos.mass * idr;
//				nphi = mass[childs[current*8 + child]] * idr;
//				scale = nphi * idr * idr;
//
//				ax += drx * scale;
//				ay += dry * scale;
//				az += drz * scale;
//
//				//if (p == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);
//
//				child++;
//
//			}
//
//			__PROF_END(10);
//
//		}__PROF_END(9);
//
//		__PROF_START(11);
//
//		atomicAdd(&(r_accx[p]), ax);
//		atomicAdd(&(r_accy[p]), ay);
//		atomicAdd(&(r_accz[p]), az);
//
//		__PROF_END(11);
//
//	}
//
//}

__device__ void k_CUDA_devptr_compute_force_FULLT(ubkSpecificTask task,
		CudaLocalQueue<ubkTask_ptr>* local_frees, ubkTask_ptr bag[], uint& elements_bag) {

	int p;
	float epssq;
	float dsq;

	float drx, dry, drz, nphi, scale, idr;
	double drsq;
	float ax, ay, az;

	int nbodies = global_task->nbodies;

	float pos_p_x;
	float pos_p_y;
	float pos_p_z;

	THREAD0(atomicAdd((uint*) &(task_exe_count_T), 1);)

	epssq = global_task->epssq;
	dsq = task.dsq;

	p = sort_order[task.p_target_start + threadIdx.x];

	r_t_accx[p] = accx[p];
	r_t_accy[p] = accy[p];
	r_t_accz[p] = accz[p];

	r_accx[p] = 0.0;
	r_accy[p] = 0.0;
	r_accz[p] = 0.0;

	ax = 0.0;
	ay = 0.0;
	az = 0.0;

	pos_p_x = posx[p];
	pos_p_y = posy[p];
	pos_p_z = posz[p];

	__shared__
	volatile int node[100];
	__shared__
	volatile short pos[100];

	register int depth = 0;

	if (0 == threadIdx.x) {
		node[depth] = TREE_ROOT;
		pos[depth] = 0;
	}

	while (depth != 0) {

		while (pos[depth] < 8) {

			float pos_x;
			float pos_y;
			float pos_z;

			int CURRENT = childs[node[depth] * 8 + pos[depth]];
			pos[depth]++;

			if (CURRENT != -1) {
				pos_x = posx[CURRENT];
				pos_y = posy[CURRENT];
				pos_z = posz[CURRENT];
			} else {
				continue;
			}

			drx = pos_x - pos_p_x;
			dry = pos_y - pos_p_y;
			drz = pos_z - pos_p_z;

			// CRITICAL ACCURACY SECTION //

			drsq = powf(drx, 2);
			drsq += powf(dry, 2);
			drsq += powf(drz, 2);

			//drsq = drx * drx + dry * dry + drz * drz;
			if (drsq < dsq) {
				if (IS_CELL(CURRENT)) {
					depth++;
					if (threadIdx.x == 0) {
						dsq *= 0.25;
						node[depth] = CURRENT;
						pos[depth] = 0;

					}
				} else if (IS_PARTICLE(CURRENT)) {

					if (CURRENT != p) {

						drsq += epssq;
						idr = 1 / sqrtf(drsq);
						nphi = mass[CURRENT] * idr;
						scale = nphi * idr * idr;

						ax += drx * scale;
						ay += dry * scale;
						az += drz * scale;

						//if (p == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);

					}
				}

			} else {

				drsq += epssq;
				idr = 1 / sqrtf(drsq);
				//nphi = mass_pos.mass * idr;
				nphi = mass[CURRENT] * idr;
				scale = nphi * idr * idr;

				ax += drx * scale;
				ay += dry * scale;
				az += drz * scale;

				//if (p == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);
			}

		}
		depth--;
	}

	atomicAdd(&(r_accx[p]), ax);
	atomicAdd(&(r_accy[p]), ay);
	atomicAdd(&(r_accz[p]), az);

}

__device__
__forceinline void flush_to_global(CudaLocalQueue<ubkTask_ptr> *& q) {

	ubkTask_ptr t = INIT_TASK;
	while (q->getSize() >= WARPSIZE && !inbox->isFull()) {
		q->dequeue(t) ? : printf("Error: X\n");
		if (!(inbox->enqueue(t))) {
			printf("Error 4\n");
			//__trap();

		}
	}

	t = INIT_TASK;
	if (q->getSize() > 0) {
		if (!inbox->isFull()) {

			if (TIDX < q->getSize()) {
				if (!q->dequeue(t)) {
					printf("Error 5\n");
					//__trap();
				}
			}
			if (!inbox->enqueue(t)) {
				printf("Error 6\n");
				//__trap(); // the outbox size will be different from the valid out tasks count
			}

		} //Outbox pode encher senao tiver espaço na global
		else {
			THREAD0(printf("Error 7\n");)
		}
	}

}

__device__
__forceinline void try_donate(CudaLocalQueue<ubkTask_ptr> *& local_outbox) {

	//THREAD_ID_0(printf("SM donating...\n");)

	if (!inbox->isFull() && !local_outbox->isEmpty() && inbox->tryLock()) {
		flush_to_global(local_outbox);
		inbox->Release();
	}

}

__global__ void uberkernel(Task_ForcesCalc global_task_, Domain<float> domain_input,
		Domain<float> domain_result, ubkTask_ptr* inbox_buffer, void* mem_buffer, void* qs_buffer,
		int child_task_count) {

	int tID = blockIdx.x * blockDim.x + threadIdx.x;

	if (WARP0) {

		if (tID == 0) {
			global_task = &global_task_;
			printf("Inside uberkernel OPT...\n");
			printf("Task size: %d\n", sizeof(ubkSpecificTask));
			printf("Number of warps: %d\n", WARPS);
			printf("Global number of task chunks: %d\n", MEM_CHUNKS_PER_SM * 15);

			//////// User-side ////////

			int nnodes = global_task_.nbodies * 2;
			int nbodies = global_task_.nbodies;

			FLOAT_ARRAY input_chunk = (FLOAT_ARRAY) domain_input.device_data_buffer;
			FLOAT_ARRAY result_chunk = (FLOAT_ARRAY) domain_result.device_data_buffer;

			posx = input_chunk;
			posy = posx + (nnodes);
			posz = posy + (nnodes);

			mass = posz + (nnodes);

			childs = (INT_ARRAY) input_chunk;

			accx = (nnodes) * 4 + (nbodies * (8)) + nbodies * 3 + input_chunk;
			accy = accx + nbodies;
			accz = accy + nbodies;

			t_accx = accz + nbodies;
			t_accy = t_accx + nbodies;
			t_accz = t_accy + nbodies;

			sort_order = (INT_ARRAY) (t_accz + nbodies);

			r_accx = result_chunk + nbodies * 3 + nbodies * 3;
			r_accy = r_accx + nbodies;
			r_accz = r_accy + nbodies;
			r_t_accx = r_accz + nbodies;
			r_t_accy = r_t_accx + nbodies;
			r_t_accz = r_t_accy + nbodies;

			///////////////////////////
		}

		//__shared__
		//double times[N_BLOCK_SECTIONS][THR_PER_BLOCK];
		//__shared__
		//double before[N_BLOCK_SECTIONS][THR_PER_BLOCK];

#ifdef __PROF
		memset(times, 0, sizeof(double) * THR_PER_BLOCK * N_BLOCK_SECTIONS);
#endif

		__PROF_START(0);__PROF_START(1);

		frees_left = 0;

		if (tID == 0) {

			loop_count = new unsigned int[N_BLOCKS + 1];
			task_exe_count = new unsigned int[N_BLOCKS + 1];
			sub_task_exe_count = new unsigned int[N_BLOCKS + 1];

			loop_count[N_BLOCKS] = 0;
			task_exe_count[N_BLOCKS] = 0;
			sub_task_exe_count[N_BLOCKS] = 0;

			loop_count_T = 0;
			task_exe_count_T = 0;
			sub_task_exe_count_T = 0;

			local_inbox_buffer = (ubkTask_ptr*) qs_buffer;
			local_outbox_buffer = local_inbox_buffer + LOCAL_INBOX_QUEUE_SIZE * N_BLOCKS;
			local_frees_buffer = local_outbox_buffer + LOCAL_OUTBOX_QUEUE_SIZE * N_BLOCKS;

			heads_inbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
			heads_outbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
			heads_frees = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];

			inbox = new CudaConcurrentQueue<ubkTask_ptr> (inbox_buffer, child_task_count, UBK_INBOX_SIZE);

				}
			}

			__gpu_sync(N_BLOCKS);

			CudaLocalQueue<ubkTask_ptr>* local_inbox = heads_inbox+blockIdx.x;
			CudaLocalQueue<ubkTask_ptr>* local_outbox = heads_outbox+blockIdx.x;
			CudaLocalQueue<ubkTask_ptr>* local_frees = heads_frees+blockIdx.x;

			if (WARP0)
			{

				if (threadIdx.x == 0)
				{

					loop_count[blockIdx.x] = 0;
					task_exe_count[blockIdx.x] = 0;
					sub_task_exe_count[blockIdx.x] = 0;

					local_inbox->setBuffer((ubkTask_ptr*) (local_inbox_buffer + blockIdx.x *LOCAL_INBOX_QUEUE_SIZE), 0, LOCAL_INBOX_QUEUE_SIZE);
			local_outbox->setBuffer((ubkTask_ptr*) (local_outbox_buffer + blockIdx.x * LOCAL_OUTBOX_QUEUE_SIZE), 0, LOCAL_OUTBOX_QUEUE_SIZE);
			local_frees->setBuffer((ubkTask_ptr*) (local_frees_buffer + blockIdx.x * LOCAL_FREES_QUEUE_SIZE), 0, LOCAL_FREES_QUEUE_SIZE);
		}

		ubkTask_ptr sm_chunk = (ubkTask_ptr)((char*)mem_buffer + blockIdx.x*MEM_TOTAL);

		for (int yyy=0; yyy < WARPS; yyy++){
			for (int yy = 0; yy < MEM_MEM_CHUNKS_PER_THREAD ; yy++){
					//if (TIDX == 0) printf("e: %ld d: %ld l: %d\n",local_frees->_enqueue,local_frees->_dequeue,local_frees->_length);
				if (!local_frees->enqueue(sm_chunk + WARPSIZE*MEM_MEM_CHUNKS_PER_THREAD*yyy + threadIdx.x*MEM_MEM_CHUNKS_PER_THREAD + yy))
				{
					printf("Error here\n");
				}
			}
		}
	}

	__shared__ ubkTask_ptr work_bag[WARPS*WARPSIZE];
	__shared__ uint elements_bag[WARPS*WARPSIZE];
	__shared__ uint elements_bag2[WARPS*WARPSIZE];
	__shared__ ubkTask_ptr out_bag[WARPS*WARPSIZE*MAX_SEC_TASK];

	__gpu_sync(N_BLOCKS);

	//THREAD_ID_0(printf("local frees count SM: %d count: %ld\n",blockIdx.x,local_frees->getSize());)

	__PROF_END(1);
	__PROF_START(2);

	////////// PARTY START /////////////

	THREADG0(printf("Loop started...\n");)

	while (!inbox->isEmpty() || !__all(heads_inbox[LANEIDX % N_BLOCKS].isEmpty()) || !__all(heads_outbox[LANEIDX % N_BLOCKS].isEmpty()))
	{

		//if (tID == 0 && task_exe_count % 10000 == 0 ) printf("Tasks executed: %d\n",task_exe_count);

		//__syncthreads();

		if (WARP0)
		{

			if (threadIdx.x == 0)
			{
				//atomicAdd((int*)&loop_count_T,1);
				int a = blockIdx.x; // optimized compiler problem
				//loop_count[a]++;
			}

			__PROF_START(3);

			//Se a inbox esta vazia e a outbox tem trabalho, despeja na inbox ate ficar cheia ou não haver mais trabalho
			while (!local_inbox->isFull() && !local_outbox->isEmpty())
			{
				ubkTask_ptr t_item = INIT_TASK;
				if (local_outbox->dequeue(t_item) && t_item != NULL)
				{
					local_inbox->enqueue(t_item) ? : printf("Error 0\n");
				}
			}

			//Se a inbox não estiver cheia tenta roubar trabalho na global
			if (!local_inbox->isFull() && inbox->tryLock())
			{
				ubkTask_ptr t_item = INIT_TASK;
				while ( !local_inbox->isFull() && !inbox->isEmpty() )
				{
					if (inbox->dequeue(t_item) && (t_item != NULL))
					{
						if (!local_inbox->enqueue(t_item))
						{
							printf("Error 1\n");
							//__brkpt();
						}
					}
				}
				inbox->Release();
			}

			// work donation using global inbox
			try_donate(local_outbox);

			if (local_outbox->isFull())
			{
				THREAD0(printf("%d Forcing lock 0\n",blockIdx.x);)
                inbox->Acquire();
				flush_to_global(local_outbox);
				inbox->Release();
			}

			__PROF_END(3);

			__PROF_START(4);

		}

		work_bag[threadIdx.x] = NULL;

		if (WARP0 && !local_inbox->isEmpty() && local_inbox->dequeue_bag(work_bag,WARPS))
		{

			if (local_frees->getSize() < THR_PER_BLOCK*MAX_SEC_TASK)
			{
				printf("Error: not enough frees for execution %u\n",local_frees->getSize());

					if (TIDX < N_BLOCKS)
					{
						printf("Error: not enough frees for execution %u\n",heads_frees[TIDX % N_BLOCKS].getSize());
					}

			}

		}

		__syncthreads();

		__PROF_START(5);

		//if (threadIdx.x == 0)		printf("Executing task %d %d %f\n", t->start, t->current_cell, t->dsq);
		elements_bag[threadIdx.x] = 0;
		for (int ggg=0; ggg < MAX_SEC_TASK; ggg++)	out_bag[threadIdx.x+ggg] = NULL;


		//__threadfence_block();

		if (work_bag[threadIdx.x] != NULL){
			k_CUDA_devptr_compute_force_FULLT(*work_bag[threadIdx.x],local_frees,
					out_bag + TIDX*MAX_SEC_TASK, elements_bag[TIDX]/*,before,times*/);
			// printf("%d produced %d tasks\n", threadIdx.x,elements_bag[TIDX]);

		}

		//__threadfence_block();

		__PROF_END(5);

		__syncthreads();

		//	if(work_bag[0]->tree_at_root) atomicAdd(&count_root,1);
		if (WARP0)
			for (int warp_i =0; warp_i < WARPS; warp_i++)
			{
				if (work_bag[threadIdx.x] != NULL)
					if (!local_frees->isFull())
						if (!local_frees->enqueue(work_bag[threadIdx.x])) printf("Error 2\n");
			}

		//THREAD0(printf("local_frees %d\n",local_frees->getSize());)

		__PROF_START(6);


		// Total task created per SM -> elements_bag2[0]
		elements_bag2[threadIdx.x] = elements_bag[threadIdx.x];

		__threadfence_block();
		__syncthreads();


		for (int KK=THR_PER_BLOCK/2; KK > 0; KK/=2)
			if (threadIdx.x < KK) elements_bag2[threadIdx.x] += elements_bag2[threadIdx.x+KK];

		__threadfence_block();

		__syncthreads();



		if (WARP0)
		{
			try_donate(local_outbox);

			if (local_outbox->getSize()+elements_bag2[0] >= local_outbox->_length)
			{
				THREAD0(printf("%d Forcing lock 1\n",blockIdx.x);)
                				inbox->Acquire();
				flush_to_global(local_outbox);
				inbox->Release();
			}

			if (local_outbox->getSize()+elements_bag2[0] > local_outbox->_length)
			{
				printf("Error: still not enough room\n");
			}

			uint before;

			if (LANEIDX==0) {
				before = local_outbox->getSize();
				//printf(" %d b: %d e: %d\n",local_outbox->getSize(),before,elements_bag2[0]);

			}


			for (int warp_i =0; warp_i < WARPS; warp_i++)
			{
				if (elements_bag[warp_i*WARPSIZE + TIDX] !=0 )
				{
					if (!local_outbox->enqueue_bag(out_bag + (warp_i*WARPSIZE*MAX_SEC_TASK) + TIDX*MAX_SEC_TASK, elements_bag[warp_i*WARPSIZE+TIDX]))
					{
						printf("Error 3\n");
						//printf("e: %d\n",local_outbox->getSize());
						// printf("e: %d\n",local_outbox->_length);
						//__brkpt();
					}
				}
			}

			if (LANEIDX==0) {
				if (elements_bag2[0] + before != local_outbox->getSize()){
					if (before !=0) printf("Error 10 - %u b: %u e: %u\n",local_outbox->getSize(),before,elements_bag2[0]);
				}
			}

			try_donate(local_outbox);
		}

		__PROF_END(6);

		__PROF_END(4);

		//__threadfence_block();

		__syncthreads();

	}

	//THREAD_ID_0(printf("local frees count SM: %d count: %ld\n",blockIdx.x,local_frees->getSize());)

	__PROF_END(2);
	__PROF_START(7);

	__gpu_sync(N_BLOCKS);

	__PROF_END(7);

	__PROF_END(0);

#ifdef __PROF
	for (int tt = 0; tt < N_BLOCK_SECTIONS; tt++)
	{

		int nTotalThreads = THR_PER_BLOCK;
		while (nTotalThreads > 1)
		{
			int halfPoint = (nTotalThreads >> 1); // divide by two
			if (T_IDX < halfPoint)
			{
				double temp = times[tt][T_IDX + halfPoint];
				if (temp > times[tt][T_IDX]) times[tt][T_IDX] = temp;
			}
			__syncthreads();
			nTotalThreads = (nTotalThreads >> 1); // divide by two.
		}
	}

	if (T_IDX == 0)
	{
		double elapsed[N_BLOCK_SECTIONS];

		for (int it = 0; it <N_BLOCK_SECTIONS; it++)
		{
			elapsed[it] = ((double)times[it][0])/1400000.0;
		}

		printf("%2d; | 0; %5.2lf | 1; %5.2lf | 2; %5.2lf | 3; %5.2lf | 4; %5.2lf | 5; %5.2lf | 6; %5.2lf | 7; %5.2lf | 8; %5.2lf | 9; %5.2lf | 10; %5.2lf | 11; %5.2lf ;(ms)\n",blockIdx.x,
				elapsed[0],elapsed[1],elapsed[2],elapsed[3],elapsed[4],elapsed[5],elapsed[6],elapsed[7],elapsed[8],elapsed[9],elapsed[10],elapsed[11]);

	}

	if (tID == 0)
	{

		printf("\n0 - Total\n");
		printf("1 - Initialization before loop\n");
		printf("2 - Total loop\n");
		printf("3 - Inside loop: Queue management\n");
		printf("4 - Inside loop: Total of if statement that controls successful work fetch from inbox\n");
		printf("5 - Inside loop: Inside if: execution\n");
		printf("6 - Inside loop: Inside if: enqueue bags with new tasks in outbox(if not enough room force to global\n");
		printf("7 - Wait for the others time\n\n");

	}
#endif


	if (WARP0 && TIDX==0){

		printf("%d - %u %u\n",blockIdx.x, local_frees->getSize(), local_outbox->_dequeue);

	}
	if (tID == 0)
	{

		// delete[] (char*)local_inbox_buffer;
		// delete[] (char*)local_outbox_buffer;
		// delete[] (char*)local_frees_buffer;

		delete heads_inbox;
		delete heads_outbox;
		delete heads_frees;

		for (int o= 0; o< N_BLOCKS; o++){
			//printf("\nLoops %ld Executed %ld Executed sub %ld", loop_count[o],task_exe_count[o],sub_task_exe_count[o]);
			// printf("\nE\n", task_exe_count);
			//printf("\n \n", sub_task_exe_count);


		}

		printf("\nLoops total %u\n",loop_count_T);
		printf("\nExecuted total %u\n",task_exe_count_T);
		printf("\nExecuted sub total %u\n",sub_task_exe_count_T);

		printf("\nFrees required %u \n", frees_left);
		printf("Elements in inbox %u \n", inbox->getSize());
		printf("Elements in local_inbox %u \n", local_inbox->getSize());
		printf("Elements in local_outbox %u \n", local_outbox->getSize());

	}

}

void k_CUDA_wrapper_uberkernel(Task* t, ubkTask** tasks_, int task_count) {

	PERFORM_acc_timer *TOTAL = new PERFORM_acc_timer();
	PERFORM_acc_timer *PRE_FORCES = new PERFORM_acc_timer();
	PERFORM_acc_timer *FORCES = new PERFORM_acc_timer();
	PERFORM_acc_timer *UPD = new PERFORM_acc_timer();

	TOTAL->start();
	PRE_FORCES->start();

	checkCUDAmemory("start");

	printf("Task size: %d\n", (int) sizeof(ubkSpecificTask));

	Domain<float> domain_input;
	t->getDomain(0, domain_input);

	Domain<float> domain_result;
	t->getDomain(1, domain_result);

	//user task object marshalling
	ubkSpecificTask* tasks = new ubkSpecificTask[task_count];
	for (int f = 0; f < task_count; f++) {
		tasks[f] = *((ubkSpecificTask*) *(tasks_ + f));
	}

	ubkSpecificTask* d_tasks;
	cudaMalloc((void**) &d_tasks, task_count * sizeof(ubkSpecificTask));
	printf("UBK: Allocated %d for primary tasks\n",
			(int) (task_count * sizeof(ubkSpecificTask)) / 1024 / 1024);

	cudaMemset(d_tasks, 0, task_count * sizeof(ubkSpecificTask));
	checkCUDAError();

	cudaMemcpy(d_tasks, tasks, (task_count) * sizeof(ubkSpecificTask), cudaMemcpyHostToDevice);
	checkCUDAError();

	//Breaking tasks' array into pointers
	//why host side?
	ubkSpecificTask* tasks_ptrs[task_count];
	for (int f = 0; f < task_count; f++) {
		tasks_ptrs[f] = d_tasks + f;
	}

	ubkSpecificTask** d_inbox_buffer;
	cudaMalloc((void**) &d_inbox_buffer, UBK_INBOX_SIZE * sizeof(ubkSpecificTask*));
	printf("UBK: Allocated %d for d_inbox_buffer\n",
			(int) (UBK_INBOX_SIZE * sizeof(ubkTask_ForcesCalc*)) / 1024 / 1024);

	cudaMemset(d_inbox_buffer, 0, UBK_INBOX_SIZE * sizeof(ubkSpecificTask*));
	checkCUDAError();

	cudaMemcpy(d_inbox_buffer, tasks_ptrs, (task_count) * sizeof(ubkSpecificTask*),
			cudaMemcpyHostToDevice);
	checkCUDAError();

	void* d_mem_buffer;
	cudaMalloc((void**) &d_mem_buffer, MEM_TOTAL * N_BLOCKS);
	printf("UBK: Allocated %d for task memory chunks\n", (int) (MEM_TOTAL * N_BLOCKS) / 1024 / 1024);

	cudaMemset(d_mem_buffer, 0, MEM_TOTAL * N_BLOCKS);
	checkCUDAError();

	void* d_queues_buffer;
	cudaMalloc(
			(void**) &d_queues_buffer,
			N_BLOCKS * sizeof(ubkTask_ptr) * (LOCAL_INBOX_QUEUE_SIZE + LOCAL_OUTBOX_QUEUE_SIZE
					+LOCAL_FREES_QUEUE_SIZE));

	printf(
			"UBK: allocated %d for queues\n",
			((sizeof(ubkTask_ptr) * LOCAL_INBOX_QUEUE_SIZE * N_BLOCKS) + (sizeof(ubkTask_ptr)
					* LOCAL_OUTBOX_QUEUE_SIZE * N_BLOCKS) + (sizeof(ubkTask_ptr)
					* LOCAL_FREES_QUEUE_SIZE * N_BLOCKS)) / 1024 / 1024);

	printf("Launching uberkernel for %d particles...\n", task_count);

	checkCUDAmemory("before launch");

	PRE_FORCES->stop();
	FORCES->start();

	uberkernel<<<N_BLOCKS,THR_PER_BLOCK,0,t->stream>>>
	(*(Task_ForcesCalc*) t, domain_input, domain_result, d_inbox_buffer, d_mem_buffer, d_queues_buffer, task_count);
	checkCUDAError();

	cudaStreamSynchronize(t->stream);
	FORCES->stop();

	cudaFree(d_tasks);
	cudaFree(d_inbox_buffer);
	cudaFree(d_mem_buffer);
	cudaFree(d_queues_buffer);
	checkCUDAError();

	checkCUDAmemory("after launch");

	//////////////////////////

	UPD->start();

	k_CUDA_update_vel_positions(t);

	cudaStreamSynchronize(t->stream);

	UPD->stop();

	TOTAL->stop();

	printf("\nTOTAL: %s\n\n", TOTAL->print());
	printf("PRE_FORCES: %s\n", PRE_FORCES->print());
	printf("FORCES: %s\n", FORCES->print());
	printf("UPD: %s\n", UPD->print());

}
