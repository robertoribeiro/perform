#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut_job.h"

using namespace std;
using namespace tbb;


double jobBARNES_HUT(int argc, char *argv[]) {

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error
	BH_TYPE diameter, centerx, centery, centerz;
	BH_TYPE radius;

	int nbodies = atoi(argv[6]);
	int ncells = nbodies;
	int nnodes = nbodies+ncells;
	int timesteps = atoi(argv[5]);
	int tree_height = 0;

	char* filename = argv[4];

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();


	particle* particles;
	cell* nodes;
	particle * particles_new = new particle[nbodies];


	FLOAT_ARRAY input_chunk;
	FLOAT_ARRAY result_chunk;

	int input_chunk_length = (nnodes)*4 //posxyz,mass for particles and cells
			+ (nbodies*(8)) //childs
			+ nbodies *3*3// vel,acc,t_acc;
			+ nbodies; // sort

	int result_chunk_length = (nbodies)*3 //posxyz for particles and cells
				+ nbodies * 9; // vel,acc,t_acc;

	input_chunk = new float[input_chunk_length];
	result_chunk = new float[result_chunk_length];

	memset(input_chunk,0,input_chunk_length*sizeof(float));
	memset(result_chunk,0,result_chunk_length*sizeof(float));

	FLOAT_ARRAY posx= input_chunk;
	FLOAT_ARRAY posy= posx + (nnodes);
	FLOAT_ARRAY posz= posy + (nnodes);
	FLOAT_ARRAY mass= posz + (nnodes);
	INT_ARRAY childs = (INT_ARRAY)input_chunk;

	FLOAT_ARRAY r_posx= result_chunk;
	FLOAT_ARRAY r_posy= r_posx + (nbodies);
	FLOAT_ARRAY r_posz= r_posy + (nbodies);
	INT_ARRAY sort_order = (INT_ARRAY)input_chunk + (nnodes)*4 + (nbodies*(8)) + nbodies *3*3;

	INT_ARRAY particle_count = new int[nnodes];



	load_data_set_OPT(posx, posy, posz ,mass, filename, nbodies, timesteps, dtime, eps, tol);

	memcpy(r_posx,posx,nbodies*sizeof(float));
	memcpy(r_posy,posy,nbodies*sizeof(float));
	memcpy(r_posz,posz,nbodies*sizeof(float));

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	Domain<float> *domain_input = new Domain<float> (1, R, input_chunk, dim_space(0, input_chunk_length));
	Domain<float> *domain_result = new Domain<float> (2, RW, result_chunk, dim_space(0, result_chunk_length));

	float dice_v;

	dice_v = atoi(argv[3]) == 0 ? 0 : 1.0 / atoi(argv[3]);

	for (int step = 0; step < timesteps; step++) {

		compute_bounding_box_OPT(posx,posy,posz, nbodies, diameter, centerx, centery, centerz);

		posx[TREE_ROOT] = centerx;
		posy[TREE_ROOT] = centery;
		posz[TREE_ROOT] = centerz;

		for (int k = 0; k < 8; k++)
			childs[TREE_ROOT*8+k] = -1;

		radius = diameter * 0.5;

		ncells = build_tree_array_OPT( posx,  posy,  posz,  childs,radius, nbodies,nnodes, tree_height);

		memset(particle_count,0,sizeof(int)*nnodes);
		compute_center_of_mass_OPT(posx,  posy,  posz,  childs, particle_count, mass,tree_height,nnodes,nbodies);

		memset(sort_order,0,sizeof(int)*nbodies);
		sort(childs,sort_order,particle_count,nbodies,nnodes,ncells);

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different
//
//		if (step == 0)
//			PERFORM_signal_JobStarted();
//
		timer->start();

		Task_ForcesCalc* t;

		 t = new Task_ForcesCalc(IRREGULAR);
		 t->device_preference= GPU0;

		t->associate_domain(domain_input);
		t->associate_domain(domain_result);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		t->nbodies = nbodies;

		t->diceable = dice_v == 0 ? false : true;

		//t->associate_kernel(CPU, &k_TBB_compute_force);
		//t->associate_kernel(GPU, &k_CUDA_compute_force);

		performQ->addTasks(t);
//
		wait_for_all_tasks();
//
//		if (step + 1 == timesteps)
//			PERFORM_signal_taskSubmissionEnded();
//
		performDL->splice(domain_result);
//
//	//	PrintParticles((particle*)domain_new_particles->phy_chunk->original_host_pointer, nbodies-1023);
//
//
		timer->stop();
//
//		if (step + 1 == timesteps)
//			PERFORM_signal_JobFinished(timer->duration);
//
//		performDL->delete_domain(domain_nodes);
//
//		Domain<struct s_particle> *tmp;
//		tmp = domain_particles;
//		domain_particles = domain_new_particles;
//		domain_new_particles = tmp;
//
//		domain_particles->intention = R;
//		domain_new_particles->intention = RW;
	}

	printf("DURATION: %s\n",timer->print());

	//PrintParticles((particle*)domain_new_particles->phy_chunk->original_host_pointer, nbodies-1023);

	particle* particles_to_check = new particle[nbodies];

	for (int i = 0; i < (nbodies); i++) {

		particles_to_check[i].mass_pos.x = r_posx[i];
		particles_to_check[i].mass_pos.y = r_posy[i];
		particles_to_check[i].mass_pos.z = r_posz[i];

	}

	jobBARNES_HUT_check(argc,argv,particles_to_check);

	return timer->duration;

}
