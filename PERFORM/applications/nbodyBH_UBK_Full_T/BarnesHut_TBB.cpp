#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut_TBB.h"

using namespace std;
using namespace tbb;

void compute_force(Domain<cell> cells, Domain<particle> particles, Domain<particle> particles_new,
		int p, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {

	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
	BH_TYPE ax, ay, az;

	particle x = particles.at(p);

	ax = x.accx;
	ay = x.accy;
	az = x.accz;

	x.accx = 0.0;
	x.accy = 0.0;
	x.accz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;

	dsq *= 0.25;

	mass_position mass_pos_p = x.mass_pos;

	while (!(stack_top == 0 && child == 8)) {

		if (child == 8) {
			//current = cells[current].parent_cell_index;
			current = cells.at(current).parent_cell_index;
			dsq *= 4;
			child = stack[--stack_top];
			child++;

		} else {

			mass_position mass_pos;
			if (cells.at(current).child_types[child] == PARTICLE)
				mass_pos = particles.at(cells.at(current).child_indexes[child]).mass_pos;
			else if (cells.at(current).child_types[child] == CELL)
				mass_pos = cells.at(cells.at(current).child_indexes[child]).mass_pos;
			else {
				child++;
				continue;
			}

			drx = mass_pos.x - mass_pos_p.x;
			dry = mass_pos.y - mass_pos_p.y;
			drz = mass_pos.z - mass_pos_p.z;

			drsq = drx * drx + dry * dry + drz * drz;
			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
			if (drsq < dsq) {
				if (cells.at(current).child_types[child] == CELL) {
					dsq *= 0.25;
					current = cells.at(current).child_indexes[child];
					stack[stack_top++] = child;
					child = 0;
				} else if (cells.at(current).child_types[child] == PARTICLE) {
					if (mass_pos.id != mass_pos_p.id) {
						drsq += epssq;
						idr = 1 / sqrt(drsq);
						nphi = mass_pos.mass * idr;
						scale = nphi * idr * idr;
						x.accx += drx * scale;
						x.accy += dry * scale;
						x.accz += drz * scale;

					}
					child++;
				} else if (cells.at(current).child_types[child] == NILL) {
					child++;
				}
			} else {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = mass_pos.mass * idr;
				scale = nphi * idr * idr;
				x.accx += drx * scale;
				x.accy += dry * scale;
				x.accz += drz * scale;
				child++;
			}

		}
	}

	BH_TYPE dthf = 0.5 * dtime;

	if (step > 0) {
		x.velx += (x.accx - ax) * dthf;
		x.vely += (x.accy - ay) * dthf;
		x.velz += (x.accz - az) * dthf;
	}

	particles_new.at(p) = x;

	/*PrintBH_TYPE(particles[particle].velx);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].vely);
	 printf("   ");
	 PrintBH_TYPE(par ticles[particle].velz);
	 printf("\n");*/

}

void update_positions(Domain<particle> particles, int part, BH_TYPE dtime) {

	BH_TYPE dthf = 0.5 * dtime;

	BH_TYPE dvelx, dvely, dvelz;
	BH_TYPE velhx, velhy, velhz;

	particle* p = &(particles.at(part));

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;
}

void k_TBB_compute_force(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(0, particles);

	Domain<cell> nodes;
	t->getDomain(1, nodes);

	Domain<particle> particles_new;
	t->getDomain(2, particles_new);

	tbb::parallel_for(
			blocked_range<int> (particles_new.X.start, particles_new.X.end),
			ComputeForceSet(nodes, particles, particles_new, t->tree_height, t->dtime, t->epssq,
					t->dsq, t->step));

	tbb::parallel_for(blocked_range<int> (particles_new.X.start, particles_new.X.end),
			UpdatePositionSet(particles_new, t->dtime));

}

