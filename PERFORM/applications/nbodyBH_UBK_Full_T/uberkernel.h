/*
 * uberkernel.h
 *
 *  Created on: Jun 14, 2011
 *      Author: rr
 */
#ifndef UBERKERNEL_H
#define UBERKERNEL_H

#ifdef __PROF
#define __PROF_START(SECTION) save_before(before, threadIdx.x, SECTION)
#define __PROF_END(SECTION) save_elapsed(before,times,threadIdx.x,SECTION)
#else
#define __PROF_START(SECTION)
#define __PROF_END(SECTION)
#endif

#define INIT_TASK NULL
typedef ubkSIMDTask_ForcesCalc ubkSpecificTask;
typedef ubkSpecificTask* ubkTask_ptr;

#endif /* UBERKERNEL_H_ */
