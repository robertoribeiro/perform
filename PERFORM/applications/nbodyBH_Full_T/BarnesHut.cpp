#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut.h"

using namespace std;
using namespace tbb;

void compute_force(Domain<cell> cells, Domain<particle> particles, Domain<particle> particles_new,
		int p, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {

	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
	BH_TYPE ax, ay, az;

	particle x = particles.at(p);

	ax = x.accx;
	ay = x.accy;
	az = x.accz;

	x.accx = 0.0;
	x.accy = 0.0;
	x.accz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;

	dsq *= 0.25;

	mass_position mass_pos_p = x.mass_pos;

	while (!(stack_top == 0 && child == 8)) {

		if (child == 8) {
			//current = cells[current].parent_cell_index;
			current = cells.at(current).parent_cell_index;
			dsq *= 4;
			child = stack[--stack_top];
			child++;

		} else {

			mass_position mass_pos;
			if (cells.at(current).child_types[child] == PARTICLE)
				mass_pos = particles.at(cells.at(current).child_indexes[child]).mass_pos;
			else if (cells.at(current).child_types[child] == CELL)
				mass_pos = cells.at(cells.at(current).child_indexes[child]).mass_pos;
			else {
				child++;
				continue;
			}

			drx = mass_pos.x - mass_pos_p.x;
			dry = mass_pos.y - mass_pos_p.y;
			drz = mass_pos.z - mass_pos_p.z;

			drsq = drx * drx + dry * dry + drz * drz;
			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
			if (drsq < dsq) {
				if (cells.at(current).child_types[child] == CELL) {
					dsq *= 0.25;
					current = cells.at(current).child_indexes[child];
					stack[stack_top++] = child;
					child = 0;
				} else if (cells.at(current).child_types[child] == PARTICLE) {
					if (mass_pos.id != mass_pos_p.id) {
						drsq += epssq;
						idr = 1 / sqrt(drsq);
						nphi = mass_pos.mass * idr;
						scale = nphi * idr * idr;
						x.accx += drx * scale;
						x.accy += dry * scale;
						x.accz += drz * scale;

					}
					child++;
				} else if (cells.at(current).child_types[child] == NILL) {
					child++;
				}
			} else {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = mass_pos.mass * idr;
				scale = nphi * idr * idr;
				x.accx += drx * scale;
				x.accy += dry * scale;
				x.accz += drz * scale;
				child++;
			}

		}
	}

	BH_TYPE dthf = 0.5 * dtime;

	if (step > 0) {
		x.velx += (x.accx - ax) * dthf;
		x.vely += (x.accy - ay) * dthf;
		x.velz += (x.accz - az) * dthf;
	}

	particles_new.at(p) = x;

	/*PrintBH_TYPE(particles[particle].velx);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].vely);
	 printf("   ");
	 PrintBH_TYPE(par ticles[particle].velz);
	 printf("\n");*/

}

void k_TBB_compute_force(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(0, particles);

	Domain<cell> nodes;
	t->getDomain(1, nodes);

	Domain<particle> particles_new;
	t->getDomain(2, particles_new);

	tbb::parallel_for(
			blocked_range<int> (particles_new.X.start, particles_new.X.end),
			ComputeForceSet(nodes, particles, particles_new, t->tree_height, t->dtime, t->epssq,
					t->dsq, t->step));

	tbb::parallel_for(blocked_range<int> (particles_new.X.start, particles_new.X.end),
			UpdatePositionSet(particles_new, t->dtime));

}

//void k_TBB_update_positions(Task* t_) {
//
//	//Task_UpdatePos* t = (Task_UpdatePos*) t_;
//
//	Domain<particle> particles;
//	//t->getDomain(0, particles);
//
//	tbb::parallel_for(blocked_range<int> (particles.X.start, particles.X.end),
//			UpdatePositionSet(particles, t->dtime));
//
//}

void update_positions(Domain<particle> particles, int part, BH_TYPE dtime) {

	BH_TYPE dthf = 0.5 * dtime;

	BH_TYPE dvelx, dvely, dvelz;
	BH_TYPE velhx, velhy, velhz;

	particle* p = &(particles.at(part));

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;
}

double jobBARNES_HUT(int argc, char *argv[]) {

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	PERFORM_timer* timer = new PERFORM_timer();

	initBARNES_HUT( argc,argv, nbodies, timesteps,  dtime,  eps,  tol);

	particle* particles = new particle[nbodies];

	//load_data_set2(particles, filename, nbodies, timesteps, dtime, eps, tol);

	Point3D* points = new Point3D[nbodies];
	GenerasteGaussPointSet(points,nbodies);
	PointsToParticleSystem(points,nbodies,particles);


	int nnodes = nbodies * 2;

	cell* nodes;

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	Domain<struct s_particle> *domain_particles = new Domain<struct s_particle> (1, R, particles,
			dim_space(0, nbodies));

	particle * particles_new = new particle[nbodies];

	memcpy(particles_new, particles, nbodies * sizeof(particle));

	Domain<struct s_particle> *domain_new_particles = new Domain<struct s_particle> (1, W,
			particles_new, dim_space(0, nbodies));

	float dice_v;
	//	if (nbodies >= 4096)
	//		dice_v = nbodies / 4096;
	//	else
	//		dice_v = 1;

	dice_v = atoi(argv[3]) == 0 ? 0 : 1.0 / atoi(argv[3]);

	//timer->start();

	for (int step = 0; step < timesteps; step++) {

		particle * particles_buffer =
				(particle*) domain_particles->phy_chunk->original_host_pointer;
		BH_TYPE diameter, centerx, centery, centerz;

		compute_bounding_box(particles_buffer, nbodies, diameter, centerx, centery, centerz);

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)	root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)	root.child_indexes[k] = -1;

		nodes = new cell[nnodes];

		nodes[0] = root;

		const BH_TYPE radius = diameter * 0.5;

		cell_count = build_tree_array(nodes, radius, particles_buffer, nbodies, tree_height);

		compute_center_of_mass(nodes, particles_buffer, 0, tree_height,0);

		Domain<struct cell_s> *domain_nodes = new Domain<struct cell_s> (1, R, nodes,
				dim_space(0, nnodes));

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different

		if (step == 0)
			PERFORM_signal_JobStarted();
		timer->start();

		Task_ForcesCalc* t = new Task_ForcesCalc(TASK_FORCE_TYPE);

		t->associate_domain(domain_particles);
		t->associate_domain(domain_nodes);
		t->associate_domain(domain_new_particles);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		t->parent_host_task = (void*) t;
		t->diceable = dice_v == 0 ? false : true;
		t->dice_value = PM_Metric_normalized(dice_v);

		t->associate_kernel(CPU, &k_TBB_compute_force);
		t->associate_kernel(GPU, &k_CUDA_compute_force);

		performQ->addTasks(t);

		wait_for_all_tasks();

		if (step + 1 == timesteps)
			PERFORM_signal_taskSubmissionEnded();

		performDL->splice(domain_new_particles);

		timer->stop();

		if (step + 1 == timesteps)
			PERFORM_signal_JobFinished(timer->duration);

		performDL->delete_domain(domain_nodes);

		Domain<struct s_particle> *tmp;
		tmp = domain_particles;
		domain_particles = domain_new_particles;
		domain_new_particles = tmp;

		domain_particles->intention = R;
		domain_new_particles->intention = W;
	}



	fprintf(stderr,"DURATION: %f\n",timer->duration);


	//PrintParticles((particle*)domain_particles->phy_chunk->original_host_pointer, nbodies-1000);


//#ifdef _DEBUG
#ifdef TEST
	jobBARNES_HUT_check(argc,argv,points,(particle*)domain_particles->phy_chunk->original_host_pointer);
#endif
//#endif

	//	timer->print();

	return timer->duration;

}

