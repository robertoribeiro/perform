#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "../../task/Task.h"
//#include "BarnesHut.h"

#define MAX_SEC_TASK 8
#define MAX_P_PER_TASK 4


class ubkTask_ForcesCalc /*: public ubkTask*/ {
public:

	short target_count;
	int p_target[MAX_P_PER_TASK];
	//float dtime;
	//float epssq;
	float dsq;
	bool tree_at_root;
	//short step;
	int current_cell[MAX_P_PER_TASK];

	__H_D__
	ubkTask_ForcesCalc() {
	}



};

class Task_ForcesCalc: public Task {
public:

	int tree_height;
	int start;

	float dtime;
	float epssq;
	float dsq;

	bool tree_at_root;
	int step;

	int current_cell;

	__H_D__
	Task_ForcesCalc() :
		Task() {
	}
	__H_D__
	Task_ForcesCalc(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<particle>* particles;
		getDomain(0, particles);

		Domain<cell>* nodes;
		getDomain(1, nodes);

		Domain<particle>* p_particles_new;
		getDomain(2, p_particles_new);

		float N = p_particles_new->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);

		if (divide_task_by == -1) {
			divide_task_by = p_particles_new->X.length();

			if (divide_task_by % 32 != 0) {
				printf("Error: devide parameter for uberkernel needs to be a divisor of 32\n");
				exit(-1);
			};
		}

		int block_step = (p_particles_new->X.length() / divide_task_by);

		Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {

			Task_ForcesCalc* t = new Task_ForcesCalc(TASK_FORCE_TYPE);

			dim_space a = dim_space(p_particles_new->X.start + i * block_step,
					(p_particles_new->X.start + i * block_step) + block_step);

			Domain<particle>* particles_new = new Domain<particle> (p_particles_new, 1, RW, a);

			t->associate_domain(particles);
			t->associate_domain(nodes);
			t->associate_domain(particles_new);

			t->tree_height = tree_height;
			t->dtime = dtime;
			t->epssq = epssq;
			t->dsq = dsq;
			t->step = step;
			t->start = a.start;
			t->current_cell = 0;
			t->device_preference = device_preference;
			t->parent_host_task = (void*) this;
			t->wl = PM_Metric_normalized((block_step * wl.value) / N);
			t->dice_lvl = dice_lvl + 1;

			t->associate_kernel(this);

			new_tasks[task_count++] = t;

		}

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

	int diceTo(ubkTask**& new_tasks_, int n_bwu) {

		int task_count = 0;

		Domain<particle>* particles;
		getDomain(0, particles);

		Domain<cell>* nodes;
		getDomain(1, nodes);

		Domain<particle>* p_particles_new;
		getDomain(2, p_particles_new);

		float N = p_particles_new->X.length();

		int divide_task_by = N / n_bwu;

		int block_step = n_bwu;

		ubkTask_ForcesCalc** new_tasks = new ubkTask_ForcesCalc*[divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {

			ubkTask_ForcesCalc* t = new ubkTask_ForcesCalc();

			//t->dtime = dtime;
			//t->epssq = epssq;
			t->dsq = dsq*0.25;
			//t->step = step;
			t->p_target[0] = p_particles_new->X.start + i * block_step;
			t->current_cell[0] = 0;
			t->tree_at_root = true;
			t->target_count=1;
			new_tasks[task_count++] = t;

		}

		new_tasks_ = (ubkTask**) new_tasks;
		return task_count;
	}

};

#endif /* UBK_TASK_H_ */
