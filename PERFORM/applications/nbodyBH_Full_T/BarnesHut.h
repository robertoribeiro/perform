#ifndef BARNESHUT_H_
#define BARNESHUT_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "../../perform.h"

using namespace std;
using namespace tbb;



//#define TASK_FORCE_TYPE_IRREGULAR

#ifdef TASK_FORCE_TYPE_IRREGULAR
#define TASK_FORCE_TYPE IRREGULAR
#else
#define TASK_FORCE_TYPE REGULAR
#endif

#include "../nbodyBH_common/BarnesHut_common.h"
#include "user_defined_task.h"


void compute_force(Domain<cell> cells, Domain<particle> particles, Domain<particle> particles_new, int particle, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step);
void update_positions(Domain<particle> particles, int part, BH_TYPE dtime);

void k_TBB_compute_force(Task* t);
void k_TBB_update_positions(Task* t);
void k_CUDA_compute_force(Task* t);
void k_CUDA_update_positions(Task* t);

double jobBARNES_HUT(int argc, char *argv[]);

class ComputeForceSet {
public:

	Domain<cell> cells;
	Domain<particle> particles;
	Domain<particle> particles_new;
	int tree_height;
	BH_TYPE dtime;
	BH_TYPE epssq;
	BH_TYPE dsq;
	int step;

	void operator()(const blocked_range<int>& range) const {
		for (int i = range.begin(); i != range.end(); i++) {
			compute_force(cells, particles,particles_new, i, tree_height, dtime, epssq, dsq, step);
		}
	}

	ComputeForceSet(Domain<cell> cells_, Domain<particle> particles_, Domain<particle> particles_new_,int tree_height_, BH_TYPE dtime_, BH_TYPE epssq_, BH_TYPE dsq, int step_) :
		cells(cells_), particles(particles_), tree_height(tree_height_), dtime(dtime_), epssq(epssq_), dsq(dsq), step(step_),particles_new(particles_new_) {
	}
};

class UpdatePositionSet {
public:
	Domain<particle> particles;
	BH_TYPE dtime;

	void operator()(const blocked_range<int>& range) const {
		for (int i = range.begin(); i != range.end(); i++) {
			update_positions(particles, i, dtime);
		}
	}

	UpdatePositionSet(Domain<particle> particles_, BH_TYPE dtime_) :
		particles(particles_), dtime(dtime_) {
	}
};


#endif
