#ifndef BARNESHUTCOMM_H_
#define BARNESHUTCOMM_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>

#include "core.h"

using namespace std;

#define BH_FLOAT

#ifdef BH_FLOAT
typedef float BH_TYPE;
#else
typedef double BH_TYPE;
#endif


typedef struct BH_TYPE4_struct {
	BH_TYPE x;
	BH_TYPE y;
	BH_TYPE z;
	BH_TYPE mass;

	int id;

} mass_position;

typedef struct s_particle {

	mass_position mass_pos;

	BH_TYPE velx;
	BH_TYPE vely;
	BH_TYPE velz;
	BH_TYPE accx;
	BH_TYPE accy;
	BH_TYPE accz;
	BH_TYPE t_accx;
		BH_TYPE t_accy;
		BH_TYPE t_accz;
	int lock;

} particle;

typedef enum {
	CELL, PARTICLE, NILL
} TYPE_T;

typedef struct cell_s {
	mass_position mass_pos;
	TYPE_T child_types[8];
	int child_indexes[8];
	int parent_cell_index;
} cell;

void initBARNES_HUT(int argc, char *argv[], int &nbodies, int& timesteps,
		 BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol);

void DEPRECATED_load_data_set2(particle* particles, char *filename, int nbodies, int& timesteps,
		BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol);

void DEPRECATED_load_data_set(particle* particles, char *filename, int nbodies, int& timesteps,
		BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol);

void compute_bounding_box(particle* particles, const int n, BH_TYPE &diameter, BH_TYPE &centerx,
		BH_TYPE &centery, BH_TYPE &centerz);

int build_tree_array(cell* cells, BH_TYPE radius, particle* particles, int nbodies,
		int& tree_height);

void compute_center_of_mass(cell* cells, particle* particles, int cell_count, int tree_height,int*);

void sequential_compute_force(cell* cells, particle* particles, int particle, int tree_height, BH_TYPE dtime,
		BH_TYPE epssq, BH_TYPE dsq, int step);

void sequential_update_positions(particle* particles, int part, BH_TYPE dtime);

double jobBARNES_HUT(int argc, char *argv[]);

void jobBARNES_HUT_check(int argc, char *argv[], Point3D* points ,particle* particles_to_check);

void jobBARNES_HUT_SEQ(int argc, char *argv[]);

static inline int MIN(int a, int b) {
	if (a < b)
		a = b;
	return a;
}

void PrintParticles(particle* particles, int nbodies);

void sort( int* sortd, int* countd,cell* cells, particle* particles, int nbodies, int ncells);

void GenerasteGaussPointSet(int nbds);

void ReadGaussPointSet(Point3D* particles,int nbds);
void balanced(Point3D* points,int nbds,float r) ;


void PointsToParticleSystem(Point3D* points, int nbodies, FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, FLOAT_ARRAY mass);

void PointsToParticleSystem(Point3D* points, int nbodies, particle* particles);


void PrintBH_TYPE(BH_TYPE d);

#endif
