#include "../../perform.h"
#include "BarnesHut_common.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	//argv[1] = "3";//sch
	//argv[2] = "4";//dev
	//argv[3] = "2";//dice
	//argv[4] = "1"; //pkernel
	//argv[5] = "131072"; // nbodies
	//argv[6]="0"; //FIB
	//argv[7]="1"; //timesteps
	//  32768
	//	65536
	//	131072
	//	262144
	//	524288
	//1048576

	fprintf(stderr,"FIB: %d\n",atoi(argv[6]));
	fprintf(stderr, "sch: %s dev: %s dice: %s timesteps: %s nbodies: %s\n", argv[1], argv[2], argv[3], argv[4],
			argv[5]);

	PERFORM_timer *timer = new PERFORM_timer();

	timer->start();

	initPERFORM(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[8]));

	double time = jobBARNES_HUT(argc, argv);

#ifdef TEST_TIME
	//timer->getMinResult(time, EXECUTIONS);
#endif

	shutdownPERFORM();

	timer->stop();

	LOG(0.1,cerr << "\n\nRun-time terminated in " << timer->duration << "s." << endl;)

	return 0;
}
