#include <iostream>


#include "core.h"
#include "perform.h"
#include "device_apis/CUDA/cuda_utils.h"
#include "device_apis/CUDA/global_sync.h"
#include "path.h"
#include "camera.h"
#include "ray.h"
#include "geometry/bvhaccel.h"
#include "geometry/triangle.h"
#include "geometry/light.h"
#include "geometry.h"
#include "math.h"
#include "utils.h"
#include "randomgen.h"
#include "config.h"

#include "math_functions.h"

#include "user_defined_task.h"

#include "kernels.h"
#include "uberkernel.h"

#include "pathtracer.cuh"

//using namespace std;

////// CONFIG

#define COHERENT_PT
#define WARP_RR

#define WARPSIZE 32

#define DEQ_MULTIPLIER 16 // <32
//#define N_SM 0

#define WARPS 8

#define N_BLOCKS (WARPS*N_SM)

#define TASKS_PER_LOOP (DEQ_MULTIPLIER)  /*Se muito alto e muitas tarefas produzidas, falha no espaço disponivel da out o que causa force
							 e pode tb falhar nas slots de tasks*/

#define UBK_INBOX_SIZE 5000000

#define LOCAL_INBOX_QUEUE_SIZE (32)           // se local inbox muito grande, rouba muito trabalho pa apenas uma SM. calcular em funçao do child_task_count?
#define LOCAL_OUTBOX_QUEUE_SIZE (1024*20)         //se local outboc muito pequeno força muito o lock
#define MEM_MEM_CHUNKS_PER_PE (1024) // PE is a warp or thread 350
#define THR_PER_BLOCK (WARPSIZE)
#define QUEUE_FULL_HANDICAP 0
#define MEM_CHUNKS_PER_SM (MEM_MEM_CHUNKS_PER_PE)
#define MEM_CHUNK_SIZE sizeof(ubkSpecificTask)
#define MEM_TOTAL (MEM_CHUNKS_PER_SM * MEM_CHUNK_SIZE)
//#define LOCAL_FREES_QUEUE_SIZE (MEM_CHUNKS_PER_SM + 1024*30) //Extra open slots on each SM frees q

#define TIDX (threadIdx.x)
#define THREAD0 (threadIdx.x == 0)
#define WARP0 (threadIdx.x < 32)
#define LANE0 ((threadIdx.x & 31)==0)    // or warp lane index
#define THREADG0 (tID == 0)

#define CHECK_BIT_1(var,pos) ((var) & (1 << (pos)))

#include "device_apis/CUDA/workqueue.h"

__device__ RAYCOUNTER_TYPE * ray_count;
__device__ CudaConcurrentQueue<ubkTask_ptr>* inbox;
__device__ ubkTask_ptr* local_inbox_buffer;
__device__ ubkTask_ptr* local_outbox_buffer;
//__device__ ubkTask_ptr* local_frees_buffer;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_outbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_inbox;
//__device__ CudaLocalQueue<ubkTask_ptr>* heads_frees;

__device__ volatile int out_votes[N_BLOCKS];
__device__ Task_pathtracer* global_task;
__device__ Point *vertices;
__device__ Normal *vertNormals;
__device__ RGB *vertColors;
__device__ Triangle * triangles;
__device__ TriangleLight *lights;
__device__ Domain<RGB>* pixelsRadiance;
__device__ BVHAccelArrayNode* bvh;
__device__ prePath* paths;

__device__
inline
bool Shade(ubkSpecificTask& task, RayHit& rayHit, Geometry* geometry,
		RGB& out_radiance, short depth) {

	RGB throughput;
	bool shade = !task.done[TIDX];
	uint tracedShadowRayCount;
	Point hitPoint;
	float RdotShadeN = 0;
	Normal shadeN;
	RGB triInterpCol = RGB();
	unsigned int currentTriangleIndex;

	task.throughput[TIDX].getRGB(throughput);

	if (!task.done[TIDX] && rayHit.index == 0xffffffffu) { // first time that hits nothing
	//atomicAdd((uint*) task_exe_count_T, 1);
	//task->live_threads--;
		atomicSub(&task.live_threads, 1);
		task.done[TIDX] = true;
		shade = false;
	}

	if (shade) {
		// Something was hit
		currentTriangleIndex = rayHit.index;
		triInterpCol = triangles[currentTriangleIndex].InterpolateColor(
				vertColors, rayHit.b1, rayHit.b2);
		shadeN = triangles[currentTriangleIndex].InterpolateNormal(vertNormals,
				rayHit.b1, rayHit.b2);
	}

	// Check if I have to stop
	if (depth >= global_task->CONFIG.MAX_PATH_DEPTH) {
		// Too depth, terminate the path
		return false;
	} else if (depth > 0.6 * global_task->CONFIG.MAX_PATH_DEPTH) {
		//} else if (false) {

#ifdef WARP_RR
		volatile __shared__ float probs[WARPSIZE];
		volatile __shared__ float probs2[WARPSIZE];
		volatile __shared__ float u;

		if (THREAD0)
			u = 0.f;
		probs[TIDX] = 0.f;

		if (shade) {
			probs[TIDX] = ComputeProbability(triInterpCol, task.pathRay[TIDX].d,
					shadeN);
			//if (probs[TIDX] == 0.f) printf("%d\n",task->live_threads);
		}

		// average

		probs2[TIDX] = probs[TIDX];

		for (int KK = WARPSIZE / 2; KK > 0; KK /= 2) {
			if (TIDX < KK)
				probs2[TIDX] += probs2[TIDX + KK];
		}

		if (THREAD0) {
			u = getFloatRNG(task.seed[0]); // random number
			probs2[0] = probs2[0] / static_cast<float>(task.live_threads); // average probability
		}

		if (probs2[0] > u) {
			if (shade && (probs[TIDX] != 0.f)) {
				throughput /= probs[TIDX];
			}

		} else {
			return false;
		}

#else

		// Russian Rulette
		float p;
		if (shade) p = ComputeProbability(triInterpCol, task.pathRay[TIDX].d, shadeN);

		if (p > getFloatRNG(task.seed[TIDX])) {
			//if (throughput.r < 0.01 && throughput.g < 0.01 && throughput.b < 0.01) printf("%.15f %.15f %.15f %f\n",throughput.r,throughput.g,throughput.b,p);
			throughput /= p;
		} else {
			// Terminate the path
			return false;
		}
#endif

	}

	if (shade) {

		//--------------------------------------------------------------------------
		// Build the shadow ray
		//--------------------------------------------------------------------------

		// Check if it is a light source
		RdotShadeN = Dot(task.pathRay[TIDX].d, shadeN);
		if (geometry->IsLight(currentTriangleIndex)) {
			// Check if we are on the right side of the light source
			if ((depth == 1) && (RdotShadeN < 0.f)) {
				out_radiance += triInterpCol * throughput;

			}
			// Terminate the path
			atomicSub(&task.live_threads, 1);
			task.done[TIDX] = true;
			shade = false;

			//printf("path temrinatedc\n");
			//return false;
		}
	}

	if (shade) {

		if (RdotShadeN > 0.f) {
			// Flip shade  normal
			shadeN = -shadeN;
		} else
			RdotShadeN = -RdotShadeN;

		//task->throughput[TIDX] *= RdotShadeN * triInterpCol;
		throughput *= RdotShadeN * triInterpCol;

		// Trace shadow rays
		//const Point hitPoint = task->pathRay[TIDX](rayHit.t);
		hitPoint = task.pathRay[TIDX].o + task.pathRay[TIDX].d * rayHit.t; // = path->pathRay(rayHit.t);

		tracedShadowRayCount = 0;
		const float lightStrategyPdf = static_cast<float>(SHADOWRAY)
				/ static_cast<float>(geometry->nLights);

		float lightPdf[SHADOWRAY];
		RGB lightColor[SHADOWRAY];
		Ray shadowRay[SHADOWRAY];

		for (unsigned int i = 0; i < SHADOWRAY; ++i) {

			// Select the light to sample
			const unsigned int currentLightIndex = geometry->SampleLights(
					getFloatRNG(task.seed[TIDX]));

			// Select a point on the surface
			lightColor[tracedShadowRayCount] = Sample_L(currentLightIndex,
					geometry, hitPoint, shadeN, getFloatRNG(task.seed[TIDX]),
					getFloatRNG(task.seed[TIDX]),
					&lightPdf[tracedShadowRayCount],
					&shadowRay[tracedShadowRayCount], vertices, vertNormals,
					vertColors, triangles, lights);

			// Scale light pdf for ONE_UNIFORM strategy
			lightPdf[tracedShadowRayCount] *= lightStrategyPdf;

			// Using 0.1 instead of 0.0 to cut down fireflies
			if (lightPdf[tracedShadowRayCount] > 0.1f)
				tracedShadowRayCount++;
		}

		//if (blockIdx.x == 0 && TIDX == 0 ) printf("%u\n", tracedShadowRayCount);

		RayHit rh[SHADOWRAY];

		for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
			atomicAdd((RAYCOUNTER_TYPE*) ray_count, 1);
			Intersect(shadowRay[i], rh[i], bvh, triangles, vertices);
		}

		if ((tracedShadowRayCount > 0)) {
			for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
				const RayHit *shadowRayHit = &rh[i];
				if (shadowRayHit->index == 0xffffffffu) {
					// Nothing was hit, light is visible
					out_radiance += (throughput * lightColor[i]) / lightPdf[i];
				}
			}
		}

		//		//--------------------------------------------------------------------------
		//		// Build the next vertex path ray
		//		//--------------------------------------------------------------------------
		//
		// Calculate exit direction

		//ComputeExitDir(task,rayHit);
	}

	float r1, r2;

#ifdef COHERENT_PT
	volatile __shared__ float ur[2];

	if (THREAD0) {
		ur[0] = getFloatRNG(task.seed[0]);
		ur[1] = getFloatRNG(task.seed[1]);
	}

	//int t = 50;
	//float ur20 =  getFloatRNG(task.seed[TIDX])/t - (1/(t*2));
	//float ur21 =  getFloatRNG(task.seed[TIDX])/t - (1/(t*2));

	//float ur20 = 0;
	//float ur21 = 0;

	//float ur20 =  getFloatRNG(task.seed[TIDX])/2;
	//float ur21 =  getFloatRNG(task.seed[TIDX])/2;
	// if (blockIdx.x == 0) printf("%.3f  -> %.3f, %.3f  -> %.3f\n",ur[0],ur[0] + ur20,ur[1],ur[1] + ur21);

	r1 = 2.f * M_PI * (ur[0]);
	r2 = ur[1];

#else

	r1 = 2.f * M_PI * getFloatRNG(task.seed[TIDX]);
	r2 = getFloatRNG(task.seed[TIDX]);

#endif

	if (shade) {

		float r2s = sqrt(r2);
		const Vector w(shadeN);

		Vector u;
		if (fabsf(shadeN.x) > .1f) {
			const Vector a(0.f, 1.f, 0.f);
			u = Cross(a, w);
		} else {
			const Vector a(1.f, 0.f, 0.f);
			u = Cross(a, w);
		}
		u = Normalize(u);

		Vector v = Cross(w, u);

		Vector newDir = u * (cosf(r1) * r2s) + v * (sinf(r1) * r2s)
				+ w * sqrtf(1.f - r2);
		newDir = Normalize(newDir);

		task.pathRay[TIDX].o = hitPoint;
		task.pathRay[TIDX].d = newDir;

	}

	task.throughput[TIDX] = RGBe(throughput);

	return true;
}

__device__
__forceinline void k_CUDA_devptr_pathtracer_Fine_T_SIMD32(ubkSpecificTask& task,
		PerspectiveCamera camera, Geometry geometry,
		volatile uint& elements_bag) {

	uint path_i = (task.p_target_start[0] + (TIDX >> 3))
			* (global_task->CONFIG.WIDTH * global_task->CONFIG.STRATA)
			+ (task.p_target_start[1] + (TIDX & 7));

	if (task.depth[TIDX] == 0) {

		task.seed[TIDX] = paths[path_i].seed;

		task.pathRay[TIDX] = camera.GenerateRay(&paths[path_i]);
	}

	Ray ray = Ray(task.pathRay[TIDX].o, task.pathRay[TIDX].d);

	RayHit hit;

	//		float maxX = 64-8+1;
	//		float maxY = 32-8+1;
	//
	//
	//		RGB out_radiance  = RGB((task.p_target_start[0] +1 ) / (maxX),
	//				(task.p_target_start[1] +1) / maxX,
	//				(task.p_target_start[0]+1) / maxY);

	RGB out_radiance = RGB();

	volatile __shared__ bool r[32];

	r[TIDX] = false;

	if (!task.done[TIDX]) {
		atomicAdd((RAYCOUNTER_TYPE*) ray_count, 1);
		Intersect(ray, hit, bvh, triangles, vertices);
	}

	task.depth[TIDX]++;
	r[TIDX] = Shade(task, hit, &geometry, out_radiance, task.depth[TIDX]);

	if (!task.done[TIDX]) {

		if (!r[TIDX]) {
			task.done[TIDX] = true;
		}
	}

	if (__any(r[TIDX])) {
		if (THREAD0)
			elements_bag++;
	}

	//if (!task.done[TIDX]) {
	int x = (int) paths[path_i].screenX;
	int y = (int) paths[path_i].screenY;

	uint offset = x + y * global_task->CONFIG.WIDTH;

	atomicAdd(&(pixelsRadiance->at(offset).r), out_radiance.r);
	atomicAdd(&(pixelsRadiance->at(offset).g), out_radiance.g);
	atomicAdd(&(pixelsRadiance->at(offset).b), out_radiance.b);
	//}
}

__device__
__forceinline void flush_to_global(CudaLocalQueue<ubkTask_ptr> *& q) {

	ubkTask_ptr t = NULL;
	while (!q->isEmpty() && !inbox->isFull()) {

#ifdef __DEBUG
		if (q->dequeue(t) && !inbox->enqueue(t)) {
			printf("Error 4\n");
			//__trap();

		}
#else
		if (q->dequeue(t))
		inbox->enqueue(t);
#endif
	}

}

__device__
__forceinline void try_donate(CudaLocalQueue<ubkTask_ptr> *& local_outbox) {

	if (!inbox->isFull() && !local_outbox->isEmpty() && inbox->tryLock()) {
		flush_to_global(local_outbox);
		inbox->Release();
	}

}

//__device__
//__forceinline bool check_all_isEmpty() {
//
//#if N_BLOCKS > WARPSIZE
//
//	register int yy = 0;
//	register int t1 = N_BLOCKS - WARPSIZE;
//	register int t2 = (N_BLOCKS & 31);
//
//	for (yy = 0; (yy < t1); yy += WARPSIZE) {
//		if (!__all(heads_inbox[TIDX + yy].isEmpty()) || !__all(heads_outbox[TIDX + yy].isEmpty()))
//			return false;
//	}
//
//	if (!__all(heads_inbox[(TIDX % t2) + yy].isEmpty()) || !__all(
//			heads_outbox[(TIDX % t2) + yy].isEmpty()))
//		return false;
//#else
//	if (!__all(heads_inbox[(TIDX % N_BLOCKS)].isEmpty()) || !__all(
//					heads_outbox[(TIDX % N_BLOCKS)].isEmpty()))
//	return false;
//#endif
//
//	return true;
//}

__device__
__forceinline void vote_out(CudaLocalQueue<ubkTask_ptr>* local_inbox) {

	if (inbox->isEmpty() && local_inbox->isEmpty()) {
		out_votes[blockIdx.x] = 1;
	} else
		out_votes[blockIdx.x] = 0;

	__threadfence_system();
}

__device__
__forceinline bool check_all_isEmpty() {

	register int yy = 0;

	for (yy = 0; yy < N_BLOCKS; yy += WARPSIZE) {
		if (!__all(out_votes[TIDX + yy] == 1))
			return false;
	}

	return true;
}

__global__ void pkernel(Task_pathtracer* g_task, PerspectiveCamera camera,
		Geometry geometry, Domain<RGB> d_pixelsRadiance,
		Domain<char> geometry_chunk, Domain<BVHAccelArrayNode> d_bvh,
		Domain<prePath> d_paths, ubkTask_ptr* inbox_buffer, void* mem_buffer,
		void* qs_buffer, int child_task_count, RAYCOUNTER_TYPE* ray_count_) {

	const int tID = blockIdx.x * blockDim.x + threadIdx.x;

	if (tID == 0) {

		//////// User-side ////////
		pixelsRadiance = &d_pixelsRadiance;	//.device_data_buffer;
		bvh = (BVHAccelArrayNode*) d_bvh.device_data_buffer;
		paths = (prePath*) d_paths.device_data_buffer;

		char *vertices_c = (char*) geometry_chunk.device_data_buffer;
		char *vertNormals_c = vertices_c + geometry.vertexCount * sizeof(Point);
		char *vertColors_c = vertNormals_c
				+ geometry.vertexCount * sizeof(Normal);
		char *triangles_c = vertColors_c + geometry.vertexCount * sizeof(RGB);
		char *lights_c = triangles_c
				+ geometry.triangleCount * sizeof(Triangle);

		vertices = (Point*) (vertices_c);
		vertNormals = (Normal*) (vertNormals_c);
		vertColors = (RGB*) (vertColors_c);
		triangles = (Triangle*) (triangles_c);
		lights = (TriangleLight*) (lights_c);

		///////////////////////////
	}

	//frees_left = 0;

	if (tID == 0) {

		global_task = g_task;

		ray_count = ray_count_;

		*ray_count = 0;

		local_inbox_buffer = (ubkTask_ptr*) qs_buffer;
		local_outbox_buffer = local_inbox_buffer
				+ LOCAL_INBOX_QUEUE_SIZE * N_BLOCKS;
		//	local_frees_buffer = local_outbox_buffer + LOCAL_OUTBOX_QUEUE_SIZE * N_BLOCKS;

		heads_inbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
		heads_outbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
		//heads_frees = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];

		inbox = new CudaConcurrentQueue<ubkTask_ptr>(inbox_buffer,
				child_task_count, UBK_INBOX_SIZE);

	}

	__gpu_sync(N_BLOCKS);

	CudaLocalQueue<ubkTask_ptr>* local_inbox = heads_inbox + blockIdx.x;
	CudaLocalQueue<ubkTask_ptr>* local_outbox = heads_outbox + blockIdx.x;
	//	CudaLocalQueue<ubkTask_ptr>* local_frees = heads_frees + blockIdx.x;

	if (THREAD0) {

		local_inbox->setBuffer(
				(ubkTask_ptr*) (local_inbox_buffer
						+ blockIdx.x * LOCAL_INBOX_QUEUE_SIZE), 0,
				LOCAL_INBOX_QUEUE_SIZE);
		local_outbox->setBuffer(
				(ubkTask_ptr*) (local_outbox_buffer
						+ blockIdx.x * LOCAL_OUTBOX_QUEUE_SIZE), 0,
				LOCAL_OUTBOX_QUEUE_SIZE);
		//		local_frees->setBuffer(
		//				(ubkTask_ptr*) (local_frees_buffer + blockIdx.x * LOCAL_FREES_QUEUE_SIZE), 0,
		//				LOCAL_FREES_QUEUE_SIZE);

		local_inbox->clear();
		local_outbox->clear();
		//local_frees->clear();

		memset((int*) out_votes, 0, sizeof(int) * N_BLOCKS);

	}

	//	ubkSpecificTask* warp_chunk = (ubkSpecificTask*) ((char*) mem_buffer + blockIdx.x * MEM_TOTAL);
	//
	//	for (int yy = 0; yy < MEM_MEM_CHUNKS_PER_PE; yy += WARPSIZE) {
	//#ifdef __DEBUG
	//		if (!local_frees->enqueue(warp_chunk + yy + TIDX))
	//		{
	//			printf("Error here\n");
	//		}
	//#else
	//		local_frees->enqueue(warp_chunk + yy + TIDX);
	//#endif
	//	}
	//__shared__ ubkSpecificTask work_bag_obj[TASKS_PER_LOOP];
	volatile __shared__ ubkTask_ptr v_work_bag[TASKS_PER_LOOP];

	__shared__ ubkTask_ptr* work_bag;
	work_bag = (ubkTask_ptr*) v_work_bag;

	volatile __shared__ uint elements_bag[TASKS_PER_LOOP];

#if MAX_SEC_TASK > 1
	volatile __shared__ ubkTask_ptr out_bag[MAX_SEC_TASK*TASKS_PER_DEQ];
#endif

	__gpu_sync(N_BLOCKS);

	////////// PARTY START /////////////
	//#ifdef __DEBUG
	//if (THREADG0) printf("Loop started...\n");
	//#endif

	//while (!inbox->isEmpty() || !__all(heads_inbox[TIDX % N_BLOCKS].isEmpty()) || !__all(heads_outbox[TIDX % N_BLOCKS].isEmpty()))
//	while (!inbox->isEmpty() || !check_all_isEmpty()) {
	while (!check_all_isEmpty()) {

		//					//Se a inbox esta vazia e a outbox tem trabalho, despeja na inbox ate ficar cheia ou não haver mais trabalho
		//					while (!local_inbox->isFull() && !local_outbox->isEmpty()) {
		//						ubkTask_ptr t_item = NULL;
		//
		//						if (local_outbox->dequeue(t_item) && t_item != NULL) {
		//#ifdef __DEBUG
		//
		//				local_inbox->enqueue(t_item) ? : printf("Error 0\n");
		//#else
		//				local_inbox->enqueue(t_item);
		//#endif
		//			}
		//
		//		}

		//TODO empty by one element is nnot empty

		//Se a inbox não estiver cheia tenta roubar trabalho na global
		if (local_inbox->isEmpty() && inbox->tryLock()) {
			ubkTask_ptr t_item = NULL;

			while (!local_inbox->isFull() && !inbox->isEmpty()) {
				if (inbox->dequeue(t_item) && (t_item != NULL)) {
#ifdef __DEBUG
					if (!local_inbox->enqueue(t_item)) {
						printf("Error 1\n");
						//__brkpt();
					}
#else
					local_inbox->enqueue(t_item);
#endif
				}
			}
			inbox->Release();
		}

		// work donation using global inbox
		//try_donate(local_outbox);

#if MAX_SEC_TASK > 1
		// work donation using global inbox
		try_donate(local_outbox);

		if (local_outbox->isFull()) {
			//#ifdef __DEBUG
			if (THREAD0)
			printf("%d Forcing lock 0 %u\n", blockIdx.x, local_outbox->getSize());
			//#endif
			inbox->Acquire();
			flush_to_global(local_outbox);
			inbox->Release();
		}
#endif

		//		if (THREAD0){
		//			memset(work_bag, 0, TASKS_PER_LOOP * sizeof(ubkTask_ptr));
		//			memset(elements_bag, 0, TASKS_PER_LOOP * sizeof(uint));
		//		}

		if (TIDX < DEQ_MULTIPLIER) {
			work_bag[TIDX] = NULL;
			elements_bag[TIDX] = 0;
		}

#if MAX_SEC_TASK > 1
		if (THREAD0)
		memset(out_bag, 0, MAX_SEC_TASK * TASKS_PER_DEQ * sizeof(ubkTask_ptr));
		//if (TIDX < 8) out_bag[TIDX] = NULL;

#endif

		//#ifdef __DEBUG
		//
		//		if ( (TIDX < TASKS_PER_DEQ) && !local_inbox->isEmpty() && local_inbox->dequeue(work_bag[TIDX]))
		//			if (local_frees->getSize() < MAX_SEC_TASK*10)
		//			{
		//				printf("Error: not enough frees for execution %u\n",local_frees->getSize());
		//
		//			}
		//#else
		if (TIDX < DEQ_MULTIPLIER)
			if (!local_inbox->isEmpty()) {
				local_inbox->dequeue(work_bag[TIDX]);
				//if (work_bag[TIDX] != NULL) work_bag_obj[TIDX]=*work_bag[TIDX];

			}

		//#endif

		//__threadfence_block();

		//		for (int task_i = 0; task_i < TASKS_PER_DEQ; task_i++) {
		//			if (work_bag[task_i] != NULL) {
		//
		//				k_CUDA_devptr_pathtracer_Fine_T_SIMD32(*work_bag[task_i], camera, geometry,
		//						local_frees, out_bag + task_i * MAX_SEC_TASK, elements_bag[task_i]);
		//
		//			}
		//		}

		for (int l = 0; l < DEQ_MULTIPLIER; l++) {

			if (work_bag[l] != NULL) {
				k_CUDA_devptr_pathtracer_Fine_T_SIMD32(*work_bag[l], camera,
						geometry, elements_bag[l]);

				//				k_CUDA_devptr_pathtracer_Fine_T_SIMD32(work_bag_obj[l], camera, geometry,
				//						elements_bag[l]);

			}
		}

#if MAX_SEC_TASK > 1

		if ((TIDX < TASKS_PER_DEQ) && work_bag[TIDX] != NULL && !local_frees->isFull()) {
#ifdef __DEBUG
			if (!local_frees->enqueue(work_bag)) printf("Error 2\n");
#else
			local_frees->enqueue(work_bag[TIDX]);
#endif
		}

		if (local_outbox->getSize() + MAX_SEC_TASK * TASKS_PER_DEQ >= local_outbox->_length) {
			//#ifdef __DEBUG
			if (THREAD0)
			printf("%d Forcing lock 1\n", blockIdx.x);
			//#endif
			inbox->Acquire();
			flush_to_global(local_outbox);
			inbox->Release();
		}
#endif

		//TODO why not direct outbox enqueue inside user function?

		//		for (int task_i = 0; task_i < TASKS_PER_DEQ; task_i++) {
		//			if (TIDX < elements_bag[task_i]) {
		//#ifdef __DEBUG
		//
		//				if (!local_outbox->enqueue(out_bag[task_i*MAX_SEC_TASK + TIDX])) printf("Error 3\n");
		//#else
		//				local_outbox->enqueue(out_bag[task_i * MAX_SEC_TASK + TIDX]);
		//#endif
		//			}
		//		}
		//		//try_donate(local_outbox);

		if (TIDX < DEQ_MULTIPLIER) {
			if (work_bag[TIDX] != NULL && elements_bag[TIDX] != 0) {
				//	*work_bag[TIDX]= work_bag_obj[TIDX];
				local_inbox->enqueue(work_bag[TIDX]);
				//local_outbox->enqueue(work_bag[TIDX]);
			}
		}

		//			else  if (work_bag[WARPSIZE*l+TIDX] != NULL)
		//				printf("depth: %d\n", work_bag[WARPSIZE*l+TIDX]->depth);

		//vote_out(local_inbox);
		__threadfence();

		if (local_inbox->getSize() == 0 && inbox->getSize() == 0) {
			//kill = true;
			break;
		}
	}

	__gpu_sync(N_BLOCKS);

	//#ifdef __DEBUG
	//if (WARP0 && TIDX==0)	printf("%d - %u\n",blockIdx.x, task_exe_count[blockIdx.x]);

	//if (THREAD0 && (local_inbox->getSize() != 0 /*|| local_outbox->getSize() != 0*/))
	//	printf("%d HAS TASKS NOT EXECUTED\n\n", blockIdx.x);
	//
#ifdef __DEBUG
	if (tID == 0) {

		//		delete heads_inbox;
		//		delete heads_outbox;
		//		delete heads_frees;

		//		for (int i=0; i<100; i++){
		//			printf("p: %d hits: %d\n", i, hit_count[i]);
		//		}

		//printf("\nFrees required %u \n", frees_left);
		printf("Elements in inbox %u \n", inbox->getSize());

	}
#endif
}

void k_CUDA_wrapper_uberkernel(Task* t, ubkTask* tasks_, int task_count) {

	Task_pathtracer* t_ = (Task_pathtracer*) t;

	//	PERFORM_acc_timer *TOTAL = new PERFORM_acc_timer();
	//PERFORM_acc_timer *PRE_FORCES = new PERFORM_acc_timer();
	//	PERFORM_acc_timer *FORCES = new PERFORM_acc_timer();
	//	PERFORM_acc_timer *UPD = new PERFORM_acc_timer();
	//
	//	TOTAL->start();
	//PRE_FORCES->start();

#ifdef __DEBUG

	cudaStreamSynchronize(t->stream);

	checkCUDAmemory("start");

	fprintf(stderr, "Task size: %d\n", (int) sizeof(ubkSpecificTask));
#endif

	Domain<RGB> pixelsRadiance;
	t->getDomain(0, pixelsRadiance);

	Domain<char> geometry_chunk;
	t->getDomain(1, geometry_chunk);

	Domain<BVHAccelArrayNode> bvh;
	t->getDomain(2, bvh);

	Domain<prePath> paths;
	t->getDomain(3, paths);

	//user task object marshalling
	ubkSpecificTask* tasks = new ubkSpecificTask[task_count];

	//	ubkSpecificTask* tasks1 = (ubkSpecificTask*)tasks_;
	//	for (int f = 0; f < task_count; f++) {
	//		tasks[f] = tasks1[f];
	//	}

	ubkSpecificTask* d_tasks;
	cudaMalloc((void**) &d_tasks, task_count * sizeof(ubkSpecificTask));

#ifdef __DEBUG
	fprintf(stderr, "UBK: Allocated %d for primary tasks\n",
			(int) (task_count * sizeof(ubkSpecificTask)) / 1024 / 1024);
#endif

	cudaMemcpy(d_tasks, tasks_, (task_count) * sizeof(ubkSpecificTask),
			cudaMemcpyHostToDevice);

	//Breaking tasks' array into pointers
	//why host side?
	ubkSpecificTask** tasks_ptrs = new ubkSpecificTask*[task_count];

	for (int f = 0; f < task_count; f++)
		tasks_ptrs[f] = d_tasks + f;

	ubkSpecificTask** d_inbox_buffer;
	cudaMalloc((void**) &d_inbox_buffer,
			UBK_INBOX_SIZE * sizeof(ubkSpecificTask*));

#ifdef __DEBUG
	fprintf(stderr, "UBK: Allocated %d for d_inbox_buffer\n",
			(int) (UBK_INBOX_SIZE * sizeof(ubkSpecificTask*)) / 1024 / 1024);
#endif

	cudaMemset(d_inbox_buffer, 0, UBK_INBOX_SIZE * sizeof(ubkSpecificTask*));

	cudaMemcpy(d_inbox_buffer, tasks_ptrs,
			(task_count) * sizeof(ubkSpecificTask*), cudaMemcpyHostToDevice);

	//void* d_mem_buffer;
	//cudaMalloc((void**) &d_mem_buffer, MEM_TOTAL * N_BLOCKS);

#ifdef __DEBUG
	//fprintf(stderr, "UBK: Allocated %d for task memory chunks\n",
	//		(int) (MEM_TOTAL * N_BLOCKS) / 1024 / 1024);
#endif

	//cudaMemset(d_mem_buffer, 0, MEM_TOTAL * N_BLOCKS);

	void* d_queues_buffer;
	cudaMalloc((void**) &d_queues_buffer,
			N_BLOCKS * sizeof(ubkTask_ptr)
					* (LOCAL_INBOX_QUEUE_SIZE + LOCAL_OUTBOX_QUEUE_SIZE));

#ifdef __DEBUG
	fprintf(stderr, "UBK: allocated %d for queues\n",
			N_BLOCKS * sizeof(ubkTask_ptr)
					* (LOCAL_INBOX_QUEUE_SIZE + LOCAL_OUTBOX_QUEUE_SIZE)
					/ 1024 / 1024);

			checkCUDAmemory
	("before launch");
#endif

	RAYCOUNTER_TYPE ray_count = 0;
	RAYCOUNTER_TYPE* ray_count_d;
	cudaMalloc((void**) &ray_count_d, sizeof(RAYCOUNTER_TYPE));

	Task_pathtracer* gtask_d;
	cudaMalloc((void**) &gtask_d, sizeof(Task_pathtracer));
	cudaMemcpy(gtask_d, t_, sizeof(Task_pathtracer), cudaMemcpyHostToDevice);

	PerspectiveCamera* cameraBuff;
	cudaMalloc((void**) &cameraBuff, sizeof(PerspectiveCamera));
	cudaMemcpy(cameraBuff, t_->camera, sizeof(PerspectiveCamera),
			cudaMemcpyHostToDevice);

	Geometry* geometryBuff;
	cudaMalloc((void**) &geometryBuff, sizeof(Geometry));
	cudaMemcpy(geometryBuff, t_->geometry, sizeof(Geometry),
			cudaMemcpyHostToDevice);

	//FORCES->start();

	fprintf(stderr, "Launching pkernel for %d tasks...\n", task_count);

	//PRE_FORCES->stop();
	checkCUDAError();

	//checkCUDAmemory("run");

	pkernel<<<N_BLOCKS,32,0,t->stream>>>
	(gtask_d, *(t_->camera), *(t_->geometry), pixelsRadiance, geometry_chunk, bvh, paths, d_inbox_buffer, NULL, d_queues_buffer, task_count, ray_count_d);
	checkCUDAError();

	//FORCES->stop();

	// ATTENTION
	cudaStreamSynchronize(t->stream);
	cudaMemcpy(&ray_count, ray_count_d, sizeof(RAYCOUNTER_TYPE),
			cudaMemcpyDeviceToHost);
	t_->update_ray_count(ray_count);

	//fprintf(stderr, "\nTotal rays processed %u\n", ray_count);

	cudaFree(d_tasks);
	cudaFree(d_inbox_buffer);
	cudaFree(d_queues_buffer);

	delete tasks;
	delete tasks_ptrs;
	delete (ubkSIMDTask_pathtracerSIMD32*) tasks_;

	checkCUDAError();

	//
	//	checkCUDAmemory("after launch");
	//
	//	//////////////////////////
	//
	//	TOTAL->stop();
	//
	//	fprintf(stderr, "\nTOTAL: %s\n\n", TOTAL->print());
	//fprintf(stderr, "PRE_FORCES: %s\n", PRE_FORCES->print());
	//	fprintf(stderr, "FORCES: %s\n", FORCES->print());
	//	fprintf(stdout, "%s\t", FORCES->print());

}
