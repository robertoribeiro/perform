#include "path.h"
#include "camera.h"
#include "ray.h"
#include "geometry/bvhaccel.h"
#include "geometry/triangle.h"
#include "geometry/light.h"
#include "geometry.h"
#include "math.h"
#include "utils.h"
#include "randomgen.h"
#include "config.h"

#include "user_defined_task.h"

#include "device_apis/CUDA/my_cutil_math.h"
#include "math_functions.h"

#include "pathtracer.cuh"

__device__
inline
bool Shade(PT_CONFIG* CONFIG, Path* path, Point *vertices, Normal *vertNormals,
		RGB *vertColors, Triangle *triangles, TriangleLight *lights, BVHAccelArrayNode *bvh,
		RayHit& rayHit, Geometry* geometry, RAYCOUNTER_TYPE* ray_count) {

	uint tracedShadowRayCount;

	if (rayHit.index == 0xffffffffu) {
		return false;
	}

	// Something was hit
	unsigned int currentTriangleIndex = rayHit.index;
	RGB triInterpCol = triangles[currentTriangleIndex].InterpolateColor(vertColors, rayHit.b1,
			rayHit.b2);
	Normal shadeN = triangles[currentTriangleIndex].InterpolateNormal(vertNormals, rayHit.b1,
			rayHit.b2);

	// Calculate next step
	path->depth++;

	// Check if I have to stop
	if (path->depth >= CONFIG->MAX_PATH_DEPTH) {
		// Too depth, terminate the path
		return false;
	} else if (path->depth >  0.6*CONFIG->MAX_PATH_DEPTH) {

		// Russian Rulette
		const float p = Min(1.f, triInterpCol.filter() * AbsDot(shadeN, path->pathRay.d));

		if (p > getFloatRNG(path->seed)) {
			path->throughput /= p;
		} else {
			// Terminate the path
			return false;
		}
	}

	//--------------------------------------------------------------------------
	// Build the shadow ray
	//--------------------------------------------------------------------------

	// Check if it is a light source
	float RdotShadeN = Dot(path->pathRay.d, shadeN);
	if (geometry->IsLight(currentTriangleIndex)) {
		// Check if we are on the right side of the light source
		if ((path->depth == 1) && (RdotShadeN < 0.f)){
			path->radiance += triInterpCol * path->throughput;
		}
		// Terminate the path
		return false;
	}

	if (RdotShadeN > 0.f) {
		// Flip shade  normal
		shadeN = -shadeN;
	} else
		RdotShadeN = -RdotShadeN;

	path->throughput *= RdotShadeN * triInterpCol;

	// Trace shadow rays
	//const Point hitPoint = path->pathRay(rayHit.t);

	const Point hitPoint = path->pathRay.o + path->pathRay.d * rayHit.t; // = path->pathRay(rayHit.t);


	tracedShadowRayCount = 0;
	const float lightStrategyPdf = static_cast<float> (SHADOWRAY)
			/ static_cast<float> (geometry->nLights);

	float lightPdf[SHADOWRAY];
	RGB lightColor[SHADOWRAY];
	Ray shadowRay[SHADOWRAY];

	for (unsigned int i = 0; i < SHADOWRAY; ++i) {
		// Select the light to sample
		const unsigned int currentLightIndex = geometry->SampleLights(getFloatRNG(path->seed));
		//	const TriangleLight &light = scene->lights[currentLightIndex];

		// Select a point on the surface

		lightColor[tracedShadowRayCount] = Sample_L(currentLightIndex, geometry, hitPoint, shadeN,
				getFloatRNG(path->seed), getFloatRNG(path->seed), &lightPdf[tracedShadowRayCount],
				&shadowRay[tracedShadowRayCount], vertices, vertNormals, vertColors, triangles,
				lights);

		// Scale light pdf for ONE_UNIFORM strategy
		lightPdf[tracedShadowRayCount] *= lightStrategyPdf;

//		int x = (int) path->screenX;
//				int y = (int) path->screenY;
//				if (x == 240 && y == 240)
//					printf("%u\n",currentLightIndex);

		// Using 0.1 instead of 0.0 to cut down fireflies
		if (lightPdf[tracedShadowRayCount] > 0.1f)
			tracedShadowRayCount++;
	}

	RayHit rh[SHADOWRAY];

	for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
		atomicAdd((RAYCOUNTER_TYPE*) ray_count, 1);
		Intersect(shadowRay[i], rh[i], bvh, triangles, vertices);
	}

	if ((tracedShadowRayCount > 0)) {
		for (unsigned int i = 0; i < tracedShadowRayCount; ++i) {
			const RayHit *shadowRayHit = &rh[i];
			if (shadowRayHit->index == 0xffffffffu) {
				// Nothing was hit, light is visible
				path->radiance += path->throughput * lightColor[i] / lightPdf[i];
			}
		}
	}


	//--------------------------------------------------------------------------
	// Build the next vertex path ray
	//--------------------------------------------------------------------------

	// Calculate exit direction

	float r1 = 2.f * M_PI * getFloatRNG(path->seed);
	float r2 = getFloatRNG(path->seed);
	float r2s = sqrt(r2);
	const Vector w(shadeN);

	Vector u;
	if (fabsf(shadeN.x) > .1f) {
		const Vector a(0.f, 1.f, 0.f);
		u = Cross(a, w);
	} else {
		const Vector a(1.f, 0.f, 0.f);
		u = Cross(a, w);
	}
	u = Normalize(u);

	Vector v = Cross(w, u);

	Vector newDir = u * (cosf(r1) * r2s) + v * (sinf(r1) * r2s) + w * sqrtf(1.f - r2);
	newDir = Normalize(newDir);

	path->pathRay.o = hitPoint;
	path->pathRay.d = newDir;


	return true;
}

__global__ void kernel(PT_CONFIG* CONFIG, PerspectiveCamera* camera,
		Domain<RGB> pixelsRadiance, BVHAccelArrayNode* bvh, Point *vertices, Normal *vertNormals,
		RGB *vertColors, Triangle *triangles, TriangleLight *lights, Geometry* geometry,
		Domain<prePath> paths, RAYCOUNTER_TYPE* ray_count) {

	 //int tID = (gridDim.x * blockDim.x * blockIdx.y * blockDim.y) +
	//		(gridDim.x * blockDim.x * threadIdx.y) + (blockDim.x * blockIdx.x + threadIdx.x) + paths.X.start;

	int len_X = gridDim.x * blockDim.x;
	int pos_x = blockIdx.x*blockDim.x  + threadIdx.x;
	int pos_y = blockIdx.y*blockDim.y  + threadIdx.y;

	int tID = pos_y * len_X + pos_x + paths.X.start;

	if ((tID >= paths.X.start) && (tID < paths.X.end)) {

		bool not_done = false;

		prePath pp = paths.at(tID);
		Path p = Path(&pp);
		RayHit hit;
		Ray a;

		camera->GenerateRay(&p);

		do {
		atomicAdd((RAYCOUNTER_TYPE*) ray_count, 1);
			a = Ray(p.pathRay.o, p.pathRay.d);
			Intersect(a, hit, bvh, triangles, vertices);
			not_done = Shade(CONFIG, &p, vertices, vertNormals, vertColors, triangles, lights, bvh,
				hit, geometry, ray_count);

		} while (not_done);

		int x = (int) p.screenX;
		int y = (int) p.screenY;

		uint offset = x + y * CONFIG->WIDTH;

		atomicAdd(&(pixelsRadiance.at(offset).r), p.radiance.r);
		atomicAdd(&(pixelsRadiance.at(offset).g), p.radiance.g);
		atomicAdd(&(pixelsRadiance.at(offset).b), p.radiance.b);
	}

}

void k_CUDA_pathtracer(Task* t_) {

	Domain<RGB> pixelsRadiance;
	t_->getDomain(0, pixelsRadiance);

	Domain<char> geometry_chunk;
	t_->getDomain(1, geometry_chunk);

	Domain<BVHAccelArrayNode> bvh;
	t_->getDomain(2, bvh);

	Domain<prePath> paths;
	t_->getDomain(3, paths);

	Task_pathtracer* t = (Task_pathtracer*) t_;

	char *vertices_c = (char*) geometry_chunk.device_data_buffer;
	char *vertNormals_c = vertices_c + t->geometry->vertexCount * sizeof(Point);
	char *vertColors_c = vertNormals_c + t->geometry->vertexCount * sizeof(Normal);
	char *triangles_c = vertColors_c + t->geometry->vertexCount * sizeof(RGB);
	char *lights_c = triangles_c + t->geometry->triangleCount * sizeof(Triangle);

	Point *vertices = (Point*) (vertices_c);
	Normal *vertNormals = (Normal*) (vertNormals_c);
	RGB *vertColors = (RGB*) (vertColors_c);
	Triangle *triangles = (Triangle*) (triangles_c);
	TriangleLight *lights = (TriangleLight*) (lights_c);

	int sqrtn = sqrt(paths.X.length());

	dim3 blockDIM = dim3(16, 16);
	dim3 gridDIM = dim3((sqrtn / blockDIM.x) + 1, (sqrtn / blockDIM.y) + 1);
	//dim3 gridDIM = dim3( 1,1);

	RAYCOUNTER_TYPE ray_count = 0;
	RAYCOUNTER_TYPE* ray_count_d;
	cudaMalloc((void**) &ray_count_d, sizeof(RAYCOUNTER_TYPE));
	cudaMemcpyAsync(ray_count_d, &t->ray_count, sizeof(RAYCOUNTER_TYPE), cudaMemcpyHostToDevice,t->stream);

	PT_CONFIG* gtask_d;
	cudaMalloc((void**) &gtask_d, sizeof(PT_CONFIG));
	cudaMemcpyAsync(gtask_d, &t->CONFIG, sizeof(PT_CONFIG), cudaMemcpyHostToDevice,t->stream);

	PerspectiveCamera* camera_d;
		cudaMalloc((void**) &camera_d, sizeof(PerspectiveCamera));
		cudaMemcpyAsync(camera_d, t->camera, sizeof(PerspectiveCamera), cudaMemcpyHostToDevice,t->stream);

		Geometry* geometry_d;
			cudaMalloc((void**) &geometry_d, sizeof(Geometry));
			cudaMemcpyAsync(geometry_d, t->geometry, sizeof(Geometry), cudaMemcpyHostToDevice,t->stream);


	fprintf(stderr, "Launching CUDA kernel for %d paths...\n",paths.X.length());


	kernel<<<gridDIM,blockDIM,0,(cudaStream_t)(t->stream) >>>(gtask_d,camera_d,pixelsRadiance,
			(BVHAccelArrayNode*)(bvh.device_data_buffer), vertices, vertNormals,
			vertColors, triangles, lights,geometry_d,paths, ray_count_d);


	// ATTENTION
	cudaStreamSynchronize(t->stream);
	cudaMemcpy(&ray_count, ray_count_d, sizeof(RAYCOUNTER_TYPE), cudaMemcpyDeviceToHost);
	t->update_ray_count(ray_count);

	cudaFree(ray_count_d);
	cudaFree(camera_d);
	cudaFree(geometry_d);
	cudaFree(gtask_d);


	//

	//	cudaFree(task_exec_d);

}
