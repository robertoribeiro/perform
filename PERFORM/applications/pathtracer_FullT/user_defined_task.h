#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"

class Task_pathtracer: public Task {
public:

	PerspectiveCamera *camera;
	Geometry *geometry;
	RAYCOUNTER_TYPE ray_count;
	PT_CONFIG CONFIG;

	__H_D__
	Task_pathtracer() :
		Task() {
	}
	__H_D__
	Task_pathtracer(TASK_TYPE t_) :
		Task(t_) {
	}
	void update_ray_count(RAYCOUNTER_TYPE ray_c) {
		__sync_fetch_and_add(&ray_count, ray_c);
		if (parent_host_task != NULL)
			((Task_pathtracer*) parent_host_task)->update_ray_count(ray_c);
	}
/**
 * 1d height divisions
 */
	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<RGB>* pixelsRadiance;
		getDomain(0, pixelsRadiance);

		Domain<char>* geometry_chunk;
		getDomain(1, geometry_chunk);

		Domain<BVHAccelArrayNode>* bvh;
		getDomain(2, bvh);

		Domain<prePath>* paths;
		getDomain(3, paths);

		//uint shuffle_size = 4* 30;
		uint shuffle_size = 1;

		//if (((shuffle_size) % CONFIG.STRATA) != 0) {
		//	printf("ERROR: strata must be divisor of 120\n");
		//	exit(0);
		//}

		// rows / minimal chunk height
		float task_shuffle_chunks = (paths->X.length() / (CONFIG.SPP * CONFIG.WIDTH)) / (shuffle_size);

		float len1 = (floor((job_elements) * m.value));
		if (len1 == 0)
			len1 = 1;

		if (task_shuffle_chunks <= len1 || len1 == 0 || m.value == 0) {
			Task_pathtracer** new_tasks = new Task_pathtracer*[1];
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			fprintf(stdout,"dicing last task\n");
			return 1;
		}

		int chunk = len1 * (shuffle_size);

		Task_pathtracer** new_tasks = new Task_pathtracer*[2];

		Task_pathtracer* t = new Task_pathtracer(type);

		dim_space bX = dim_space(paths->X.start,
				paths->X.start + chunk * (CONFIG.SPP * CONFIG.WIDTH));

		dim_space a = dim_space(pixelsRadiance->X.start,
				pixelsRadiance->X.start + (chunk) * CONFIG.WIDTH);

		Domain<RGB>* sub_pixelsRadiance = new Domain<RGB> (pixelsRadiance, 1, W, a);
		Domain<prePath>* sub_paths = new Domain<prePath> (paths, 1, R, bX);

		t->associate_domain(sub_pixelsRadiance);
		t->associate_domain(geometry_chunk);
		t->associate_domain(bvh);
		t->associate_domain(sub_paths);
		t->camera = camera;
		t->geometry = geometry;
		t->ray_count = ray_count;
		t->CONFIG = CONFIG;
		t->device_preference = device_preference;
		t->parent_host_task = (void*) this;
		t->wl.value = len1 / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->associate_kernel(this);
		t->job_elements = job_elements;
		new_tasks[task_count++] = t;

		t = new Task_pathtracer(type);

		bX = dim_space(paths->X.start + chunk * (CONFIG.SPP * CONFIG.WIDTH), paths->X.end);

		a = dim_space(pixelsRadiance->X.start + (chunk) * CONFIG.WIDTH,
				pixelsRadiance->X.end);

		sub_pixelsRadiance = new Domain<RGB> (pixelsRadiance, 1, W, a);
		sub_paths = new Domain<prePath> (paths, 1, R, bX);

		t->associate_domain(sub_pixelsRadiance);
		t->associate_domain(geometry_chunk);
		t->associate_domain(bvh);
		t->associate_domain(sub_paths);
		t->camera = camera;
		t->geometry = geometry;
		t->ray_count = ray_count;
		t->CONFIG = CONFIG;
		t->device_preference = device_preference;
		t->parent_host_task = (void*) this;
		t->wl.value = (task_shuffle_chunks - len1) / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->job_elements = job_elements;
		t->associate_kernel(this);
		new_tasks[task_count++] = t;

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

	int dice2(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<RGB>* pixelsRadiance;
		getDomain(0, pixelsRadiance);

		Domain<char>* geometry_chunk;
		getDomain(1, geometry_chunk);

		Domain<BVHAccelArrayNode>* bvh;
		getDomain(2, bvh);

		Domain<prePath>* paths;
		getDomain(3, paths);

		float N = pixelsRadiance->X.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);

		if (divide_task_by == -1) {
			divide_task_by = N;

			//			if (divide_task_by % 32 != 0) {
			//				printf("Error: devide parameter for uberkernel needs to be a divisor of 32\n");
			//				exit(-1);
			//			}
		}

		int block_step = (N / divide_task_by);

		Task_pathtracer** new_tasks = new Task_pathtracer*[divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {

			Task_pathtracer* t = new Task_pathtracer(REGULAR);

			dim_space a = dim_space(pixelsRadiance->X.start + i * block_step,
					(pixelsRadiance->X.start + i * block_step) + block_step);

			dim_space b = dim_space(
					paths->X.start + i * block_step * CONFIG.SPP,
					(pixelsRadiance->X.start + i * block_step * CONFIG.SPP) + block_step
							* CONFIG.SPP);

			Domain<RGB>* sub_pixelsRadiance = new Domain<RGB> (pixelsRadiance, 1, W, a);
			Domain<prePath>* sub_paths = new Domain<prePath> (paths, 1, R, b);

			t->associate_domain(sub_pixelsRadiance);
			t->associate_domain(geometry_chunk);
			t->associate_domain(bvh);
			t->associate_domain(sub_paths);

			t->camera = camera;
			t->geometry = geometry;
			t->ray_count = ray_count;
			t->CONFIG = CONFIG;

			t->device_preference = device_preference;
			t->parent_host_task = (void*) this;
			t->wl = PM_Metric_normalized((block_step * wl.value) / N);
			t->dice_lvl = dice_lvl + 1;

			t->associate_kernel(this);

			new_tasks[task_count++] = t;

		}

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

};

#endif /* UBK_TASK_H_ */
