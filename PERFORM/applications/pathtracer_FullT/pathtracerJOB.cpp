#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "display/film.h"
#include "display/displayfunc.h"
#include "path.h"
#include "randomgen.h"
#include "geometry.h"
#include "geometry/bvhaccel.h"
#include "camera.h"
#include "config.h"
#include "perform.h"
#include <time.h>

#include "utils.h"
#include "user_defined_task.h"

#include <omp.h>

#include <cuda.h>
#include <cuda_runtime.h>

//#include <boost/thread/mutex.hpp>

#include "pathtracer.hpp"

using namespace tbb;


PT_CONFIG CONFIG;

int jobPATHTRACER(int argc, char *argv[]) {

	initCONFIG(argv);

	int dice_v = atoi(argv[3]);

	cerr << "PARAMETERS:" << CONFIG.WIDTH << "x" << CONFIG.HEIGHT << "x" << CONFIG.SPP << "x"
			<< SHADOWRAY << "x" << CONFIG.MAX_PATH_DEPTH << endl;

	// Entities
	RGB *pixelsRadiance;
	float *pixels;
	PerspectiveCamera *camera = new PerspectiveCamera();
	BVHAccel *bvh;
	Geometry *geometry = new Geometry(); // include object and light areas

	ParseSceneFile(camera, geometry);
//
	bvh = new BVHAccel(geometry->triangleCount, geometry->triangles, geometry->vertices);

	//pixelsRadiance = new RGB[PIXEL_COUNT];

	pixelsRadiance = (RGB*)PERFORM_alloc(PIXEL_COUNT * sizeof(RGB));

	pixels = new float[PIXEL_COUNT * 3];

	memset(pixelsRadiance, 0, PIXEL_COUNT * sizeof(RGB));

	memset(pixels, 0, 3 * PIXEL_COUNT * sizeof(float));

//	printf("size of prepath %d\n", sizeof(prePath));

	//prePath* paths = new prePath[PATH_COUNT];
	int shuffle_size = 30;
	prePath* paths = preGeneratePaths(false, shuffle_size);


	//

	// -----------------  JOB

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();



	PERFORM_signal_JobStarted();

	uint chunk_length = geometry->vertexCount * sizeof(Point) + geometry->vertexCount
			* sizeof(Normal) + geometry->vertexCount * sizeof(RGB) + geometry->triangleCount
			* sizeof(Triangle) + geometry->nLights * sizeof(TriangleLight);

	geometry->Inline();


	Domain<RGB>* pixelsRadiance_D = new Domain<RGB> (1, W, pixelsRadiance,
			dim_space(0, PIXEL_COUNT));

	Domain<char>* geometry_chunk_D = new Domain<char> (1, R, geometry->chunk,
			dim_space(0, chunk_length));

	Domain<BVHAccelArrayNode>* bvh_D = new Domain<BVHAccelArrayNode> (1, R, bvh->bvhTree,
			dim_space(0, bvh->nNodes));

	//Domain<prePath>* paths_D = new Domain<prePath> (2, R, paths, dim_space(0, CONFIG.STRATA * CONFIG.HEIGHT),
	//		dim_space(0, CONFIG.STRATA * CONFIG.WIDTH));

	Domain<prePath>* paths_D = new Domain<prePath> (1, R, paths, dim_space(0, PATH_COUNT));

	timer->start();

	Task_pathtracer* t = new Task_pathtracer(REGULAR);

	t->associate_domain(pixelsRadiance_D);
	t->associate_domain(geometry_chunk_D);
	t->associate_domain(bvh_D);
	t->associate_domain(paths_D);

	t->camera = camera;
	t->geometry = geometry;
	t->ray_count = 0;
	t->CONFIG = CONFIG;

	t->parent_host_task = NULL;
	t->diceable = dice_v == 0 ? false : true;
	t->dice_value = 1/float(dice_v);
	//t->job_elements = (paths_D->X.length() / (CONFIG.STRATA * CONFIG.WIDTH)) / (4 * shuffle_size);
	t->job_elements = (paths_D->X.length() / (CONFIG.SPP * CONFIG.WIDTH));


	t->associate_kernel(CPU, &k_CPU_pathtracer);
	t->associate_kernel(GPU, &k_CUDA_pathtracer);

	performQ->addTasks(t);

	wait_for_all_tasks();
	PERFORM_signal_taskSubmissionEnded();


	performDL->splice(pixelsRadiance_D);

	timer->stop();
	PERFORM_signal_JobFinished(timer->duration);


	// -----------------

	fprintf(stderr, "%lld\t", t->ray_count);
	fprintf(stderr, "%s\t", timer->print());
	fprintf(stdout, "%.2f\t", ((t->ray_count/timer->duration))/1000000);
	fprintf(stderr, "Million rays per second: %.2f MRays/s\n", ((t->ray_count/timer->duration))/1000000);

	film = new Film(CONFIG.WIDTH, CONFIG.HEIGHT);

	UpdateScreenBuffer(film, CONFIG.WIDTH, CONFIG.HEIGHT, pixelsRadiance, pixels);

	film->pixels = pixels;

	//InitGlut(argc, argv, CONFIG.WIDTH, CONFIG.HEIGHT);

	clock_t a = clock();
	string s = "_" + to_string<clock_t>(a, std::dec);
	string MD = "MD" + string(argv[7]);
	string S = "S" + string(argv[8]);
	string SPP = "SPP" + string(argv[6]);
	string SH = "SH" + to_string<int>(SHADOWRAY, std::dec);
	string SHF = "SHF" + to_string<int>(0, std::dec);
	film->SavePPM("image" + MD + S + SPP + SH + SHF + s + ".ppm");

	PERFORM_free(pixelsRadiance);
	PERFORM_free(geometry->chunk);
	PERFORM_free(bvh->bvhTree);
	PERFORM_free(paths);
	//RunGlut();

	return EXIT_SUCCESS;
}
