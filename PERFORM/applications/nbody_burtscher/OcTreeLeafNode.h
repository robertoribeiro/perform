/*
 * OcTreeLeafNode.h
 *
 *  Created on: Mar 11, 2011
 *      Author: rr
 */

#ifndef OCTREELEAFNODE_H_
#define OCTREELEAFNODE_H_

#include "OcTree.h"
#include "OcTreeInternalNode.h"

class OctTreeLeafNode: public OctTreeNode { // the tree leaves are the bodies
public:

	//static int leaf_count;
	particle* p;

	OctTreeLeafNode() {
		static int leaf_count;
		leaf_count++;

		std::cout << leaf_count << std::endl;
		type = PARTICLE;

	}

	OctTreeLeafNode(particle * p_) {
		static int leaf_count;
		leaf_count++;
		//std::cout << "leaf count:"<< leaf_count << std::endl;
		type = PARTICLE;
		p = p_;
	}

	~OctTreeLeafNode() {
	}

};

#endif /* OCTREELEAFNODE_H_ */
