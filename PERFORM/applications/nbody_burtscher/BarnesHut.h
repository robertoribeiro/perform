#ifndef BARNESHUT_H_
#define BARNESHUT_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "OcTreeInternalNode.h"
#include "OcTreeLeafNode.h"

static double dtime; // length of one time step
static double eps; // potential softening parameter
static double tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

static double dthf, epssq, itolsq;

static int leaf_count, cell_count;

static int step;

using namespace tbb;

static int nbodies; // number of bodies in system
static int timesteps; // number of time steps to run
static int grainSize; // number of parallel tasks


static inline particle* ReadInput(char *filename, int& nbodies, int& timesteps, double& dtime, double& eps, double& tol) {
	double vx, vy, vz;
	register FILE *f;
	particle* particles;

	f = fopen(filename, "r+t");
	if (f == NULL) {
		fprintf(stderr, "file not found: %s\n", filename);
		exit(-1);
	}

	fscanf(f, "%d", &nbodies);
	fscanf(f, "%d", &timesteps);
	fscanf(f, "%lf", &dtime);
	fscanf(f, "%lf", &eps);
	fscanf(f, "%lf", &tol);

	dthf = 0.5 * dtime;
	epssq = eps * eps;
	itolsq = 1.0 / (tol * tol);
	//fprintf(stderr, "configuration: %d bodies, %d time steps and automatically determined grain size\n", nbodies, timesteps);

	particles = new particle[nbodies];

	for (int i = 0; i < (nbodies); i++) {
		fscanf(f, "%lE", &(particles[i].mass_pos.mass));
		fscanf(f, "%lE", &(particles[i].mass_pos.x));
		fscanf(f, "%lE", &(particles[i].mass_pos.y));
		fscanf(f, "%lE", &(particles[i].mass_pos.z));
		fscanf(f, "%lE", &(particles[i].velx));
		fscanf(f, "%lE", &(particles[i].vely));
		fscanf(f, "%lE", &(particles[i].velz));
		 particles[i].mass_pos.id=i;
		//bodies[i]->setVelocity(vx, vy, vz);
	}

	fclose(f);
	return particles;
}

static inline void ComputeCenterAndDiameter(particle* particles, const int n, double &diameter, double &centerx, double &centery, double &centerz) {
	register double minx, miny, minz;
	register double maxx, maxy, maxz;
	register double posx, posy, posz;

	minx = 1.0E90;
	miny = 1.0E90;
	minz = 1.0E90;
	maxx = -1.0E90;
	maxy = -1.0E90;
	maxz = -1.0E90;

	for (int i = 0; i < n; i++) {
		posx = particles[i].mass_pos.x;
		posy = particles[i].mass_pos.y;
		posz = particles[i].mass_pos.z;

		if (minx > posx)
			minx = posx;
		if (miny > posy)
			miny = posy;
		if (minz > posz)
			minz = posz;

		if (maxx < posx)
			maxx = posx;
		if (maxy < posy)
			maxy = posy;
		if (maxz < posz)
			maxz = posz;
	}

	diameter = maxx - minx;
	if (diameter < (maxy - miny))
		diameter = (maxy - miny);
	if (diameter < (maxz - minz))
		diameter = (maxz - minz);

	centerx = (maxx + minx) * 0.5;
	centery = (maxy + miny) * 0.5;
	centerz = (maxz + minz) * 0.5;
}

static inline int min(int a, int b) {
	if (a < b)
		a = b;
	return a;
}

static void PrintDouble(double d) {
	register int i;
	char str[16];

	sprintf(str, "%.4lE", d);

	i = 0;
	while ((i < 16) && (str[i] != 0)) {
		if ((str[i] == 'E') && (str[i + 1] == '-') && (str[i + 2] == '0') && (str[i + 3] == '0')) {
			printf("E00");
			i += 3;
		} else if (str[i] != '+') {
			printf("%c", str[i]);
		}
		i++;
	}
}

static OctTreeInternalNode *root;
static double gDiameter;

class ParallelForProcessor {
public:
	void operator()(const blocked_range<int>& range) const {
		for (int i = range.begin(); i != range.end(); i++) {
			//bodies[i]->ComputeForce(root, gDiameter);
		}
	}
};

void Advance(particle* p);
void RecurseForce(particle* p, const OctTreeNode * const n, double dsq);
void ComputeForce(particle* p, const OctTreeInternalNode * const root, const double size);
void IterativeForce(particle* p, vector<traversal_cell>& cells);
void jobBARNES_HUT(int argc, char *argv[]);

#endif
