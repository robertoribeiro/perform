/*
 * OcTreeInternalNode.h
 *
 *  Created on: Mar 11, 2011
 *      Author: rr
 */

#ifndef OCTREEINTERNALNODE_H_
#define OCTREEINTERNALNODE_H_

#include "OcTreeLeafNode.h"

class OctTreeInternalNode: public OctTreeNode {

private:
	//OctTreeInternalNode *link; // links all internal tree nodes so they can be recycled
	//static OctTreeInternalNode *head, *freelist; // free list for recycling

public:
	//static int cell_count;
	OctTreeNode *child[8];
	mass_position mass_pos;

	OctTreeInternalNode(const double px, const double py, const double pz) {
		static int cell_count;
		cell_count++;
	//	cout << "cel created:" << cell_count << endl;

		//std::cout << "cell count:"<< cell_count << std::endl;
		//register OctTreeInternalNode *in;

		//if (freelist == NULL) {
		//in = new OctTreeInternalNode();
		//in->link = head;
		//head = in;
		//} else { // get node from freelist
		//in = freelist;
		//freelist = freelist->link;
		//}

		type = CELL;

		mass_pos.mass = 0.0;
		mass_pos.x = px;
		mass_pos.y = py;
		mass_pos.z = pz;
		for (int i = 0; i < 8; i++)
			child[i] = NULL;

		//return in;
	}

	static void RecycleTree() {
		//freelist = head;
	}

	void Insert(particle * part, const double r) // builds the tree
	{
		register int i = 0;
		register double x = 0.0, y = 0.0, z = 0.0;
		OctTreeLeafNode* b = new OctTreeLeafNode(part);
		//leaf_count++;
		//find which child/division
		if (mass_pos.x < b->p->mass_pos.x) {
			i = 1;
			x = r;
		}
		if (mass_pos.y < b->p->mass_pos.y) {
			i += 2;
			y = r;
		}
		if (mass_pos.z < b->p->mass_pos.z) {
			i += 4;
			z = r;
		}
		//if child empty
		if (child[i] == NULL) {
			child[i] = b;
		}
		//if child already is divided
		else if (child[i]->type == CELL) {
			((OctTreeInternalNode *) (child[i]))->Insert(part, 0.5 * r);
		}
		//if child is a Leaf, divide
		else {
			//if (((OctTreeLeafNode *)(child[i]))->p->mass_pos.id == 1){
				//	cout << "Moving particle 1" <<endl;
					//}
			register const double rh = 0.5 * r;
			OctTreeInternalNode * const cell = new OctTreeInternalNode(mass_pos.x - rh + x, mass_pos.y - rh + y, mass_pos.z - rh + z);
			cell->Insert(part, rh);//insert the new particle
			cell->Insert(((OctTreeLeafNode *) (child[i]))->p, rh);//insert the particle that was already in the node
			child[i] = cell;
		}

	}

};

#endif /* OCTREEINTERNALNODE_H_ */
