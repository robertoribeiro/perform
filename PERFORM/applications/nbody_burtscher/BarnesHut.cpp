/*
 Lonestar BarnesHut: Simulation of the gravitational forces in a
 galactic cluster using the Barnes-Hut n-body algorithm

 Author: Martin Burtscher
 Center for Grid and Distributed Computing
 The University of Texas at Austin

 Copyright (C) 2007, 2008 The University of Texas at Austin

 Licensed under the Eclipse Public License, Version 1.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.eclipse.org/legal/epl-v10.html

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 File: BarnesHut.cpp
 Modified: Feb. 19, 2008 by Martin Burtscher (initial C++ version)
 Modified: May 26, 2009 by Nicholas Chen (uses TBB constructs)
 */

/**
 *  Iterative attempt: iterative structure (depth first traversal) with iterative method
 */

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "BarnesHut.h"

#include "../../myUtil.h"


using namespace tbb;
using namespace std;

static int cofm_count;

void ComputeCenterOfMass(OctTreeInternalNode* node) {
	register double m, px = 0.0, py = 0.0, pz = 0.0;
	register OctTreeNode *ch;

	register int j = 0;
	node->mass_pos.mass = 0.0;
	for (int i = 0; i < 8; i++) {
		ch = node->child[i];
		if (ch != NULL) {
			if (ch->type == CELL) {
				OctTreeInternalNode* cell_child = (OctTreeInternalNode*) ch;
				ComputeCenterOfMass(cell_child);
				m = cell_child->mass_pos.mass;
				node->mass_pos.mass += m;
				px += cell_child->mass_pos.x * m;
				py += cell_child->mass_pos.y * m;
				pz += cell_child->mass_pos.z * m;
			} else {
				OctTreeLeafNode* leaf = (OctTreeLeafNode*) ch;
				m = leaf->p->mass_pos.mass;
				node->mass_pos.mass += m;
				px += leaf->p->mass_pos.x * m;
				py += leaf->p->mass_pos.y * m;
				pz += leaf->p->mass_pos.z * m;
			}
		}
	}

	m = 1.0 / node->mass_pos.mass;
	node->mass_pos.x = px * m;
	node->mass_pos.y = py * m;
	node->mass_pos.z = pz * m;

	node->mass_pos.id = cofm_count++;

	/*printf("Center of mass:  ");
	 PrintDouble(node->centerM_x);
	 printf(" ");
	 PrintDouble(node->centerM_y);
	 printf(" ");
	 PrintDouble(node->centerM_z );
	 printf("\n");*/
}

void ComputeForceIter(particle* p, vector<traversal_cell>& traversal, const double size) {
	register double ax, ay, az;

	ax = p->accx;
	ay = p->accy;
	az = p->accz;

	p->accx = 0.0;
	p->accy = 0.0;
	p->accz = 0.0;

	//RecurseForce(p, root, size * size * itolsq);
	IterativeForce(p, traversal);

	if (step > 0) {
		p->velx += (p->accx - ax) * dthf;
		p->vely += (p->accy - ay) * dthf;
		p->velz += (p->accz - az) * dthf;
	}
}

void ComputeForceRec(particle* p, OctTreeNode* root, const double size) {
	register double ax, ay, az;

	ax = p->accx;
	ay = p->accy;
	az = p->accz;

	p->accx = 0.0;
	p->accy = 0.0;
	p->accz = 0.0;

	RecurseForce(p, root, size * size * itolsq);

	if (step > 0) {
		p->velx += (p->accx - ax) * dthf;
		p->vely += (p->accy - ay) * dthf;
		p->velz += (p->accz - az) * dthf;
	}
}

int Traverse(OctTreeNode* root, vector<traversal_cell>& traversal, int& iter, double distance_tol) {

	traversal_cell c;
	c.type = NILL;
	int id_p;
	int at;

	if (root == NULL) {

		c.next_sibling = iter + 1;
		at = iter;
		traversal.insert(traversal.begin() + iter, c);
		iter++;

	} else {

		if (root->type == CELL) {
			OctTreeInternalNode *in = (OctTreeInternalNode *) root;
			c.mass_pos = in->mass_pos;
			c.type = CELL;
			c.distance_tol = distance_tol;
			at = iter;
			id_p = c.mass_pos.id;
			//traversal.push_back(c);
			traversal.insert(traversal.begin() + iter, c);
			iter++;
			distance_tol *= 0.25;
			int i;
			for (i = 0; i < 7; i++) {
				Traverse(in->child[i], traversal, iter, distance_tol);
			}

			int last_child_at = Traverse(in->child[i], traversal, iter, distance_tol);
			traversal[last_child_at].next_sibling = iter;
			traversal[at].next_sibling = iter;

			/*
			 if (child_n == 7) {
			 traversal[last_child_at].next_sibling = -1;
			 //iter++;
			 }*/

			//else traversal[at].next_sibling = iter;

		} else {
			OctTreeLeafNode* leaf = (OctTreeLeafNode*) root;
			c.mass_pos = leaf->p->mass_pos;
			c.type = PARTICLE;
			c.distance_tol = distance_tol;
			c.next_sibling = iter + 1;
			id_p = c.mass_pos.id;
			at = iter;
			traversal.insert(traversal.begin() + iter, c);
			iter++;
			//traversal.push_back(c);
		}
	}
	return at;

}

void IterativeForce(particle* p, vector<traversal_cell>& cells) {

	double drx, dry, drz, drsq, nphi, scale, idr;

	for (int i = 0; i < cells.size();) {

		traversal_cell cell = cells[i];
		if (cell.type == NILL || cell.mass_pos.id == p->mass_pos.id) {
			i++;
		} else {
			drx = cell.mass_pos.x - p->mass_pos.x;
			dry = cell.mass_pos.y - p->mass_pos.y;
			drz = cell.mass_pos.z - p->mass_pos.z;
			drsq = drx * drx + dry * dry + drz * drz;
			//cout << "contribution cell:" << cell.mass_pos.id << " drsq:" << drsq << " distance:" << cell.distance_tol << endl;
			if (drsq < cell.distance_tol) {// go deep
				if (cell.type == CELL) {
					i++;
					//dsq *= 0.25;
				} else {
					drsq += epssq;
					idr = 1 / sqrt(drsq);
					nphi = cell.mass_pos.mass * idr;
					scale = nphi * idr * idr;
					p->accx += drx * scale;
					p->accy += dry * scale;
					p->accz += drz * scale;
					i++;
					//cout << "contribution cell:" << cell.mass_pos.id << " scale:" << scale << " accx:" << p->accx << endl;
				}
			} else {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = cell.mass_pos.mass * idr;
				scale = nphi * idr * idr;
				p->accx += drx * scale;
				p->accy += dry * scale;
				p->accz += drz * scale;
				//cout << "contribution cell:" << cell.mass_pos.id << " scale:" << scale << " accx:" << p->accx << endl;

				if (cell.next_sibling != -1)
					i = cell.next_sibling;
				else
					i++;
			}
		}

	}
}

void RecurseForce(particle* p, const OctTreeNode * const n, double dsq) {
	double drx, dry, drz, drsq, nphi, scale, idr;
	int cell_id;
	if (n->type == CELL) {
		OctTreeInternalNode* cell_child = (OctTreeInternalNode*) n;
		drx = cell_child->mass_pos.x - p->mass_pos.x;
		dry = cell_child->mass_pos.y - p->mass_pos.y;
		drz = cell_child->mass_pos.z - p->mass_pos.z;
		cell_id = cell_child->mass_pos.id;
	} else {
		OctTreeLeafNode* leaf = (OctTreeLeafNode*) n;
		drx = leaf->p->mass_pos.x - p->mass_pos.x;
		dry = leaf->p->mass_pos.y - p->mass_pos.y;
		drz = leaf->p->mass_pos.z - p->mass_pos.z;
		cell_id = leaf->p->mass_pos.id;
	}

	drsq = drx * drx + dry * dry + drz * drz;
	if (drsq < dsq) {
		if (n->type == CELL) {
			OctTreeInternalNode *in = (OctTreeInternalNode *) n;
			dsq *= 0.25;
			if (in->child[0] != NULL) {
				RecurseForce(p, in->child[0], dsq);
			}
			if (in->child[1] != NULL) {
				RecurseForce(p, in->child[1], dsq);
			}
			if (in->child[2] != NULL) {
				RecurseForce(p, in->child[2], dsq);
			}
			if (in->child[3] != NULL) {
				RecurseForce(p, in->child[3], dsq);
			}
			if (in->child[4] != NULL) {
				RecurseForce(p, in->child[4], dsq);
			}
			if (in->child[5] != NULL) {
				RecurseForce(p, in->child[5], dsq);
			}
			if (in->child[6] != NULL) {
				RecurseForce(p, in->child[6], dsq);
			}
			if (in->child[7] != NULL) {
				RecurseForce(p, in->child[7], dsq);
			}

		} else { // n is a body
			OctTreeLeafNode* leaf = (OctTreeLeafNode*) n;
			if (leaf->p->mass_pos.id != p->mass_pos.id) {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = leaf->p->mass_pos.mass * idr;
				scale = nphi * idr * idr;
				p->accx += drx * scale;
				p->accy += dry * scale;
				p->accz += drz * scale;

			} else {
				//printf("\nComparing with same particle, ignoring\n");
			}

		}
	} else { // node is far enough away, don't recurse any deeper
		mass_position* mp;
		if (n->type == CELL) {
			OctTreeInternalNode* cell_child = (OctTreeInternalNode*) n;
			mp = &(cell_child->mass_pos);
		} else {
			OctTreeLeafNode* leaf = (OctTreeLeafNode*) n;
			mp = &(leaf->p->mass_pos);
		}
		drsq += epssq;
		idr = 1 / sqrt(drsq);
		nphi = mp->mass * idr;
		scale = nphi * idr * idr;
		p->accx += drx * scale;
		p->accy += dry * scale;
		p->accz += drz * scale;

	}

	/*PrintDouble(p->accx);
	 printf("   ");
	 PrintDouble(p->accy);
	 printf("   ");
	 PrintDouble(p->accz);
	 printf("\n");*/
}

void Advance(particle* p, double dtime) {
	register double dvelx, dvely, dvelz;
	register double velhx, velhy, velhz;

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;
}

void jobBARNES_HUT(int argc, char *argv[]) {

	int nbodies;
	int timesteps;
	double dtime;
	double eps;
	double tol;

	PERFORM_timer* timer = new PERFORM_timer();
		char * filename;

	//	if (argc > 1) {
	//		switch (*argv[1]) {
	//		case 'a':
	//			filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inA";
	//			break;
	//		case 'b':
	//			filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inB";
	//			break;
	//		case 'c':
				filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inD";
	//			break;
	//		default:
	//			exit(0);
	//			break;
	//		}
	//	} else {
	//		filename = "/home/rr/work/perform/PERFORM/src/applications/nbody_sequential/BarnesHut.inA";
	//	}


	particle *particles = ReadInput(filename, nbodies, timesteps, dtime, eps, tol);


	nbodies = atoi(argv[2]);
	//nbodies=100;
	timesteps = atoi(argv[3]);

	timer->start();

	for (step = 0; step < timesteps; step++) {

		double diameter, centerx, centery, centerz;

		/**
		 * Compute de center and diameter of the all system
		 */
		ComputeCenterAndDiameter(particles, nbodies, diameter, centerx, centery, centerz);

		/**
		 * Create the the octree root
		 */
		OctTreeInternalNode *local_root = new OctTreeInternalNode(centerx, centery, centerz);

		/**
		 * For each particle insert in the octree
		 */
		const double radius = diameter * 0.5;
		for (int i = 0; i < nbodies; i++) {
			local_root->Insert(&(particles[i]), radius);

		}

		//static int cell_count,leaf_count;
		//cout << "Cell count: " << cell_count << " Leaf count: " << leaf_count << endl;
		cofm_count = nbodies;
		ComputeCenterOfMass(local_root);

//		vector<traversal_cell> tr;
//		int iter = 0;
//		Traverse(local_root, tr, iter, diameter * diameter * itolsq);
//		//cout << "Traversal to : " << tr.size() << " nodes" << endl;
//
//		int p_count = nbodies;
//		for (int i = 0; i < tr.size(); i++) {
//			if (tr[i].type == CELL)
//				p_count++;
//		}

	//	cout << "voxel count:" << p_count - nbodies << " body count: " << nbodies << std::endl;


		//		for (int i = 0; i < tr.size(); i++) {
		//				if (tr[i].type != NILL)
		//					cout << i << ": " << tr[i].mass_pos.id << endl;
		//				else cout << i << ": NILL" << endl;
		//		}

		for (int i = 0; i < nbodies; i++) {
			//ComputeForceIter(&particles[i], tr, diameter);

			ComputeForceRec(&particles[i], local_root, diameter);

			/*PrintDouble(particles[i].accx);
			 printf("   ");
			 PrintDouble(particles[i].accy);
			 printf("   ");
			 PrintDouble(particles[i].accz);
			 printf("\n");*/
		}

		for (int i = 0; i < nbodies; i++) {
			Advance(&particles[i], dtime);
		}
	}

	timer->stop();

	printf("%.1f\t", timer->duration);


	for (int i = 0; i < nbodies; i++) { // print result
		PrintDouble(particles[i].mass_pos.x);
		printf("   ");
		PrintDouble(particles[i].mass_pos.y);
		printf("   ");
		PrintDouble(particles[i].mass_pos.z);
		printf("\n");
	}
}

