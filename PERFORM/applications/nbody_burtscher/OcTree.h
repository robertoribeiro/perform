#ifndef OCTREE_H_
#define OCTREE_H_

#include <iostream>

using namespace std;
//#include "BarnesHut.h"

enum {
	CELL, PARTICLE, NILL
};

typedef struct double4_struct {
	double x;
	double y;
	double z;
	double mass;

	int id;

} mass_position;

typedef struct s_particle {

	mass_position mass_pos;

	double velx;
	double vely;
	double velz;
	double accx;
	double accy;
	double accz;

} particle;

typedef struct traversal_cell_struct {
	int type;
	mass_position mass_pos;
	/** Value used to classify long or short distance, according to cell size*/
	double distance_tol;
	int next_sibling;
} traversal_cell;

class OctTreeNode {
public:
	int type; // CELL or BODY

};

#endif
