#include <iostream>
#include "perform_utils.h"
#include "core.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>

#include "../GEMM_PERFORM/dgemm.h"

#include "kernels.h"

using namespace std;

#define BLOCK_SIZE 32

__global__ void matMultiply(Domain<float> A, Domain<float> B, Domain<float> C) {

	float* AA = (float*) (A.device_data_buffer);
	float*BB = (float*) (B.device_data_buffer);
	float*CC = (float*) (C.device_data_buffer);

	int TX = threadIdx.x + blockIdx.x * blockDim.x;
	int TY = threadIdx.y + blockIdx.y * blockDim.y;

	if (TX < C.Y.length() && TY < C.X.length()) {

		float acc = 0;

		for (int k = 0; k < A.Y.length(); ++k) {
			float Melement = AA[TY * A.Y.length() + k];
			float Nelement = BB[k * B.Y.length() + TX];
			acc += Melement * Nelement;
		}

		CC[TY * C.Y.length() + TX] = acc;
	}

}

void k_CUDA_GEMM(Task* t_) {

	cublasStatus_t status;

	cublasHandle_t handle;
	GEMM_TYPE alpha = 1.0f;
	GEMM_TYPE beta = 0.0f;

	Domain<GEMM_TYPE> A;
	t_->getDomain(0, A);

	Domain<GEMM_TYPE> B;
	t_->getDomain(1, B);

	Domain<GEMM_TYPE> C;
	t_->getDomain(2, C);

	GEMM_TYPE* AA = (GEMM_TYPE*) (A.device_data_buffer);
	GEMM_TYPE*BB = (GEMM_TYPE*) (B.device_data_buffer);
	GEMM_TYPE*CC = (GEMM_TYPE*) (C.device_data_buffer);

	int Arows = A.X.length();
	int Acols = A.Y.length();
	int Brows = B.X.length();
	int Bcols = B.Y.length();
	int Crows = C.X.length();
	int Ccols = C.Y.length();

	status = cublasCreate(&handle);
	cublasSetStream(handle, t_->stream);

	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "!!!! CUBLAS initialization error\n");
		return;
	}

	//dim3 blockDIM = dim3(BLOCK_SIZE, BLOCK_SIZE);
	//dim3 gridDIM = dim3(ceil((float)C.Y.length() / (float)blockDIM.x),ceil( (float)C.X.length() / (float)blockDIM.y));
	//matMultiply<<<gridDIM,blockDIM,0,t_->stream>>>(A, B, C);


	if (C.X.length() != C.Y.length()) {
		int m = Crows;
		int n = Ccols;
		int k = Acols;
		int lda = Arows;
		int ldb = Brows;
		int ldc = Crows;

		cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, &alpha, AA, lda, BB, ldb,
				&beta, CC, ldc);

	} else {

		int m = Arows;
			int n = Bcols;
			int k = Acols;
			int lda = Acols;
			int ldb = Bcols;
			int ldc = Crows;


			cublasDgemm(handle,CUBLAS_OP_T, CUBLAS_OP_T, m, n, k, &alpha, AA, lda, BB, ldb, &beta, CC, ldc);

	}

	checkCUDAError("kernel launch");

}

void k_CUDA_wrapper_uberkernel(Task* t, Task** task_prms, int child_task_count) {
}

