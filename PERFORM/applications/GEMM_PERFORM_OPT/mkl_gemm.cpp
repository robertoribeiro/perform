#include <iostream>

//#include "utils.h"
#include "core.h"
#include "mkl.h"

#include "../GEMM_PERFORM/dgemm.h"

void k_CPU_GEMM(Task* t_) {

	Domain<GEMM_TYPE> A;
	t_->getDomain(0, A);

	Domain<GEMM_TYPE> B;
	t_->getDomain(1, B);

	Domain<GEMM_TYPE> C;
	t_->getDomain(2, C);

	GEMM_TYPE* AA = (GEMM_TYPE*) (A.device_data_buffer);
	GEMM_TYPE*BB = (GEMM_TYPE*) (B.device_data_buffer);
	GEMM_TYPE*CC = (GEMM_TYPE*) (C.device_data_buffer);

	int Arows = A.X.length();
	int Acols = A.Y.length();
	int Brows = B.X.length();
	int Bcols = B.Y.length();
	int	Ccols = C.Y.length();
	int Crows = C.X.length();

	int m = Arows;
	int n = Bcols;
	int k = Acols;
	int lda = Acols;
	int ldb = Bcols;
	int ldc = Ccols;

	cblas_dgemm(CblasRowMajor,CblasNoTrans, CblasNoTrans, m, n, k, 1.0, AA, lda, BB, ldb, 1.0, CC, ldc);

	//		float staticA;
	//		for (int i = 0; i < C.Y.length(); i++) {
	//			for (int j =0; j < C.Y.length(); j++) {
	//					for (int k = 0; k != A.Y.length(); ++k) {
	//							CC[i * C.Y.length() + j] += AA[i * A.Y.length() + k]* BB[k * B.Y.length() + j];
	//							}
	//					}
	//			}


}
