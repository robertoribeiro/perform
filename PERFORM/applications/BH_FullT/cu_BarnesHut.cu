#include "BarnesHut.h"

//#include "../../task/Task.cuh"


#define GARBAGE_FIB


__device__ double fibFULLT(double n) {
	double a = 1, b = 1;
	for (int i = 3; i <= n; i++) {
		double c = a + b;
		a = b;
		b = c;
	}
	return a;
}

__device__ uint _somethingFULLT(uint x) {

	volatile uint acc = 0;

	volatile uint garbage = 0;
	while (garbage < 1000) {
		acc += x + 1;
		garbage += 1;
		//__threadfence_block();
	}

	return acc;
}

__global__ void k_compute_force(Domain<cell> cells, Domain<particle> particles,
		Domain<particle> particles_new, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq,
		int step, int* GARBAGE, Domain<int> sort, long long unsigned int* visit_count, uint FIB) {

	int buffer_pos = blockIdx.x * blockDim.x + threadIdx.x + particles_new.X.start;

	
	if (buffer_pos < particles_new.X.end && buffer_pos >= particles_new.X.start) {

		int p = sort.at(buffer_pos);

		//printf("%d\n", p);


		BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
		BH_TYPE ax, ay, az;

		particle x = particles.at(p);

		ax = x.accx;
		ay = x.accy;
		az = x.accz;

		x.accx = 0.0;
		x.accy = 0.0;
		x.accz = 0.0;

		char stack[100];
		//register char* stack= new char[tree_height];
		int stack_top = 0;

		int current = 0;
		int child = 0;

		dsq *= 0.25;

		mass_position mass_pos_p = x.mass_pos;

		while (!(stack_top == 0 && child == 8)) {

			//printf("Stack pointer: %d\n",stack_top);

			if (child == 8) {
				current = cells.at(current).parent_cell_index;
				dsq *= 4;
				child = stack[--stack_top];
				child++;

			} else {

				mass_position mass_pos;
				if (cells.at(current).child_types[child] == PARTICLE)
					mass_pos = particles.at(cells.at(current).child_indexes[child]).mass_pos;
				else if (cells.at(current).child_types[child] == CELL)
					mass_pos = cells.at(cells.at(current).child_indexes[child]).mass_pos;
				else {
					child++;
					continue;
				}

				drx = mass_pos.x - mass_pos_p.x;
				dry = mass_pos.y - mass_pos_p.y;
				drz = mass_pos.z - mass_pos_p.z;

				//atomicAdd(( long long unsigned int*) visit_count, 1);

				drsq = drx * drx + dry * dry + drz * drz;

				///////// GARBAGE /////////
#ifdef GARBAGE_FIB
				double r = 7778742049.0;
				double a = fibFULLT(FIB);
				//GARBAGE[threadIdx.x & 31] = a;
				atomicAnd((int*) &(GARBAGE[threadIdx.x & 31]), (r) == a);
#endif

				///////// GARBAGE /////////

				if (drsq < dsq) {
					if (cells.at(current).child_types[child] == CELL) {
						//if (p==0) printf("%lf\n",dsq);
						dsq *= 0.25;
						current = cells.at(current).child_indexes[child];
						stack[stack_top++] = child;
						child = 0;
					} else if (cells.at(current).child_types[child] == PARTICLE) {
						if (mass_pos.id != mass_pos_p.id) {
							drsq += epssq;
							//idr = rsqrtf(drsq);
							idr = rsqrtf(drsq);
							nphi = mass_pos.mass * idr;
							scale = nphi * idr * idr;
							x.accx += drx * scale;
							x.accy += dry * scale;
							x.accz += drz * scale;

							//if (p==500) printf("%.15lf\n",scale);


						}
						child++;
					} else if (cells.at(current).child_types[child] == NILL) {
						child++;
					}
				} else {
					drsq += epssq;
					//idr = rsqrtf(drsq);
					idr = rsqrtf(drsq);
					nphi = mass_pos.mass * idr;
					scale = nphi * idr * idr;
					x.accx += drx * scale;
					x.accy += dry * scale;
					x.accz += drz * scale;

					child++;

					//if (p==500) printf("%.15lf\n",scale);

				}

			}
		}

		BH_TYPE dthf = 0.5 * dtime;

		if (step > 0) {
			x.velx += (x.accx - ax) * dthf;
			x.vely += (x.accy - ay) * dthf;
			x.velz += (x.accz - az) * dthf;
		}
		particles_new.at(buffer_pos) = x;

	}

}

__global__ void k_update_positions(Domain<particle> particles, BH_TYPE dtime) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x + particles.X.start;

	if (pt < particles.X.end && pt >= particles.X.start) {

		BH_TYPE dthf = 0.5 * dtime;

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;

		particle* p = &(particles.at(pt));

		dvelx = p->accx * dthf;
		dvely = p->accy * dthf;
		dvelz = p->accz * dthf;

		velhx = p->velx + dvelx;
		velhy = p->vely + dvely;
		velhz = p->velz + dvelz;

		p->mass_pos.x += velhx * dtime;
		p->mass_pos.y += velhy * dtime;
		p->mass_pos.z += velhz * dtime;

		p->velx = velhx + dvelx;
		p->vely = velhy + dvely;
		p->velz = velhz + dvelz;
	}

}

void k_CUDA_compute_force(Task* t_) {

	//PERFORM_acc_timer *FORCES = new PERFORM_acc_timer();

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(0, particles);

	Domain<cell> nodes;
	t->getDomain(1, nodes);

	Domain<particle> particles_new;
	t->getDomain(2, particles_new);

	Domain<int> sort;
	t->getDomain(3, sort);

	//FORCES->start();

	int nbodies = particles_new.X.length();

	//	dim3 blockDIM = dim3(32, 1);
	//	dim3 gridDIM = dim3(nbodies / 32, 1);

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	int garbage[32];
	int* garbaged;
	cudaMalloc((void**) &garbaged, 32 * sizeof(int));
	//for (int i = 0; i < 32; i++)
	//	garbage[i] = 3;
	//cudaMemcpyAsync(garbaged, garbage, 32 * sizeof(int), cudaMemcpyHostToDevice,
	//		(cudaStream_t) (t->stream));

	//	int hit_count[100];
	//	int* hit_countd;
	//	cudaMalloc((void**) &hit_countd, 100 * sizeof(int));
	//	for (int i = 0; i < 100; i++)
	//		hit_count[i] = 0;
	//	cudaMemcpyAsync(hit_countd, hit_count, 100 * sizeof(int), cudaMemcpyHostToDevice,
	//			(cudaStream_t) (t->stream));

	//	long long unsigned int visit_count =0;
	long long unsigned int* visit_count_d;
	//	cudaMalloc((void**) &visit_count_d, sizeof(long long unsigned int));
	//	cudaMemcpy(visit_count_d, &visit_count, sizeof(long long unsigned int), cudaMemcpyHostToDevice);



	fprintf(stderr, "Launching fullt for %d particles...\n", nbodies);

	checkCUDAmemory(""); 

	checkCUDAError();
	k_compute_force<<<gridDIM,blockDIM,0,(cudaStream_t)(t->stream)>>>
	(nodes, particles, particles_new, t->tree_height, t->dtime, t->epssq, t->dsq, t->step, garbaged, sort, visit_count_d, t->FIB);
	checkCUDAError();

	cudaStreamSynchronize(t->stream);

	//cudaMemcpy(&visit_count, visit_count_d, sizeof(long long unsigned int), cudaMemcpyDeviceToHost);
	//printf("%lld\n",visit_count);

	//cudaMemcpy(garbage, garbaged, 32 * sizeof(int), cudaMemcpyDeviceToHost);

	//for (int i = 0; i < 32; i++)
	//	fprintf(stderr, "garbage %d\n", garbage[i]);

	//	cudaMemcpy(hit_count, hit_countd, 100 * sizeof(int), cudaMemcpyDeviceToHost);
	//
	//		for (int i = 0; i < 100; i++)
	//			printf("p: %d hits: %d\n", i, hit_count[i]);


	//cudaStreamSynchronize((cudaStream_t) (t->stream));

	//UPD->start();


	k_update_positions<<<gridDIM,blockDIM,0,(cudaStream_t)(t->stream)>>>
	(particles_new, t->dtime);

	cudaStreamSynchronize(t->stream);
	//FORCES->stop();

	//UPD->stop();

	checkCUDAError();

	//fprintf(stderr, "FORCES: %s\n", FORCES->print());
	//fprintf(stdout, "%s\t", FORCES->print());
	//fprintf(stderr, "UPD: %s\n", UPD->print());

}

//void k_CUDA_update_positions(Task* t_) {
////
////	size_t free, total;
////	cuMemGetInfo(&free, &total);
////	printf("cuda positions free mem start %d total %d\n", free / 1024 / 1024, total / 1024 / 1024);
//
//	//Task_UpdatePos* t = (Task_UpdatePos*) t_;
//
//	Domain<particle> particles;
//	t->getDomain(0, particles);
//
//	int nbodies = particles.X.length();
//
//	dim3 blockDIM = dim3(512, 1);
//	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);
//
//	//dim3 blockDIM = dim3(1000, 1);
//	//dim3 gridDIM = dim3( 1, 1);
//
//	k_update_positions<<<blockDIM,gridDIM,0,(cudaStream_t)(t->stream)>>>(particles, t->dtime);
//	checkCUDAError();
//
//}
