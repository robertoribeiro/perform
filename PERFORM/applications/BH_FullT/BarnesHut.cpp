#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut.h"

using namespace std;
using namespace tbb;

#define GARBAGE_FIB


void compute_force(Domain<cell> cells, Domain<particle> particles, Domain<particle> particles_new,
		int buffer_pos, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step, Domain<int> sort,
		int* GARBAGE, int FIB) {

	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
	BH_TYPE ax, ay, az;


	int p = sort.at(buffer_pos);
	//int p = buffer_pos;
	particle x = particles.at(p);

	ax = x.accx;
	ay = x.accy;
	az = x.accz;

	x.accx = 0.0;
	x.accy = 0.0;
	x.accz = 0.0;

	char stack[tree_height];
	int stack_top = 0;

	int current = 0;
	int child = 0;

	dsq *= 0.25;

	mass_position mass_pos_p = x.mass_pos;

	while (!(stack_top == 0 && child == 8)) {

		if (child == 8) {
			//current = cells[current].parent_cell_index;
			current = cells.at(current).parent_cell_index;
			dsq *= 4;
			child = stack[--stack_top];
			child++;

		} else {

			mass_position mass_pos;
			if (cells.at(current).child_types[child] == PARTICLE)
				mass_pos = particles.at(cells.at(current).child_indexes[child]).mass_pos;
			else if (cells.at(current).child_types[child] == CELL)
				mass_pos = cells.at(cells.at(current).child_indexes[child]).mass_pos;
			else {
				child++;
				continue;
			}

			drx = mass_pos.x - mass_pos_p.x;
			dry = mass_pos.y - mass_pos_p.y;
			drz = mass_pos.z - mass_pos_p.z;

			drsq = drx * drx + dry * dry + drz * drz;


#ifdef GARBAGE_FIB
				double r = 7778742049.0;
				double a = fib(FIB);
				//GARBAGE[threadIdx.x & 31] = a;
				GARBAGE[0] = r == a;
#endif



			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
			if (drsq < dsq) {
				if (cells.at(current).child_types[child] == CELL) {
					dsq *= 0.25;
					current = cells.at(current).child_indexes[child];
					stack[stack_top++] = child;
					child = 0;
				} else if (cells.at(current).child_types[child] == PARTICLE) {
					if (mass_pos.id != mass_pos_p.id) {
						drsq += epssq;
						idr = 1 / sqrt(drsq);
						nphi = mass_pos.mass * idr;
						scale = nphi * idr * idr;
						x.accx += drx * scale;
						x.accy += dry * scale;
						x.accz += drz * scale;

					}
					child++;
				} else if (cells.at(current).child_types[child] == NILL) {
					child++;
				}
			} else {
				drsq += epssq;
				idr = 1 / sqrt(drsq);
				nphi = mass_pos.mass * idr;
				scale = nphi * idr * idr;
				x.accx += drx * scale;
				x.accy += dry * scale;
				x.accz += drz * scale;
				child++;
			}

		}
	}

	BH_TYPE dthf = 0.5 * dtime;

	if (step > 0) {
		x.velx += (x.accx - ax) * dthf;
		x.vely += (x.accy - ay) * dthf;
		x.velz += (x.accz - az) * dthf;
	}

	particles_new.at(buffer_pos) = x;

	/*PrintBH_TYPE(particles[particle].velx);
	 printf("   ");
	 PrintBH_TYPE(particles[particle].vely);
	 printf("   ");
	 PrintBH_TYPE(par ticles[particle].velz);
	 printf("\n");*/

}

void k_TBB_compute_force(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<particle> particles;
	t->getDomain(0, particles);

	Domain<cell> nodes;
	t->getDomain(1, nodes);

	Domain<particle> particles_new;
	t->getDomain(2, particles_new);

	Domain<int> sort;
	t->getDomain(3, sort);


	fprintf(stderr, "Launching TBB for %d particles...\n", particles_new.X.length());


	int* garbage = new int[1];

	tbb::parallel_for(
			blocked_range<int> (particles_new.X.start, particles_new.X.end),
			ComputeForceSet(nodes, particles, particles_new, t->tree_height, t->dtime, t->epssq,
					t->dsq, t->step, sort,garbage,  t->FIB));

	delete garbage;


	tbb::parallel_for(blocked_range<int> (particles_new.X.start, particles_new.X.end),
			UpdatePositionSet(particles_new, t->dtime));

}

//void k_TBB_update_positions(Task* t_) {
//
//	//Task_UpdatePos* t = (Task_UpdatePos*) t_;
//
//	Domain<particle> particles;
//	//t->getDomain(0, particles);
//
//	tbb::parallel_for(blocked_range<int> (particles.X.start, particles.X.end),
//			UpdatePositionSet(particles, t->dtime));
//
//}

void update_positions(Domain<particle> particles, int part, BH_TYPE dtime) {

	BH_TYPE dthf = 0.5 * dtime;

	BH_TYPE dvelx, dvely, dvelz;
	BH_TYPE velhx, velhy, velhz;

	particle* p = &(particles.at(part));

	dvelx = p->accx * dthf;
	dvely = p->accy * dthf;
	dvelz = p->accz * dthf;

	velhx = p->velx + dvelx;
	velhy = p->vely + dvely;
	velhz = p->velz + dvelz;

	p->mass_pos.x += velhx * dtime;
	p->mass_pos.y += velhy * dtime;
	p->mass_pos.z += velhz * dtime;

	p->velx = velhx + dvelx;
	p->vely = velhy + dvely;
	p->velz = velhz + dvelz;
}

double jobBARNES_HUT(int argc, char *argv[]) {

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error
	BH_TYPE diameter, centerx, centery, centerz;

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();

	initBARNES_HUT( argc,argv, nbodies, timesteps,  dtime,  eps,  tol);

	particle* particles = new particle[nbodies];
	Point3D* points = new Point3D[nbodies];
	ReadGaussPointSet(points,nbodies);
	PointsToParticleSystem(points,nbodies,particles);
	compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);


//	balanced(points,nbodies,diameter/2);
//	PointsToParticleSystem(points,nbodies,particles);


	int nnodes = nbodies * 2;
//	if (nnodes < 1024 * 16)
//		nnodes = 1024 * 20;


	int* particle_count = new int[nnodes];
	int* sort_order = new int[nbodies];

	cell* nodes;

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	Domain<struct s_particle> *domain_particles = new Domain<struct s_particle> (1, R, particles,
			dim_space(0, nbodies));

	particle * particles_new = new particle[nbodies];

	memcpy(particles_new, particles, nbodies * sizeof(particle));

	Domain<struct s_particle> *domain_new_particles = new Domain<struct s_particle> (1, W,
			particles_new, dim_space(0, nbodies));

	float dice_v;

	sleep(5);
	dice_v = atoi(argv[3]) == 0 ? 0 : 1.0 / atoi(argv[3]);

	for (int step = 0; step < timesteps; step++) {

		particle * particles_buffer =
				(particle*) domain_particles->phy_chunk->hook_pointer;

		compute_bounding_box(particles_buffer, nbodies, diameter, centerx, centery, centerz);

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)	root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)	root.child_indexes[k] = -1;

		nodes = new cell[nnodes];

		nodes[0] = root;

		const BH_TYPE radius = diameter * 0.5;

		cell_count = build_tree_array(nodes, radius, particles_buffer, nbodies, tree_height);

		memset(particle_count,0,sizeof(int)*nnodes);
		compute_center_of_mass(nodes, particles_buffer, 0, tree_height,particle_count);


		memset(sort_order,0,sizeof(int)*nbodies);
		sort(sort_order,particle_count,nodes, particles_buffer,nbodies,cell_count);

		Domain<struct cell_s> *domain_nodes = new Domain<struct cell_s> (1, R, nodes,
				dim_space(0, nnodes));

		Domain<int> *domain_sort = new Domain<int> (1, R, sort_order,
					dim_space(0, nbodies));

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different


		fprintf(stderr,"dsq: %.10f\n",dsq);

		//if (step == 0)
	//		PERFORM_signal_JobStarted();
		timer->start();

		Task_ForcesCalc* t = new Task_ForcesCalc(TASK_FORCE_TYPE);

		t->associate_domain(domain_particles);
		t->associate_domain(domain_nodes);
		t->associate_domain(domain_new_particles);
		t->associate_domain(domain_sort);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		t->parent_host_task = (void*) t;
		t->diceable = dice_v == 0 ? false : true;
		t->dice_value = PM_Metric_normalized(dice_v);
		t->FIB= atoi(argv[6]);
		t->job_elements = nbodies;

		t->associate_kernel(CPU, &k_TBB_compute_force);
		t->associate_kernel(GPU, &k_CUDA_compute_force);

		performQ->addTasks(t);

		wait_for_all_tasks();

	//	if (step + 1 == timesteps)
//			PERFORM_signal_taskSubmissionEnded();

		performDL->splice(domain_new_particles);

		timer->stop();

//		if (step + 1 == timesteps)
//			PERFORM_signal_JobFinished(timer->duration);

		performDL->delete_domain(domain_nodes);

		Domain<struct s_particle> *tmp;
		tmp = domain_particles;
		domain_particles = domain_new_particles;
		domain_new_particles = tmp;

		domain_particles->permissions = R;
		domain_new_particles->permissions = W;

	}

	fprintf(stdout,"Simulation done in %s\t",timer->print());

	//PrintParticles((particle*)domain_particles->phy_chunk->original_host_pointer, nbodies-1000);

	particle* particles_to_check = new particle[nbodies];


	for (int i = 0; i < (nbodies); i++) {

			int particle = sort_order[i];
			//int particle = i;
			particles_to_check[particle] = particles_new[i];
		}
	
	jobBARNES_HUT_check(argc,argv,points,particles_to_check);

	return timer->duration;

}

