#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"
//#include "BarnesHut.h"

#define MAX_SEC_TASK 8
#define MAX_P_PER_TASK 4


class ubkTask_ForcesCalc /*: public ubkTask*/ {
public:

	short target_count;
	int p_target[MAX_P_PER_TASK];
	//float dtime;
	//float epssq;
	float dsq;
	bool tree_at_root;
	//short step;
	int current_cell[MAX_P_PER_TASK];

	__H_D__
	ubkTask_ForcesCalc() {
	}



};

class Task_ForcesCalc: public Task {
public:

	int tree_height;
	int start;

	float dtime;
	float epssq;
	float dsq;

	bool tree_at_root;
	int step;

	int current_cell;
	uint FIB;


	__H_D__
	Task_ForcesCalc() :
		Task() {
	}
	__H_D__
	Task_ForcesCalc(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<particle>* particles;
		getDomain(0, particles);

		Domain<cell>* nodes;
		getDomain(1, nodes);

		Domain<particle>* p_particles_new;
		getDomain(2, p_particles_new);

		Domain<particle>* sort;
		getDomain(3, sort);


		float len1 = (floor((job_elements / 32) * m.value)) * 32;

		if (p_particles_new->X.length() <= len1 || len1 == 0) {
					Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[1];
					new_tasks[0] = this;
					new_tasks_ = (Task**) new_tasks;
					return 1;
				}

		Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[2];


		// task 0
		Task_ForcesCalc* t = new Task_ForcesCalc(type);

		dim_space a = dim_space(p_particles_new->X.start, p_particles_new->X.start + len1);

		Domain<float> *sub_domain_result = new Domain<float> (p_particles_new, 1,
				p_particles_new->permissions, a);

		t->associate_domain(particles);
		t->associate_domain(nodes);
		t->associate_domain(sub_domain_result);
		t->associate_domain(sort);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		//t->start = a.start;
		//t->current_cell = 0;
		t->device_preference = device_preference;
		t->parent_host_task = (void*) this;
		t->wl.value = len1 / job_elements;
		t->dice_lvl = dice_lvl + 1;
		//t->nbodies = nbodies;
		t->associate_kernel(this);
		t->job_elements = job_elements;
		t->FIB = FIB;
		new_tasks[task_count++] = t;




		// task 1
		t = new Task_ForcesCalc(type);

		a = dim_space(p_particles_new->X.start + len1, p_particles_new->X.end);

		sub_domain_result = new Domain<float> (p_particles_new, 1,
				p_particles_new->permissions, a);

		t->associate_domain(particles);
		t->associate_domain(nodes);
		t->associate_domain(sub_domain_result);
		t->associate_domain(sort);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		//t->start = a.start;
		//t->current_cell = 0;
		t->device_preference = device_preference;
		t->parent_host_task = (void*) this;
		t->wl.value = (p_particles_new->Y.length() - len1) / job_elements;
		t->dice_lvl = dice_lvl + 1;
	//	t->nbodies = nbodies;
		t->associate_kernel(this);
		t->job_elements = job_elements;
		t->FIB = FIB;

		new_tasks[task_count++] = t;

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}


	int dice2(Task**& new_tasks_, PM_Metric_normalized m) {

			int task_count = 0;

			Domain<particle>* particles;
			getDomain(0, particles);

			Domain<cell>* nodes;
			getDomain(1, nodes);

			Domain<particle>* p_particles_new;
			getDomain(2, p_particles_new);

			Domain<particle>* sort;
			getDomain(3, sort);

			float N = p_particles_new->X.length();

			float fst_length = floor(N * (m.value));

			int pow1 = floor(log2(fst_length));
			int powN = log2(N);

			int divide_task_by = pow(2, powN - pow1);

			if (divide_task_by == -1) {
				divide_task_by = p_particles_new->X.length();

				if (divide_task_by % 32 != 0) {
					printf("Error: devide parameter for uberkernel needs to be a divisor of 32\n");
					exit(-1);
				};
			}

			int block_step = (p_particles_new->X.length() / divide_task_by);

			Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[divide_task_by];

			for (int i = 0; i < divide_task_by; i++) {

				Task_ForcesCalc* t = new Task_ForcesCalc(TASK_FORCE_TYPE);

				dim_space a = dim_space(p_particles_new->X.start + i * block_step,
						(p_particles_new->X.start + i * block_step) + block_step);

				Domain<particle>* particles_new = new Domain<particle> (p_particles_new, 1, RW, a);

				t->associate_domain(particles);
				t->associate_domain(nodes);
				t->associate_domain(particles_new);
				t->associate_domain(sort);


				t->tree_height = tree_height;
				t->dtime = dtime;
				t->epssq = epssq;
				t->dsq = dsq;
				t->step = step;
				t->start = a.start;
				t->current_cell = 0;
				t->device_preference = device_preference;
				t->parent_host_task = (void*) this;
				t->wl = PM_Metric_normalized((block_step * wl.value) / N);
				t->dice_lvl = dice_lvl + 1;

				t->associate_kernel(this);

				new_tasks[task_count++] = t;

			}

			new_tasks_ = (Task**) new_tasks;
			return task_count;
		}



};

#endif /* UBK_TASK_H_ */
