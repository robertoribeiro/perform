#include "../../perform.h"
#include "task/Task.h"
#include "user_defined_task.h"

void k_CUDA_wrapper_uberkernel(Task* t, Task** task_prms, int child_task_count) {

}

void k_CPU_init(Task* t_) {

	Task_Initialize t = *(Task_Initialize*) t_;

	Domain<int> d;
	t.getDomain(0, d);

	for (int i = d.X.start; i < d.X.end; i++) {
		for (int j = d.Y.start; j < d.Y.end; j++) {
			d.at(i, j) = t.value;
		}
	}
}

void k_CUDA_init(Task* t_) ;



int main(int argc, char *argv[]) {

	PERFORM_timer *timer = new PERFORM_timer();

	timer->start();


	//int sch, int dev_config, int initial_dice, int cpu_flops
	initPERFORM(3,4,4,650);


	int N = 32;

	int* matrix = new int[N*N];
	memset(matrix, 0, sizeof(int) * N * N);

	Domain<int>* d = new Domain<int>(2, W, matrix, dim_space(0, N), dim_space(0, N));

	Task_Initialize* t = new Task_Initialize(REGULAR);

	t->value = 1;
	t->job_elements= N;

	t->associate_domain(d);
	t->associate_kernel(CPU, &k_CPU_init);
	t->associate_kernel(GPU, &k_CUDA_init);


	//Task** v;
	//t->dice(v,2);

	performQ->addTasks(t);

	wait_for_all_tasks();

	performDL->splice(d);


		printf("\n\n");
		int ii, jj;
		for (ii = 0; ii < N; ii++) {
			for (jj = 0; jj < N; jj++)
				cout << matrix[ii * N + jj] << " ";
			printf("\n");
		}

	timer->stop();

	shutdownPERFORM();

	LOG(0.1,cout << "\n\nRun-time terminated in " << timer->duration << "s." << endl;)

	return 0;
}

