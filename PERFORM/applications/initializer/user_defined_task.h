#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"

void k_CUDA_init(Task* t_);
void k_CPU_init(Task* t_);

class Task_Initialize: public Task {

public:

	int value;

	Task_Initialize(TASK_TYPE t_) :
			Task(t_) {

	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		Domain<int>* d;
		getDomain(0, d);

		float len1 = ceil(job_elements * m.value);

		Task_Initialize** new_tasks = new Task_Initialize*[2];

		if (d->X.length() <= len1 || len1 == 0) {
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			return 1;
		}

		Domain<int>* subD1 = new Domain<int>(d, 2, W,
				dim_space(d->X.start,d->X.start + len1),
				dim_space(d->Y.start, d->Y.end));

		Domain<int>* subD2 = new Domain<int>(d, 2, W,
				dim_space(d->X.start + len1, d->X.end),
				dim_space(d->Y.start, d->Y.end));

		Task_Initialize* t1 = new Task_Initialize(REGULAR);

		t1->value = value;

		t1->associate_domain(subD1);
		t1->associate_kernel(CPU, &k_CPU_init);
		t1->associate_kernel(GPU, &k_CUDA_init);
		t1->wl.value = len1 / job_elements;
		t1->dice_lvl = dice_lvl + 1;
		t1->job_elements = job_elements;

		Task_Initialize* t2 = new Task_Initialize(REGULAR);

		t2->value = t1->value +1;

		t2->associate_domain(subD2);
		t2->associate_kernel(CPU, &k_CPU_init);
		t2->associate_kernel(GPU, &k_CUDA_init);
		t2->wl.value = (d->X.length() - len1) / job_elements;
		t2->dice_lvl = dice_lvl + 1;
		t2->job_elements = job_elements;

		new_tasks[0] = t1;
		new_tasks[1] = t2;

		new_tasks_ = (Task**) new_tasks;

		return 2;

	}

};

#endif /* UBK_TASK_H_ */
