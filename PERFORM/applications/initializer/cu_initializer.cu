

#include "user_defined_task.h"

__global__ void k_subCUDA_init(Task_Initialize t, Domain<int> d){

	for (int i = d.X.start; i< d.X.end; i++) {
		for (int j = d.Y.start; j< d.Y.end; j++){
		d.at(i,j)= t.value;
	}
	}

}

void k_CUDA_init(Task* t_) {

	Task_Initialize t = *(Task_Initialize*)t_;

	Domain<int>  d;
	t.getDomain(0,d);

	dim3 dimBlock(1,1);
	dim3 dimGrid(1,1);

	k_subCUDA_init<<<1,1>>>(t,d);

}

