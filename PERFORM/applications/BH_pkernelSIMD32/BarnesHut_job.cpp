#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "BarnesHut_job.h"

using namespace std;
using namespace tbb;

double jobBARNES_HUT(int argc, char *argv[]) {

	PERFORM_acc_timer* timer = new PERFORM_acc_timer();

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error
	BH_TYPE diameter, centerx, centery, centerz;
	BH_TYPE radius;
	int nbodies;
	int timesteps;

	initBARNES_HUT(argc, argv, nbodies, timesteps, dtime, eps, tol);

	int ncells = nbodies;
	int nnodes = nbodies + ncells;
	int tree_height = 0;

	FLOAT_ARRAY input_chunk;
	FLOAT_ARRAY result_chunk;

	int input_chunk_length = (nnodes) * 4 //posxyz,mass for particles and cells
			+ (nbodies * (8)) //childs
			+ nbodies * 3 * 3// vel,acc,t_acc;
			+ nbodies; // sort

	int result_chunk_length = (nbodies) * 3 //posxyz for particles and cells
			+ nbodies * 9; // vel,acc,t_acc;

	input_chunk = (FLOAT_ARRAY) PERFORM_alloc(input_chunk_length * sizeof(float));
	result_chunk = (FLOAT_ARRAY) PERFORM_alloc(result_chunk_length * sizeof(float));

	memset(input_chunk, 0, input_chunk_length * sizeof(float));
	memset(result_chunk, 0, result_chunk_length * sizeof(float));

	FLOAT_ARRAY posx = input_chunk;
	FLOAT_ARRAY posy = posx + (nnodes);
	FLOAT_ARRAY posz = posy + (nnodes);
	FLOAT_ARRAY mass = posz + (nnodes);
	INT_ARRAY childs = (INT_ARRAY) input_chunk;

	FLOAT_ARRAY r_posx = result_chunk;
	FLOAT_ARRAY r_posy = r_posx + (nbodies);
	FLOAT_ARRAY r_posz = r_posy + (nbodies);
	INT_ARRAY sort_order = (INT_ARRAY) input_chunk + (nnodes) * 4 + (nbodies * (8)) + nbodies * 3
			* 3;

	INT_ARRAY particle_count = new int[nnodes];

	for (int po = 0; po < nbodies; po++)
		sort_order[po] = -1;

	Point3D* points = new Point3D[nbodies];
	ReadGaussPointSet(points, nbodies);
	PointsToParticleSystem(points, nbodies, posx, posy, posz, mass);
	compute_bounding_box_OPT(posx, posy, posz, nbodies, diameter, centerx, centery, centerz);

			//balanced(points, nbodies, diameter / 2);
			//PointsToParticleSystem(points, nbodies, posx, posy, posz, mass);

	//memcpy(r_posx, posx, nbodies * sizeof(float));
	//memcpy(r_posy, posy, nbodies * sizeof(float));
	//memcpy(r_posz, posz, nbodies * sizeof(float));

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

sleep(5);
	float dice_v;

	dice_v = atoi(argv[3]) == 0 ? 0 : 1.0 / atoi(argv[3]);

	for (int step = 0; step < timesteps; step++) {

		Domain<float> *domain_input = new Domain<float> (1, R, input_chunk,
				dim_space(0, input_chunk_length));
		Domain<float> *domain_result = new Domain<float> (2, RW, result_chunk, dim_space(0, 12),
				dim_space(0, nbodies));

		compute_bounding_box_OPT(posx, posy, posz, nbodies, diameter, centerx, centery, centerz);

		posx[TREE_ROOT] = centerx;
		posy[TREE_ROOT] = centery;
		posz[TREE_ROOT] = centerz;

		for (int k = 0; k < 8; k++)
			childs[TREE_ROOT * 8 + k] = -1;

		radius = diameter * 0.5;

		ncells = build_tree_array_OPT(posx, posy, posz, childs, radius, nbodies, nnodes,
				tree_height);

		memset(particle_count, 0, sizeof(int) * nnodes);
		compute_center_of_mass_OPT(posx, posy, posz, childs, particle_count, mass, tree_height,
				nnodes, nbodies);

		memset(sort_order, 0, sizeof(int) * nbodies);
		sort(childs, sort_order, particle_count, nbodies, nnodes, ncells);

		for (int i = 0; i < (nbodies); i++) {

			int buf = sort_order[i];
			//int particle = i;
			r_posx[i] = posx[buf];
			r_posy[i] = posy[buf];
			r_posz[i] = posz[buf];

		}

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different

		fprintf(stderr, "dsq: %.10f\n", dsq);

		if (step == 0)
			PERFORM_signal_JobStarted();
		timer->start();

		Task_ForcesCalc* t;

		if (atoi(argv[4]) == 0){
			t = new Task_ForcesCalc(REGULAR);
		}else
			t = new Task_ForcesCalc(IRREGULAR);

		t->associate_domain(domain_input);
		t->associate_domain(domain_result);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		t->nbodies = nbodies;
		t->FIB = atoi(argv[6]);
		t->diceable = dice_v == 0 ? false : true;
		t->job_elements = nbodies;

		t->associate_kernel(CPU, &k_TBB_compute_force);
		t->associate_kernel(GPU, &k_CUDA_compute_force);

		performQ->addTasks(t);
		//
		wait_for_all_tasks();

		if (step + 1 == timesteps)
			PERFORM_signal_taskSubmissionEnded();

		performDL->splice(domain_result);
		timer->stop();

		if (step + 1 == timesteps)
			PERFORM_signal_JobFinished(timer->duration);

		performDL->delete_domain(domain_result);
		performDL->delete_domain(domain_input);

		memcpy(posx, r_posx, nbodies * sizeof(float));
		memcpy(posy, r_posy, nbodies * sizeof(float));
		memcpy(posz, r_posz, nbodies * sizeof(float));

		FLOAT_ARRAY vel = input_chunk + (nnodes) * 4 + (nbodies * 8);
		memcpy(vel, result_chunk + (nbodies) * 3, nbodies * 9 * sizeof(float));

	}

	delete particle_count;

	fprintf(stdout, "Simulation done in %s\t", timer->print());

	particle* particles_to_check = new particle[nbodies];

	for (int i = 0; i < (nbodies); i++) {

		int particle = sort_order[i];
		//int particle = i;
		particles_to_check[particle].mass_pos.x = r_posx[i];
		particles_to_check[particle].mass_pos.y = r_posy[i];
		particles_to_check[particle].mass_pos.z = r_posz[i];

	}

	//jobBARNES_HUT_check(argc, argv, points, particles_to_check);


	delete particles_to_check;

	return timer->duration;

}
