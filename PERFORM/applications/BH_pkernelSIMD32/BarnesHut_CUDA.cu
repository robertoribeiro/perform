#include "BarnesHut_job.h"

#define GARBAGE_FIB


__device__ double fibFULLT(double n) {
	double a = 1, b = 1;
	for (int i = 3; i <= n; i++) {
		double c = a + b;
		a = b;
		b = c;
	}
	return a;
}

__global__ void compute_force(Task_ForcesCalc t, FLOAT_ARRAY posx, FLOAT_ARRAY posy,
		FLOAT_ARRAY posz, FLOAT_ARRAY mass, INT_ARRAY childs, FLOAT_ARRAY accx, FLOAT_ARRAY accy,
		FLOAT_ARRAY accz, FLOAT_ARRAY t_accx, FLOAT_ARRAY t_accy, FLOAT_ARRAY t_accz,
		INT_ARRAY sort_order, int* GARBAGE, int FIB, Domain<float> domain_result) {

	int buffer_pos = blockIdx.x * blockDim.x + threadIdx.x + domain_result.Y.start;

	if (buffer_pos < domain_result.Y.end && buffer_pos >= domain_result.Y.start) {

		int particle;
		float epssq;
		float dsq;

		float drx, dry, drz, nphi, scale, idr;
		double drsq;
		float ax, ay, az;

		int nbodies = t.nbodies;

		float pos_p_x;
		float pos_p_y;
		float pos_p_z;

		epssq = t.epssq;
		dsq = t.dsq;

		//particle = buffer_pos;
		particle = sort_order[buffer_pos];

		domain_result.at(9, buffer_pos) = accx[particle];
		domain_result.at(10, buffer_pos) = accy[particle];
		domain_result.at(11, buffer_pos) = accz[particle];

		//r_t_accx[p] = accx[p];
		//r_t_accy[p] = accy[p];
		//r_t_accz[p] = accz[p];

		domain_result.at(6, buffer_pos) = 0.0;
		domain_result.at(7, buffer_pos) = 0.0;
		domain_result.at(8, buffer_pos) = 0.0;

		//r_accx[p] = 0.0;
		//r_accy[p] = 0.0;
		//r_accz[p] = 0.0;

		ax = 0.0;
		ay = 0.0;
		az = 0.0;

		pos_p_x = posx[particle];
		pos_p_y = posy[particle];
		pos_p_z = posz[particle];

		int node[100];
		short pos[100];

		register int depth = 0;

		node[depth] = TREE_ROOT;
		pos[depth] = 0;

		dsq *= 0.25;

		while (depth != -1) {

			while (pos[depth] < 8) {

				float pos_x;
				float pos_y;
				float pos_z;

				int CURRENT = childs[node[depth] * 8 + pos[depth]];
				pos[depth]++;

				if (CURRENT != -1) {
					pos_x = posx[CURRENT];
					pos_y = posy[CURRENT];
					pos_z = posz[CURRENT];
				} else {
					continue;
				}

				drx = pos_x - pos_p_x;
				dry = pos_y - pos_p_y;
				drz = pos_z - pos_p_z;

				// CRITICAL ACCURACY SECTION //

				drsq = drx * drx + dry * dry + drz * drz;


				///////// GARBAGE /////////


#ifdef GARBAGE_FIB
				double r = 7778742049.0;
				double a = fibFULLT(FIB);
				//GARBAGE[threadIdx.x & 31] = a;
				atomicAnd((int*)&(GARBAGE[threadIdx.x & 31]),(r) == a);
#endif

				///////// GARBAGE /////////


				//drsq = drx * drx + dry * dry + drz * drz;
				if (drsq < dsq) {
					if (IS_CELL(CURRENT)) {
						depth++;
						dsq *= 0.25;
						node[depth] = CURRENT;
						pos[depth] = 0;

					} else if (IS_PARTICLE(CURRENT)) {

						if (CURRENT != particle) {

							drsq += epssq;
							idr = rsqrtf(drsq);
							nphi = mass[CURRENT] * idr;
							scale = nphi * idr * idr;

							ax += drx * scale;
							ay += dry * scale;
							az += drz * scale;

							//if (particle==500) printf("%.10lf\n",scale);

							//if (p == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);

						}
					}

				} else {

					drsq += epssq;
					idr = rsqrtf(drsq);
					//nphi = mass_pos.mass * idr;
					nphi = mass[CURRENT] * idr;
					scale = nphi * idr * idr;

					ax += drx * scale;
					ay += dry * scale;
					az += drz * scale;

					//if (particle==500) printf("%.10lf\n",scale);


					//if (p == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);
				}

			}
			dsq *= 4;
			depth--;
		}

		domain_result.at(6, buffer_pos) += ax;
		domain_result.at(7, buffer_pos) += ay;
		domain_result.at(8, buffer_pos) += az;

		//	if (particle == 0)
		//				printf("%.7f\n",domain_result.at(6, buffer_pos));


		//r_accx[p] += ax;
		//r_accy[p] += ay;
		//r_accz[p] += az;
	}

}

__global__ void k_update_vel_positions(Domain<float> domain_result, BH_TYPE dtime, int step,
		int nbodies) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x /*+ particles.X.start*/;

	FLOAT_ARRAY result_chunk = (FLOAT_ARRAY) domain_result.device_data_buffer;

	FLOAT_ARRAY r_posx = result_chunk;
	FLOAT_ARRAY r_posy = r_posx + (nbodies);
	FLOAT_ARRAY r_posz = r_posy + (nbodies);
	FLOAT_ARRAY r_velx = r_posz + nbodies;
	FLOAT_ARRAY r_vely = r_velx + (nbodies);
	FLOAT_ARRAY r_velz = r_vely + (nbodies);
	FLOAT_ARRAY r_accx = r_velz + nbodies;
	FLOAT_ARRAY r_accy = r_accx + nbodies;
	FLOAT_ARRAY r_accz = r_accy + nbodies;
	FLOAT_ARRAY r_t_accx = r_accz + nbodies;
	FLOAT_ARRAY r_t_accy = r_t_accx + nbodies;
	FLOAT_ARRAY r_t_accz = r_t_accy + nbodies;

	//if (pt < particles.X.end && pt >= particles.X.start) {
	if (pt < nbodies && pt >= 0) {

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;
		BH_TYPE dthf = 0.5 * dtime;
		//particle* p = &(particles.at(pt));

		//		if (pt == 0){
		//			printf("ACCt_x: %.15f\n",p->t_accx);
		//			printf("ACCx: %.15f\n",p->accx);
		//		}

		if (step > 0) {
			r_velx[pt] += (r_accx[pt] - r_t_accx[pt]) * dthf;
			r_vely[pt] += (r_accy[pt] - r_t_accy[pt]) * dthf;
			r_velz[pt] += (r_accz[pt] - r_t_accz[pt]) * dthf;
		}

		dvelx = r_accx[pt] * dthf;
		dvely = r_accy[pt] * dthf;
		dvelz = r_accz[pt] * dthf;

		velhx = r_velx[pt] + dvelx;
		velhy = r_vely[pt] + dvely;
		velhz = r_velz[pt] + dvelz;

		r_posx[pt] += velhx * dtime;
		r_posy[pt] += velhy * dtime;
		r_posz[pt] += velhz * dtime;

		r_velx[pt] = velhx + dvelx;
		r_vely[pt] = velhy + dvely;
		r_velz[pt] = velhz + dvelz;

	}

}

void k_CUDA_update_vel_positions(Task* t_) {

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;

	Domain<float> domain_result;
	t->getDomain(1, domain_result);

	int nbodies = domain_result.Y.length();

	dim3 blockDIM = dim3(768, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	k_update_vel_positions<<<gridDIM,blockDIM,0,(cudaStream_t)(t->stream)>>>
	(domain_result, t->dtime, t->step, nbodies);
	checkCUDAError();

	//cudaDeviceSynchronize();

	//PrintParticles((particle*)particles.phy_chunk->original_host_pointer, nbodies-1023);


}

void k_CUDA_compute_force(Task* t_) {

	PERFORM_acc_timer *FORCES = new PERFORM_acc_timer();

	Task_ForcesCalc* t = (Task_ForcesCalc*) t_;
	Domain<float> domain_input;
	t->getDomain(0, domain_input);

	Domain<float> domain_result;
	t->getDomain(1, domain_result);

	int task_nbodies = domain_result.Y.length();

	FLOAT_ARRAY input_chunk = (FLOAT_ARRAY) domain_input.device_data_buffer;
	FLOAT_ARRAY result_chunk = (FLOAT_ARRAY) domain_result.device_data_buffer;

	int nnodes = t->nbodies * 2;
	int nbodies = t->nbodies;

	FLOAT_ARRAY posx;
	FLOAT_ARRAY posy;
	FLOAT_ARRAY posz;
	FLOAT_ARRAY mass;
	INT_ARRAY childs;
	FLOAT_ARRAY accx;
	FLOAT_ARRAY accy;
	FLOAT_ARRAY accz;
	FLOAT_ARRAY t_accx;
	FLOAT_ARRAY t_accy;
	FLOAT_ARRAY t_accz;
	INT_ARRAY sort_order;

	FLOAT_ARRAY r_posx = result_chunk;
	FLOAT_ARRAY r_posy = r_posx + (task_nbodies);
	FLOAT_ARRAY r_posz = r_posy + (task_nbodies);
	FLOAT_ARRAY r_velx = r_posz + task_nbodies;
	FLOAT_ARRAY r_vely = r_velx + (task_nbodies);
	FLOAT_ARRAY r_velz = r_vely + (task_nbodies);
	FLOAT_ARRAY r_accx = r_velz + task_nbodies;
	FLOAT_ARRAY r_accy = r_accx + task_nbodies;
	FLOAT_ARRAY r_accz = r_accy + task_nbodies;
	FLOAT_ARRAY r_t_accx = r_accz + task_nbodies;
	FLOAT_ARRAY r_t_accy = r_t_accx + task_nbodies;
	FLOAT_ARRAY r_t_accz = r_t_accy + task_nbodies;

	posx = input_chunk;
	posy = posx + (nnodes);
	posz = posy + (nnodes);

	mass = posz + (nnodes);

	childs = (INT_ARRAY) input_chunk;

	accx = (nnodes) * 4 + (nbodies * (8)) + nbodies * 3 + input_chunk;
	accy = accx + nbodies;
	accz = accy + nbodies;

	t_accx = accz + nbodies;
	t_accy = t_accx + nbodies;
	t_accz = t_accy + nbodies;

	sort_order = (INT_ARRAY) (t_accz + nbodies);

	r_accx = result_chunk + task_nbodies * 3 + task_nbodies * 3;
	r_accy = r_accx + task_nbodies;
	r_accz = r_accy + task_nbodies;
	r_t_accx = r_accz + task_nbodies;
	r_t_accy = r_t_accx + task_nbodies;
	r_t_accz = r_t_accy + task_nbodies;

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	int garbage[32];
	int* garbaged;
	cudaMalloc((void**) &garbaged, 32 * sizeof(int));

	//for (int i = 0; i < 32; i++)
	//	garbage[i] = 3;
	//cudaMemcpyAsync(garbaged, garbage, 32 * sizeof(int), cudaMemcpyHostToDevice,
	//		(cudaStream_t) (t->stream));

	//	int hit_count[100];
	//	int* hit_countd;
	//	cudaMalloc((void**) &hit_countd, 100 * sizeof(int));
	//	for (int i = 0; i < 100; i++)
	//		hit_count[i] = 0;
	//	cudaMemcpyAsync(hit_countd, hit_count, 100 * sizeof(int), cudaMemcpyHostToDevice,
	//			(cudaStream_t) (t->stream));

	//	long long unsigned int visit_count =0;
	long long unsigned int* visit_count_d;
	//	cudaMalloc((void**) &visit_count_d, sizeof(long long unsigned int));
	//	cudaMemcpy(visit_count_d, &visit_count, sizeof(long long unsigned int), cudaMemcpyHostToDevice);


	checkCUDAError();
	FORCES->start();

	compute_force<<<gridDIM,blockDIM,0,(t->stream)>>>
	(*t, posx, posy, posz, mass, childs, accx, accy, accz, t_accx, t_accy, t_accz, sort_order, garbaged, t->FIB, domain_result);

	checkCUDAError();

	cudaStreamSynchronize(t->stream);

	FORCES->stop();

	//cudaMemcpy(&visit_count, visit_count_d, sizeof(long long unsigned int), cudaMemcpyDeviceToHost);
	//printf("%lld\n",visit_count);

	//cudaMemcpy(garbage, garbaged, 32 * sizeof(int), cudaMemcpyDeviceToHost);

	//for (int i = 0; i < 32; i++)
	//	fprintf(stderr, "garbage %d\n", garbage[i]);

	//	cudaMemcpy(hit_count, hit_countd, 100 * sizeof(int), cudaMemcpyDeviceToHost);
	//
	//		for (int i = 0; i < 100; i++)
	//			printf("p: %d hits: %d\n", i, hit_count[i]);


	//cudaStreamSynchronize((cudaStream_t) (t->stream));

	//UPD->start();

	k_CUDA_update_vel_positions(t);

	cudaStreamSynchronize(t->stream);

	//UPD->stop();

	checkCUDAError();

	fprintf(stderr, "FORCES: %s\n", FORCES->print());
	//fprintf(stdout, "%s\t", FORCES->print());
	//fprintf(stderr, "UPD: %s\n", UPD->print());


}
