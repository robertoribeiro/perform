#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <string.h>

#include "BarnesHut_utils.h"

#include "perform_utils.h"
#include "core.h"

using namespace std;

void DEPRECATED_load_data_set_OPT(FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, FLOAT_ARRAY mass,
		char *filename, int nbodies, int& timesteps, BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol) {

	BH_TYPE vx, vy, vz;
	register FILE *f;

	int f_nbodies;
	int f_timesteps;

	f = fopen(filename, "r+t");
	if (f == NULL) {
		fprintf(stderr, "file not found: %s\n", filename);
		exit(-1);
	}

	fscanf(f, "%d", &f_nbodies);
	fscanf(f, "%d", &f_timesteps);
	fscanf(f, "%f", &dtime);
	fscanf(f, "%f", &eps);
	fscanf(f, "%f", &tol);

	eps = 0.0005;
	tol = 0.2;
	//particles = new particle[nbodies];

	if (f_nbodies < nbodies)
		printf("Not enough particles\n");

	float* garbage;

	float tmpx,tmpy,tmpz;

	for (int i = 0; i < (nbodies); i++) {

		fscanf(f, "%fE", &(tmpx));
		fscanf(f, "%fE", &(tmpy));
		fscanf(f, "%fE", &(tmpz));


		posx[i]=tmpx;
		posy[i]=tmpy;
		posz[i]=tmpz;

		fscanf(f, "%fE", &(garbage));
		mass[i] = 0.23;
		//	particles[i].velx = 0.0f;
		//	particles[i].vely = 0.0f;
		//	particles[i].velz = 0.0f;
		//particles[i].mass_pos.id = i;
		//particles[i].lock = 0;


		//particles[i].accx = 0.0;
		//particles[i].accy = 0.0;
		//particles[i].accz = 0.0;
	}

	fclose(f);

}

void compute_bounding_box_OPT(FLOAT_ARRAY posx_array, FLOAT_ARRAY posy_array, FLOAT_ARRAY posz_array,
		const int nbodies, BH_TYPE &diameter, BH_TYPE &centerx, BH_TYPE &centery, BH_TYPE &centerz) {

	BH_TYPE minx, miny, minz;
	BH_TYPE maxx, maxy, maxz;
	BH_TYPE posx, posy, posz;

	minx = 0.;
	miny = 0.;
	minz = 0.;
	maxx = 0.;
	maxy = 0.;
	maxz = 0.;

	for (int i = 0; i < nbodies; i++) {
		posx = posx_array[i];
		posy = posy_array[i];
		posz = posz_array[i];

		if (minx > posx)
			minx = posx;
		if (miny > posy)
			miny = posy;
		if (minz > posz)
			minz = posz;

		if (maxx < posx)
			maxx = posx;
		if (maxy < posy)
			maxy = posy;
		if (maxz < posz)
			maxz = posz;
	}

	diameter = maxx - minx;
	if (diameter < (maxy - miny))
		diameter = (maxy - miny);
	if (diameter < (maxz - minz))
		diameter = (maxz - minz);

	centerx = (maxx + minx) * 0.5;
	centery = (maxy + miny) * 0.5;
	centerz = (maxz + minz) * 0.5;
}

int build_tree_array_OPT(FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, INT_ARRAY childs,
		BH_TYPE radius, int nbodies, int nnodes, int& tree_height) {

	BH_TYPE x, y, z, r;
	BH_TYPE p_new_x, p_new_y, p_new_z;
	BH_TYPE parent_x, parent_y, parent_z;
	BH_TYPE new_x, new_y, new_z;
	BH_TYPE rh;

	int child, child1, parent;
	BH_TYPE rootx, rooty, rootz;
	int cell_count = TREE_ROOT - 1;
	int particle_pos = 0;
	int tmp_height;

	//system center
	//	rootx = cells[0].mass_pos.x;
	//	rooty = cells[0].mass_pos.y;
	//	rootz = cells[0].mass_pos.z;


	rootx = posx[TREE_ROOT];
	rooty = posy[TREE_ROOT];
	rootz = posz[TREE_ROOT];

	//For all particles in the system
	while (particle_pos < nbodies) {

		tmp_height = 0;
		child = 0;
		x = 0;
		y = 0;
		z = 0;

		//particle to insert position
		p_new_x = posx[particle_pos];
		p_new_y = posy[particle_pos];
		p_new_z = posz[particle_pos];

		//r reinitialized to system radius
		r = radius;

		//get voxel where the particle will be positioned
		if (rootx < p_new_x) {
			child = 1;
			x = r;
		}

		if (rooty < p_new_y) {
			child += 2;
			y = r;
		}
		if (rootz < p_new_z) {
			child += 4;
			z = r;
		}

		//if child is a cell find and follow until a particle/leaf or nothing is found
		parent = TREE_ROOT;
		while (IS_CELL(childs[parent*8 + child])) {
			//while (cells[parent].child_types[child] == CELL) {
			tmp_height++;
			r *= 0.5f; //go down in the octree
			//the child cell is in the cells array in this position
			parent = childs[parent * 8 + child];
			parent_x = posx[parent];
			parent_y = posy[parent];
			parent_z = posz[parent];
			child = 0;
			x = 0;
			y = 0;
			z = 0;
			//find the child voxel
			if (parent_x < p_new_x) {
				child = 1;
				x = r;
			}
			if (parent_y < p_new_y) {
				child += 2;
				y = r;
			}
			if (parent_z < p_new_z) {
				child += 4;
				z = r;
			}
		}

		if (childs[parent * 8 + child] == -1) { //if there is no particle in the octant, add the particle

			//cells[parent].child_types[child] = PARTICLE; //the child of the octant found is now a particle
			childs[parent * 8 + child] = particle_pos; //the particle is in this index of the particles array

		} else if (IS_PARTICLE(childs[parent*8 + child])) {//if a particle is already in the octant, devide the octant

			int particle = childs[parent * 8 + child]; //particle that was there
			BH_TYPE p_old_x, p_old_y, p_old_z;
			p_old_x = posx[particle];
			p_old_y = posy[particle];
			p_old_z = posz[particle];

			if (!(p_old_x == p_new_x && p_old_y == p_new_y && p_old_z == p_new_z)) {

				// the particles may be in the same octant again so multiple divisions are required, stop where they are in different octants
				do {

					//cells[parent].child_types[child] = CELL;
					childs[parent * 8 + child] = cell_count;

					//cell new_cell;
					//for (int k = 0; k < 8; k++)
					//	new_cell.child_types[k] = NILL;
					for (int k = 0; k < 8; k++)
						//new_cell.child_indexes[k] = -1;
						childs[cell_count * 8 + k] = -1;

					rh = 0.5 * r;
					parent_x = posx[parent];
					parent_y = posy[parent];
					parent_z = posz[parent];

					//the new child bb
					new_x = parent_x - rh + x;
					new_y = parent_y - rh + y;
					new_z = parent_z - rh + z;

					posx[cell_count] = new_x;
					posy[cell_count] = new_y;
					posz[cell_count] = new_z;

					//the r,x,y,z will be used in the next iteration
					r *= 0.5f;
					child = 0;
					x = 0;
					y = 0;
					z = 0;

					//find the new voxel child where the particle that was already there will be inserted
					if (new_x < p_old_x) {
						child = 1;
						x = r;
					}
					if (new_y < p_old_y) {
						child += 2;
						y = r;
					}
					if (new_z < p_old_z) {
						child += 4;
						z = r;
					}

					child1 = child; //child1 is the child for the new particle, child is for the old one
					child = 0;

					//find the new octet child where the particle that we are adding will be inserted
					if (new_x < p_new_x)
						child = 1;
					if (new_y < p_new_y)
						child += 2;
					if (new_z < p_new_z)
						child += 4;

					//childs[cell_count*8 + 8]= parent;

					if (tmp_height++ >= tree_height)
						tree_height++;

					parent = cell_count;
					//cells[cell_count++] = new_cell;
					cell_count--;

				} while (child == child1); //repeat if the child is the same


				//child is the child for the new particle, child1 is for the old one
				//cells[cell_count - 1].child_types[child] = PARTICLE;
				//cells[cell_count - 1].child_indexes[child] = particle_pos;
				childs[(cell_count + 1) * 8 + child] = particle_pos;

				//child is the child for the new particle, child1 is for the old one
				//cells[cell_count - 1].child_types[child1] = PARTICLE;
				//cells[cell_count - 1].child_indexes[child1] = particle;
				childs[(cell_count + 1) * 8 + child1] = particle;

			} else {

				printf("error in particle %d\n",particle_pos);

			}

		}
		particle_pos++;
	}
	//cout << "voxel count:" << cell_count << " body count: " << nbodies << std::endl;

	return nnodes - cell_count;
}

void compute_center_of_mass_OPT(FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz,
		INT_ARRAY childs, INT_ARRAY particle_count, FLOAT_ARRAY mass,  int tree_height, int nnodes,
		int nbodies) {

	BH_TYPE m, px = 0.0, py = 0.0, pz = 0.0;

	char stack[tree_height];
	int stack2[tree_height];
	int stack_top = 0;
	int stack2_top = 0;

	int current = TREE_ROOT;
	int child = 0;
	int p_t_count = 0;

	float mass_pos_x, mass_pos_y, mass_pos_z;

	while (!(stack_top == 0 && child == 8)) {
		if (child == 8) {

			m = 0;
			px = 0.0;
			py = 0.0;
			pz = 0.0;

			for (int i = 0; i < 8; i++) {
				//mass_position mass_pos;
				if (childs[current*8 + i] != -1) {
					mass_pos_x = posx[childs[current * 8 + i]];
					mass_pos_y = posy[childs[current * 8 + i]];
					mass_pos_z = posz[childs[current * 8 + i]];
				}
				//else if (cells[current].child_types[i] == CELL)
				///mass_pos = cells[cells[current].child_indexes[i]].mass_pos;
				else
					continue;

				m = mass[childs[current * 8 + i]];
				mass[current] += m;

				px += mass_pos_x * m;
				py += mass_pos_y * m;
				pz += mass_pos_z * m;

			}

			m = 1.0 / mass[current];
			posx[current] = px * m;
			posy[current] = py * m;
			posz[current] = pz * m;

			/*	printf("Center of mass:  ");
			 PrintBH_TYPE(cells[current].mass_pos.x);
			 printf(" ");
			 PrintBH_TYPE(cells[current].mass_pos.y);
			 printf(" ");
			 PrintBH_TYPE(cells[current].mass_pos.z);
			 printf("\n");*/


			particle_count[stack2[stack2_top-1]]+=particle_count[current];
			current = stack2[--stack2_top];
			child = stack[--stack_top];
			child++;
		} else {
			if (IS_CELL(childs[current*8 + child])) {
				stack2[stack2_top++] = current;
				current = childs[current * 8 + child];
				stack[stack_top++] = child;
				child = 0;
			} else if (IS_PARTICLE(childs[current*8 + child])) {
				child++;
				particle_count[current]++;
			} else {
				child++;
			}

		}
	}

	// Center of mass of the root
	m = 0;
	px = 0.0;
	py = 0.0;
	pz = 0.0;

	for (int i = 0; i < 8; i++) {

		if (childs[current*8 + i] != -1) {
			mass_pos_x = posx[childs[current * 8 + i]];
			mass_pos_y = posy[childs[current * 8 + i]];
			mass_pos_z = posz[childs[current * 8 + i]];
		}
		//else if (cells[current].child_types[i] == CELL)
		///mass_pos = cells[cells[current].child_indexes[i]].mass_pos;
		else
			continue;

		m = mass[childs[current * 8 + i]];
		mass[current]+= m;

		px += mass_pos_x * m;
		py += mass_pos_y * m;
		pz += mass_pos_z * m;

	}

	m = 1.0 /  mass[current];
	posx[current] = px * m;
	posy[current] = py * m;
	posz[current] = pz * m;

	/* printf("Center of mass:  ");
	 PrintBH_TYPE(cells[current].mass_pos.x);
	 printf(" ");
	 PrintBH_TYPE(cells[current].mass_pos.y);
	 printf(" ");
	 PrintBH_TYPE(cells[current].mass_pos.z);
	 printf("\n");

	 cout << "center of mass done" << endl;  */
}

void sort(INT_ARRAY child, INT_ARRAY sortd, INT_ARRAY countd, int nbodies, int nnodes, int ncells)
{

int i, ch, start, bottom;

  // iterate over all cells assigned to thread


int k = TREE_ROOT;
bottom = nnodes - (ncells-1);

INT_ARRAY startd = new int[nnodes];
memset(startd,0,sizeof(int)*nnodes);

 while (k >= bottom) {
    start = startd[k];
    if (start >= 0) {
      for (i = 0; i < 8; i++) {
        ch = child[k*8+i];
        if (ch >= nbodies) {
          // child is a cell
          startd[ch] = start;  // set start ID of child
          start += countd[ch];  // add #bodies in subtree
        } else if (ch >= 0) {
          // child is a body
          sortd[start] = ch;  // record body in 'sorted' array
          start++;
        }
      }
      k--;  // move on to next cell
    }
  }

delete startd;

}



//
//void sequential_compute_force(cell* cells, particle* particles, int particle, int tree_height,
//		BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {
//
//	BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
//	BH_TYPE ax, ay, az;
//
//	ax = particles[particle].accx;
//	ay = particles[particle].accy;
//	az = particles[particle].accz;
//
//	particles[particle].accx = 0.0;
//	particles[particle].accy = 0.0;
//	particles[particle].accz = 0.0;
//
//	char stack[tree_height];
//	int stack_top = 0;
//
//	int current = 0;
//	int child = 0;
//
//	dsq *= 0.25;
//
//	mass_position mass_pos_p = particles[particle].mass_pos;
//
//	while (!(stack_top == 0 && child == 8)) {
//
//		if (child == 8) {
//			//current = cells[current].parent_cell_index;
//			dsq *= 4;
//			child = stack[--stack_top];
//			child++;
//
//		} else {
//
//			mass_position mass_pos;
//			if (cells[current].child_types[child] == PARTICLE)
//				mass_pos = particles[cells[current].child_indexes[child]].mass_pos;
//			else if (cells[current].child_types[child] == CELL)
//				mass_pos = cells[cells[current].child_indexes[child]].mass_pos;
//			else {
//				child++;
//				continue;
//			}
//
//			drx = mass_pos.x - mass_pos_p.x;
//			dry = mass_pos.y - mass_pos_p.y;
//			drz = mass_pos.z - mass_pos_p.z;
//
//			drsq = drx * drx + dry * dry + drz * drz;
//			//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
//			if (drsq < dsq) {
//				if (cells[current].child_types[child] == CELL) {
//					dsq *= 0.25;
//					current = cells[current].child_indexes[child];
//					stack[stack_top++] = child;
//					child = 0;
//					//printf("going down\n");
//
//				} else if (cells[current].child_types[child] == PARTICLE) {
//					if (mass_pos.id != mass_pos_p.id) {
//						drsq += epssq;
//						idr = 1 / sqrt(drsq);
//						nphi = mass_pos.mass * idr;
//						scale = nphi * idr * idr;
//						particles[particle].accx += drx * scale;
//						particles[particle].accy += dry * scale;
//						particles[particle].accz += drz * scale;
//
//						//if (particle == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);
//
//
//					}
//					child++;
//				} else if (cells[current].child_types[child] == NILL) {
//					child++;
//				}
//			} else {
//				//printf("Aproximating\n");
//				drsq += epssq;
//				idr = 1 / sqrt(drsq);
//				nphi = mass_pos.mass * idr;
//				scale = nphi * idr * idr;
//				particles[particle].accx += drx * scale;
//				particles[particle].accy += dry * scale;
//				particles[particle].accz += drz * scale;
//
//				//if (particle == 0 && step == 0) printf("%.15f %.15f %.15f %.15f\n", powf(drx,2)+powf(dry,2)+powf(drz,2), drx,dry,drz);
//
//
//				child++;
//			}
//
//		}
//	}
//
//	BH_TYPE dthf = 0.5 * dtime;
//
//	if (step > 0) {
//		particles[particle].velx += (particles[particle].accx - ax) * dthf;
//		particles[particle].vely += (particles[particle].accy - ay) * dthf;
//		particles[particle].velz += (particles[particle].accz - az) * dthf;
//	}
//
//	if (particle == 0) {
//		//printf("ACCt_x: %.15f\n",ax);
//		//printf("ACCx: %.15f\n",particles[particle].accx);
//	}
//
//	/*	PrintBH_TYPE(particles[particle].velx);
//	 printf("   ");
//	 PrintBH_TYPE(particles[particle].vely);
//	 printf("   ");
//	 PrintBH_TYPE(particles[particle].velz);
//	 printf("\n");*/
//
//}
//
//void sequential_update_positions(particle* particles, int part, BH_TYPE dtime) {
//
//	BH_TYPE dthf = 0.5 * dtime;
//
//	BH_TYPE dvelx, dvely, dvelz;
//	BH_TYPE velhx, velhy, velhz;
//
//	particle* p = &(particles[part]);
//
//	dvelx = p->accx * dthf;
//	dvely = p->accy * dthf;
//	dvelz = p->accz * dthf;
//
//	velhx = p->velx + dvelx;
//	velhy = p->vely + dvely;
//	velhz = p->velz + dvelz;
//
//	p->mass_pos.x += velhx * dtime;
//	p->mass_pos.y += velhy * dtime;
//	p->mass_pos.z += velhz * dtime;
//
//	//printf("%.10lf\n",velhx * dtime);
//
//	p->velx = velhx + dvelx;
//	p->vely = velhy + dvely;
//	p->velz = velhz + dvelz;
//
//}
//
//void PrintParticles(particle* particles, int nbodies) {
//
//	for (int i = 0; i < nbodies; i++) { // print result
//		printf("p %d: ", i);
//		PrintBH_TYPE(particles[i].mass_pos.x);
//		printf("   ");
//		PrintBH_TYPE(particles[i].mass_pos.y);
//		printf("   ");
//		PrintBH_TYPE(particles[i].mass_pos.z);
//		printf("\n");
//	}
//}
//
//void PrintBH_TYPE(BH_TYPE d) {
//	register int i;
//	char str[16];
//
//	sprintf(str, "%.4lE", d);
//
//	i = 0;
//	while ((i < 16) && (str[i] != 0)) {
//		if ((str[i] == 'E') && (str[i + 1] == '-') && (str[i + 2] == '0') && (str[i + 3] == '0')) {
//			printf("E00");
//			i += 3;
//		} else if (str[i] != '+') {
//			printf("%c", str[i]);
//		}
//		i++;
//	}
//}

//void jobBARNES_HUT_check(int argc, char *argv[], particle* particles_to_check) {
//
//	printf("Checking results...\n");
//
//	BH_TYPE dtime; // length of one time step
//	BH_TYPE eps; // potential softening parameter
//	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error
//
//	int nbodies;
//	int cell_count;
//	int timesteps;
//	int tree_height = 0;
//
//	char* filename;
//
//	PERFORM_timer* timer = new PERFORM_timer();
//	particle* particles;
//
//	initBARNES_HUT(argc, argv, particles, nbodies, timesteps, filename);
//
//	load_data_set2(particles, filename, nbodies, timesteps, dtime, eps, tol);
//
//	BH_TYPE itolsq = 1.0 / (tol * tol);
//	BH_TYPE epssq = eps * eps;
//
//	for (int step = 0; step < timesteps; step++) {
//
//		BH_TYPE diameter, centerx, centery, centerz;
//
//		/**
//		 * Compute the center and diameter of the all system
//		 */
//		compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);
//
//		/**
//		 * Create the the octree root
//		 */
//
//		/**
//		 * based in the burtsher code ???
//		 */
//		int nnodes = nbodies * 2;
//			if (nnodes < 1024 * 16)
//				nnodes = 1024 * 20;
//
//		//tree_cells_array nodes = tree_cells_array();
//
//		cell* nodes = new cell[nnodes];
//
//		cell root;
//		root.mass_pos.x = centerx;
//		root.mass_pos.y = centery;
//		root.mass_pos.z = centerz;
//
//		for (int k = 0; k < 8; k++)	root.child_types[k] = NILL;
//
//		for (int k = 0; k < 8; k++)	root.child_indexes[k] = -1;
//
//		nodes[0] = root;
//		//nodes.push_back(root);
//
//		const BH_TYPE radius = diameter * 0.5;
//
//		build_tree_array(nodes, radius, particles, nbodies, tree_height);
//
//		compute_center_of_mass(nodes, particles, 0, tree_height);
//
//		BH_TYPE dsq = diameter * diameter * itolsq;
//
//		//PERFORM_acc_timer times[nbodies];
//
//		for (int i = 0; i < nbodies; i++){
//			//times[i].start();
//			sequential_compute_force(nodes, particles, i, tree_height, dtime, epssq, dsq, step);
//			//times[i].stop();
//			//printf("%s\n", times[i].print());
//		}
//
////		PERFORM_acc_timer times;
////		int i = 92864;
////	//	for (int i = 0; i < nbodies; i++){
////			times.start();
////			compute_force_(nodes, particles, i, tree_height, dtime, epssq, dsq, step);
////			times.stop();
////			printf("SINGLE PARTICLE: %s\n", times.print());
////	//	}
//
//
//		for (int i = 0; i < nbodies; i++) sequential_update_positions(particles, i, dtime);
//
//	}
//
//	//PrintParticles((particle*)particles, nbodies-1023);
//
//
//	mass_position* errors = new mass_position[nbodies];
//	for (int i = 0; i < nbodies; i++) {
//		errors[i].x = particles[i].mass_pos.x - particles_to_check[i].mass_pos.x;
//		errors[i].y = particles[i].mass_pos.y - particles_to_check[i].mass_pos.y;
//		errors[i].z = particles[i].mass_pos.z - particles_to_check[i].mass_pos.z;
//	}
//
//	double norm_x_err = 0.;
//	double norm_y_err = 0.;
//	double norm_z_err = 0.;
//
//	double norm_x = 0.;
//	double norm_y = 0.;
//	double norm_z = 0.;
//
//	for (int i = 0; i < nbodies; i++) {
//		norm_x_err += errors[i].x * errors[i].x;
//		norm_y_err += errors[i].y * errors[i].y;
//		norm_z_err += errors[i].z * errors[i].z;
//
//		norm_x += particles[i].mass_pos.x * particles[i].mass_pos.x;
//		norm_y += particles[i].mass_pos.y * particles[i].mass_pos.y;
//		norm_z += particles[i].mass_pos.z * particles[i].mass_pos.z;
//	}
//
//	norm_x_err = sqrtf(norm_x_err);
//	norm_y_err = sqrtf(norm_y_err);
//	norm_z_err = sqrtf(norm_z_err);
//
//	norm_x = sqrtf(norm_x);
//	norm_y = sqrtf(norm_y);
//	norm_z = sqrtf(norm_z);
//
//	double error_x = norm_x_err / norm_x;
//	double error_y = norm_y_err / norm_y;
//	double error_z = norm_z_err / norm_z;
//
//	printf("\nError X: %.1lE  Y: %.1lE Z: %.1lE\n", error_x, error_y, error_z);
//	printf("\nError X: %.15lf  Y: %.15lf Z: %.15lf\n", error_x, error_y, error_z);
//
//}

