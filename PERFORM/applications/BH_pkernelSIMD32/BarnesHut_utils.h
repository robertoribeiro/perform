#ifndef BARNESHUTUTILS_H_
#define BARNESHUTUTILS_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "core.h"

using namespace std;

#define BH_FLOAT

#ifdef BH_FLOAT
typedef float BH_TYPE;
#else
typedef double BH_TYPE;
#endif

//typedef int CELL;

#define TREE_ROOT (2*nbodies-1)

#define IS_CELL(X) (X >= nbodies)
#define IS_PARTICLE(X) (X < nbodies && X >= 0)

void DEPRECATED_load_data_set_OPT(FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, FLOAT_ARRAY mass,
		char *filename, int nbodies, int& timesteps, BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol);

void PointsToParticleSystem(Point3D* points, int nbodies, FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, FLOAT_ARRAY mass,
		 int& timesteps, BH_TYPE& dtime, BH_TYPE& eps, BH_TYPE& tol);

void compute_bounding_box_OPT(FLOAT_ARRAY posx_array, FLOAT_ARRAY posy_array,
		FLOAT_ARRAY posz_array, const int n, BH_TYPE &diameter, BH_TYPE &centerx, BH_TYPE &centery,
		BH_TYPE &centerz);

int build_tree_array_OPT(FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz, INT_ARRAY childs,
		BH_TYPE radius, int nbodies, int nnodes, int& tree_height);

void compute_center_of_mass_OPT(FLOAT_ARRAY posx, FLOAT_ARRAY posy, FLOAT_ARRAY posz,
		INT_ARRAY childs, INT_ARRAY particle_count, FLOAT_ARRAY mass, int tree_height, int nnodes,
		int nbodies);

void sort(INT_ARRAY child, INT_ARRAY sortd, INT_ARRAY countd, int nbodies, int nnodes, int ncells);

#endif
