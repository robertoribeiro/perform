#ifndef UBK_TASK_H_
#define UBK_TASK_H_

/**
 * Written by the user
 */

#include "task/Task.h"
//#include "BarnesHut.h"

#define MAX_SEC_TASK 8
#define MAX_P_PER_TASK 1

class ubkTask_ForcesCalc /*: public ubkTask*/{
public:

	short target_count;
	int p_target[MAX_P_PER_TASK];
	//float dtime;
	//float epssq;
	float dsq;
	bool tree_at_root;
	//short step;
	int current_cell[MAX_P_PER_TASK];

	__H_D__
	ubkTask_ForcesCalc() {
	}

};

class ubkSIMDTask_ForcesCalc /*: public ubkTask*/{
public:

	int p_target_start;
	float dsq;
	int current_cell;
	//bool done[32];
	uint done_bitwise;

	__H_D__
	ubkSIMDTask_ForcesCalc() {
		//		memset(done,0,32);
		done_bitwise = 0;

	}

};

class Task_ForcesCalc: public Task {
public:

	int tree_height;
	int start;

	float dtime;
	float epssq;
	float dsq;

	int nbodies;

	bool tree_at_root;
	int step;

	int current_cell;
	int FIB;

	__H_D__
	Task_ForcesCalc() :
		Task() {
	}
	__H_D__
	Task_ForcesCalc(TASK_TYPE t_) :
		Task(t_) {
	}

	int dice(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<float>* domain_input;
		getDomain(0, domain_input);

		Domain<float>* domain_result;
		getDomain(1, domain_result);

		float len1 = (floor((job_elements / 32) * m.value)) * 32;

		if (domain_result->Y.length() <= len1 || len1 == 0) {
			Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[1];
			new_tasks[0] = this;
			new_tasks_ = (Task**) new_tasks;
			return 1;
		}

		Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[2];
		// task 0
		Task_ForcesCalc* t = new Task_ForcesCalc(type);

		dim_space a = dim_space(domain_result->Y.start, domain_result->Y.start + len1);

		Domain<float> *sub_domain_result = new Domain<float> (domain_result, 2,
				domain_result->permissions, dim_space(0, 12), a);

		t->associate_domain(domain_input);
		t->associate_domain(sub_domain_result);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		//t->start = a.start;
		//t->current_cell = 0;
		t->device_preference = device_preference;
		t->parent_host_task = (void*) this;
		t->wl.value = len1 / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->nbodies = nbodies;
		t->associate_kernel(this);
		t->job_elements = job_elements;
		t->FIB = FIB;
		new_tasks[task_count++] = t;

		// task 1
		t = new Task_ForcesCalc(type);

		a = dim_space(domain_result->Y.start + len1, domain_result->Y.end);

		sub_domain_result = new Domain<float> (domain_result, 2,
				domain_result->permissions, dim_space(0, 12), a);

		t->associate_domain(domain_input);
		t->associate_domain(sub_domain_result);

		t->tree_height = tree_height;
		t->dtime = dtime;
		t->epssq = epssq;
		t->dsq = dsq;
		t->step = step;
		//t->start = a.start;
		//t->current_cell = 0;
		t->device_preference = device_preference;
		t->parent_host_task = (void*) this;
		t->wl.value = (domain_result->Y.length() - len1) / job_elements;
		t->dice_lvl = dice_lvl + 1;
		t->nbodies = nbodies;
		t->associate_kernel(this);
		t->job_elements = job_elements;
		t->FIB = FIB;

		new_tasks[task_count++] = t;

		//		float N = domain_result->Y.length();
		//
		//		float fst_length = floor(N * (m.value));
		//
		//		int pow1 = floor(log2(fst_length));
		//		int powN = log2(N);
		//
		//		int divide_task_by = pow(2, powN - pow1);
		//
		//		if (divide_task_by == -1) {
		//			//divide_task_by = p_particles_new->X.length();
		//
		//			//if (divide_task_by % 32 != 0) {
		//			//	printf("Error: devide parameter for uberkernel needs to be a divisor of 32\n");
		//			//	exit(-1);
		//			//};
		//		}
		//
		//		int block_step = (N / divide_task_by);

//		Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[divide_task_by];
//
//		for (int i = 0; i < divide_task_by; i++) {
//
//			Task_ForcesCalc* t = new Task_ForcesCalc(IRREGULAR);
//
//			dim_space a = dim_space(domain_result->Y.start + i * block_step,
//					(domain_result->Y.start + i * block_step) + block_step);
//
//			Domain<float> *sub_domain_result = new Domain<float> (domain_result, 2,
//					domain_result->permissions, dim_space(0, 12), a);
//
//			t->associate_domain(domain_input);
//			t->associate_domain(sub_domain_result);
//
//			t->tree_height = tree_height;
//			t->dtime = dtime;
//			t->epssq = epssq;
//			t->dsq = dsq;
//			t->step = step;
//			//t->start = a.start;
//			//t->current_cell = 0;
//			t->device_preference = device_preference;
//			t->parent_host_task = (void*) this;
//			t->wl = PM_Metric_normalized((block_step * wl.value) / N);
//			t->dice_lvl = dice_lvl + 1;
//			t->nbodies = nbodies;
//			t->associate_kernel(this);
//
//			new_tasks[task_count++] = t;
//
//		}

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

	int dice2(Task**& new_tasks_, PM_Metric_normalized m) {

		int task_count = 0;

		Domain<float>* domain_input;
		getDomain(0, domain_input);

		Domain<float>* domain_result;
		getDomain(1, domain_result);

		float N = domain_result->Y.length();

		float fst_length = floor(N * (m.value));

		int pow1 = floor(log2(fst_length));
		int powN = log2(N);

		int divide_task_by = pow(2, powN - pow1);

		if (divide_task_by == -1) {
			//divide_task_by = p_particles_new->X.length();

			//if (divide_task_by % 32 != 0) {
			//	printf("Error: devide parameter for uberkernel needs to be a divisor of 32\n");
			//	exit(-1);
			//};
		}

		int block_step = (N / divide_task_by);

		Task_ForcesCalc** new_tasks = new Task_ForcesCalc*[divide_task_by];

		for (int i = 0; i < divide_task_by; i++) {

			Task_ForcesCalc* t = new Task_ForcesCalc(IRREGULAR);

			dim_space a = dim_space(domain_result->Y.start + i * block_step,
					(domain_result->Y.start + i * block_step) + block_step);

			Domain<float> *sub_domain_result = new Domain<float> (domain_result, 2,
					domain_result->permissions, dim_space(0, 12), a);

			t->associate_domain(domain_input);
			t->associate_domain(sub_domain_result);

			t->tree_height = tree_height;
			t->dtime = dtime;
			t->epssq = epssq;
			t->dsq = dsq;
			t->step = step;
			//t->start = a.start;
			//t->current_cell = 0;
			t->device_preference = device_preference;
			t->parent_host_task = (void*) this;
			t->wl = PM_Metric_normalized((block_step * wl.value) / N);
			t->dice_lvl = dice_lvl + 1;
			t->nbodies = nbodies;
			t->associate_kernel(this);

			new_tasks[task_count++] = t;

		}

		new_tasks_ = (Task**) new_tasks;
		return task_count;
	}

	//	int diceTo(ubkTask**& new_tasks_, int n_bwu) {
	//
	//		int task_count = 0;
	//
	//		Domain<float>* domain_input;
	//		getDomain(0, domain_input);
	//
	//		Domain<float>* domain_result;
	//		getDomain(1, domain_result);
	//
	//		float N = nbodies; //p_particles_new->X.start
	//
	//		int divide_task_by = N / n_bwu;
	//
	//		int block_step = n_bwu;
	//
	//		ubkTask_ForcesCalc** new_tasks = new ubkTask_ForcesCalc*[divide_task_by];
	//
	//		for (int i = 0; i < divide_task_by; i++) {
	//
	//			ubkTask_ForcesCalc* t = new ubkTask_ForcesCalc();
	//
	//			//t->dtime = dtime;
	//			//t->epssq = epssq;
	//			t->dsq = dsq*0.25;
	//			//t->step = step;
	//			t->p_target[0] = /*p_particles_new->X.start + */ i * block_step;
	//			t->current_cell[0] = (nbodies*2)-1;
	//			t->tree_at_root = true;
	//			t->target_count=1;
	//			new_tasks[task_count++] = t;
	//
	//		}
	//
	//		new_tasks_ = (ubkTask**) new_tasks;
	//		return task_count;
	//	}

	int diceTo(ubkTask*& new_tasks_, int n_bwu) {

		int task_count = 0;

		Domain<float>* domain_input;
		getDomain(0, domain_input);

		Domain<float>* domain_result;
		getDomain(1, domain_result);

		float N = domain_result->Y.length();

		int divide_task_by = N / n_bwu;

		int block_step = n_bwu;

		ubkSIMDTask_ForcesCalc* new_tasks = new ubkSIMDTask_ForcesCalc[divide_task_by];

		int j = 0;
		for (int i = 0; i < divide_task_by; i += 1) {

			ubkSIMDTask_ForcesCalc* t = new_tasks + j++;

			//t->dtime = dtime;
			//t->epssq = epssq;
			t->dsq = dsq * 0.25;
			//t->step = step;
			t->p_target_start = domain_result->Y.start + i * block_step;
			t->current_cell = (nbodies * 2) - 1;
			//new_tasks[task_count++] = t;
			task_count++;

		}

		new_tasks_ = (ubkTask*) new_tasks;
		return task_count;
	}

};

#endif /* UBK_TASK_H_ */
