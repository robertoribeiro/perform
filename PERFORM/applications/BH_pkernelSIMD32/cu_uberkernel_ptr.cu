#include <iostream>

#include "BarnesHut_job.h"

#include "perform_utils.h"
#include "core.h"
#include "perform.h"

#include "kernels.h"
#include "uberkernel.h"
#include "device_apis/CUDA/global_sync.h"

using namespace std;

////// CONFIG

#define GARBAGE_FIB

#define WARPSIZE 32

#define WARPS 8

#define N_BLOCKS (WARPS*N_SM)

#define TASKS_PER_DEQ 32 /*Se muito alto e muitas tarefas produzidas, falha no espaço disponivel da out o que causa force
							 e pode tb falhar nas slots de tasks*/

#define UBK_INBOX_SIZE 10000000

#define LOCAL_INBOX_QUEUE_SIZE (64)           // se local inbox muito grande, rouba muito trabalho pa apenas uma SM. calcular em funçao do child_task_count?
#define LOCAL_OUTBOX_QUEUE_SIZE (1024*100)         //se local outboc muito pequeno força muito o lock
#define MEM_MEM_CHUNKS_PER_PE (1024*200) // PE is a warp or thread 350
#define QUEUE_FULL_HANDICAP 32

#define THR_PER_BLOCK (WARPSIZE)
#define MEM_CHUNKS_PER_SM (MEM_MEM_CHUNKS_PER_PE)
#define MEM_CHUNK_SIZE sizeof(ubkSpecificTask)
#define MEM_TOTAL (MEM_CHUNKS_PER_SM * MEM_CHUNK_SIZE)
#define LOCAL_FREES_QUEUE_SIZE (MEM_CHUNKS_PER_SM + 1024*30) //Extra open slots on each SM frees q
#define TIDX (threadIdx.x)
#define THREAD0 (threadIdx.x == 0)
#define WARP0 (threadIdx.x < 32)
#define LANE0 ((threadIdx.x & 31)==0)    // or warp lane index
#define THREADG0 (tID == 0)

#define CHECK_BIT_1(var,pos) ((var) & (1 << (pos)))

#include "device_apis/CUDA/workqueue.h"

//__device__ Task_ForcesCalc* global_task;
//__device__ volatile unsigned int * task_exe_count_T;

//__device__ unsigned int * task_exe_count;

__device__ unsigned int frees_left;

__device__ CudaConcurrentQueue<ubkTask_ptr>* inbox;

__device__ ubkTask_ptr* local_inbox_buffer;
__device__ ubkTask_ptr* local_outbox_buffer;
__device__ ubkTask_ptr* local_frees_buffer;

__device__ CudaLocalQueue<ubkTask_ptr>* heads_outbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_inbox;
__device__ CudaLocalQueue<ubkTask_ptr>* heads_frees;

__device__ volatile int out_votes[N_BLOCKS];

__device__ FLOAT_ARRAY posx;
__device__ FLOAT_ARRAY posy;
__device__ FLOAT_ARRAY posz;
__device__ FLOAT_ARRAY mass;
__device__ INT_ARRAY childs;
__device__ FLOAT_ARRAY accx;
__device__ FLOAT_ARRAY accy;
__device__ FLOAT_ARRAY accz;
__device__ FLOAT_ARRAY t_accx;
__device__ FLOAT_ARRAY t_accy;
__device__ FLOAT_ARRAY t_accz;
__device__ INT_ARRAY sort_order;

__device__ double fibPK(double n) {
	double a = 1, b = 1;
	for (int i = 3; i <= n; i++) {
		double c = a + b;
		a = b;
		b = c;
	}
	return a;
}

__device__ uint _somethingPK(uint x) {

	volatile uint acc = 0;

	volatile uint garbage = 0;
	while (garbage < 1000) {
		acc += x + 1;
		garbage += 1;
		//__threadfence_block();
	}

	return acc;
}

__device__
__forceinline void k_CUDA_devptr_compute_force_Fine_T_SIMD32(
		const Task_ForcesCalc global_task_, const ubkSpecificTask task,
		CudaLocalQueue<ubkTask_ptr>* local_frees, ubkTask_ptr bag[],
		uint& elements_bag, int* GARBAGE, long long unsigned int* visit_count,
		Domain<float> domain_result) {

	int CURRENT_CHILD = -1;
	ubkTask_ptr current_new_task = INIT_TASK;
	int particle;
	uint buffer_pos;
	float epssq;
	float dsq;
	int current;
	double drx, dry, drz, nphi, scale, idr;
	double drsq;
	float ax, ay, az;
	short child;
	int nbodies = global_task_.nbodies;
	float pos_p_x;
	float pos_p_y;
	float pos_p_z;

	__shared__ volatile float sh_pos_x; //[WARPS];
	__shared__ volatile float sh_pos_y; //[WARPS];
	__shared__ volatile float sh_pos_z; //[WARPS];
	__shared__ volatile float sh_mass; //[WARPS];

	__shared__ volatile int down[MAX_SEC_TASK];

	__shared__
	int dones_bitwise[8];

	dones_bitwise[TIDX & 7] = 0;

	//task = *(work_bag[threadIdx.x]);

	//#ifdef __DEBUG
	if (THREAD0) {
		//atomicAdd((uint*) task_exe_count_T, 1);
		//atomicAdd((uint*) &(task_exe_count[blockIdx.x]), 1);
	}
	//#endif

	epssq = global_task_.epssq;
	dsq = task.dsq;

	particle = sort_order[task.p_target_start + TIDX];
	buffer_pos = task.p_target_start + TIDX;
	//p = task.p_target_start + TIDX;

	current = task.current_cell;

	if (current == TREE_ROOT) {

		domain_result.at(9, buffer_pos) = accx[particle];
		domain_result.at(10, buffer_pos) = accy[particle];
		domain_result.at(11, buffer_pos) = accz[particle];

		domain_result.at(6, buffer_pos) = 0.0;
		domain_result.at(7, buffer_pos) = 0.0;
		domain_result.at(8, buffer_pos) = 0.0;

	}

	ax = 0.0;
	ay = 0.0;
	az = 0.0;

	child = 0;

	//cache particle position
	pos_p_x = posx[particle];
	pos_p_y = posy[particle];
	pos_p_z = posz[particle];

	//int next = 0;
	down[TIDX & 7] = -1;

	while (child < 8) {

		CURRENT_CHILD = childs[current * 8 + child];

		if (CURRENT_CHILD >= 0) {

			//single thread (single access) cache cell position and mass
			if (THREAD0) {
				sh_pos_x = posx[CURRENT_CHILD];
				sh_pos_y = posy[CURRENT_CHILD];
				sh_pos_z = posz[CURRENT_CHILD];
				sh_mass = mass[CURRENT_CHILD];
			}

			drx = sh_pos_x - pos_p_x;
			dry = sh_pos_y - pos_p_y;
			drz = sh_pos_z - pos_p_z;

			// CRITICAL ACCURACY SECTION //

			drsq = drx * drx + dry * dry + drz * drz;

			//atomicAdd(( long long unsigned int*) visit_count, 1);

			if (!CHECK_BIT_1(task.done_bitwise,TIDX)) {
				///////// GARBAGE /////////
#ifdef GARBAGE_FIB
				double r = 7778742049.0;
				double a = fibPK(global_task_.FIB);
				//GARBAGE[threadIdx.x & 31] = a;
				atomicAnd((int*) &(GARBAGE[threadIdx.x & 31]), (r) == a);
#endif
				///////// END GARBAGE /////////
			}

			if (IS_PARTICLE(CURRENT_CHILD) || __all(drsq >= dsq)) { // verificar para toda as threads (__all) se a celula está far away
				if (CURRENT_CHILD
						!= particle&& !CHECK_BIT_1(task.done_bitwise,TIDX)) {

				drsq
					+= epssq;
					idr = rsqrtf(drsq);
					nphi = sh_mass * idr;
					scale = nphi * idr * idr;

					ax += drx * scale;
					ay += dry * scale;
					az += drz * scale;

				}

				child++;
			} else {
				down[child] = CURRENT_CHILD;

				//				if (task.done[TIDX])
				//					dones[child][TIDX] = true;

				dones_bitwise[child] = task.done_bitwise;

				if (!CHECK_BIT_1(task.done_bitwise,TIDX) && drsq >= dsq) {
					//if (!task.done[TIDX] && drsq >= dsq) {

					drsq += epssq;
					idr = rsqrtf(drsq);
					nphi = sh_mass * idr;
					scale = nphi * idr * idr;

					//if (particle==500) printf("%.10lf\n",scale);

					ax += drx * scale;
					ay += dry * scale;
					az += drz * scale;

					//dones_bitwise[child] |=  (1 << TIDX);
					atomicOr(&dones_bitwise[child], (1 << TIDX));

				}

				child++;

			}
		} else
			child++;
	}

	if (TIDX < 8 && down[TIDX] != -1) {
		//#ifdef __DEBUG
		if (!local_frees->dequeue(current_new_task))
			printf("Error dequeuing frees\n");
		//#else
		//	local_frees->dequeue(current_new_task);
		//#endif

		if (current_new_task != INIT_TASK) {
			//	memset(current_new_task->done, 0, 32);
			current_new_task->dsq = dsq * 0.25;

			current_new_task->p_target_start = task.p_target_start;
			current_new_task->current_cell = down[TIDX];

			//for (int i =0; i < 32; i++)
			current_new_task->done_bitwise = dones_bitwise[TIDX];

			bag[atomicAdd(&elements_bag, 1)] = current_new_task;

		} else {
			printf("Erro 8: tasks lost\n");
			__trap();
			atomicAdd((int*) &frees_left, 1);
		}

	}

	if (!CHECK_BIT_1(task.done_bitwise,TIDX)) {
		atomicAdd(&(domain_result.at(6, buffer_pos)), ax);
		atomicAdd(&(domain_result.at(7, buffer_pos)), ay);
		atomicAdd(&(domain_result.at(8, buffer_pos)), az);

	}

}

__device__
__forceinline void flush_to_global(CudaLocalQueue<ubkTask_ptr> *& q) {

	ubkTask_ptr t = INIT_TASK;
	while (!q->isEmpty() && !inbox->isFull()) {

#ifdef __DEBUG
		if (q->dequeue(t) && !inbox->enqueue(t)) {
			printf("Error 4\n");
			//__trap();

		}
#else
		if (q->dequeue(t))
			inbox->enqueue(t);
#endif
	}

}

__device__
__forceinline void try_donate(CudaLocalQueue<ubkTask_ptr> *& local_outbox) {

	if (!inbox->isFull() && !local_outbox->isEmpty()&& inbox->tryLock()) {

			flush_to_global(local_outbox);
			inbox->Release();

	}

}

__device__
__forceinline void vote_out(CudaLocalQueue<ubkTask_ptr>* local_inbox,
		CudaLocalQueue<ubkTask_ptr>* local_outbox) {

	if (inbox->isEmpty() && local_inbox->isEmpty() && local_outbox->isEmpty()) {
		out_votes[blockIdx.x] = 1;
	} else
		out_votes[blockIdx.x] = 0;

}

__device__
__forceinline bool check_all_isEmpty() {

	register int yy = 0;
	register int t1 = N_BLOCKS / WARPSIZE;
	register int t2 = (N_BLOCKS & 31);

//	for (yy = 0; yy < t1 * WARPSIZE; yy += WARPSIZE) {
//		if (__any(!out_votes[TIDX + yy]))
//			return false;
//	}
//
//	for (yy = 0; yy < t2; yy++) {
//		if (out_votes[yy] != 1)
//			return false;
//	}


	for (int i = 0; i < N_BLOCKS; i++) {
		if (out_votes[i] != 1)
			return false;
	}


	return true;
}

__global__ void uberkernel(Task_ForcesCalc global_task_,
		Domain<float> domain_input, Domain<float> domain_result,
		ubkTask_ptr* inbox_buffer, void* mem_buffer, void* qs_buffer,
		int child_task_count, int* GARBAGE, uint* task_exec,
		long long unsigned int* visit_count, int* pipe) {

	const int tID = blockIdx.x * blockDim.x + threadIdx.x;

	if (tID == 0) {
		//	global_task = &global_task_;

		//////// User-side ////////

		int nbodies = global_task_.nbodies;
		int nnodes = nbodies * 2;

		FLOAT_ARRAY input_chunk = (FLOAT_ARRAY) domain_input.device_data_buffer;
		FLOAT_ARRAY result_chunk =
				(FLOAT_ARRAY) domain_result.device_data_buffer;

		posx = input_chunk;
		posy = posx + (nnodes);
		posz = posy + (nnodes);

		mass = posz + (nnodes);

		childs = (INT_ARRAY) input_chunk;

		accx = (nnodes) * 4 + (nbodies * (8)) + nbodies * 3 + input_chunk;
		accy = accx + nbodies;
		accz = accy + nbodies;

		t_accx = accz + nbodies;
		t_accy = t_accx + nbodies;
		t_accz = t_accy + nbodies;

		sort_order = (INT_ARRAY) (t_accz + nbodies);

		///////////////////////////
	}

	frees_left = 0;

	GARBAGE[TIDX] = 3; // making sure that the value changes

	if (tID == 0) {

		//task_exe_count_T = task_exec;

		//*task_exe_count_T = 0;

		local_inbox_buffer = (ubkTask_ptr*) qs_buffer;
		local_outbox_buffer = local_inbox_buffer
				+ LOCAL_INBOX_QUEUE_SIZE * N_BLOCKS;
		local_frees_buffer = local_outbox_buffer
				+ LOCAL_OUTBOX_QUEUE_SIZE * N_BLOCKS;

		heads_inbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
		heads_outbox = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];
		heads_frees = new CudaLocalQueue<ubkTask_ptr> [N_BLOCKS];

		inbox = new CudaConcurrentQueue<ubkTask_ptr>(inbox_buffer,
				child_task_count, UBK_INBOX_SIZE);

	}

	__gpu_sync(N_BLOCKS);

	CudaLocalQueue<ubkTask_ptr>* local_inbox = heads_inbox + blockIdx.x;
	CudaLocalQueue<ubkTask_ptr>* local_outbox = heads_outbox + blockIdx.x;
	CudaLocalQueue<ubkTask_ptr>* local_frees = heads_frees + blockIdx.x;

	if (THREAD0) {

		local_inbox->setBuffer(
				(ubkTask_ptr*) (local_inbox_buffer
						+ blockIdx.x * LOCAL_INBOX_QUEUE_SIZE), 0, LOCAL_INBOX_QUEUE_SIZE)
				;
		local_outbox->setBuffer(
				(ubkTask_ptr*) (local_outbox_buffer
						+ blockIdx.x * LOCAL_OUTBOX_QUEUE_SIZE), 0, LOCAL_OUTBOX_QUEUE_SIZE)
				;
		local_frees->setBuffer(
				(ubkTask_ptr*) (local_frees_buffer
						+ blockIdx.x * LOCAL_FREES_QUEUE_SIZE), 0, LOCAL_FREES_QUEUE_SIZE)
				;

		local_inbox->clear();
		local_outbox->clear();
		local_frees->clear();

		memset((int*) out_votes, 0, sizeof(int) * N_BLOCKS);
	}

	ubkSpecificTask* warp_chunk = (ubkSpecificTask*) ((char*) mem_buffer
			+ blockIdx.x * MEM_TOTAL);

	for (int yy = 0; yy < MEM_MEM_CHUNKS_PER_PE; yy += WARPSIZE) {
#ifdef __DEBUG
		if (!local_frees->enqueue(warp_chunk + yy + TIDX))
		{
			printf("Error here\n");
		}
#else
		local_frees->enqueue(warp_chunk + yy + TIDX);
#endif
	}

	__shared__ ubkTask_ptr work_bag[TASKS_PER_DEQ];
	__shared__ uint elements_bag[TASKS_PER_DEQ];
	__shared__ ubkTask_ptr out_bag[MAX_SEC_TASK * TASKS_PER_DEQ];

	__gpu_sync(N_BLOCKS);

	while (!check_all_isEmpty()) {

		//Se a inbox esta vazia e a outbox tem trabalho, despeja na inbox ate ficar cheia ou não haver mais trabalho
		while (!local_inbox->isFull() && !local_outbox->isEmpty()) {
			ubkTask_ptr t_item = INIT_TASK;

			if (local_outbox->dequeue(t_item) && t_item != NULL) {
#ifdef __DEBUG

				local_inbox->enqueue(t_item) ? : printf("Error 0\n");
#else
				local_inbox->enqueue(t_item);
#endif
			}

		}

		//TODO empty by one element is nnot empty

		//Se a inbox não estiver cheia tenta roubar trabalho na global
		if (local_inbox->isEmpty() && inbox->tryLock()) {
			ubkTask_ptr t_item = INIT_TASK;
			while (!local_inbox->isFull() && !inbox->isEmpty()) {
				if (inbox->dequeue(t_item) && (t_item != NULL)) {
#ifdef __DEBUG
					if (!local_inbox->enqueue(t_item))
					{
						printf("Error 1\n");
						//__brkpt();
					}
#else
					local_inbox->enqueue(t_item);
#endif
				}
			}
			inbox->Release();
		}

		// work donation using global inbox
		try_donate(local_outbox);

		if (local_outbox->isFull()) {
			//#ifdef __DEBUG
			if (THREAD0)
				printf("%d Forcing lock 0 %u\n", blockIdx.x,
						local_outbox->getSize());
			//#endif
			inbox->Acquire();
			flush_to_global(local_outbox);
			inbox->Release();
		}

		if (THREAD0) {
			memset(out_bag, 0,
					MAX_SEC_TASK * TASKS_PER_DEQ * sizeof(ubkTask_ptr));
			memset(work_bag, 0, TASKS_PER_DEQ * sizeof(ubkTask_ptr));
			memset(elements_bag, 0, TASKS_PER_DEQ * sizeof(uint));
		}

#ifdef __DEBUG

		if ( (TIDX < TASKS_PER_DEQ) && !local_inbox->isEmpty() && local_inbox->dequeue(work_bag[TIDX]))
		if (local_frees->getSize() < MAX_SEC_TASK*10)
		{
			printf("Error: not enough frees for execution %u\n",local_frees->getSize());

		}
#else
		if ((TIDX < TASKS_PER_DEQ) && !local_inbox->isEmpty())
			local_inbox->dequeue(work_bag[TIDX]);

#endif

		__threadfence_block();

		for (int task_i = 0; task_i < TASKS_PER_DEQ; task_i++) {
			if (work_bag[task_i] != NULL) {
				k_CUDA_devptr_compute_force_Fine_T_SIMD32(global_task_,
						*work_bag[task_i], local_frees,
						out_bag + task_i * MAX_SEC_TASK, elements_bag[task_i],
						GARBAGE, visit_count, domain_result);
			}
		}

		if ((TIDX < TASKS_PER_DEQ) && work_bag[TIDX] != NULL
				&& !local_frees->isFull()) {
#ifdef __DEBUG
			if (!local_frees->enqueue(work_bag[TIDX])) printf("Error 2\n");
#else
			local_frees->enqueue(work_bag[TIDX]);
#endif
		}

		if (local_outbox->getSize() + MAX_SEC_TASK * TASKS_PER_DEQ
				>= local_outbox->_length) {
			//#ifdef __DEBUG
			if (THREAD0)
				printf("%d Forcing lock 1\n", blockIdx.x);
			//#endif
			inbox->Acquire();
			flush_to_global(local_outbox);
			inbox->Release();
		}

		//TODO why not direct outbox enqueue?

		for (int task_i = 0; task_i < TASKS_PER_DEQ; task_i++) {
			if (TIDX < elements_bag[task_i]) {
#ifdef __DEBUG

				if (!local_outbox->enqueue(out_bag[task_i*MAX_SEC_TASK + TIDX])) printf("Error 3\n");
#else
				local_outbox->enqueue(out_bag[task_i * MAX_SEC_TASK + TIDX]);
#endif
			}
		}

		vote_out(local_inbox, local_outbox);

//		if (inbox->isEmpty() && local_inbox->isEmpty()
//				&& local_outbox->isEmpty()) {
//			break;
//		}

	}

	__gpu_sync(N_BLOCKS);

//	if (THREAD0 && (local_inbox->getSize() != 0 || local_outbox->getSize() != 0)){
//		printf("%d HAS TASKS NOT EXECUTED %d %d %d\n",blockIdx.x,local_inbox->getSize(),
//		local_outbox->getSize(),inbox->getSize() );
//	}

//	if (tID == 0) {
//		delete heads_inbox;
//		delete heads_outbox;
//		delete heads_frees;
//
//	}

}

void k_CUDA_wrapper_uberkernel(Task* t, ubkTask* tasks_, int task_count) {

	//checkCUDAmemory("START");

	PERFORM_acc_timer *FORCES = new PERFORM_acc_timer();

	Domain<float> domain_input;
	t->getDomain(0, domain_input);

	Domain<float> domain_result;
	t->getDomain(1, domain_result);

	FORCES->start();

//	ubkSpecificTask* d_tasks;
//	ubkSpecificTask** d_inbox_buffer;
//	void* d_mem_buffer;
//	void* d_queues_buffer;
//
//	__E(cudaMalloc((void**) &d_tasks, task_count * sizeof(ubkSpecificTask)));
//	__E(cudaMalloc((void**) &d_inbox_buffer, UBK_INBOX_SIZE * sizeof(ubkSpecificTask*)));
//	__E(cudaMalloc((void**) &d_mem_buffer, MEM_TOTAL * N_BLOCKS));
//	__E(
//			cudaMalloc(
//					(void**) &d_queues_buffer,
//					N_BLOCKS * sizeof(ubkTask_ptr) * (LOCAL_INBOX_QUEUE_SIZE
//							+ LOCAL_OUTBOX_QUEUE_SIZE + LOCAL_FREES_QUEUE_SIZE)));

	size_t overhead_chunk_len1 = task_count * sizeof(ubkSpecificTask);
	size_t overhead_chunk_len2 = UBK_INBOX_SIZE * sizeof(ubkSpecificTask*);
	size_t overhead_chunk_len3 = MEM_TOTAL * N_BLOCKS;
	size_t overhead_chunk_len4 = N_BLOCKS * sizeof(ubkTask_ptr)
			* (LOCAL_INBOX_QUEUE_SIZE + LOCAL_OUTBOX_QUEUE_SIZE
					+ LOCAL_FREES_QUEUE_SIZE);

	size_t overhead_total = overhead_chunk_len1 + overhead_chunk_len2
			+ overhead_chunk_len3 + overhead_chunk_len4;

	//checkCUDAmemory("START");
	byte_address overhead_chunk;
	__E(cudaMalloc((void**) &overhead_chunk, overhead_total));

	ubkSpecificTask* d_tasks = (ubkSpecificTask*) overhead_chunk;
	ubkSpecificTask** d_inbox_buffer = (ubkSpecificTask**) (overhead_chunk
			+ overhead_chunk_len1);
	void* d_mem_buffer = (void*) (overhead_chunk + overhead_chunk_len1
			+ overhead_chunk_len2);
	void* d_queues_buffer = (void*) (overhead_chunk + overhead_chunk_len1
			+ overhead_chunk_len2 + overhead_chunk_len3);

#ifdef __DEBUG

//	fprintf(stderr, "UBK: Allocated %d for primary tasks\n",
//			(int) (task_count * sizeof(ubkSpecificTask)) / 1024 / 1024);
//
//	fprintf(stderr, "UBK: Allocated %d for d_inbox_buffer\n",
//			(int) (UBK_INBOX_SIZE * sizeof(ubkSpecificTask*)) / 1024 / 1024);
//
//	fprintf(stderr, "UBK: Allocated %d for task memory slots\n",
//			(int) (MEM_TOTAL * N_BLOCKS) / 1024 / 1024);
//	fprintf(
//			stderr,
//			"UBK: allocated %d for local queues\n",
//			((sizeof(ubkTask_ptr) * LOCAL_INBOX_QUEUE_SIZE * N_BLOCKS) + (sizeof(ubkTask_ptr)
//					* LOCAL_OUTBOX_QUEUE_SIZE * N_BLOCKS) + (sizeof(ubkTask_ptr)
//					* LOCAL_FREES_QUEUE_SIZE * N_BLOCKS)) / 1024 / 1024);

	fprintf(stderr, "Overhead memory allocated %u\n",
			(overhead_chunk_len1 + overhead_chunk_len2 + overhead_chunk_len3 + overhead_chunk_len4) / 1024 / 1024);

#endif

	__E(cudaMemset(d_tasks, 0, task_count * sizeof(ubkSpecificTask)));
	__E(
			cudaMemcpy(d_tasks, tasks_, (task_count) * sizeof(ubkSpecificTask),
					cudaMemcpyHostToDevice));

	//Breaking tasks' array into pointers
	//why host side?
	ubkSpecificTask* tasks_ptrs[task_count];
	for (int f = 0; f < task_count; f++) {
		tasks_ptrs[f] = d_tasks + f;
	}

	__E(
			cudaMemset(d_inbox_buffer, 0,
					UBK_INBOX_SIZE * sizeof(ubkSpecificTask*)));
	__E(
			cudaMemcpy(d_inbox_buffer, tasks_ptrs,
					(task_count) * sizeof(ubkSpecificTask*),
					cudaMemcpyHostToDevice));
	__E(cudaMemset(d_mem_buffer, 0, MEM_TOTAL * N_BLOCKS));

	fprintf(stderr, "Launching pkernel for %d x32 particles...\n", task_count);

	int garbage[32];
	int* garbaged;
	__E(cudaMalloc((void**) &garbaged, 32 * sizeof(int)));
	//	for (int i = 0; i < 32; i++)
	//		garbage[i] = 3;
	//	cudaMemcpyAsync(garbaged, garbage, 32 * sizeof(int), cudaMemcpyHostToDevice,
	//			(cudaStream_t) (t->stream));

	uint task_exec = 0;
	uint* task_exec_d;
	__E(cudaMalloc((void**) &task_exec_d, sizeof(uint)));

	//cudaHostAlloc((void**) &task_exec_d, sizeof(uint), cudaHostAllocDefault);

	int* pipe = 0;
	//	cudaHostAlloc((void**) &pipe, sizeof(int) * N_BLOCKS + 1, cudaHostAllocDefault);
	//	memset(pipe, 0, sizeof(int) * N_BLOCKS + 1);
	//PRE_FORCES->stop();

	//	long long unsigned int visit_count = 0;
	long long unsigned int* visit_count_d;
	//	cudaMalloc((void**) &visit_count_d, sizeof(long long unsigned int));
	//	cudaMemcpy(visit_count_d, &visit_count, sizeof(long long unsigned int), cudaMemcpyHostToDevice);

	checkCUDAError();

	//if (task_count >= 32 )
	//	{
	uberkernel<<<N_BLOCKS,32,0,t->stream>>>
	(*(Task_ForcesCalc*) t, domain_input, domain_result, d_inbox_buffer, d_mem_buffer, d_queues_buffer, task_count, garbaged, task_exec_d, visit_count_d, pipe);
	checkCUDAError();

	cudaStreamSynchronize(t->stream);
//	printf("pkernel done\n");

	FORCES->stop();

	//cudaMemcpy(&visit_count, visit_count_d, sizeof(long long unsigned int), cudaMemcpyDeviceToHost);
	//printf("%lld\n",visit_count);

	//cudaMemcpy(garbage, garbaged, 32 * sizeof(int), cudaMemcpyDeviceToHost);
	//__E(
	//		cudaMemcpy(&task_exec, task_exec_d, sizeof(uint),
	//				cudaMemcpyDeviceToHost));

	//for (int i = 0; i < 32; i++)
	//fprintf(stderr,"garbage %d\n", garbage[i]);

	//fprintf(stderr, "\nExecuted total %u\n", task_exec);

	k_CUDA_update_vel_positions(t);

	cudaStreamSynchronize(t->stream);

//	__E(cudaFree(d_tasks));
//	__E(cudaFree(d_inbox_buffer));
//	__E(cudaFree(d_mem_buffer));
//	__E(cudaFree(d_queues_buffer));

	__E(cudaFree(overhead_chunk));

	checkCUDAError();

	//checkCUDAmemory("FINAL");

	//fprintf(stderr, "FORCES: %s\n", FORCES->print());

}
