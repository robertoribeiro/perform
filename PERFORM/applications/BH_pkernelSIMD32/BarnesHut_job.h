#ifndef BARNESHUT_JOB_H_
#define BARNESHUT_JOB_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "../../perform.h"

using namespace std;
using namespace tbb;

#include "BarnesHut_common.h"
#include "BarnesHut_utils.h"
#include "user_defined_task.h"


void k_TBB_compute_force(Task* t);

void k_CUDA_compute_force(Task* t);

void k_CUDA_update_positions(Task* t);
void k_CUDA_update_vel_positions(Task* t);


double jobBARNES_HUT(int argc, char *argv[]);


#endif
