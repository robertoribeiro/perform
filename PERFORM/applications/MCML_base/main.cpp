#include "../../perform.h"
#include "task/Task.h"
#include "user_defined_task.h"
#include "mcml.h"

void k_CUDA_wrapper_uberkernel(Task* t, Task** task_prms,
		int child_task_count) {

}

void k_CPU_MCM_FULLT(Task* t_);

void k_CUDA_MCM_FULLT(Task* t_);

int job(int argc, char *argv[], int dev) {

	string data_location = "/home/rr/work/perform/PERFORM/data/mcml/";

	//string filename = data_location + "/input/test.mci";

	string filename;
	if (argc < 4) {
		filename = data_location + "input/test.mci";
	} else {

		data_location = "";
		filename = argv[5];
	}

	printf("Input file: %s\n", filename.c_str());

	unsigned long long seed = (unsigned long long) time(NULL);
	//unsigned long long seed = 1391623846;
	int ignoreAdetection = 0;

	SimulationStruct* simulation;

	// Read the simulation inputs.
	int n_simulations = read_simulation_data(filename.c_str(), &simulation,
			ignoreAdetection);

	if (n_simulations == 0) {
		printf("Something wrong with read_simulation_data!\n");
		return 1;
	}
	printf("Read %d simulations\n", n_simulations);

	//simulation->number_of_photons=1024*1024*10;

	if (argc < 4) {
		simulation->number_of_photons = 1024 * 1024 * 32 ;
	} else {
		simulation->number_of_photons = atoi(argv[4]);
	}

// Allocate and initialize RNG seeds.
	unsigned int len = simulation->number_of_photons;

	unsigned long long *x = (unsigned long long*) malloc(
			len * sizeof(unsigned long long));

	unsigned int *a = (unsigned int*) malloc(len * sizeof(unsigned int));

	if (init_RNG(x, a, len,
			(data_location + "executable/safeprimes_base32.txt").c_str(), seed))
		return 1;

// Output the execution configuration.
	printf("\n====================================\n");
	printf("EXECUTION MODE:\n");
	printf("  ignore A-detection:      %s\n", ignoreAdetection ? "YES" : "NO");
	printf("  seed:                    %llu\n", seed);
	printf("====================================\n\n");
	printf("\n------------------------------------------------------------\n");
	printf("        Simulation #%d\n", 0);
	printf("        - number_of_photons = %u\n", simulation->number_of_photons);
#ifdef MCML_FLUOR
	printf("        - Fluroscence simulation, chance %.3f, QY %.2f\n",
			FLUOR_CHANCE, FLUOR_QY);
#endif
	printf("------------------------------------------------------------\n\n");

	printf("Using the MWC random number generator ...\n");

	PERFORM_timer *timer = new PERFORM_timer();
	timer->start();

// For each GPU, init the host-side structure.
	HostThreadState* hstates;
	hstates = (HostThreadState*) malloc(sizeof(HostThreadState));

	hstates->sim = simulation;

	SimState *hss = &(hstates->host_sim_state);

// number of photons responsible
	hss->n_photons_left = (unsigned int*) malloc(sizeof(unsigned int));
	*(hss->n_photons_left) = simulation->number_of_photons;

// random number seeds
	hss->x = &x[0];
	hss->a = &a[0];

	// Init the remaining states.
	InitSimStates(hss, simulation);

	CPU_MCML::InitConstantCPUMem(simulation);

	//CUT_DEVICE_INIT(argc, argv);

	if (dev == 1 || dev == 2) {
		cudaSetDevice(CUDA_DEVICE_0);
		GPU_MCML::InitConstantGPUMem(simulation, CUDA_DEVICE_0);
	} else if (dev == 3 || dev == 4) {
		cudaSetDevice(CUDA_DEVICE_0);
		GPU_MCML::InitConstantGPUMem(simulation, CUDA_DEVICE_0);
		cudaSetDevice(CUDA_DEVICE_1);
		GPU_MCML::InitConstantGPUMem(simulation, CUDA_DEVICE_1);
	} else if (dev == 5 || dev == 6) {

		cudaSetDevice(CUDA_DEVICE_0);
		GPU_MCML::InitConstantGPUMem(simulation, CUDA_DEVICE_0);
		cudaSetDevice(CUDA_DEVICE_1);
		GPU_MCML::InitConstantGPUMem(simulation, CUDA_DEVICE_1);
		cudaSetDevice(CUDA_DEVICE_2);
		GPU_MCML::InitConstantGPUMem(simulation, CUDA_DEVICE_2);

	}

	/**
	 *
	 * 		PERFORM
	 *
	 *
	 */

	UINT32 num_of_seeds;

#ifdef __FULLT
	num_of_seeds= simulation->number_of_photons;
#else
#if defined(__PKERNEL) || defined(__PKERNELSIMD32)
	num_of_seeds = (15*8)*32;//max N_BLOCKS * WARSIZE;
#endif
#endif

	//num_of_seeds= simulation->number_of_photons;


	Domain<UINT32>* rng_a = new Domain<UINT32>(1, R, hss->a,
			dim_space(0, num_of_seeds));

	Domain<UINT64>* rng_x = new Domain<UINT64>(1, R, hss->x,
			dim_space(0, num_of_seeds));

	int rz_size = simulation->det.nr * simulation->det.nz;
	int ra_size = simulation->det.nr * simulation->det.na;

	Domain<UINT64>* A_rz = new Domain<UINT64>(1, WD, hss->A_rz,
			dim_space(0, rz_size));

	Domain<UINT64>* Rd_ra = new Domain<UINT64>(1, WD, hss->Rd_ra,
			dim_space(0, ra_size));

	Domain<UINT64>* Tt_ra = new Domain<UINT64>(1, WD, hss->Tt_ra,
			dim_space(0, ra_size));

#ifdef __FULLT
	Task_MCML* t = new Task_MCML(REGULAR);
#else
#if defined(__PKERNEL) || defined(__PKERNELSIMD32)
	Task_MCML* t = new Task_MCML(IRREGULAR);
#endif
#endif

	t->job_elements = simulation->number_of_photons;
	t->photons_todo = simulation->number_of_photons;

	t->associate_domain(rng_a);
	t->associate_domain(rng_x);
	t->associate_domain(A_rz);
	t->associate_domain(Rd_ra);
	t->associate_domain(Tt_ra);

	t->associate_kernel(CPU, &k_CPU_MCM_FULLT);

#ifdef __FULLT
	t->associate_kernel(GPU, &k_CUDA_MCM_FULLT);
#endif

	performQ->addTasks(t);

	wait_for_all_tasks();

	performDL->splice(A_rz, NULL, RETRIEVE_TO_HOST_BLOCKING_OP);
	performDL->splice(Rd_ra, NULL, RETRIEVE_TO_HOST_BLOCKING_OP);
	performDL->splice(Tt_ra, NULL, RETRIEVE_TO_HOST_BLOCKING_OP);

	/**
	 *
	 * 		PERFORM
	 *
	 *
	 */

	timer->stop();
	printf("\nSimulation done in %.3fs!\n", timer->duration);

	Write_Simulation_Results(hss, simulation, 0);

// Free SimState structs.
	FreeHostSimState(hss);
	free(hstates);
// Free the random number seed arrays.
	free(x);
	free(a);
	FreeSimulationStruct(simulation, n_simulations);

}

int main(int argc, char *argv[]) {

	PERFORM_timer *timer = new PERFORM_timer();

	timer->start();

	int scheduler, dev, initial_dice, cpu_flops;

	scheduler = 3;

	printf("Argc %d\n", argc);

	if (argc < 4) {
		dev = 1;
		initial_dice = 16;
		cpu_flops = 400;
	} else {
		dev = atoi(argv[1]);
		initial_dice = atoi(argv[2]);
		cpu_flops = atoi(argv[3]);

	}

	initPERFORM(scheduler, dev, initial_dice, cpu_flops);

	job(argc, argv, dev);

	timer->stop();

	shutdownPERFORM();

	LOG(0.1,
			cout << "\n\nRun-time terminated in " << timer->duration << "s."
					<< endl
			;
	)

	return 0;
}

