/*****************************************************************************
 *
 *   GPU memory allocation, initialization, and transfer (Host <--> GPU)
 *
 ****************************************************************************/
/*	 
 *   This file is part of GPUMCML.
 *
 *   GPUMCML is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   GPUMCML is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with GPUMCML.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "mcml.h"

#include <string.h>


//////////////////////////////////////////////////////////////////////////////
//   Initialize Device Memory (global) for read/write data
//////////////////////////////////////////////////////////////////////////////
int InitSimStates(SimState* HostMem, SimulationStruct* sim) {
	int rz_size = sim->det.nr * sim->det.nz;
	int ra_size = sim->det.nr * sim->det.na;

	unsigned int size;

	// Allocate A_rz on host and device
	size = rz_size * sizeof(UINT64);
	HostMem->A_rz = (UINT64*) malloc(size);
	memset(HostMem->A_rz, 0, size);

	if (HostMem->A_rz == NULL) {
		fprintf(stderr, "Error allocating HostMem->A_rz");
		exit(1);
	}

	// Allocate Rd_ra on host and device
	size = ra_size * sizeof(UINT64);
	HostMem->Rd_ra = (UINT64*) malloc(size);

	memset(HostMem->Rd_ra, 0, size);
	if (HostMem->Rd_ra == NULL) {
		printf("Error allocating HostMem->Rd_ra");
		exit(1);
	}

	// Allocate Tt_ra on host and device
	size = ra_size * sizeof(UINT64);
	HostMem->Tt_ra = (UINT64*) malloc(size);
	memset(HostMem->Tt_ra, 0, size);

	if (HostMem->Tt_ra == NULL) {
		printf("Error allocating HostMem->Tt_ra");
		exit(1);
	}


	return 1;
}

//////////////////////////////////////////////////////////////////////////////
//   Free Host Memory
//////////////////////////////////////////////////////////////////////////////
void FreeHostSimState(SimState *hstate) {
	if (hstate->n_photons_left != NULL) {
		free(hstate->n_photons_left);
		hstate->n_photons_left = NULL;
	}

	// DO NOT FREE RANDOM NUMBER SEEDS HERE.

	if (hstate->A_rz != NULL) {
		free(hstate->A_rz);
		hstate->A_rz = NULL;
	}
	if (hstate->Rd_ra != NULL) {
		free(hstate->Rd_ra);
		hstate->Rd_ra = NULL;
	}
	if (hstate->Tt_ra != NULL) {
		free(hstate->Tt_ra);
		hstate->Tt_ra = NULL;
	}
}


