#include "BarnesHut.h"

__global__ void compute_force(cell* cells, particle* particles, int offset_s,int offset_e, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {

	int p = blockIdx.x * blockDim.x + threadIdx.x + offset_s;

	if (p <= offset_e && p >= offset_s) {

		BH_TYPE drx, dry, drz, drsq, nphi, scale, idr;
		BH_TYPE ax, ay, az;

		particle x =  particles[p];

				ax = x.accx;
				ay = x.accy;
				az = x.accz;

				x.accx = 0.0;
				x.accy = 0.0;
				x.accz = 0.0;

		/* __shared__ */ register char stack[100];
		//register char* stack= new char[tree_height];
		int stack_top = 0;

		int current = 0;
		int child = 0;

		dsq *= 0.25;

		mass_position mass_pos_p = x.mass_pos;


		while (!(stack_top == 0 && child == 8)) {
			
			//printf("Stack pointer: %d\n",stack_top);

			if (child == 8) {
				current = cells[current].parent_cell_index;
				dsq *= 4;
				child = stack[--stack_top];
				child++;

			} else {

				mass_position mass_pos;
				if (cells[current].child_types[child] == PARTICLE)
					mass_pos = particles[cells[current].child_indexes[child]].mass_pos;
				else if (cells[current].child_types[child] == CELL)
					mass_pos = cells[cells[current].child_indexes[child]].mass_pos;
				else {
					child++;
					continue;
				}


				drx = mass_pos.x - mass_pos_p.x;
				dry = mass_pos.y - mass_pos_p.y;
				drz = mass_pos.z - mass_pos_p.z;

				drsq = drx * drx + dry * dry + drz * drz;
				//cout << "drsq:" << drsq << " dsq:" << dsq << endl;
				if (drsq < dsq) {
					if (cells[current].child_types[child] == CELL) {
						dsq *= 0.25;
						current = cells[current].child_indexes[child];
						stack[stack_top++] = child;
						child = 0;
					} else if (cells[current].child_types[child] == PARTICLE) {
						if (mass_pos.id != mass_pos_p.id) {
							drsq += epssq;
							idr = 1 / sqrt(drsq);
							nphi = mass_pos.mass * idr;
							scale = nphi * idr * idr;
							x.accx += drx * scale;
													x.accy += dry * scale;
													x.accz += drz * scale;
						}
						child++;
					} else if (cells[current].child_types[child] == NILL) {
						child++;
					}
				} else {
					drsq += epssq;
					idr = 1 / sqrt(drsq);
					nphi = mass_pos.mass * idr;
					scale = nphi * idr * idr;
					x.accx += drx * scale;
						x.accy += dry * scale;
						x.accz += drz * scale;
					child++;
				}

			}
		}

		BH_TYPE dthf = 0.5 * dtime;

		if (step > 0) {
			x.velx += (x.accx - ax) * dthf;
			x.vely += (x.accy - ay) * dthf;
			x.velz += (x.accz - az) * dthf;
		}

		particles[p]=x;

	}

}

__global__ void update_positions(particle* particles, int offset_s,int offset_e, BH_TYPE dtime) {

	int pt = blockIdx.x * blockDim.x + threadIdx.x + offset_s;

	if (pt <= offset_e && pt >= offset_s) {
		BH_TYPE dthf = 0.5 * dtime;

		BH_TYPE dvelx, dvely, dvelz;
		BH_TYPE velhx, velhy, velhz;

		particle* p = &(particles[pt]);

		dvelx = p->accx * dthf;
		dvely = p->accy * dthf;
		dvelz = p->accz * dthf;

		velhx = p->velx + dvelx;
		velhy = p->vely + dvely;
		velhz = p->velz + dvelz;

		p->mass_pos.x += velhx * dtime;
		p->mass_pos.y += velhy * dtime;
		p->mass_pos.z += velhz * dtime;

		p->velx = velhx + dvelx;
		p->vely = velhy + dvely;
		p->velz = velhz + dvelz;
	}

}

void compute_force_cuda(cell* cells, particle* particles,int offset_s,int offset_e, int tree_height, BH_TYPE dtime, BH_TYPE epssq, BH_TYPE dsq, int step) {
	
	int nbodies = (offset_e-offset_s) + 1;

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	//dim3 blockDIM = dim3(1000, 1);
	//dim3 gridDIM = dim3( 1, 1);

compute_force<<<blockDIM,gridDIM>>>(cells,particles, offset_s, offset_e,tree_height, dtime, epssq, dsq, step);

}

void update_positions_cuda(particle* particles,int offset_s, int offset_e, BH_TYPE dtime) {
	
	int nbodies = (offset_e-offset_s) + 1;

	dim3 blockDIM = dim3(512, 1);
	dim3 gridDIM = dim3((nbodies / blockDIM.x) + 1, 1);

	//dim3 blockDIM = dim3(1000, 1);
	//dim3 gridDIM = dim3( 1, 1);

update_positions<<<blockDIM,gridDIM>>>(particles, offset_s, offset_e,dtime);

}
