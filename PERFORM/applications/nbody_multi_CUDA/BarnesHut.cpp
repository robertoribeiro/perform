#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <vector>
#include "../../myUtil.h"

#include "BarnesHut.h"
#include "../../common.h"

using namespace std;

void jobBARNES_HUT(int argc, char *argv[]) {

	BH_TYPE dtime; // length of one time step
	BH_TYPE eps; // potential softening parameter
	BH_TYPE tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	int nbodies;
	int cell_count;
	int timesteps;
	int tree_height = 0;

	char* filename;

	PERFORM_timer* timer = new PERFORM_timer();
	particle* particles;

	initBARNES_HUT(argc,argv,particles,nbodies,timesteps,filename);

	load_data_set(particles, filename, nbodies, timesteps, dtime, eps, tol);

	//fprintf(stderr, "configuration: %d bodies, %d time steps running in multi-GPU\n", nbodies, timesteps);

	BH_TYPE itolsq = 1.0 / (tol * tol);
	BH_TYPE epssq = eps * eps;

	//cudaDeviceProp deviceProp;
	//cudaGetDeviceProperties(&deviceProp, 2);

	//cout << "Using a device with compute capability: " << deviceProp.major << "." << deviceProp.minor << endl;

	timer->start();

	cell* d_0_nodes;
	particle* d_0_particles;

	cell* d_1_nodes;
	particle* d_1_particles;

	for (int step = 0; step < timesteps; step++) {

		BH_TYPE diameter, centerx, centery, centerz;

		compute_bounding_box(particles, nbodies, diameter, centerx, centery, centerz);

		int nnodes = nbodies * 2;
		if (nnodes < 1024 * 16)
			nnodes = 1024 * 16;

		cell* nodes = new cell[nnodes];

		cell root;
		root.mass_pos.x = centerx;
		root.mass_pos.y = centery;
		root.mass_pos.z = centerz;

		for (int k = 0; k < 8; k++)
			root.child_types[k] = NILL;

		for (int k = 0; k < 8; k++)
			root.child_indexes[k] = -1;

		nodes[0] = root;

		const BH_TYPE radius = diameter * 0.5;
		cell_count= build_tree_array(nodes, radius, particles, nbodies, tree_height);

		compute_center_of_mass(nodes, particles, 0, tree_height);

		BH_TYPE dsq = diameter * diameter * itolsq; //TODO explaind or do different

		cudaSetDevice(0);
		checkCUDAError();

		cudaMalloc(&d_0_nodes, cell_count * sizeof(struct cell_s));
		cudaMalloc(&d_0_particles, nbodies * sizeof(struct s_particle));

		cudaMemcpy(d_0_nodes, nodes, cell_count * sizeof(struct cell_s), cudaMemcpyHostToDevice);
		checkCUDAError();

		cudaMemcpy(d_0_particles, particles, nbodies * sizeof(struct s_particle), cudaMemcpyHostToDevice);
		checkCUDAError();

		compute_force_cuda(d_0_nodes, d_0_particles, 0, nbodies / 2 - 1, tree_height, dtime, epssq, dsq, step);
		checkCUDAError();

		cudaSetDevice(2);
		checkCUDAError();
		cudaThreadSynchronize();
		checkCUDAError();

		cudaMalloc(&d_1_nodes, cell_count * sizeof(struct cell_s));
		cudaMalloc(&d_1_particles, nbodies * sizeof(struct s_particle));

		cudaMemcpy(d_1_nodes, nodes, cell_count * sizeof(struct cell_s), cudaMemcpyHostToDevice);
		checkCUDAError();

		cudaMemcpy(d_1_particles, particles, nbodies * sizeof(struct s_particle), cudaMemcpyHostToDevice);
		checkCUDAError();

		compute_force_cuda(d_1_nodes, d_1_particles, nbodies / 2, nbodies - 1, tree_height, dtime, epssq, dsq, step);
		checkCUDAError();

		cudaThreadSynchronize();
		checkCUDAError();

		cudaSetDevice(0);
		checkCUDAError();

		update_positions_cuda(d_0_particles, 0, nbodies, dtime);

		cudaSetDevice(2);

		update_positions_cuda(d_1_particles, nbodies / 2, nbodies - 1, dtime);
		checkCUDAError();

		cudaSetDevice(0);
		checkCUDAError();

		cudaMemcpy(particles, d_0_particles, (nbodies / 2) * sizeof(struct s_particle), cudaMemcpyDeviceToHost);
		checkCUDAError();

		//	cudaMemcpy(particles, d_0_particles, nbodies * sizeof(struct s_particle), cudaMemcpyDeviceToHost);


		cudaSetDevice(2);
		checkCUDAError();

		cudaMemcpy(particles + nbodies / 2, d_1_particles + nbodies / 2, (nbodies / 2) * sizeof(struct s_particle), cudaMemcpyDeviceToHost);
		checkCUDAError();

	}

	timer->stop();

#ifdef _DEBUG
	jobBARNES_HUT_check(argc,argv,particles);
#endif

	printf("%.1f\t", timer->duration);



	cudaFree(d_0_particles);
	cudaFree(d_0_nodes);
	cudaFree(d_1_particles);
	cudaFree(d_1_nodes);

}

