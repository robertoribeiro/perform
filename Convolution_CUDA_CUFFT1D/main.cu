/* System includes */
#include <stdio.h>
#include <math.h>
#include <string.h>

/* My includes */
#include "matrix_ops.h"
#include "imageOps.h"

/* Cuda includes */
#include <cuda.h>
#include <cuda_runtime.h>
#include <cufft.h>

#include "../PERFORM/src/myUtil.h"

using namespace std;

struct IMAGE {
    double *data;
};

char *inamex,
*inamey;

float   UPLOAD=0,
        FFT1 =0, TRANS1 = 0, TRANS2 = 0, FFT2 = 0, TRANS3 = 0, TRANS4 = 0,
        FFT3 = 0, TRANS5 = 0, TRANS6 = 0, FFT4 = 0, TRANS7 = 0, TRANS8 = 0,
        FREQMULTI = 0,
        FFT5 = 0, TRANS9 = 0, TRANS10 = 0, FFT6 = 0, TRANS11 = 0, TRANS12 = 0,
        DOWNLOAD=0,
        TOTAL = 0;


__global__ void k_Normalize(double2 *x, int N) {

    int i = blockIdx.x * blockDim.x + threadIdx.x;

    double tmpx;
    double tmpy;
    
    tmpx = x[i].x * (double) (1.0/N);
    tmpy = x[i].y * (double) (1.0/N);
    
    x[i].x = tmpx;
    x[i].y = tmpy;
}

__global__ void TRANSPOSE(double2 *x, double2* y, int H, int W) {

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if (j < H && i < W)
        y[j * H + i] = x[i * W + j];
}

__global__ void FREQMUL(double2 *x, double2 *y, int N) {

    int i = blockIdx.x * blockDim.x + threadIdx.x;

    double tmpx;
    double tmpy;
    
    // do frequency multiplication: zReal = xReal*yReal - xImag*yImag;
    tmpx = (x[i].x * y[i].x) - (x[i].y * y[i].y);

    // do frequency multiplication: zImag = xImag*yReal + xReal*yImag;
    tmpy = (x[i].y * y[i].x) + (x[i].x * y[i].y);
    
    x[i].x = tmpx;
    x[i].y = tmpy;
}

int k_CUDACONV(double2 **h_InputSignal, int* args, void* stream) {

    int N = args[0];
    int twoN = N*N;
    int h = args[0];
    int w = args[0];

    cudaEvent_t timeBefore, timeAfter;
    cudaEventCreate(&timeBefore);
    cudaEventCreate(&timeAfter);
    
    /*
     * SETUP
     */
    cufftHandle plan;
    cufftPlan1d(&plan, N, CUFFT_Z2Z, N);

    int blockW = args[0];
    int gridW = 1;
    blockW = args[0];
    while (blockW * blockW > 1024) { // TODO: max thread value should come from GPU info
        gridW *= 2;
        blockW = blockW / 2;
    }
    dim3 transblockSize = dim3(blockW, blockW, 1);
    dim3 transgridSize = dim3(gridW, gridW, 1);

    /*
     *  ALLOCATE AND COPY DATA TO DEVICE
     */
  //  printf("Copying data from host to device\n");
    cudaEventRecord(timeBefore, 0);
    double2* d_InputSignal;
    double2* d_kernel;
    double2* d_InputSignal_out;
    cudaMalloc((void**) &d_InputSignal, twoN * sizeof (double2));
    cudaMalloc((void**) &d_kernel, twoN * sizeof (double2));
    cudaMalloc((void**) &d_InputSignal_out, twoN * sizeof (double2));
    cudaMemcpy(d_InputSignal, h_InputSignal[0], twoN * sizeof (double2), cudaMemcpyHostToDevice);
    cudaMemcpy(d_kernel, h_InputSignal[1], twoN * sizeof (double2), cudaMemcpyHostToDevice);
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&UPLOAD, timeBefore, timeAfter);

    /*
     * COMPUTE INPUT IMAGE FFT1
     */
   //printf("Fourier transform of the input image\n");
    cudaEventRecord(timeBefore, 0);
    cufftExecZ2Z(plan, (cufftDoubleComplex *) d_InputSignal, (cufftDoubleComplex *) d_InputSignal, CUFFT_FORWARD);
    cudaThreadSynchronize(); 
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FFT1, timeBefore, timeAfter);

    
    /*
     *  TRANSPOSE 1
     */
    cudaEventRecord(timeBefore, 0);
    TRANSPOSE << <transgridSize, transblockSize >> >(d_InputSignal, d_InputSignal_out, h, w);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&TRANS1, timeBefore, timeAfter);

    /*
     * COMPUTE INPUT IMAGE FFT2
     */
    cudaEventRecord(timeBefore, 0);
    cufftExecZ2Z(plan, (cufftDoubleComplex *) d_InputSignal_out, (cufftDoubleComplex *) d_InputSignal_out, CUFFT_FORWARD);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FFT2, timeBefore, timeAfter);


    /*
     *  TRANSPOSE 2
     */
    cudaEventRecord(timeBefore, 0);
    TRANSPOSE << <transgridSize, transblockSize >> >(d_InputSignal_out, d_InputSignal, h, w);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&TRANS2, timeBefore, timeAfter);

    /*
     * COMPUTE KERNEL FFT3
     */
  //  printf("Fourier transform of the kernel\n");
    cudaEventRecord(timeBefore, 0);
    cufftExecZ2Z(plan, (cufftDoubleComplex *) d_kernel, (cufftDoubleComplex *) d_kernel, CUFFT_FORWARD);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FFT3, timeBefore, timeAfter);


    /*
     *  TRANSPOSE 3
     */
    cudaEventRecord(timeBefore, 0);
    TRANSPOSE<< <transgridSize, transblockSize >> >(d_kernel, d_InputSignal_out, h, w);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&TRANS3, timeBefore, timeAfter);


    /*
     * COMPUTE KERNEL FFT4
     */
    cudaEventRecord(timeBefore, 0);
    cufftExecZ2Z(plan, (cufftDoubleComplex *) d_InputSignal_out, (cufftDoubleComplex *) d_InputSignal_out, CUFFT_FORWARD);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FFT4, timeBefore, timeAfter);


    /*
     *  TRANSPOSE 4
     */
    cudaEventRecord(timeBefore, 0);
    TRANSPOSE << <transgridSize, transblockSize >> >(d_InputSignal_out, d_kernel, h, w);
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&TRANS4, timeBefore, timeAfter);
   

    // FREQUENCY DOMAIN MULTIPLICATION
    // do frequency multiplication: zReal = xReal*yReal - xImag*yImag;
    // do frequency multiplication: zImag = xImag*yReal + xReal*yImag;
   // printf("Doing frequency domain multiplication\n");
    blockW = args[0];
    gridW = 1;
    blockW = args[0];
    while (blockW * blockW > 1024) {
        gridW *= 2;
        blockW = blockW / 2;
    }
    dim3 blockSize = dim3(blockW*blockW, 1, 1);
    dim3 gridSize = dim3(gridW*gridW, 1, 1);
    cudaThreadSynchronize();
    cudaEventRecord(timeBefore, 0);
    FREQMUL<<<gridSize, blockSize >>>(d_InputSignal, d_kernel, twoN);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FREQMULTI, timeBefore, timeAfter);

/*
    cudaMemcpy(h_InputSignal[0], d_InputSignal, twoN * sizeof (double2), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_InputSignal[1], d_kernel, twoN * sizeof (double2), cudaMemcpyDeviceToHost);
    return 1;
*/
    
    /*
     * COMPUTE KERNEL FFT5
     */
  // printf("Inverse Fourier transform of the output image\n");
    cudaEventRecord(timeBefore, 0);
    cufftExecZ2Z(plan, (cufftDoubleComplex *) d_InputSignal, (cufftDoubleComplex *) d_InputSignal, CUFFT_INVERSE);
    blockW = args[0];
    gridW = 1;
    blockW = args[0];
    while (blockW * blockW > 1024) { // TODO: max thread value should come from GPU info
        gridW *= 2;
        blockW = blockW / 2;
    }
    blockSize = dim3(blockW*blockW, 1, 1);
    gridSize = dim3(gridW*gridW, 1, 1);
    cudaThreadSynchronize();
    k_Normalize <<<gridSize, blockSize >>>(d_InputSignal, N);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FFT5, timeBefore, timeAfter);

    /*
     *  TRANSPOSE 5
     */
    cudaEventRecord(timeBefore, 0);
    TRANSPOSE << <transgridSize, transblockSize >> >(d_InputSignal, d_InputSignal_out, h, w);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&TRANS5, timeBefore, timeAfter);
    
    /*
     * COMPUTE KERNEL FFT6
     */
    cudaEventRecord(timeBefore, 0);
    cufftExecZ2Z(plan, (cufftDoubleComplex *) d_InputSignal_out, (cufftDoubleComplex *) d_InputSignal_out, CUFFT_INVERSE);
    blockW = args[0];
    gridW = 1;
    blockW = args[0];
    while (blockW * blockW > 1024) { // TODO: max thread value should come from GPU info
        gridW *= 2;
        blockW = blockW / 2;
    }
    blockSize = dim3(blockW*blockW, 1, 1);
    gridSize = dim3(gridW*gridW, 1, 1);
    cudaThreadSynchronize();
    k_Normalize <<<gridSize, blockSize>>>(d_InputSignal_out, N);
    cudaThreadSynchronize();
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&FFT6, timeBefore, timeAfter);

    /*
     *  TRANSPOSE 6
     */    
    cudaEventRecord(timeBefore, 0);
    TRANSPOSE<< <transgridSize, transblockSize >> >(d_InputSignal_out, d_InputSignal, h, w);
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&TRANS6, timeBefore, timeAfter);


    /*
     * COPY DATA FROM DEVICE
     */
 //   printf("Copying data from device to host\n");
    cudaEventRecord(timeBefore, 0);
    cudaMemcpy(h_InputSignal[0], d_InputSignal, twoN * sizeof (double2), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_InputSignal[1], d_kernel, twoN * sizeof (double2), cudaMemcpyDeviceToHost);
    cudaEventRecord(timeAfter, 0);
    cudaEventSynchronize(timeAfter);
    cudaEventElapsedTime(&DOWNLOAD, timeBefore, timeAfter);

    cufftDestroy(plan);
    cudaFree(d_InputSignal);
    cudaFree(d_InputSignal_out);
    cudaFree(d_kernel);

    return 1;
}

double job_CONV() {

    int Nx = getImageSize(inamex);
    int Ny = 3; //getImageSize(inamey);

    int args[3];
    struct IMAGE xReal, xImag,
            yReal, yImag,
            zReal, zImag;
    struct IMAGE tmp;
    struct IMAGE tmp2;

    /* Read input images ********************************************************* */
    //printf("Reading input image with size: %d\n", Nx);
    xReal.data = new double [Nx * Nx];
    yReal.data = new double [Ny * Ny];
    readImage(xReal.data, inamex);
    // Edge detection kernel
    yReal.data[0] = -1;
    yReal.data[1] = -1;
    yReal.data[2] = -1;
    yReal.data[3] = -1;
    yReal.data[4] = 8;
    yReal.data[5] = -1;
    yReal.data[6] = -1;
    yReal.data[7] = -1;
    yReal.data[8] = -1;
    //readImage(yReal.data, inamey);

    /* Padd input images ******************************************************** */
    int paddedN = 0;
    paddedN = getNextPower(getImageSize(inamex), 3);
    //printf("Padding images to size: %d x %d\n", paddedN, paddedN);
    int n = -1;
    tmp.data = new double [paddedN * paddedN];
    for (int i = 0; i < paddedN; i++) {
        n++;
        for (int j = 0; j < paddedN; j++) {
            if (j >= Nx || n >= Nx)
                tmp.data[i * paddedN + j] = 0;
            else
                tmp.data[i * paddedN + j] = xReal.data[n * Nx + j];
        }
    }
    tmp2.data = xReal.data;
    xReal.data = tmp.data;
    delete[] tmp2.data;

    tmp.data = new double [paddedN * paddedN];
    n = -1;
    for (int i = 0; i < paddedN; i++) {
        n++;
        for (int j = 0; j < paddedN; j++) {
            if (j >= Ny || n >= Ny)
                tmp.data[i * paddedN + j] = 0;
            else
                tmp.data[i * paddedN + j] = yReal.data[n * Ny + j];
        }
    }
    tmp2.data = yReal.data;
    yReal.data = tmp.data;
    delete[] tmp2.data;

    int N = paddedN;


    /* Alloc memory to remaingin images ****************************************** */
  //  printf("Allocating space for remaining images\n");
    xImag.data = new double [N * N];
    yImag.data = new double [N * N];
    zReal.data = new double [N * N];
    zImag.data = new double [N * N];
    tmp.data = new double [N * N];

    memset(xImag.data, '0', N * N * sizeof (double));
    memset(yImag.data, '0', N * N * sizeof (double));
    memset(zReal.data, '0', N * N * sizeof (double));
    memset(zImag.data, '0', N * N * sizeof (double));
    memset(tmp.data, '0', N * N * sizeof (double));


	PERFORM_timer *timer = new PERFORM_timer();
	timer->start();


    /* FFT of first image ******************************************************* */
  // printf("Fourier transform of the input image\n");

    int twoN = N*N;
    double2* h_InputSignal = new double2[twoN];
    for (int i = 0; i < twoN; i++) {
        h_InputSignal[i].x = xReal.data[i];
        h_InputSignal[i].y = 0;
    }
    double2* h_kernel = new double2[twoN];
    for (int i = 0; i < twoN; i++) {
        h_kernel[i].x = yReal.data[i];
        h_kernel[i].y = 0;
    }

    double2 **data_pointers_cuda = new double2*[2];

    data_pointers_cuda[0] = h_InputSignal;
    data_pointers_cuda[1] = h_kernel;

    args[0] = N;
    args[1] = 1;
    k_CUDACONV(data_pointers_cuda, args, NULL);


	timer->stop();


   // printf("Coverting data\n");
/*
    for (int i = 0; i < twoN; i++) {
        xReal.data[i] = h_InputSignal[i].x;
        xImag.data[i] = h_InputSignal[i].y;
    }
*/
/*
    for (int i = 0; i < twoN; i++) {
        yReal.data[i] = h_kernel[i].x;
        yImag.data[i] = h_kernel[i].y;
    }
*/
    for (int i = 0; i < twoN; i++) {
        zReal.data[i] = h_InputSignal[i].x;
        zImag.data[i] = h_InputSignal[i].y;
    }
 
    /*******************************************************************************
     *
     * Ending
     *  
     */

    TOTAL = UPLOAD +
            FFT1 + TRANS1 +
            FFT2 + TRANS2 +
            FFT3 + TRANS3 +
            FFT4 + TRANS4 + 
            FREQMULTI +
            FFT5 + TRANS5 +
            FFT6 + TRANS6 +
            DOWNLOAD;

//    printf("Times:\n");
//    printf("    UPLOAD:    %f\n", UPLOAD * 0.001);
//    printf("    FFT1:      %f\n", FFT1 * 0.001);
//    printf("    TRANS1:    %f\n", TRANS1 * 0.001);
//    printf("    FFT2:      %f\n", FFT2 * 0.001);
//    printf("    TRANS2:    %f\n", TRANS2 * 0.001);
//    printf("    FFT3:      %f\n", FFT3 * 0.001);
//    printf("    TRANS3:    %f\n", TRANS3 * 0.001);
//    printf("    FFT4:      %f\n", FFT4 * 0.001);
//    printf("    TRANS4:    %f\n", TRANS4 * 0.001);
//    printf("    FREQMULTI: %f\n", FREQMULTI * 0.001);
//    printf("    FFT5:      %f\n", FFT5 * 0.001);
//    printf("    TRANS5:    %f\n", TRANS5 * 0.001);
//    printf("    FFT6:      %f\n", FFT6 * 0.001);
//    printf("    TRANS6:    %f\n", TRANS6 * 0.001);
//    printf("    DOWNLOAD:  %f\n", DOWNLOAD * 0.001);
//    printf("    TOTAL:     %f\n", TOTAL * 0.001);
//    printf("Storing convolution result\n");
    // crop final image
    N = getImageSize(inamex);

    n = -1;
    tmp.data = new double [N * N];
    for (int i = 0; i < paddedN; i++) {
        n++;
        for (int j = 0; j < paddedN; j++) {
            // TODO: I now this is odd! ... but not important! 
            if (j >= N || n >= N)
                continue;
            else
                tmp.data[n * N + j] = zReal.data[i * paddedN + j];
        }
    }
    tmp2.data = zReal.data;
    zReal.data = tmp.data;
    delete[] tmp2.data;

    writeImage(tmp.data, "Convolution_CUDA_CUFFT1Dallgpu", N);
    // The end. TADAAAA!!!

    //    // Save information on a file
    //    FILE *fp=fopen("CUDAcustom_result.txt", "w");
    //    for(int i=0 ; i<N*N; i++)
    //        fprintf(fp, "%f\n", zReal.data[i]);
    //    fclose(fp);

    // final clean
    delete[] zReal.data;
    delete[] zImag.data;

    return TOTAL/1000;
}

int main(int argc, char **argv) {


    inamex = argv[1];
    inamey = argv[1];

    int deviceCount;
    cudaGetDeviceCount(&deviceCount);
    if (deviceCount == 0) {
        printf("Erro, # devices: %d\n", deviceCount);
        return 0;
    }
    //printf("# devices: %d\n", deviceCount);
    if (deviceCount > 1) {
        cudaDeviceProp deviceProp;
        cudaGetDeviceProperties(&deviceProp, 0);
      //  printf("Using device %d: \"%s\"\n\n", 0, deviceProp.name);
        cudaSetDevice(0);
    } else {
        printf("Using device 0\n");
        cudaSetDevice(0);
    }

	printf("%.2f ",  job_CONV());
    if (deviceCount > 1)
        cudaThreadExit();

    return ( 0);
}
