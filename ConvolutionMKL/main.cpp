/* System includes */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* TBB includes */
#include <tbb/tick_count.h>

/* My includes */
#include "tbbConvOp.h"
#include "matrix_ops.h"
#include "imageOps.h"

#include "../PERFORM/src/myUtil.h"

using namespace tbb;
using namespace std;

struct IMAGE {
	double *data;
};

char *inamex, *inamey;

double FFT1 = 0, TRANS1 = 0, TRANS2 = 0, FFT2 = 0, TRANS3 = 0, TRANS4 = 0, FFT3 = 0, TRANS5 = 0, TRANS6 = 0, FFT4 = 0, TRANS7 = 0, TRANS8 = 0, FREQMULTI = 0,
		FFT5 = 0, TRANS9 = 0, TRANS10 = 0, FFT6 = 0, TRANS11 = 0, TRANS12 = 0, TOTAL = 0;

double job_CONV() {

	tbb::tick_count timeBefore, timeAfter;

	int Nx = getImageSize(inamex);
	int Ny = 3;

	int args[3];
	double *data_pointers[3];
	struct IMAGE xReal, xImag, yReal, yImag, zReal, zImag;
	struct IMAGE tmp;
	struct IMAGE tmp2;

	/* Read input images ********************************************************* */
	// printf("Reading input image with size: %d x %d\n", Nx, Nx);
	xReal.data = new double[Nx * Nx];
	yReal.data = new double[Ny * Ny];
	readImage(xReal.data, inamex);
	// Edge detection kernel
	yReal.data[0] = -1;
	yReal.data[1] = -1;
	yReal.data[2] = -1;
	yReal.data[3] = -1;
	yReal.data[4] = 8;
	yReal.data[5] = -1;
	yReal.data[6] = -1;
	yReal.data[7] = -1;
	yReal.data[8] = -1;
	//readImage(yReal.data, inamey);

	/* Padd input images ******************************************************** */
	int paddedN = 0;
	paddedN = getNextPower(getImageSize(inamex), 3);
	//  printf("Padding images to size: %d x %d\n", paddedN, paddedN);
	int n = -1;
	tmp.data = new double[paddedN * paddedN];
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= Nx || n >= Nx)
				tmp.data[i * paddedN + j] = 0;
			else
				tmp.data[i * paddedN + j] = xReal.data[n * Nx + j];
		}
	}
	tmp2.data = xReal.data;
	xReal.data = tmp.data;
	delete[] tmp2.data;

	tmp.data = new double[paddedN * paddedN];
	n = -1;
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= Ny || n >= Ny)
				tmp.data[i * paddedN + j] = 0;
			else
				tmp.data[i * paddedN + j] = yReal.data[n * Ny + j];
		}
	}
	tmp2.data = yReal.data;
	yReal.data = tmp.data;
	delete[] tmp2.data;

	int N = paddedN;

	/* Alloc memory to remaining images ****************************************** */
	//   printf("Allocating space for remaining images\n");
	xImag.data = new double[N * N];
	yImag.data = new double[N * N];
	zReal.data = new double[N * N];
	zImag.data = new double[N * N];
	tmp.data = new double[N * N];

	memset(xImag.data, '0', N * N * sizeof(double));
	memset(yImag.data, '0', N * N * sizeof(double));
	memset(zReal.data, '0', N * N * sizeof(double));
	memset(zImag.data, '0', N * N * sizeof(double));
	memset(tmp.data, '0', N * N * sizeof(double));

	/* FFT of first image ******************************************************* */
	//  printf("Fourier transform of the input image\n");

	args[0] = N;
	args[1] = 1;
	data_pointers[0] = xReal.data;
	data_pointers[1] = xImag.data;
	MKL_Complex16* input;
	input = (MKL_Complex16*) malloc(sizeof(MKL_Complex16) * N * N);
	for (int i = 0; i < N * N; i++) {
		input[i].real = data_pointers[0][i];
		input[i].imag = data_pointers[1][i];
	}

	PERFORM_timer *timer = new PERFORM_timer();
	timer->start();

	timeBefore = tbb::tick_count::now();
	k_FFT2(data_pointers, args, NULL, input);
	timeAfter = tbb::tick_count::now();
	// Copy data to host format
	for (int i = 0; i < N * N; i++) {
		data_pointers[0][i] = input[i].real;
		data_pointers[1][i] = input[i].imag;
	}
	FFT1 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	args[1] = N;
	data_pointers[0] = xReal.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = xReal.data;
	xReal.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS1 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = xImag.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = xImag.data;
	xImag.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS2 = (timeAfter - timeBefore).seconds();

	data_pointers[0] = xReal.data;
	data_pointers[1] = xImag.data;
	args[1] = 1;
	for (int i = 0; i < N * N; i++) {
		input[i].real = data_pointers[0][i];
		input[i].imag = data_pointers[1][i];
	}
	timeBefore = tbb::tick_count::now();
	k_FFT2(data_pointers, args, NULL, input);
	timeAfter = tbb::tick_count::now();
	for (int i = 0; i < N * N; i++) {
		data_pointers[0][i] = input[i].real;
		data_pointers[1][i] = input[i].imag;
	}
	FFT2 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = xReal.data;
	data_pointers[1] = tmp.data;
	args[1] = N;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = xReal.data;
	xReal.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS3 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = xImag.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = xImag.data;
	xImag.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS4 = (timeAfter - timeBefore).seconds();

	//    printf("...Saving results\n");
	//    writeImage(xReal.data, "MKL_freq_x_real", N);
	//    writeImage(xImag.data, "MKL_freq_x_imag", N);

	/* FFT of second  image ***************************************************** */
	// printf("Fourier transform of the kernel\n");
	args[0] = N;
	args[1] = 1;
	data_pointers[0] = yReal.data;
	data_pointers[1] = yImag.data;
	for (int i = 0; i < N * N; i++) {
		input[i].real = data_pointers[0][i];
		input[i].imag = data_pointers[1][i];
	}
	timeBefore = tbb::tick_count::now();
	k_FFT2(data_pointers, args, NULL, input);
	timeAfter = tbb::tick_count::now();
	for (int i = 0; i < N * N; i++) {
		data_pointers[0][i] = input[i].real;
		data_pointers[1][i] = input[i].imag;
	}
	FFT3 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	args[1] = N;
	data_pointers[0] = yReal.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = yReal.data;
	yReal.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS5 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = yImag.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = yImag.data;
	yImag.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS6 = (timeAfter - timeBefore).seconds();

	data_pointers[0] = yReal.data;
	data_pointers[1] = yImag.data;
	args[1] = 1;
	for (int i = 0; i < N * N; i++) {
		input[i].real = data_pointers[0][i];
		input[i].imag = data_pointers[1][i];
	}
	timeBefore = tbb::tick_count::now();
	k_FFT2(data_pointers, args, NULL, input); // Do fft of each row
	timeAfter = tbb::tick_count::now();
	for (int i = 0; i < N * N; i++) {
		data_pointers[0][i] = input[i].real;
		data_pointers[1][i] = input[i].imag;
	}
	FFT4 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = yReal.data;
	data_pointers[1] = tmp.data;
	args[1] = N;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = yReal.data;
	yReal.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS7 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = yImag.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = yImag.data;
	yImag.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS8 = (timeAfter - timeBefore).seconds();

	//    printf("...Saving results\n");
	//    writeImage(yReal.data, "MKL_freq_y_real", N);
	//    writeImage(yImag.data, "MKL_freq_y_imag", N);


	/* *****************************************************************************
	 *
	 * Now that we have to frequency domain images,
	 * we must multiplicate them.
	 *
	 */

	//  printf("Doing frequency domain multiplication\n");
	timeBefore = tbb::tick_count::now();
	// do frequency multiplication: zReal = xReal*yReal - xImag*yImag;
	data_pointers[0] = xReal.data;
	data_pointers[1] = yReal.data;
	data_pointers[2] = zReal.data;
	args[0] = N;
	k_PWM(data_pointers, args, NULL); //zReal = xReal*yReal

	data_pointers[0] = xImag.data;
	data_pointers[1] = yImag.data;
	data_pointers[2] = tmp.data;
	k_PWM(data_pointers, args, NULL); //tmp = xImag*yImag

	data_pointers[0] = zReal.data;
	data_pointers[1] = tmp.data;
	k_MSUB(data_pointers, args, NULL);

	// do frequency multiplication: zImag = xImag*yReal + xReal*yImag;
	data_pointers[0] = xImag.data;
	data_pointers[1] = yReal.data;
	data_pointers[2] = zImag.data;
	k_PWM(data_pointers, args, NULL);

	data_pointers[0] = xReal.data;
	data_pointers[1] = yImag.data;
	data_pointers[2] = tmp.data;
	k_PWM(data_pointers, args, NULL);

	data_pointers[0] = zImag.data;
	data_pointers[1] = tmp.data;
	k_MADD(data_pointers, args, NULL);
	timeAfter = tbb::tick_count::now();
	FREQMULTI = (timeAfter - timeBefore).seconds();
	//
	//    printf("...Saving results\n");
	//    writeImage(zReal.data, "MKL_real_mul", N);
	//    writeImage(zImag.data, "MKL_imag_mul", N);


	/* *****************************************************************************
	 *
	 * After the multiplication is done, we have to apply the
	 * inverse FFT in order to obtain the image in time domain.
	 *
	 */

	// delete unused space
	delete[] xReal.data;
	delete[] yReal.data;
	delete[] xImag.data;
	delete[] yImag.data;

	//  printf("Inverse Fourier transform of the output image\n");
	/* Inverse fft of the result image */
	args[0] = N;
	args[1] = -1;
	data_pointers[0] = zReal.data;
	data_pointers[1] = zImag.data;
	for (int i = 0; i < N * N; i++) {
		input[i].real = data_pointers[0][i];
		input[i].imag = data_pointers[1][i];
	}
	timeBefore = tbb::tick_count::now();
	k_FFT2(data_pointers, args, NULL, input);
	timeAfter = tbb::tick_count::now();
	for (int i = 0; i < N * N; i++) {
		data_pointers[0][i] = input[i].real;
		data_pointers[1][i] = input[i].imag;
	}
	FFT5 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	args[1] = N;
	data_pointers[0] = zReal.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = zReal.data;
	zReal.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS9 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = zImag.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = zImag.data;
	zImag.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS10 = (timeAfter - timeBefore).seconds();

	data_pointers[0] = zReal.data;
	data_pointers[1] = zImag.data;
	args[1] = -1;
	for (int i = 0; i < N * N; i++) {
		input[i].real = data_pointers[0][i];
		input[i].imag = data_pointers[1][i];
	}
	timeBefore = tbb::tick_count::now();
	k_FFT2(data_pointers, args, NULL, input);
	timeAfter = tbb::tick_count::now();
	for (int i = 0; i < N * N; i++) {
		data_pointers[0][i] = input[i].real;
		data_pointers[1][i] = input[i].imag;
	}
	FFT6 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = zReal.data;
	data_pointers[1] = tmp.data;
	args[1] = N;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = zReal.data;
	zReal.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS11 = (timeAfter - timeBefore).seconds();

	timeBefore = tbb::tick_count::now();
	data_pointers[0] = zImag.data;
	data_pointers[1] = tmp.data;
	k_TRANSPOSE(data_pointers, args, NULL);
	tmp2.data = zImag.data;
	zImag.data = tmp.data;
	tmp.data = tmp2.data;
	timeAfter = tbb::tick_count::now();
	TRANS12 = (timeAfter - timeBefore).seconds();

	/*******************************************************************************
	 *
	 * Ending
	 *
	 */

	timer->stop();

	TOTAL = FFT1 + TRANS1 + TRANS2 + FFT2 + TRANS3 + TRANS4 + FFT3 + TRANS5 + TRANS6 + FFT4 + TRANS7 + TRANS8 + FREQMULTI + FFT5 + TRANS9 + TRANS10 + FFT6
			+ TRANS11 + TRANS12;

	//    printf("Times:\n");
	//    printf("    FFT1:      %f\n", FFT1);
	//    printf("    TRANS1:    %f\n", TRANS1);
	//    printf("    TRANS2:    %f\n", TRANS2);
	//    printf("    FFT2:      %f\n", FFT2);
	//    printf("    TRANS3:    %f\n", TRANS3);
	//    printf("    TRANS4:    %f\n", TRANS4);
	//    printf("    FFT3:      %f\n", FFT3);
	//    printf("    TRANS5:    %f\n", TRANS5);
	//    printf("    TRANS6:    %f\n", TRANS6);
	//    printf("    FFT4:      %f\n", FFT4);
	//    printf("    TRANS7:    %f\n", TRANS7);
	//    printf("    TRANS8:    %f\n", TRANS8);
	//    printf("    FREQMULTI: %f\n", FREQMULTI);
	//    printf("    FFT5:      %f\n", FFT5);
	//    printf("    TRANS9:    %f\n", TRANS9);
	//    printf("    TRANS10:   %f\n", TRANS10);
	//    printf("    FFT6:      %f\n", FFT6);
	//    printf("    TRANS11:   %f\n", TRANS11);
	//    printf("    TRANS12:   %f\n", TRANS12);
	//    printf("    TOTAL:     %f\n", TOTAL);


	//  printf("Storing convolution result\n");
	// 'unpad' final image
	N = getImageSize(inamex);
	n = -1;
	for (int i = 0; i < paddedN; i++) {
		n++;
		for (int j = 0; j < paddedN; j++) {
			if (j >= N || n >= N)
				continue;
			else
				tmp.data[n * N + j] = zReal.data[i * paddedN + j];
		}
	}
	tmp2.data = zReal.data;
	zReal.data = tmp.data;
	delete[] tmp2.data;

	writeImage(tmp.data, "Convolution_MKL", N);
	// The end. TADAAAA!!!

	// Save information on a file
	//    FILE *fp=fopen("TBB_result.txt", "w");
	//    for(int i=0 ; i<N*N; i++)
	//        fprintf(fp, "%f\n", zReal.data[i]);
	//    fclose(fp);

	// final clean
	delete[] zReal.data;
	delete[] zImag.data;

	return TOTAL;
}

int main(int argc, char **argv) {

	// printf("\nBegin\n");

	inamex = argv[1];
	inamey = argv[1];

	//ob_CONV();

	printf("%.2f ", job_CONV());

	// printf("End\n");
	return (0);
}
