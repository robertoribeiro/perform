#include <stdio.h>
#include <math.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "imageOps.h"



int getImageSize(char *name){

    IplImage* image=0;
    image=cvLoadImage(name, 0); // Load grey scale image
     if(!image){
        printf("\nERROR:Could not open image : %s\n",name);
        exit(1);
        //return 0;
    }

    return image->width;
}

// padd image to the next power of 2
// return the new N
int getNextPower( int Ndata, int Nkernel){
    
    // calc new size
    int newN;
    newN = Ndata+Nkernel-1;

    int power = ceil(log2(Ndata));
    //printf("N is %d. Power is %d\n", Ndata,power);

    int newSize = pow(2,power+1);
    //printf("New power is %d\n", power+1);
    //printf("New size is %d\n", newSize);

    //printf("Return \n");
    return (newSize);

}

int readImage(double * handler, char *name){

    //printf("\nOpenning image...\n");

    IplImage* image;
    image=cvLoadImage(name, 0);
    if(!image){
        printf("\nERROR: Could not open: %s\n",name);
        exit(1);
        //return 0;
    }
    //printf("\nImage '%s' open with success!\n", name);

    // Access image data
    //printf("\nReading image data...\n");
    int step = image->widthStep;
    uchar* data = (uchar*)image->imageData;
    for(int i=0; i< image->width; i++){
        for(int j=0; j< image->width; j++){
           handler[i*step+j]=data[i*step+j];
        }
    }

    //printf("\nDone\n");
 
    return image->width;
}

int saveImage(double * handler, char *name, int width){

    //printf("\nSaving image...\n");
    IplImage* image = cvCreateImage(cvSize(width,width),IPL_DEPTH_8U,1);;

    // Access image data
    //printf("\nWriting  image data...\n");
    int step = image->widthStep;
    uchar* data = (uchar*)image->imageData;
    for(int i=0; i< image->width; i++){
        for(int j=0; j< image->width; j++){
                data[i*step+j] = handler[i*step+j];
        }
    }
    //printf("\nDone\n");

    char tmp[30];
    sprintf(tmp, "%s.jpg", name);
    //printf("\nSaving '%s' image\n", tmp);

    if(!cvSaveImage(tmp,image)){
        printf("\nCould not save: %s\n",name);
        exit(1);
        //return 0;
    }
    //printf("\nImage %s saved with success\n", name);
    return 1;
}

int writeImage(double * handler, char *name, int width){

    //printf("\nSaving image...\n");
    IplImage* image = cvCreateImage(cvSize(width,width),IPL_DEPTH_8U,1);;

    // Access image data
    //printf("\nWriting  image data...\n");
    int step = image->widthStep;
    uchar* data = (uchar*)image->imageData;
    for(int i=0; i< image->width; i++){
        for(int j=0; j< image->width; j++){
            if(handler[i*step+j] < 0){
                data[i*step+j] = 0;
            }else
            if(handler[i*step+j] > 255){
                data[i*step+j] = 255;
            }else
            if(handler[i*step+j] == 0){
                data[i*step+j] = 128;
            }else
                data[i*step+j] = handler[i*step+j];
        }
    }
    //printf("\nDone\n");

    char tmp[30];
    sprintf(tmp, "%s.jpg", name);
    //printf("\nSaving '%s' image\n", tmp);

    if(!cvSaveImage(tmp,image)){
        printf("\nERROR: Could not save: %s\n",name);
        exit(1);
        //return 0;
    }
    //printf("\nImage %s saved with success\n", name);
    return 1;
}
