/* System includes */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* TBB includes */
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/tick_count.h>

/* MKL includes*/
#include <mkl_dfti.h>
#include <omp.h>

/* Defines */
#define PI  3.141592654

using namespace tbb;


// Addition of two matrix
class MADD {
    double *x, *y;

public:

    MADD(double *xin, double *yin) {
        x = xin;
        y = yin;
    }

    void operator()(blocked_range<int> r) const {
        for (int i = r.begin(); i != r.end(); ++i) {
            x[i] += y[i];
        }
    }

};

// Subtraction of two matrix

class MSUB {
    double *x, *y;

public:

    MSUB(double *xin, double *yin) {
        x = xin;
        y = yin;
    }

    void operator()(blocked_range<int> r) const {
        for (int i = r.begin(); i != r.end(); ++i) {
            x[i] -= y[i];
        }
    }

};

class PWM {
    double *x, *y, *z;

public:

    PWM(double *xin, double *yin, double *zin) {
        x = xin;
        y = yin;
        z = zin;
    }

    void operator()(blocked_range<int> r) const {
        for (int i = r.begin(); i != r.end(); ++i) {
            z[i] = x[i] * y[i];
        }
    }
};

void k_PWM(double **data_pointers, int *args, void* stream) {
    int N = args[0];
    parallel_for(blocked_range<int>(0, N * N), PWM(data_pointers[0], data_pointers[1], data_pointers[2]));
}

void k_FFT2(double **data_pointers, int* args, void* stream, MKL_Complex16 *input) {
    
    int N = args[0];
    
    /*
     * SETUP
     */
    int nThreads = task_scheduler_init::default_num_threads();
    omp_set_num_threads(nThreads);
    
    // 1.Allocate a fresh descriptor for the problem
    DFTI_DESCRIPTOR_HANDLE mklDescHandle;
    DftiCreateDescriptor( &mklDescHandle, DFTI_DOUBLE, DFTI_COMPLEX, 1, N );
    
    // 2.Optionally adjust the descriptor configuration
    DftiSetValue( mklDescHandle, DFTI_PLACEMENT, DFTI_INPLACE);
    DftiSetValue( mklDescHandle, DFTI_INPUT_DISTANCE, N);
    DftiSetValue( mklDescHandle, DFTI_NUMBER_OF_TRANSFORMS, N); // DFTI_NUMBER_OF_TRANSFORMS
    DftiSetValue( mklDescHandle, DFTI_BACKWARD_SCALE, 1.0/(double)N);     // DFTI_BACKWARD_SCALE
    
    // 3.Commit the descriptor
    DftiCommitDescriptor(mklDescHandle);    
    
    /*
     * COMPUTING
     */
    // 4.Compute the transform 
    if(args[1] == 1){
        DftiComputeForward(mklDescHandle, input);
    }
    else{
        DftiComputeBackward(mklDescHandle, input);
    }   

    // 5.Deallocate the descriptor
    DftiFreeDescriptor( &mklDescHandle );
    //free(input);    
}

void k_FFT(double **data_pointers, int* args, void* stream) {
    
    int N = args[0];
    
    MKL_Complex16* input;
    input = (MKL_Complex16*) malloc (sizeof(MKL_Complex16)* N*N);
    for(int i=0; i< N*N; i++){
        input[i].real = data_pointers[0][i];
        input[i].imag = data_pointers[1][i];
    }
     
    /*
     * SETUP
     */
    int nThreads = task_scheduler_init::default_num_threads();
    omp_set_num_threads(nThreads);
    
    // 1.Allocate a fresh descriptor for the problem
    DFTI_DESCRIPTOR_HANDLE mklDescHandle;
    DftiCreateDescriptor( &mklDescHandle, DFTI_DOUBLE, DFTI_COMPLEX, 1, N );
    
    // 2.Optionally adjust the descriptor configuration
    DftiSetValue( mklDescHandle, DFTI_PLACEMENT, DFTI_INPLACE);
    DftiSetValue( mklDescHandle, DFTI_INPUT_DISTANCE, N);
    DftiSetValue( mklDescHandle, DFTI_NUMBER_OF_TRANSFORMS, N); // DFTI_NUMBER_OF_TRANSFORMS
    DftiSetValue( mklDescHandle, DFTI_BACKWARD_SCALE, 1.0/(double)N);     // DFTI_BACKWARD_SCALE
    
    // 3.Commit the descriptor
    DftiCommitDescriptor(mklDescHandle);    
    
    /*
     * COMPUTING
     */
       
    // 4.Compute the transform 
    if(args[1] == 1){
        DftiComputeForward(mklDescHandle, input);
    }
    else{
        DftiComputeBackward(mklDescHandle, input);
    }    
   
    // Copy data to host format
    for(int i=0; i< N*N; i++){
        data_pointers[0][i] = input[i].real;
        data_pointers[1][i] = input[i].imag;
    } 
    
    // 5.Deallocate the descriptor
    DftiFreeDescriptor( &mklDescHandle );
    free(input);    
}

void k_MADD(double **data_pointers, int *args, void* stream) {
    int N = args[0];
    parallel_for(blocked_range<int>(0, N * N), MADD(data_pointers[0], data_pointers[1]));
}

void k_MSUB(double **data_pointers, int *args, void* stream) {
    int N = args[0];
    parallel_for(blocked_range<int>(0, N * N), MSUB(data_pointers[0], data_pointers[1]));
}


// Next, my own matrix tranpose code. Crap! Is not very efficient... 

//void matrixTranspose2(double *x, double *y, int h, int w) {
//
//    for (int i = 0; i < h; i++) {
//        for (int j = 0; j < w; j++) {
//            y[j * w + i] = x[i * h + j];
//        }
//    }
//}



void matrixTranspose2(double *input, double *output, int H, int W) {

    for (int j = 0; j < W; j++) {
        for (int i = 0; i < H; i++) {
            output[j * H + i] = input[i * W + j];
         }
    }
}

class TRANSPOSE {
    double *input,
        *output;
    int W,H;

public:

    TRANSPOSE(double *in, double *out, int h, int w) {
        input = in;
        output = out;
        W = w;
        H = h;
    }

    void operator()(blocked_range2d<int> r) const {
        for (int j = 0; j < W; j++) {
                for (int i = r.rows().begin(); i < r.cols().end(); i++) {
                        output[j * H + i] = input[i * W + j];
                }
        }
    }

};

void k_TRANSPOSE(double **data_pointers, int *args, void* stream) {

  
    //matrixTranspose2(data_pointers[0], data_pointers[1], args[0], args[1]);
    int W = args[0];
    int H = args[1];

    int nThreads = task_scheduler_init::default_num_threads();
    parallel_for(blocked_range2d<int>(0, W, W, 0, H, H/nThreads), TRANSPOSE(data_pointers[0], data_pointers[1], H, W), simple_partitioner());    
    
//    int H = 3;
//    int W = 3;
//    int* input = new int [H * W];
//    int* output = new int [H * W];
//
//    for (int i = 0; i < H * W; i++){
//        input[i] = i;
//        output[i] = 0;
//    }
//
//    printf("Input Matrix:\n");
//    for (int i = 0; i < H; i++) {
//        for (int j = 0; j < W; j++) {
//            printf("%4d ", input[i * W + j]);
//        }
//        printf("\n");
//    }
//    printf("\n");
//
//
//    for (int j = 0; j < W; j++) {
//        for (int i = 0; i < H; i++) {
//            output[j * H + i] = input[i * W + j];
//         }
//    }
    
    
//    for (int j = 0; j < W; j++) {
//        for (int i = 0; i < H/4; i++) {
//            output[j * H + i] = input[i * W + j];
//            printf("%d ", input[i * W + j]);
//
//        }
//        printf("\n");
//    }
//    printf("\n");
    
//    printf("Wait for it\n");
//    sleep(4);
//    
//    parallel_for(blocked_range2d<int>(0, W, W, 0, H, H/4), TRANSPOSE(input, output, H, W), simple_partitioner());
 
//    for (int j = 0; j < W; j++) {
//        for (int i = H/4; i < H/4 + H/4; i++) {
//            output[j * H + i] = input[i * W + j];
//            printf("%d ", input[i * W + j]);
//       
//        }
//        printf("\n");
//    }
//    printf("\n");
//    
//    for (int j = 0; j < W; j++) {
//        for (int i = H/4; i < H/4 + H/4; i++) {
//            output[j * H + i] = input[i * W + j];
//            printf("%d ", input[i * W + j]);
//       
//        }
//        printf("\n");
//    }
//    printf("\n");   

//    printf("Output Matrix:\n");
//    for (int i = 0; i < W; i++) {
//        for (int j = 0; j < H; j++) {
//            printf("%4d ", output[i * H + j]);
//        }
//        printf("\n");
//    }
//    printf("\n");

}
